<?php
	session_start();
	
	require 'dbconnect.php';
	date_default_timezone_set("Asia/Jakarta");
	error_reporting(0);
	ini_set('display_errors', 1);
	if($_GET['keyresponse'] == 1){
		$_SESSION['keyresponse'] = 1;
	}
	$get_eod 	= mysqli_query($conpos,"select * from trx_ctl where TRX_STATUS = '0'");
	$dataeod	= mysqli_fetch_object($get_eod);
	if(!isset($dataeod)){
	//	print_r($_SESSION);exit;
		
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="refresh" content="18000">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>Untitled Document</title>
		<link href="css/style-pos.css?v=<?php echo date("his")?>" rel="stylesheet" type="text/css">
		<link href="css/bootstrap.css?v=<?php echo date("his")?>" rel="stylesheet" type="text/css">
		<link href="<?php echo  $base_url ?>sweetalert2/dist/sweetalert2.min.css" rel="stylesheet">
		<link href="<?php echo  $base_url ?>css/jquery-ui.css" rel="stylesheet">
		
		 <link href="<?php echo  $base_url ?>assets_login/css/fontawsom-all.min.css" rel="stylesheet">
		
		<script src="js/jquery.js" type="text/javascript"></script>
		<script src="js/bootstrap.js" type="text/javascript"></script>
		<script>
			var strukid 		= "<?php echo $dataTrx->POS_ID.".".$dataTrx->TRX_ID."."?>";
			var min_order		= "<?php echo $mindlamount ?>";
			var default_type	= "<?php echo $default_type ?>";
			var base_url		= "<?php echo $base_url ?>";
			var screenWidth		= screen.width;
			if(screenWidth < 600){
				location.href = "system_pos_mobile.php";
			}
		</script>
		
		<script src="js/layanan.js" type="text/javascript"></script>
		<script src="js/pembayaran.js" type="text/javascript"></script>
		<script src="js/posting_menu.js" type="text/javascript"></script>
		<script src="js/posting_discount.js" type="text/javascript"></script>
		<script src="js/posting_promo.js" type="text/javascript"></script>
		<script src="js/function.js" type="text/javascript"></script>
		<script src="js/proc_dp.js" type="text/javascript"></script>
		<script src="<?php echo  $base_url ?>sweetalert2/dist/sweetalert2.all.min.js" type="text/javascript"></script>				
		<script src="<?php echo  $base_url ?>js/jquery-ui.js"></script>
		<style>
		*:fullscreen
			*:-ms-fullscreen,
			*:-webkit-full-screen,
			*:-moz-full-screen {
			   overflow: auto !important;
			   background:#fff;
			}
			body {
			   background:#fff;
			  color: #000; 
			  font-size:16px;}

			html:-webkit-full-screen-ancestor {
			  background-color: #fff; }

			html:-moz-full-screen-ancestor {
			  background-color: #fff; }
			  .nav-tabs > li.active > a, .nav-tabs > li.active > a:hover, .nav-tabs > li.active > a:focus{
				  background:red;color:#fff;
			  }
			  #content-isi {
				  display:block;
			  }
				
			html {
				background-color: #ffffff;
				/* Or any color / image you want */
			}
			.btn {
				font-size:14px;
			}
			#swal2-content{
				font-size:15px;
			}
			.swal2-styled{
				font-size:15px;
			}
			#modal_list_tacharge a,
			#load_list_layanan a{
				font-size:15px;
			}
			button{
				padding:0;
				font-size:12px;
			}
			.fa{
				font-size:30px;
				margin-botton : 5px;
			}
		</style>
		
		<!-- keyboard widget css & script (required) -->
		<link href="Keyboard/css/keyboard.css" rel="stylesheet">
		<script src="Keyboard/js/jquery.keyboard.js"></script>

		<!-- keyboard extensions (optional) -->
		<script src="Keyboard/js/jquery.mousewheel.js"></script>
		<script>
			$(function(){
				$('.numeric').keyboard({layout : 'num' });
				$('.character').keyboard({layout: 'qwerty'});
			});
		</script>
		
		
	</head>
	<body >
	
	<?php 
session_start();
include "modal/confirm_entry.php";
error_reporting(0);

	if($_GET['act'] == 'err_print'){
		echo "<SCRIPT>
		Swal.fire({
		  title: 'Error!',
		  text: 'Start Of Day Belum Dilakukan',
		  icon: 'error',
		  confirmButtonText: 'CLOSE'
		}).then((result) => {
			if (result.isConfirmed) {
				location.href='index.php';
			}
		});
		</SCRIPT>";
	}

	$getId 		= mysqli_query($conpos,"select * from trx_ctl where trx_status = 0");
	$dataTrx	= mysqli_fetch_object($getId);

	$newcounter = $dataTrx->COUNTER_NO + 1;
	//echo exit;
	
	//get date now
	$datenow	= date('Y-m-d');
	$datepos	= date('Y-m-d',strtotime($dataTrx->TRX_DATE));
	
	if($_GET['status'] != 1){
		if($datenow != $datepos){
			if(empty($_SESSION['keyresponse'])){
				echo '	
					<script>
					Swal.fire({
					  title: "CONFIRM!",
					  text: "Tanggal Berjalan dengan tanggal start of day tidak sama / Lakukan End Of Day ! \n Apakah Transaksi akan di lanjutkan ?",
					  icon: "question",
					  type: "warning",
					  showCancelButton: true,
					  confirmButtonText: "YES",
					  cancelButtonText: "NO"
					}).then((result) => {
						if (result.isConfirmed) {
							var modalsession = document.getElementById("confirm_entryModal");
							modalsession.style.display = "block";
						}else{
							location.href="index.php";
						}
					});
					</script>';
			}
		}
	}
	
	
?>
	<input type="hidden" id="tgl_berjalan" value="<?php echo $_SESSION['keyresponse']?>" />
	
		<div class="fluid-container" style="background: #fff;">
			<div class="">
				<div class="col-xs-12 col-md-12 col-sm-12"> 
				<?php
				if(!$dataTrx){
					if($_SESSION['ROLE_ID'] == '01'){
						echo "<script>
							Swal.fire({
							  title: 'Error!',
							  text: 'Start Of Day Belum Dilakukan',
							  icon: 'error',
							  confirmButtonText: 'CLOSE'
							}).then((result) => {
								if (result.isConfirmed) {
									location.href='index.php';
								}
							});
							</script>";
							
					}else{
						echo "
							<script>
							Swal.fire({
							  title: 'Error!',
							  text: 'Start Of Day Belum Dilakukan',
							  icon: 'error',
							  confirmButtonText: 'CLOSE'
							}).then((result) => {
								if (result.isConfirmed) {
									location.href='index.php';
								}
							});
							</script>";
							
						
					}
					exit;
				}
				require 'cek_kasir.php';
				//getuser
				$getuser	= mysqli_query($conpos,"select * from tbluser where USER_1STLOGIN = 1" );
				$datauser	= mysqli_fetch_object($getuser);
				?>
				</div>
				<div class="btn btn-danger col-xs-12 col-md-12 col-sm-12" style="border-radius:0px;font-weight:bold;line-height: 20px;text-align:left;"> 
					<?php
						$newcounter	= $dataTrx->COUNTER_NO + 1;
					?>
					<div class="row">
						<div class="col-xs-2 col-md-2 col-sm-2">
							No <input type="text" id="counter" value="<?php echo $newcounter ?>"  style="width:50%;color:#C9302C;" readonly />
						</div>
						<div class="col-xs-2 col-md-2 col-sm-2">
							GUEST <input type="text" id="guest_id_view"  style="width:50%;color:#C9302C;" readonly />
						</div>
						<div class="col-xs-2 col-md-2 col-sm-2">
							KASIR <input type="text" id="guest_id_view"  style="width:50%;color:#C9302C;" value="<?php echo $datauser->USER_ID ?>" readonly />
						</div>
						<div class="col-xs-2 col-md-2 col-sm-2">
							ID : <input type="text" id="trx_id" value="<?php echo $dataTrx->POS_ID.".".$dataTrx->TRX_ID.".".$newcounter ?>" readonly style="width:70%;color:#C9302C;" />
						</div>
						<div class="col-xs-2 col-md-2 col-sm-2">
							TRX :  <?php echo date('d-m-Y',strtotime($dataTrx->TRX_DATE)) ?>
							<input type="hidden" value="<?php echo $dataTrx->TRX_DATE ?>" id="trx_date" />
						</div>
						<div class="col-xs-2 col-md-2 col-sm-2">
							<span id="hours"><span id="jam"></span></span>
							<span id="minute"><span id="menit"></span></span>
							<span id="second"><span id="detik"></span></span>
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-md-12 col-sm-12" > 
					<div class="row">
						<div class="col-xs-12 col-md-6 col-sm-6"> 
							<div class="row">
								<div class="col-xs-12 col-md-12 col-sm-12">
									<div class="row">
										<div class="col-lg-10 col-xs-12">
											<div class="row">
												<input type="hidden" id="seq_no" value="1" />
												<?php include "lstmenu_left.php";?>												
											</div>
											<div class="row">
												<?php include "payment_confirmation.php"; ?>
											</div>
										</div>
										<div class="col-lg-2 col-xs-12"
											style="
												font-size:15px;
												font-weight:bold;
												padding-top:10px;
												padding-bottom:10px;
												line-height:50px;">
										
											<div class="row">
												<?php include "btn_lstmenu_right.php";?>												
											</div>
										</div>
										<div class="col-xs-12 col-md-12 col-sm-12">
											<div class="row">
												<div class="col-xs-2 col-md-2 col-sm-2">
													<div class="row">
														<?php include "btn_keypad_leftside.php";?>
													</div>
												</div>
												<div class="col-xs-6 col-md-6 col-sm-6">
													<div class="row">
														<div id="result" class="btn btn-default" style="font-size:25px;line-height:40px;font-weight:bold;width:100%;height:46px;"></div>
														<?php include "keypad.php";?>
													</div>
												</div>
												<div class="col-xs-2 col-md-2 col-sm-2">
													<div class="row">
														<div class="">
															
															
															<div id="servicecharge" class="btn btn-primary" style="font-size:15px;display:none;"></div>
															<div id="salesDisc" class="btn btn-success" style="font-size:15px;display:none;"></div>
															<input id="unik_dlcharge" type="hidden" style="font-size:15px;">
															
															
														</div>
														<?php include "btn_keypad_left.php";?>
													</div>
												</div>
												<div class="col-xs-2 col-md-2 col-sm-2">
													<div class="row">
														<?php include "btn_keypad_right.php";?>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								
							</div>
						</div>
						<div class="col-xs-12 col-md-6 col-sm-6"> 
							<div class="row">
								<div class="col-lg-12 col-xlg-12 col-md-12" style="height:630px">
									<div id="menu-content">
										<span id="rtvmenu">
											<?php
												$product = mysqli_query($conpos, "SELECT * FROM products_fave a left join products c  ON a.PRODCODE = c.PRODCODE
												WHERE  c.PRODMAIN = 1 AND c.STATUS = 1  ORDER BY a.ITEM_INDEX ASC 
																						");
												while($plu = mysqli_fetch_object($product)){
												if($plu->PROD_GROUP_ID == '02'){
													$boxcolor = "background:url('c_juice.bmp');color:#000";
												}else if($plu->PROD_GROUP_ID == '04'){
													$boxcolor = "background:url('c_paket.bmp');color:#000";
												}else if($plu->PROD_GROUP_ID == '03'){
													$boxcolor = "background:url('c_pokok.bmp');color:#000";
												}
												else if($plu->PROD_GROUP_ID == '08'){
													$boxcolor = "background:url('c_drink.bmp');color:#fff";
												}else if($plu->PROD_GROUP_ID == '05'){
													$boxcolor = "background:url('c_souvenir.bmp');color:#000";
												}else if($plu->PROD_GROUP_ID == '06'){
													$boxcolor = "background:url('c_bento.bmp');color:#000";
												}
												else if($plu->PROD_GROUP_ID == '07'){
													$boxcolor = "background:url('c_soup.bmp');color:#000";
												}
												echo'<a onclick=menuitem("'.$plu->PLU.'","'.$plu->PROMO_ID.'") class="col-lg-2 col-xs-3" style=" border:2px solid #fff;tex-align:center; font-size:12px; height:90px; font-family:Tahoma, Geneva, sans-serif;'.$boxcolor.'">
																'.$plu->PRODNAME.'
																<input type="hidden" id="groupcode'.$plu->PLU.'" value="'.$plu->PROMO_ID.'"/>
																<input type="hidden" id="name'.$plu->PLU.'" value="'.$plu->PRODNAME.'"/>
																<input type="hidden" id="plu'.$plu->PLU.'" value="'.$plu->PLU.'"/>
															   
															</a>';
															// <input type="hidden" id="price'.$plu->PLU.'" value="'.(int)$plu->UNIT_PRICE.'"/>
											}?>

										</span>
									</div>
								</div>
								<div class="col-lg-12 col-xlg-12 col-md-12" style="height:80px">
									<div class="">
										<?php 
											$cat = mysqli_query($conthr, "SELECT * FROM tblgmnu");
											while($ct = mysqli_fetch_object($cat)){
											?>
											<a class="col-lg-3 col-xlg-3 col-md-3 btn btn-danger rounded-pill" onclick="catproduct('<?=$ct->ID?>')" >
												<?=$ct->DESCRIPTION?>
											</a>
										<?php }?>
									</div>
								</div>
							</div>
						</div>
					</div>					
				</div>
			</div>
				
		</div>
		
		<?php include "modal/bayar.php";?>
		<?php include "modal/item_disc.php";?>
		<?php include "modal/service_charge.php";?>
		<?php include "modal/upgrade.php";?>
		<?php include "modal/confirm_bayar.php";?>
		<?php include "modal/exitpos.php";?>
		<?php include "modal/close_session.php";?>
		<?php include "modal/layanan.php";?>
		<?php include "modal/voidpos.php";?>
		<?php include "modal/release.php";?>
		<?php include "modal/reference_void.php";?>
		<?php include "modal/dp_popup.php";?>
		<?php include "modal/confirm_entry_dp.php";?>
		<style>
			.ui-keyboard{
				background : #eee;
			}
			.ui-keyboard-button{
				height: 4.2em;
				min-width: 4.2em;
				margin: .2em;
				cursor: pointer;
				overflow: hidden;
				line-height: 4.2em;
				-moz-user-focus: ignore;
				
			}
			.ui-keyboard-text{
				font-size:20px;
			}
			.ui-keyboard-preview-wrapper{
				height:30px;
			}
			.ui-keyboard-preview{
				font-size:20px;
			}
		</style>
	</body>
</html>
