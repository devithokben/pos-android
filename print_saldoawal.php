<?php
require __DIR__ . '/vendor/autoload.php';

use Mike42\Escpos\Printer;
use Mike42\Escpos\EscposImage;
use Mike42\Escpos\PrintConnectors\RawbtPrintConnector;
use Mike42\Escpos\CapabilityProfile;

require 'dbconnect.php';
//$_GET['id'] = '11501.1.1';
	$getTrans	= mysqli_query($conpos,"select * from sales_hdr where ID = '".$_GET['id']."'");
	//$getTrans	= mysqli_query($conpos,"select * from sales_hdr where ID = '11501.1.4'");
	$dataTrans	= mysqli_fetch_object($getTrans);
	if(!$dataTrans){
		
	}
		
	$getBranch	= mysqli_query($conpos,"select * from tbl_lst_branch");
	$dataBranch	= mysqli_fetch_object($getBranch);
	
	//getCprofile
	$getProfile	= mysqli_query($conpos,"select * from cprofile");
	$dataProf	= mysqli_fetch_object($getProfile);	
	try {
		$profile = CapabilityProfile::load("POS-5890");

		/* Fill in your own connector here */
		$connector = new RawbtPrintConnector();

				
		/* Start the printer */
		$printer = new Printer($connector, $profile);

		$printer->feed(2);
		$printer->setJustification(Printer::JUSTIFY_CENTER);
		$printer->text("JAPANESE RESTAURANT\n");
		$printer->text($dataBranch->BRANCH_NAME."\n");
		$printer->text("TLP : ".$dataBranch->PHONE."\n");
		$printer->feed(2);
		
		$printer->setJustification(Printer::JUSTIFY_LEFT);
		$printer->setEmphasis(true);
		$printer->text("BILL NO : ".$_GET['id']."\n");
		$printer->setEmphasis(false);
	
		$printer->text($dataTrans->ENT_USER.", ".$dataTrans->ENT_DATE. " GUEST ".$dataTrans->GUEST);
		$printer->text("\n------------------------------------------------");
		
		$printer->feed(1);
		
		$getdetail	= mysqli_query($conpos,"select ITEM_TYPE from sales_dtl where ID = '".$_GET['id']."' GROUP BY ITEM_TYPE");
		while($dataDetail	= mysqli_fetch_object($getdetail)){
			//echo "masuk";
			if($dataDetail->ITEM_TYPE == 'DI'){
				$layanan = 'DINE IN';
			}else if($dataDetail->ITEM_TYPE == 'TA'){
				$arr_layanan = 1;
				$layanan = 'TAKE AWAY';
			}else if($dataDetail->ITEM_TYPE == 'DL'){
				$arr_layanan = 1;
				$layanan = 'DELIVERY';
			}else if($dataDetail->ITEM_TYPE == 'GR'){
				$arr_layanan = 1;
				$layanan = 'GRAB FOOD';
			}else if($dataDetail->ITEM_TYPE == 'GF'){
				$arr_layanan = 1;
				$layanan = 'GO FOOD';
			}else if($dataDetail->ITEM_TYPE == 'SF'){
				$arr_layanan = 1;
				$layanan = 'SHOPEE FOOD';
			}else{
				$layanan = $dataDetail->ITEM_TYPE;
			}
		
			if($dataTrans->DELIVERY_NO){
				if(substr($dataTrans->DELIVERY_NO,0,2) == 'GR'){
				$layanan = "GRAB - NO : ".str_replace("#",'#',$dataTrans->DELIVERY_NO)."";
				}else if(substr($dataTrans->DELIVERY_NO,0,2) == 'SF'){
				$layanan = "SHOPEE FOOD - NO : ".str_replace("#",'#',$dataTrans->DELIVERY_NO)."";
				}else if(substr($dataTrans->DELIVERY_NO,0,1) == 'G'){
				$layanan =  "GO FOOD - NO : ".str_replace("#",'#',$dataTrans->DELIVERY_NO)."";
				}else{
					$layanan =  "DELIVERY : ".str_replace("#",'#',$dataTrans->DELIVERY_NO)."";
				}
			}
			$printer->setEmphasis(true);
			$printer -> text($layanan."\n");
			$printer->setEmphasis(false);
			
			/* Items */
			$printer->setJustification(Printer::JUSTIFY_LEFT);
			$getlistdetail	= mysqli_query($conpos,"select sales_dtl.*,products.PRODNAME as nmProd from sales_dtl left join products on sales_dtl.PRODCODE = products.PRODCODE where ID = '".$_GET['id']."' and ITEM_TYPE = '".$dataDetail->ITEM_TYPE."' order by SEQ_NO ASC");
			while($dataListDetail	= mysqli_fetch_object($getlistdetail)){
					if($dataListDetail->DISC_AMOUNT){
						$ket = 'DSC';
					}else{
						$ket = '   ';
					}
				$printer->text("   ".number_format($dataListDetail->QTY)."  ".str_pad(substr($dataListDetail->nmProd,0,20),27," ",STR_PAD_RIGHT).$ket.str_pad(number_format($dataListDetail->PRICE * $dataListDetail->QTY),10," ",STR_PAD_LEFT)."\n");
				$total_item 	=	$total_item + $dataListDetail->QTY;
				$total_amount	= 	$total_amount + $dataListDetail->PRICE;
				
			}	
			
			$getPayment	= mysqli_query($conpos,"select sales_payment.*,tbl_lst_bayard.DESCRIPTION from sales_payment left join
			tbl_lst_bayard on sales_payment.EDC_VDR = tbl_lst_bayard.PARAM where sales_payment.ID ='".$_GET['id']."'");
			$dataPayement	= mysqli_fetch_object($getPayment);
			//print_r($dataPayement);

			$getPromoDe	= mysqli_query($conpos,"select LineType,EditMask from tbl_promoline where tbl_promoline.PromoId ='".$dataPayement->PROMOID."' and SeqNo = 2");
			$dataPromo	= mysqli_fetch_object($getPromoDe);
			
			$printer->text("------------------------------------------------");			
			$printer->feed(1);
			
			$printer->setEmphasis(true);
			$printer->text(number_format($total_item)." Item \n");
			$printer->setEmphasis(false);
			$printer->text("      ".str_pad("SUB TOTAL",31," ",STR_PAD_RIGHT).str_pad(number_format($dataPayement->GROSS_SALES),10," ",STR_PAD_LEFT)."\n");
			$printer->text("      ".str_pad("ITEM DISCOUNT ".number_format($dataPayement->DISCVALUE)."%",31," ",STR_PAD_RIGHT).str_pad(number_format($dataPayement->ITEM_DISCOUNT),10," ",STR_PAD_LEFT)."\n");

			if($dataPayement->SALES_DISCOUNT != 0){
				$printer->text("      ".str_pad("SALES DISCOUNT ".number_format($dataPayement->DISCVALUE)."%",31," ",STR_PAD_RIGHT).str_pad(number_format($dataPayement->SALES_DISCOUNT),10," ",STR_PAD_LEFT)."\n");
			}
			$printer->text("      ".str_pad("SERVICE CHARGE",31," ",STR_PAD_RIGHT).str_pad(number_format($dataPayement->TOTAL_COST),10," ",STR_PAD_LEFT)."\n");
			$printer->text("      ".str_pad("TA CHARGE",31," ",STR_PAD_RIGHT).str_pad(number_format($dataPayement->TACHARGE),10," ",STR_PAD_LEFT)."\n");
			if($dataPayement->DP != 0){
			$printer->text("      ".str_pad("DOWN PAYMENT",33," ",STR_PAD_RIGHT).str_pad(number_format($dataPayement->DP ),8," ",STR_PAD_LEFT)."\n");;
			}
			$printer->text("      ".str_pad("PJK RESTO 10%",31," ",STR_PAD_RIGHT).str_pad(number_format($dataPayement->TAX),10," ",STR_PAD_LEFT)."\n");
			$printer->text("      ".str_pad("PEMBULATAN",31," ",STR_PAD_RIGHT).str_pad(number_format("-".$dataPayement->ROUNDING),10," ",STR_PAD_LEFT)."\n");
			$printer->setEmphasis(true);
			$printer->text("      ".str_pad("TOTAL",31," ",STR_PAD_RIGHT).str_pad(number_format($dataPayement->SUB_TOTAL),10," ",STR_PAD_LEFT)."\n");
			$printer->setEmphasis(false);
			$printer->text("      ".str_pad("NON TUNAI",31," ",STR_PAD_RIGHT).str_pad(number_format($dataPayement->NON_CASH),10," ",STR_PAD_LEFT)."\n");
			if($dataPayement->edc_vdr == 0){
				$pay_amount = $dataPayement->CASH;
			}elseif($dataPayement->edc_vdr == 1){
				$pay_amount = $dataPayement->FBCA;
			}else{
				$pay_amount = $dataPayement->CARD_AMT;
			}
			$printer->setEmphasis(true);
			$printer->text("      ".str_pad($dataPayement->DESCRIPTION,31," ",STR_PAD_RIGHT).str_pad(number_format($pay_amount),10," ",STR_PAD_LEFT)."\n");
			$printer->setEmphasis(false);
			$printer->text("      ".str_pad("KEMBALI",31," ",STR_PAD_RIGHT).str_pad(number_format($dataPayement->REFUND ),10," ",STR_PAD_LEFT)."\n");
			if($dataTrans->HREF != ''){
				$printer->setEmphasis(true);
				$printer->text(str_pad("REFERENCE",36," ",STR_PAD_RIGHT).str_pad($dataTrans->HREF,11," ",STR_PAD_LEFT)."\n");
				$printer->setEmphasis(false);
			}
			$printer->text("------------------------------------------------");			
			$printer->feed(1);
			$printer->text(str_pad($dataProf->MSG_STROOK_1,50," ",STR_PAD_BOTH).str_pad($dataProf->MSG_STROOK_2,45," ",STR_PAD_BOTH).str_pad($dataProf->MSG_STROOK_3,50," ",STR_PAD_BOTH).str_pad($dataProf->MSG_STROOK_4,50," ",STR_PAD_BOTH)."");
			$printer->feed(1);
			if($dataPromo->LineType == '70'){

			$getDesc	= mysqli_query($conpos,"select PromoDesc from tbl_promoh where tbl_promoh.PromoId ='".$dataPayement->PROMOID."'");
			$dataDesc	= mysqli_fetch_object($getDesc);

			$description 	= str_replace(";","\n",$dataDesc->PromoDesc);
			$desc_final		= str_replace("{V}", ' "'.$dataPromo->EditMask.'" ',$description);
				$printer->setEmphasis(true);
				$printer->text(str_pad($desc_final,47," ",STR_PAD_RIGHT)."\n"); 
				$printer->setEmphasis(false);
			}
			if($dataPromo->LineType != '70'){
			$printer->text("------------------------------------------------\n
			NOTE :  ".str_replace("#","::",$dataPayement->NOTE)."\n");  
			}
			if ($_GET['act'] == 'void'){
			$printer->text("----------------------------------------------\n
			".str_pad("VOID ",50," ",STR_PAD_BOTH)."\n"); 
			}if ($_GET['act'] == 'reprint'){
			$printer->text("----------------------------------------------\n
			".str_pad("COPY STRUK ".$dataTrans->PRN_REV_NO,50," ",STR_PAD_BOTH)."\n");
			}if ($arr_layanan == 1){
			$printer->text("Diterima dalam keadaan baik & Lengkap\n");
			$printer->text("Oleh :\n");
				
			$printer->feed(6);
			$printer->text("------------------------------------------------");		
			}
			$printer->text(chr(29)."V".chr(66).chr(3));

		
		}

		
		function encryp($plaintext){	
			$key 		= '05788993F8E4CE6A';
			//$plaintext 	= $_POST['text'];

			$ivlen = openssl_cipher_iv_length($cipher="AES-128-CBC");
			$iv = openssl_random_pseudo_bytes($ivlen);
			$ciphertext_raw = openssl_encrypt($plaintext, $cipher, $key, $options=OPENSSL_RAW_DATA);
			//$hmac = hash_hmac('sha256', $ciphertext_raw, $key, $as_binary=true);
			$ciphertext = base64_encode( $ciphertext_raw );
			return $ciphertext;
		}
		


	// Demo that alignment QRcode is the same as text
	
		$printer2 = new Printer($connector); // dirty printer profile hack !!
		$printer2->setJustification(Printer::JUSTIFY_CENTER);
		$printer2->qrCode("https://update.hokben.co.id/satisfaction/store?id=".encryp($dataBranch->BRANCH_ID).":::".encryp($_GET['id']), Printer::QR_ECLEVEL_H, 8);
		$printer2->setJustification();
		$printer2->feed(2);


/* Cuts */
		$printer->setJustification(Printer::JUSTIFY_CENTER);
		$printer -> text("CUSTOMER FEED BACK\n");
		$printer -> cut(Printer::CUT_PARTIAL);
		$printer -> cut(Printer::CUT_FULL);
		$printer -> cut(Printer::CUT_FULL);
		$printer->pulse();

	} catch (Exception $e) {
		echo $e->getMessage();
	} finally {
		$printer->close();
		//header("location:system_pos.php");
	}

?>