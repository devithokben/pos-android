<?php
error_reporting(0);
require 'dbconnect.php';
$getLastTrans		= mysqli_query($conpos,"select * from sales_hdr ORDER BY ENT_DATE DESC LIMIT 1");
$dataLastTrans	= mysqli_fetch_object($getLastTrans);
$last = date("Y-m-d",strtotime($dataLastTrans->ENT_DATE));
if(!$_GET['act']){
	$list_layanan = mysqli_query($conpos, "SELECT * FROM sales_dp where DATE_ULT >= '".$_GET['date']."' and DIBATALKAN = 0 and status = 0");
	echo '<table class="table table-striped col-lg-12 col-xlg-12 col-md-12">';
		echo '<thead><tr>
				<th>
					No Struk
				</th>
				<th>
					Tanggal Ultah
				</th>
				<th>
					Jumlah DP
				</th>
				<th>
					Customer Ultah
				</th>
				<th>
					Status Active
				</th>
				<th>
					Reschedule
				</th>
				<th>
					Cancel
				</th>
				<th>
					Action
				</th>
			</tr></thead>'; 
	while($lis_dp = mysqli_fetch_object($list_layanan)){
		if($lis_dp->STATUS == 0){
			$status = "<button class='btn btn-success'>YES</button>";
		}else{
			$status = "<button class='btn btn-warning'>No</button>";
		}
		if($lis_dp->DIUNDUR == 1){
			$diundur = "<button class='btn btn-success'>YES</button>";
		}else{
			$diundur = "<button class='btn btn-warning'>No</button>";
		}
		if($lis_dp->DIBATALKAN == 1){
			$dibatalkan = "<button class='btn btn-success'>YES</button>";
		}else{
			$dibatalkan = "<button class='btn btn-warning'>No</button>";
		}
		$btn_cancel		= "dp_cancel('".$lis_dp->ID."')";
		$one= '1';
		$two= '2';
		$btn_reschedule	= "dp_reschedule('".$lis_dp->ID."',$one)";
		$btn_reschedule_entry	= "dp_reschedule('".$lis_dp->ID."',$two)";
		echo '<tr>
				<td>
					'.$lis_dp->ID.'
				</td>
				<td>
					'.date("d-M-Y",strtotime($lis_dp->DATE_ULT)).'
				</td>
				<td>
					'.number_format($lis_dp->AMOUNT).'
				</td>
				<td>
					'.$lis_dp->CUST_ULT.'
				</td>
				<td>
					'.$status.'
				</td>
				<td>
					'.$diundur.'
				</td>
				<td>
					'.$dibatalkan.'
				</td>
				<td>';
		//print_r($lis_dp);
		
				
		if($lis_dp->DIBATALKAN != 0){
				echo '<h3>Canceled</h3>';
		}else if(($lis_dp->STATUS != 0)){
			echo '<h3>TERPAKAI</h3>';
			
		}else{
			// $days_ago = date('Y-m-d', strtotime('-5 days', strtotime('2008-12-02')));
			date_default_timezone_set("Asia/Bangkok");
			$date=date_create(date("Y-m-d",strtotime($lis_dp->DATE_ULT)));
			date_add($date,date_interval_create_from_date_string("2 days ago"));
			$datenow = date("Y-m-d");
			$dateago = date_format($date,"Y-m-d");
			if($datenow != $last){
				echo 'Tanggal pada Device POS tidak Up to Date';
			}
			else{
				if($datenow < $dateago){
					echo	'<button onclick = '.$btn_cancel.' style="width:50%;float:left;">C</button>
								<button onclick = '.$btn_reschedule.' style="width:50%;float:left;">R</button>';
	
				}
				else{
					echo	'<button onclick = '.$btn_cancel.' style="width:50%;float:left;">C</button>
								<button onclick = '.$btn_reschedule_entry.' style="width:50%;float:left;">R</button>';
	
				}

			}
			// echo	'<button onclick = '.$btn_cancel.' style="width:50%;float:left;">C</button>
			// 			<button onclick = '.$btn_reschedule.' style="width:50%;float:left;">R</button>';
		}
		echo	'</td>
			</tr>';
	}
	echo '</table>';
}
else if($_GET['act'] == 'insertDP'){
	$list_layanan = mysqli_query($conpos, "SELECT * FROM sales_dp where ID = '".$_GET['id']."' and STATUS = 0 ");
	$lis_dp = mysqli_fetch_object($list_layanan);
	echo $lis_dp->AMOUNT;
}else{
	$list_layanan = mysqli_query($conpos, "SELECT * FROM sales_dp where DATE_ULT = '".$_GET['date']."' and DIBATALKAN = 0 and status = 0");
	echo '<table class="table table-striped col-lg-12 col-xlg-12 col-md-12">';
		echo '<tr>
				<td>
					
				</td>
				<td>
					No Struk
				</td>
				<td>
					Tanggal Ultah
				</td>
				<td>
					Jumlah DP
				</td>
				<td>
					Customer Ultah
				</td>
			</tr>'; 
	while($lis_dp = mysqli_fetch_object($list_layanan)){
		$btn	= "addDp_val('".$lis_dp->ID."')";
		echo '<tr>
				<td>
					 <input type="checkbox" name="check_dp[]" value="'.$lis_dp->ID.'" onClick='.$btn.'>
				</td>
				<td>
					'.$lis_dp->ID.'
				</td>
				<td>
					'.date("d-M-Y",strtotime($lis_dp->DATE_ULT)).'
				</td>
				<td>
					'.number_format($lis_dp->AMOUNT).'
				</td>
				<td>
					'.$lis_dp->CUST_ULT.'
				</td>
			</tr>';
	}
		echo '<tr>
				<td colspan="3"></td>
				<td colspan="2">
					<input type="text" name="use_dp" id="use_dp" value="0" />
				</td>
			</tr>';
	echo '</table>';
}
?>

<script>
</script>