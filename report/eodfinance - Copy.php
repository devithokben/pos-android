<?php 
error_reporting(E_ALL);
session_start();
include("../dbconnect.php");
include("../template/header_h2h.php");
	
	$cash_on_bank = 0;
?>
	<div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="page-breadcrumb">
                <div class="row align-items-center">
					<div class="col-xs-12 col-md-12 col-sm-12">
		<a href="#" onclick="BtPrint(document.getElementById('pre_print').innerText)" style="font-size:16px;">Print Report</a>
		<form method="POST" >	
			<table style="font-size:14px;">
				<tr>
					<td>
						Kode Transaksi : 
					</td>
					<td>
						<?php
							$getlistdetail 		= mysqli_query($conpos,"select * from trx_ctl order by trx_id DESC");
							//;
							
							echo "<select name='trx_id'>";
							while($data	= mysqli_fetch_object($getlistdetail)){							
								if($data->TRX_DATE == $_POST['trx_id']){
									$select = "SELECTED";
								}else{
									$select = "";
								}
								echo "<option value='".$data->TRX_DATE."' style='font-size:16px;' ".$select." >".date("d-m-Y",strtotime($data->TRX_DATE))." (".$data->TRX_ID.")</option>";
							}
							echo "</select>";
						?>
					</td>
					<td>
						<input type="submit" value="find data" />
					</td>
				</tr>
			</table>
		</form>
		<pre id="pre_print" class="col-xs-5 col-md-5 col-sm-5" style="font-size:14px;"> 
<?php
if($_POST){
	
	
	$getid 		= mysqli_query($conpos,"select * from vw_eodfinance WHERE TRX_DATE = '".$_POST['trx_id']."'");
	while($datatrx	= mysqli_fetch_object($getid))
	//print_r($datatrx);
		{
	//$closing_status	= $datatrx->closing_status;		
			$trx_id 		= $datatrx->TRX_ID;
			$trx			= "P".$datatrx->POS_ID.".".$trx_id;
			$trx_date 		= $datatrx->TRX_DATE;
			$_SESSION['select_date']	= $datatrx->TRX_DATE;
			//print_r($_POST['trx_id']);
			if($datatrx->CLOSING_STATUS == 0 ){
				$status_trans	= 'TRANSAKSI BERJALAN';
			}else{
				$status_trans	= 'END OF DAY REPORT';
			}
echo"
".str_pad("FINANCIAL REPORT",32," ",STR_PAD_LEFT)."\n".str_pad($status_trans,33," ",STR_PAD_LEFT)."\n".str_pad($datatrx->BRANCH_NAME,32," ",STR_PAD_LEFT)."\n".str_pad("TRX ID ".$trx_id,28," ",STR_PAD_LEFT)."\n".str_pad("AS OF DATE ".date("d-m-Y",strtotime($trx_date)),36," ",STR_PAD_LEFT)."\n".str_pad("PRINT DATE ".date("d-m-Y H:i"),40," ",STR_PAD_LEFT);
$totalqty = 0;
$totalamt = 0;

echo "
----------------------------------------------
POS ID : ".$trx."
".$datatrx->TRX_STATUS."
----------------------------------------------";
echo "\n".str_pad("NET SALES",35," ",STR_PAD_RIGHT).str_pad(number_format($datatrx->GROSS),10," ",STR_PAD_LEFT);
echo "\n".str_pad("PELUNASAN ULTAH",35," ",STR_PAD_RIGHT).str_pad(number_format($datatrx->DP),10," ",STR_PAD_LEFT);
echo "\n".str_pad("DISCOUNT PLU",35," ",STR_PAD_RIGHT).str_pad(number_format($datatrx->DISC_ITEM),10," ",STR_PAD_LEFT);
echo "\n".str_pad("SALES DISCOUNT",35," ",STR_PAD_RIGHT).str_pad(number_format($datatrx->SALES_DISCOUNT),10," ",STR_PAD_LEFT);
echo "\n".str_pad("PROMO DISCOUNT",35," ",STR_PAD_RIGHT).str_pad(number_format($datatrx->PROMO_DISCOUNT),10," ",STR_PAD_LEFT);
echo "\n".str_pad("SERVICE CHARGE",35," ",STR_PAD_RIGHT).str_pad(number_format($datatrx->CHARGE),10," ",STR_PAD_LEFT);
echo "\n".str_pad("TA CHARGE",35," ",STR_PAD_RIGHT).str_pad(number_format($datatrx->TACHARGE),10," ",STR_PAD_LEFT);
echo "\n".str_pad("PAJAK",35," ",STR_PAD_RIGHT).str_pad(number_format($datatrx->TAX),10," ",STR_PAD_LEFT);
echo "\n".str_pad("PEMBULATAN",35," ",STR_PAD_RIGHT).str_pad(number_format($datatrx->ROUNDING),10," ",STR_PAD_LEFT);
echo "\n".str_pad("GROSS",35," ",STR_PAD_RIGHT).str_pad(number_format($datatrx->NET),10," ",STR_PAD_LEFT);
echo "\n".str_pad("NON CASH",35," ",STR_PAD_RIGHT).str_pad(number_format($datatrx->NON_CASH),10," ",STR_PAD_LEFT);
echo "\n".str_pad("EDC BCA",35," ",STR_PAD_RIGHT).str_pad(number_format($datatrx->FBCA),10," ",STR_PAD_LEFT);
echo "\n".str_pad("EDC MANDIRI",35," ",STR_PAD_RIGHT).str_pad(number_format($datatrx->CARD_AMT_MANDIRI),10," ",STR_PAD_LEFT);
echo "\n".str_pad("EDC NIAGA",35," ",STR_PAD_RIGHT).str_pad(number_format($datatrx->CARD_AMT_NIAGA),10," ",STR_PAD_LEFT);
echo "\n".str_pad("EDC KARTUKU",35," ",STR_PAD_RIGHT).str_pad(number_format($datatrx->CARD_AMT_KARTUKU),10," ",STR_PAD_LEFT);
echo "\n".str_pad("PG MANDIRI",35," ",STR_PAD_RIGHT).str_pad(number_format($datatrx->CARD_AMT_PGDOKU),10," ",STR_PAD_LEFT);
echo "\n".str_pad("PG CIMB NIAGA",35," ",STR_PAD_RIGHT).str_pad(number_format($datatrx->CARD_AMT_PGCIMBNIAGA),10," ",STR_PAD_LEFT);
echo "\n".str_pad("PG OVO",35," ",STR_PAD_RIGHT).str_pad(number_format($datatrx->CARD_AMT_PGOVO),10," ",STR_PAD_LEFT);
echo "\n".str_pad("PG WA",35," ",STR_PAD_RIGHT).str_pad(number_format($datatrx->CARD_AMT_PGWA),10," ",STR_PAD_LEFT);
echo "\n".str_pad("JD ID",35," ",STR_PAD_RIGHT).str_pad(number_format($datatrx->JD_ID),10," ",STR_PAD_LEFT);
echo "\n".str_pad("GO BIZ",35," ",STR_PAD_RIGHT).str_pad(number_format($datatrx->GO_RESTO),10," ",STR_PAD_LEFT);
echo "\n".str_pad("KARTUKU T CASH",35," ",STR_PAD_RIGHT).str_pad(number_format($datatrx->KARTUKU_TCASH),10," ",STR_PAD_LEFT);
echo "\n".str_pad("KARTUKU BNI",35," ",STR_PAD_RIGHT).str_pad(number_format($datatrx->KARTUKU_BNI),10," ",STR_PAD_LEFT);
echo "\n".str_pad("KARTUKU CITIBANK",35," ",STR_PAD_RIGHT).str_pad(number_format($datatrx->KARTUKU_CITIBANK),10," ",STR_PAD_LEFT);
echo "\n".str_pad("KARTUKU GOPAY",35," ",STR_PAD_RIGHT).str_pad(number_format($datatrx->KARTUKU_GOPAY),10," ",STR_PAD_LEFT);
echo "\n".str_pad("EDC BRI",35," ",STR_PAD_RIGHT).str_pad(number_format($datatrx->CARD_AMT_BRI),10," ",STR_PAD_LEFT);
echo "\n".str_pad("EDC OVO",35," ",STR_PAD_RIGHT).str_pad(number_format($datatrx->CARD_AMT_OVO),10," ",STR_PAD_LEFT);
echo "\n".str_pad("DANA",35," ",STR_PAD_RIGHT).str_pad(number_format($datatrx->CARD_AMT_DANA),10," ",STR_PAD_LEFT);
echo "\n".str_pad("GRAB",35," ",STR_PAD_RIGHT).str_pad(number_format($datatrx->CARD_AMT_GRAB),10," ",STR_PAD_LEFT);
echo "\n".str_pad("VA BCA",35," ",STR_PAD_RIGHT).str_pad(number_format($datatrx->CARD_AMT_VABCA),10," ",STR_PAD_LEFT);
echo "\n".str_pad("VA MANDIRI",35," ",STR_PAD_RIGHT).str_pad(number_format($datatrx->CARD_AMT_VAMANDIRI),10," ",STR_PAD_LEFT);

$cash_on_bank = ($datatrx->NET - $datatrx->FBCA - $datatrx->CARD_AMT_MANDIRI - $datatrx->CARD_AMT_NIAGA - $datatrx->CARD_AMT_KARTUKU - $datatrx->CARD_AMT_PGDOKU - $datatrx->CARD_AMT_PGCIMBNIAGA - $datatrx->CARD_AMT_PGOVO - $datatrx->CARD_AMT_PGWA - $datatrx->JD_ID - $datatrx->GO_RESTO - $datatrx->KARTUKU_TCASH - $datatrx->KARTUKU_BNI - $datatrx->KARTUKU_CITIBANK - $datatrx->KARTUKU_GOPAY - $datatrx->CARD_AMT_BRI - $datatrx->CARD_AMT_OVO - $datatrx->CARD_AMT_DANA - $datatrx->CARD_AMT_GRAB - $datatrx->CARD_AMT_VABCA - $datatrx->CARD_AMT_VAMANDIRI) - ($datatrx->NON_CASH - $datatrx->REFUND_NON_CASH) ;

echo "\n".str_pad("CASH ON BANK",35," ",STR_PAD_RIGHT).str_pad(number_format($cash_on_bank),10," ",STR_PAD_LEFT);
echo "\n".str_pad("REFUND NON CASH",35," ",STR_PAD_RIGHT).str_pad(number_format($datatrx->REFUND_NON_CASH),10," ",STR_PAD_LEFT);
$totalamt = $totalamt+ $datatrx->GROSS;
echo "
----------------------------------------------
";	

echo "".str_pad("Prepared By",10," ",STR_PAD_LEFT).
"





----------------------------------------------


";
		}
	}
	echo "
".chr(29)."V".chr(66).chr(3);
?>

		</pre>
	</div>
	</div>
	</div>
	</div>
<?php include("../template/footer_h2h.php");?>
<script>

window.onload = function() {
//alert("masuk");

		//location.href = "system_pos.php";
		//window.close();
   //BtPrint(document.getElementById('pre_print').innerText);
}
function BtPrint(prn){
		var textEncoded = encodeURI(prn);
		Android.showToast(textEncoded);
		/*
        var S = "#Intent;scheme=rawbt;";
        var P =  "package=ru.a402d.rawbtprinter;end;";
        var textEncoded = encodeURI(prn);
        window.location.href="intent:"+textEncoded+S+P;
		*/
}
    
</script>