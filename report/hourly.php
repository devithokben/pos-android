<?php 
date_default_timezone_set("Asia/Bangkok");
ini_set('display_errors', '0');
ini_set('display_startup_errors', '0');
error_reporting(0);
	
	require '../dbconnect.php';
	include("../template/header_h2h.php");
	
	
	
?>
<script>
		var screenWidth		= screen.width;
		if(screenWidth < 600){
			location.href="hourly_mobile.php";
		}
	</script>
		<div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="page-breadcrumb">
                <div class="row align-items-center">
					<div class="col-xs-12 col-md-12 col-sm-12">
		<a href="#" onclick="BtPrint(document.getElementById('pre_print').innerText)" style="font-size:16px;">Print Report</a>
		<form method="POST" >	
			<table style="font-size:14px;">
				<tr>
					<td>
						Kode Transaksi : 
					</td>
					<td>
						<?php
							$getlistdetail 		= mysqli_query($conpos,"select * from trx_ctl order by TRX_DATE DESC");
							//;
							echo "<select name='trx_id'>";
							while($data	= mysqli_fetch_object($getlistdetail)){
								$trx_date = $data->TRX_DATE;
								echo "<option value='".$data->TRX_ID."#".$data->TRX_DATE."' style='font-size:16px;'>".date("d-m-Y",strtotime($data->TRX_DATE))." (".$data->TRX_ID.")</option>";
							}
							echo "</select>";
						?>
					</td>
					<td>
						<input type="submit" value="find data" />
					</td>
				</tr>
			</table>
		</form>
		<pre id="pre_print" class="col-xs-5 col-md-5 col-sm-5" style="font-size:14px;"> 
<?php
if($_POST){

$data = explode("#",$_POST['trx_id']);
echo"
".str_pad("HOURLY TRX. REPORT",31," ",STR_PAD_LEFT)."\n".str_pad($branchname,29," ",STR_PAD_LEFT)."\n".str_pad("AS OF DATE ".date("d-m-Y",strtotime($data[1])),32," ",STR_PAD_LEFT)."\n".str_pad("PRINT DATE ".date("d-m-Y H:i"),35," ",STR_PAD_LEFT);
$totalqty = 0;
$totalamt = 0;
echo "
";
echo "\n".str_pad("TIME",15," ",STR_PAD_RIGHT).str_pad("TRX",10," ",STR_PAD_RIGHT).str_pad("GUEST",10," ",STR_PAD_RIGHT).str_pad("AMOUNT",18," ",STR_PAD_RIGHT);

$open_time = strtotime("08:00");
$close_time = strtotime("22:00");
$now = time();
$output = "";
	$total_trx = 0;
	$total_guest = 0;
	$total_amount = 0;
	for( $i=$open_time; $i<=$close_time; $i+=3600) {
		$getid 		= mysqli_query($conpos,"select * from (select `posdb`.`tbl_lst_hour`.`ID` AS `id`,`trx`.`branch_id` AS `branch_id`,`trx`.`branch_name` AS `branch_name`,`trx`.`closing_status` AS `closing_status`,`trx`.`trx_id` AS `trx_id`,`trx`.`trx_date` AS `trx_date`,ifnull(`trx`.`trx_count`,0) AS `trx_count`,ifnull(`trx`.`guest`,0) AS `guest`,ifnull(`trx`.`amount`,0) AS `amount`,ifnull(`trx`.`net`,0) AS `net` from (`posdb`.`tbl_lst_hour` left join (select `posdb`.`sales_hdr`.`BRANCH_ID` AS `branch_id`,sum(`posdb`.`sales_payment`.`GROSS_SALES`) AS `net`,`posdb`.`tbl_lst_branch`.`BRANCH_NAME` AS `branch_name`,`posdb`.`trx_ctl`.`TRX_STATUS` AS `closing_status`,`posdb`.`trx_ctl`.`TRX_ID` AS `trx_id`,`posdb`.`trx_ctl`.`TRX_DATE` AS `trx_date`,count(distinct `posdb`.`sales_hdr`.`ID`) AS `trx_count`,sum(`posdb`.`sales_hdr`.`GUEST`) AS `guest`,concat(right(concat('00',cast(extract(hour from `posdb`.`sales_hdr`.`VALID_DATE`) as char charset utf8)),2),':00') AS `hour`,sum(`dtl`.`amount`) AS `amount` from ((((`posdb`.`sales_hdr` join `posdb`.`sales_payment` on(`posdb`.`sales_hdr`.`ID` = `posdb`.`sales_payment`.`ID`)) join `posdb`.`trx_ctl` on(`posdb`.`trx_ctl`.`TRX_ID` = `posdb`.`sales_hdr`.`TRX_ID`)) join `posdb`.`tbl_lst_branch` on(`posdb`.`tbl_lst_branch`.`BRANCH_ID` = `posdb`.`sales_hdr`.`BRANCH_ID`)) join (select `posdb`.`sales_dtl`.`ID` AS `id`,sum(`posdb`.`sales_dtl`.`QTY` * `posdb`.`sales_dtl`.`PRICE`) AS `amount` from `posdb`.`sales_dtl` group by `posdb`.`sales_dtl`.`ID`) `dtl` on(`dtl`.`id` = `posdb`.`sales_hdr`.`ID`)) where `posdb`.`sales_hdr`.`STATUS` = '1' and `posdb`.`sales_hdr`.`VALID_STATUS` = '1' group by `posdb`.`sales_hdr`.`BRANCH_ID`,`posdb`.`tbl_lst_branch`.`BRANCH_NAME`,`posdb`.`trx_ctl`.`TRX_ID`,`posdb`.`trx_ctl`.`TRX_STATUS`,`posdb`.`trx_ctl`.`TRX_DATE`,concat(right(concat('00',cast(extract(hour from `posdb`.`sales_hdr`.`VALID_DATE`) as char charset utf8)),2),':00')) `trx` on(`trx`.`hour` = `posdb`.`tbl_lst_hour`.`ID`)) where `posdb`.`tbl_lst_hour`.`ID` >= (select min(concat(right(concat('00',cast(extract(hour from `posdb`.`sales_hdr`.`VALID_DATE`) as char charset utf8)),2),':00')) from `posdb`.`sales_hdr` where `posdb`.`sales_hdr`.`TRX_STATUS` = 'close') and `posdb`.`tbl_lst_hour`.`ID` <= (select max(concat(right(concat('00',cast(extract(hour from `posdb`.`sales_hdr`.`VALID_DATE`) as char charset utf8)),2),':00')) from `posdb`.`sales_hdr` where `posdb`.`sales_hdr`.`TRX_STATUS` = 'close')) xx WHERE TRX_ID = '".$data[0]."' and id = '".date("H:i",$i)."'"); 
		$datatrx	= mysqli_fetch_object($getid);
echo "\n".str_pad(date("H:i",$i),15," ",STR_PAD_RIGHT).str_pad($datatrx->trx_count,10," ",STR_PAD_RIGHT).str_pad($datatrx->guest,10," ",STR_PAD_RIGHT).str_pad(number_format($datatrx->amount),10," ",STR_PAD_LEFT);
		
		$total_trx = $datatrx->trx_count + $total_trx;
		$total_guest = $datatrx->guest + $total_guest;
		$total_amount = $datatrx->amount + $total_amount;
	}
	
}

echo "
----------------------------------------------
";	
echo "".str_pad("",13," ",STR_PAD_LEFT)." ".str_pad(number_format($total_trx),10," ",STR_PAD_RIGHT)." ".str_pad(number_format($total_guest),10," ",STR_PAD_RIGHT)." ".str_pad(number_format($total_amount),11," ",STR_PAD_RIGHT);
echo "
----------------------------------------------
";
echo "
".chr(29)."V".chr(66).chr(3);
?>

		</pre>

	</div>
	</div>
	</div>
	</div>
<?php include("../template/footer_h2h.php");?>
<script>

window.onload = function() {
//alert("masuk");

		//location.href = "system_pos.php";
		//window.close();
   //BtPrint(document.getElementById('pre_print').innerText);
}
function BtPrint(prn){
		var textEncoded = encodeURI(prn);
		Android.showToast(textEncoded);
		/*
        var S = "#Intent;scheme=rawbt;";
        var P =  "package=ru.a402d.rawbtprinter;end;";
        var textEncoded = encodeURI(prn);
        window.location.href="intent:"+textEncoded+S+P;*/
}
    
</script>