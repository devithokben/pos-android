<?php 
error_reporting(E_ALL);
session_start();
include("../dbconnect.php");
include("../template/header_h2h.php");
	
	$cash_on_bank = 0;
?>
	<div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="page-breadcrumb">
                <div class="row align-items-center">
					<div class="col-xs-12 col-md-12 col-sm-12">
		<a href="#" onclick="BtPrint(document.getElementById('pre_print').innerText)" style="font-size:16px;">Print Report</a>
		<form method="POST" >	
			<table style="font-size:14px;">
				<tr>
					<td>
						Kode Transaksi : 
					</td>
					<td>
						<?php
							$getlistdetail 		= mysqli_query($conpos,"select * from trx_ctl order by TRX_DATE DESC");
							//;
							
							echo "<select name='trx_id'>";
							while($data	= mysqli_fetch_object($getlistdetail)){							
								if($data->TRX_DATE == $_POST['trx_id']){
									$select = "SELECTED";
								}else{
									$select = "";
								}
								echo "<option value='".$data->TRX_DATE."' style='font-size:16px;' ".$select." >".date("d-m-Y",strtotime($data->TRX_DATE))." (".$data->TRX_ID.")</option>";
							}
							echo "</select>";
						?>
					</td>
					<td>
						<input type="submit" value="find data" />
					</td>
				</tr>
			</table>
		</form>
		<pre id="pre_print" class="col-xs-5 col-md-5 col-sm-5" style="font-size:14px;"> 
<?php
if($_POST){
	
	
	//$getid 		= mysqli_query($conpos,"select * from vw_eodfinance WHERE TRX_DATE = '".$_POST['trx_id']."'");
	
	$getid 		= mysqli_query($conpos,"
											select `posdb`.`sales_hdr`.`BRANCH_ID` AS `BRANCH_ID`,`posdb`.`tbl_lst_branch`.`BRANCH_NAME` AS `BRANCH_NAME`,`posdb`.`sales_hdr`.`TRX_ID` AS `TRX_ID`,`posdb`.`trx_ctl`.`TRX_STATUS` AS `CLOSING_STATUS`,`posdb`.`trx_ctl`.`CLOSING_USER` AS `CLOSING_USER`,`posdb`.`trx_ctl`.`CLOSING_DATE` AS `CLOSING_DATE`,`posdb`.`sales_hdr`.`TRX_DATE` AS `TRX_DATE`,`posdb`.`sales_hdr`.`POS_ID` AS `POS_ID`,`posdb`.`sales_hdr`.`TRX_STATUS` AS `TRX_STATUS`,count(0) AS `TRX_COUNT`,sum(`detail`.`GROSS`) AS `GROSS`,sum(`posdb`.`sales_payment`.`ITEM_DISCOUNT`) AS `DISC_ITEM`,sum(`posdb`.`sales_payment`.`TOTAL_COST`) AS `CHARGE`,sum(`posdb`.`sales_payment`.`TACHARGE`) AS `TACHARGE`,sum(`posdb`.`sales_payment`.`SALES_DISCOUNT`) AS `SALES_DISCOUNT`,sum(`posdb`.`sales_payment`.`PROMO_DISCOUNT`) AS `PROMO_DISCOUNT`,sum(`posdb`.`sales_payment`.`TAX`) AS `TAX`,sum(`posdb`.`sales_payment`.`ROUNDING`) AS `ROUNDING`,sum(`posdb`.`sales_payment`.`NON_CASH`) AS `NON_CASH`,sum(`posdb`.`sales_payment`.`NET`) AS `NET`,sum(`posdb`.`sales_payment`.`FBCA`) AS `FBCA`,sum(if(`posdb`.`sales_payment`.`EDC_VDR` = '2',`posdb`.`sales_payment`.`CARD_AMT`,0)) AS `CARD_AMT_MANDIRI`,sum(if(`posdb`.`sales_payment`.`EDC_VDR` = '3',`posdb`.`sales_payment`.`CARD_AMT`,0)) AS `CARD_AMT_NIAGA`,sum(if(`posdb`.`sales_payment`.`EDC_VDR` = '4',`posdb`.`sales_payment`.`CARD_AMT`,0)) AS `CARD_AMT_KARTUKU`,sum(if(`posdb`.`sales_payment`.`EDC_VDR` = '5',`posdb`.`sales_payment`.`CARD_AMT`,0)) AS `CARD_AMT_PGDOKU`,sum(if(`posdb`.`sales_payment`.`EDC_VDR` = '6',`posdb`.`sales_payment`.`CARD_AMT`,0)) AS `CARD_AMT_PGCIMBNIAGA`,sum(if(`posdb`.`sales_payment`.`EDC_VDR` = '7',`posdb`.`sales_payment`.`CARD_AMT`,0)) AS `JD_ID`,sum(if(`posdb`.`sales_payment`.`EDC_VDR` = '8',`posdb`.`sales_payment`.`CARD_AMT`,0)) AS `GO_RESTO`,sum(if(`posdb`.`sales_payment`.`EDC_VDR` = '9',`posdb`.`sales_payment`.`CARD_AMT`,0)) AS `KARTUKU_TCASH`,sum(if(`posdb`.`sales_payment`.`EDC_VDR` = '10',`posdb`.`sales_payment`.`CARD_AMT`,0)) AS `KARTUKU_BNI`,sum(if(`posdb`.`sales_payment`.`EDC_VDR` = '11',`posdb`.`sales_payment`.`CARD_AMT`,0)) AS `KARTUKU_CITIBANK`,sum(if(`posdb`.`sales_payment`.`EDC_VDR` = '12',`posdb`.`sales_payment`.`CARD_AMT`,0)) AS `KARTUKU_GOPAY`,sum(if(`posdb`.`sales_payment`.`EDC_VDR` = '13',`posdb`.`sales_payment`.`CARD_AMT`,0)) AS `CARD_AMT_BRI`,sum(if(`posdb`.`sales_payment`.`EDC_VDR` = '14',`posdb`.`sales_payment`.`CARD_AMT`,0)) AS `CARD_AMT_OVO`,sum(if(`posdb`.`sales_payment`.`EDC_VDR` = '15',`posdb`.`sales_payment`.`CARD_AMT`,0)) AS `CARD_AMT_DANA`,sum(if(`posdb`.`sales_payment`.`EDC_VDR` = '16',`posdb`.`sales_payment`.`CARD_AMT`,0)) AS `CARD_AMT_GRAB`,sum(if(`posdb`.`sales_payment`.`EDC_VDR` = '17',`posdb`.`sales_payment`.`CARD_AMT`,0)) AS `CARD_AMT_PGOVO`,sum(if(`posdb`.`sales_payment`.`EDC_VDR` = '18',`posdb`.`sales_payment`.`CARD_AMT`,0)) AS `CARD_AMT_PGWA`,sum(if(`posdb`.`sales_payment`.`EDC_VDR` = '19',`posdb`.`sales_payment`.`CARD_AMT`,0)) AS `CARD_AMT_VABCA`,sum(if(`posdb`.`sales_payment`.`EDC_VDR` = '20',`posdb`.`sales_payment`.`CARD_AMT`,0)) AS `CARD_AMT_VAMANDIRI`,sum(if(`posdb`.`sales_payment`.`EDC_VDR` = '21',`posdb`.`sales_payment`.`CARD_AMT`,0)) AS `CARD_AMT_SHOPEEPAY`,
											sum(if(`posdb`.`sales_payment`.`EDC_VDR` = '22',`posdb`.`sales_payment`.`CARD_AMT`,0)) AS `CARD_AMT_TRAVELOKA`,
											sum(if(`posdb`.`sales_payment`.`EDC_VDR` = '27',`posdb`.`sales_payment`.`CARD_AMT`,0)) AS `CARD_AMT_PGMIDTRANS`,
											sum(if(`posdb`.`sales_payment`.`EDC_VDR` = '23',`posdb`.`sales_payment`.`CARD_AMT`,0)) AS `SHOPEE_FOOD`,sum(if(`posdb`.`sales_payment`.`EDC_VDR` = '24',`posdb`.`sales_payment`.`CARD_AMT`,0)) AS `EDC_BNI`,sum(if(`posdb`.`sales_payment`.`EDC_VDR` = '25',`posdb`.`sales_payment`.`CARD_AMT`,0)) AS `TOKOPEDIA`,sum(`posdb`.`sales_payment`.`REFUND_VCH`) AS `REFUND_NON_CASH`,sum(`posdb`.`sales_payment`.`DP`) AS `DP`,current_timestamp() AS `PRINT_DATE` from ((((`posdb`.`sales_hdr` join `posdb`.`sales_payment` on(`posdb`.`sales_payment`.`ID` = `posdb`.`sales_hdr`.`ID`)) join (select `posdb`.`sales_dtl`.`ID` AS `ID`,sum(`posdb`.`sales_dtl`.`QTY` * `posdb`.`sales_dtl`.`PRICE`) AS `GROSS`,sum(`posdb`.`sales_dtl`.`QTY` * `posdb`.`sales_dtl`.`PRICE` * (`posdb`.`sales_dtl`.`DISC_ITEM` / 100.00)) AS `DISC_ITEM` from `posdb`.`sales_dtl` group by `posdb`.`sales_dtl`.`ID`) `detail` on(`detail`.`ID` = `posdb`.`sales_hdr`.`ID`)) join `posdb`.`tbl_lst_branch` on(`posdb`.`tbl_lst_branch`.`BRANCH_ID` = `posdb`.`sales_hdr`.`BRANCH_ID`)) join `posdb`.`trx_ctl` on(`posdb`.`trx_ctl`.`TRX_ID` = `posdb`.`sales_hdr`.`TRX_ID`)) 
											WHERE `posdb`.`sales_hdr`.TRX_DATE = '".$_POST['trx_id']."'	 group by `posdb`.`sales_hdr`.`BRANCH_ID`,`posdb`.`tbl_lst_branch`.`BRANCH_NAME`,`posdb`.`sales_hdr`.`TRX_ID`,`posdb`.`trx_ctl`.`TRX_STATUS`,`posdb`.`trx_ctl`.`CLOSING_USER`,`posdb`.`trx_ctl`.`CLOSING_DATE`,`posdb`.`sales_hdr`.`TRX_DATE`,`posdb`.`sales_hdr`.`POS_ID`,`posdb`.`sales_hdr`.`TRX_STATUS` order by `posdb`.`sales_hdr`.`TRX_STATUS` ASC
			");
	while($datatrx	= mysqli_fetch_object($getid))
	//print_r($datatrx);
		{
	//$closing_status	= $datatrx->closing_status;		
			$trx_id 		= $datatrx->TRX_ID;
			$trx			= "P".$datatrx->POS_ID.".".$trx_id;
			$trx_date 		= $datatrx->TRX_DATE;
			$_SESSION['select_date']	= $datatrx->TRX_DATE;
			//print_r($_POST['trx_id']);
			if($datatrx->CLOSING_STATUS == 0 ){
				$status_trans	= 'TRANSAKSI BERJALAN';
			}else{
				$status_trans	= 'END OF DAY REPORT';
			}
echo"
".str_pad("FINANCIAL REPORT",15," ",STR_PAD_LEFT)."\n".str_pad($status_trans,15," ",STR_PAD_LEFT)."\n".str_pad($datatrx->BRANCH_NAME,15," ",STR_PAD_LEFT)."\n".str_pad("TRX ID ".$trx_id,8," ",STR_PAD_LEFT)."\n".str_pad("AS OF DATE ".date("d-m-Y",strtotime($trx_date)),15," ",STR_PAD_LEFT)."\n".str_pad("PRINT DATE ".date("d-m-Y H:i"),10," ",STR_PAD_LEFT);
$totalqty = 0;
$totalamt = 0;

echo "
--------------------------------
POS ID : ".$trx."
".$datatrx->TRX_STATUS."
--------------------------------";
echo "\n".str_pad("NET SALES",21," ",STR_PAD_RIGHT).str_pad(number_format($datatrx->GROSS),10," ",STR_PAD_LEFT);
echo "\n".str_pad("PELUNASAN ULTAH",21," ",STR_PAD_RIGHT).str_pad(number_format($datatrx->DP),10," ",STR_PAD_LEFT);
echo "\n".str_pad("DISCOUNT PLU",21," ",STR_PAD_RIGHT).str_pad(number_format($datatrx->DISC_ITEM),10," ",STR_PAD_LEFT);
echo "\n".str_pad("SALES DISCOUNT",21," ",STR_PAD_RIGHT).str_pad(number_format($datatrx->SALES_DISCOUNT),10," ",STR_PAD_LEFT);
echo "\n".str_pad("PROMO DISCOUNT",21," ",STR_PAD_RIGHT).str_pad(number_format($datatrx->PROMO_DISCOUNT),10," ",STR_PAD_LEFT);
echo "\n".str_pad("SERVICE CHARGE",21," ",STR_PAD_RIGHT).str_pad(number_format($datatrx->CHARGE),10," ",STR_PAD_LEFT);
echo "\n".str_pad("TA CHARGE",21," ",STR_PAD_RIGHT).str_pad(number_format($datatrx->TACHARGE),10," ",STR_PAD_LEFT);
echo "\n".str_pad("PAJAK",21," ",STR_PAD_RIGHT).str_pad(number_format($datatrx->TAX),10," ",STR_PAD_LEFT);
echo "\n".str_pad("PEMBULATAN",21," ",STR_PAD_RIGHT).str_pad(number_format($datatrx->ROUNDING),10," ",STR_PAD_LEFT);
echo "\n".str_pad("GROSS",21," ",STR_PAD_RIGHT).str_pad(number_format($datatrx->NET),10," ",STR_PAD_LEFT);
echo "\n".str_pad("NON CASH",21," ",STR_PAD_RIGHT).str_pad(number_format($datatrx->NON_CASH),10," ",STR_PAD_LEFT);
echo "\n".str_pad("EDC BCA",21," ",STR_PAD_RIGHT).str_pad(number_format($datatrx->FBCA),10," ",STR_PAD_LEFT);
echo "\n".str_pad("EDC MANDIRI",21," ",STR_PAD_RIGHT).str_pad(number_format($datatrx->CARD_AMT_MANDIRI),10," ",STR_PAD_LEFT);
echo "\n".str_pad("EDC NIAGA",21," ",STR_PAD_RIGHT).str_pad(number_format($datatrx->CARD_AMT_NIAGA),10," ",STR_PAD_LEFT);
echo "\n".str_pad("EDC KARTUKU",21," ",STR_PAD_RIGHT).str_pad(number_format($datatrx->CARD_AMT_KARTUKU),10," ",STR_PAD_LEFT);
echo "\n".str_pad("EDC BNI",21," ",STR_PAD_RIGHT).str_pad(number_format($datatrx->EDC_BNI),10," ",STR_PAD_LEFT);
echo "\n".str_pad("PG MANDIRI",21," ",STR_PAD_RIGHT).str_pad(number_format($datatrx->CARD_AMT_PGDOKU),10," ",STR_PAD_LEFT);
echo "\n".str_pad("PG CIMB NIAGA",21," ",STR_PAD_RIGHT).str_pad(number_format($datatrx->CARD_AMT_PGCIMBNIAGA),10," ",STR_PAD_LEFT);
echo "\n".str_pad("PG OVO",21," ",STR_PAD_RIGHT).str_pad(number_format($datatrx->CARD_AMT_PGOVO),10," ",STR_PAD_LEFT);
echo "\n".str_pad("PG WA",21," ",STR_PAD_RIGHT).str_pad(number_format($datatrx->CARD_AMT_PGWA),10," ",STR_PAD_LEFT);
echo "\n".str_pad("PG MIDTRANS",21," ",STR_PAD_RIGHT).str_pad(number_format($datatrx->CARD_AMT_PGMIDTRANS),10," ",STR_PAD_LEFT);
echo "\n".str_pad("JD ID",21," ",STR_PAD_RIGHT).str_pad(number_format($datatrx->JD_ID),10," ",STR_PAD_LEFT);
echo "\n".str_pad("GO BIZ",21," ",STR_PAD_RIGHT).str_pad(number_format($datatrx->GO_RESTO),10," ",STR_PAD_LEFT);
echo "\n".str_pad("KARTUKU T CASH",21," ",STR_PAD_RIGHT).str_pad(number_format($datatrx->KARTUKU_TCASH),10," ",STR_PAD_LEFT);
echo "\n".str_pad("KARTUKU BNI",21," ",STR_PAD_RIGHT).str_pad(number_format($datatrx->KARTUKU_BNI),10," ",STR_PAD_LEFT);
echo "\n".str_pad("KARTUKU CITIBANK",21," ",STR_PAD_RIGHT).str_pad(number_format($datatrx->KARTUKU_CITIBANK),10," ",STR_PAD_LEFT);
echo "\n".str_pad("KARTUKU GOPAY",21," ",STR_PAD_RIGHT).str_pad(number_format($datatrx->KARTUKU_GOPAY),10," ",STR_PAD_LEFT);
echo "\n".str_pad("EDC BRI",21," ",STR_PAD_RIGHT).str_pad(number_format($datatrx->CARD_AMT_BRI),10," ",STR_PAD_LEFT);
echo "\n".str_pad("EDC OVO",21," ",STR_PAD_RIGHT).str_pad(number_format($datatrx->CARD_AMT_OVO),10," ",STR_PAD_LEFT);
echo "\n".str_pad("DANA",21," ",STR_PAD_RIGHT).str_pad(number_format($datatrx->CARD_AMT_DANA),10," ",STR_PAD_LEFT);
echo "\n".str_pad("GRAB",21," ",STR_PAD_RIGHT).str_pad(number_format($datatrx->CARD_AMT_GRAB),10," ",STR_PAD_LEFT);
echo "\n".str_pad("VA BCA",21," ",STR_PAD_RIGHT).str_pad(number_format($datatrx->CARD_AMT_VABCA),10," ",STR_PAD_LEFT);
echo "\n".str_pad("VA MANDIRI",21," ",STR_PAD_RIGHT).str_pad(number_format($datatrx->CARD_AMT_VAMANDIRI),10," ",STR_PAD_LEFT);
echo "\n".str_pad("SHOPEE PAY",21," ",STR_PAD_RIGHT).str_pad(number_format($datatrx->CARD_AMT_SHOPEEPAY),10," ",STR_PAD_LEFT);
echo "\n".str_pad("TRAVELOKA EATS",21," ",STR_PAD_RIGHT).str_pad(number_format($datatrx->CARD_AMT_TRAVELOKA),10," ",STR_PAD_LEFT);
echo "\n".str_pad("SHOPPE_FOOD",21," ",STR_PAD_RIGHT).str_pad(number_format($datatrx->SHOPEE_FOOD),10," ",STR_PAD_LEFT);
echo "\n".str_pad("TOKOPEDIA",21," ",STR_PAD_RIGHT).str_pad(number_format($datatrx->TOKOPEDIA),10," ",STR_PAD_LEFT);

$cash_on_bank = ($datatrx->NET - $datatrx->FBCA - $datatrx->CARD_AMT_MANDIRI - $datatrx->CARD_AMT_NIAGA - $datatrx->CARD_AMT_KARTUKU - $datatrx->EDC_BNI - $datatrx->CARD_AMT_PGDOKU - $datatrx->CARD_AMT_PGCIMBNIAGA - $datatrx->CARD_AMT_PGOVO - $datatrx->CARD_AMT_PGWA - $datatrx->JD_ID - $datatrx->GO_RESTO - $datatrx->KARTUKU_TCASH - $datatrx->KARTUKU_BNI - $datatrx->KARTUKU_CITIBANK - $datatrx->KARTUKU_GOPAY - $datatrx->CARD_AMT_BRI - $datatrx->CARD_AMT_OVO - $datatrx->CARD_AMT_DANA - $datatrx->CARD_AMT_GRAB - $datatrx->CARD_AMT_VABCA - $datatrx->CARD_AMT_VAMANDIRI- $datatrx->CARD_AMT_SHOPEEPAY- $datatrx->CARD_AMT_TRAVELOKA - $datatrx->SHOPEE_FOOD - $datatrx->TOKOPEDIA) - ($datatrx->NON_CASH - $datatrx->REFUND_NON_CASH) ;

echo "\n".str_pad("CASH ON BANK",21," ",STR_PAD_RIGHT).str_pad(number_format($cash_on_bank),10," ",STR_PAD_LEFT);
echo "\n".str_pad("REFUND NON CASH",21," ",STR_PAD_RIGHT).str_pad(number_format($datatrx->REFUND_NON_CASH),10," ",STR_PAD_LEFT);
$totalamt = $totalamt+ $datatrx->GROSS;
echo "
--------------------------------
";	

echo "".str_pad("Prepared By",10," ",STR_PAD_LEFT).
"





--------------------------------


";
		}
	}
	echo "
".chr(29)."V".chr(66).chr(3);
?>

		</pre>
	</div>
	</div>
	</div>
	</div>
<?php include("../template/footer_h2h.php");?>
<script>

window.onload = function() {
//alert("masuk");

		//location.href = "system_pos.php";
		//window.close();
   //BtPrint(document.getElementById('pre_print').innerText);
}
function BtPrint(prn){
		var textEncoded = encodeURI(prn);
		Android.showToast(textEncoded);
		/*
        var S = "#Intent;scheme=rawbt;";
        var P =  "package=ru.a402d.rawbtprinter;end;";
        var textEncoded = encodeURI(prn);
        window.location.href="intent:"+textEncoded+S+P;
		*/
}
    
</script>