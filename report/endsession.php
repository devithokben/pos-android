<?php 
	session_start();
	include("../template/header_h2h.php");
	error_reporting(1);
	require '../dbconnect.php';
	
	
	

	$getid 		= mysqli_query($conpos,"select * from vw_endsession");
	
	// ( [branch_id] => 115 [branch_name] => ararasa [trx_id] => 1 [closing_status] => 0 [trx_date] => 2020-11-17 00:00:00 [pos_id] => 11501 [trx_status] => close [session_id] => 1 [ent_user] => root [trx_count] => 1 [gross] => 89092.0000 [disc_item] => 0.000000000000 [charge] => 0 [tacharge] => 0 [sales_discount] => 0 [promo_discount] => 0 [tax] => 8909 [rounding] => 0 [non_cash] => 0 [net] => 98001 [refund_non_cash] => 0 [dp] => 0 [fbca] => 0 [card_amt_mandiri] => 0 [card_amt_niaga] => 0 [card_amt_kartuku] => 0 [card_amt_pgdoku] => 0 [card_amt_pgcimbniaga] => 0 [jd_id] => 0 [go_resto] => 0 [kartuku_tcash] => 0 [kartuku_bni] => 0 [kartuku_citibank] => 0 [kartuku_gopay] => 0 [card_amt_bri] => 0 [card_amt_ovo] => 0 [card_amt_dana] => 0 [card_amt_grab] => 0 [card_amt_pgovo] => 0 [card_amt_pgwa] => 0 [card_amt_vabca] => 0 [card_amt_vamandiri] => 0 [print_date] => 2020-11-18 06:52:28 ) 
?>
	<style>
	 table {
		 font-size:14px;
	 }
	</style>
	<div class="container-fluid">
	<?php while($datatrx	= mysqli_fetch_object($getid)){ ?>
		<div class="col-xs-4 col-md-4 col-sm-4"> 
			<div class="row">
				<div class="col-xs-12 col-md-12 col-sm-12" style="text-align:center;"> 
					<h3>hokben<br />session report</h3>
				</div>
				<div class="col-xs-12 col-md-12 col-sm-12">
					session : <?php echo  $datatrx->session_id ?><br />
					<?php echo  $datatrx->branch_name ?><br />
					print date : <?php echo date("d-m-y") ?>
					<hr />
					pos : <?php echo  $datatrx->pos_id." - ".$datatrx->ent_user ?> <hr />
					<?php echo  $datatrx->trx_status ?><hr />
				</div>
				<div class="col-xs-12 col-md-12 col-sm-12">
					<table class="table-strip"width="100%">
						<tbody>
							<tr>
								<td>net sales</td>
								<td><?php echo  number_format($datatrx->gross) ?></td>
							</tr>
							<tr>
								<td>down payment</td>
								<td><?php echo  number_format($datatrx->dp) ?></td>
							</tr>
							<tr>
								<td>discount plu</td>
								<td><?php echo  number_format($datatrx->disc_item) ?></td>
							</tr>
							<tr>
								<td>sales discount</td>
								<td><?php echo  number_format($datatrx->sales_discount) ?></td>
							</tr>
							<tr>
								<td>promo discount</td>
								<td><?php echo  number_format($datatrx->promo_discount) ?></td>
							</tr>
							<tr>
								<td>service charge</td>
								<td><?php echo  number_format($datatrx->charge) ?></td>
							</tr>
							<tr>
								<td>ta charge</td>
								<td><?php echo  number_format($datatrx->tacharge) ?></td>
							</tr>
							<tr>
								<td>pajak</td>
								<td><?php echo  number_format($datatrx->tax) ?></td>
							</tr>
							<tr>
								<td>pembulatan</td>
								<td><?php echo  number_format($datatrx->rounding) ?></td>
							</tr>
							<tr>
								<td>gross</td>
								<td><?php echo  number_format($datatrx->net) ?></td>
							</tr>
							<tr>
								<td>non cash</td>
								<td><?php echo  number_format($datatrx->non_cash) ?></td>
							</tr>
							<tr>
								<td>edc bca</td>
								<td><?php echo  number_format($datatrx->fbca) ?></td>
							</tr>
							<tr>
								<td>edc mandiri</td>
								<td><?php echo  number_format($datatrx->card_amt_mandiri) ?></td>
							</tr>
							<tr>
								<td>edc niaga</td>
								<td><?php echo  number_format($datatrx->card_amt_niaga) ?></td>
							</tr>
							<tr>
								<td>edc kartuku</td>
								<td><?php echo  number_format($datatrx->card_amt_kartuku) ?></td>
							</tr>
							<tr>
								<td>pg mandiri</td>
								<td><?php echo  number_format($datatrx->card_amt_pgdoku) ?></td>
							</tr>
							<tr>
								<td>pg cimb niaga</td>
								<td><?php echo  number_format($datatrx->card_amt_pgcimbniaga) ?></td>
							</tr>
							<tr>
								<td>pg ovo</td>
								<td><?php echo  number_format($datatrx->card_amt_pgovo) ?></td>
							</tr>
							<tr>
								<td>pg wa</td>
								<td><?php echo  number_format($datatrx->card_amt_pgwa) ?></td>
							</tr>
							<tr>
								<td>jd id</td>
								<td><?php echo  number_format($datatrx->jd_id) ?></td>
							</tr>
							<tr>
								<td>go biz</td>
								<td><?php echo  number_format($datatrx->go_resto) ?></td>
							</tr>
							<tr>
								<td>kartuku t cash</td>
								<td><?php echo  number_format($datatrx->kartuku_tcash) ?></td>
							</tr>
							<tr>
								<td>kartuku bni</td>
								<td><?php echo  number_format($datatrx->kartuku_bni) ?></td>
							</tr>
							<tr>
								<td>kartuku citibank</td>
								<td><?php echo  number_format($datatrx->kartuku_citibank) ?></td>
							</tr>
							<tr>
								<td>kartuku gopay</td>
								<td><?php echo  number_format($datatrx->kartuku_gopay) ?></td>
							</tr>
							<tr>
								<td>edc bri</td>
								<td><?php echo  number_format($datatrx->card_amt_bri) ?></td>
							</tr>
							<tr>
								<td>edc ovo</td>
								<td><?php echo  number_format($datatrx->card_amt_ovo) ?></td>
							</tr>
							<tr>
								<td>dana</td>
								<td><?php echo  number_format($datatrx->dana) ?></td>
							</tr>
							<tr>
								<td>grab pay</td>
								<td><?php echo  number_format($datatrx->card_amt_grab) ?></td>
							</tr>
							<tr>
								<td>va bca</td>
								<td><?php echo  number_format($datatrx->card_amt_vabca) ?></td>
							</tr>
							<tr>
								<td>va mandiri</td>
								<td><?php echo  number_format($datatrx->card_amt_vamandiri) ?></td>
							</tr>
							<tr>
								<td>cash on bank **</td>
								<td><?php echo  number_format($datatrx->go_resto) ?></td>
							</tr>
							<tr>
								<td>refund non case **</td>
								<td><?php echo  number_format($datatrx->go_resto) ?></td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	<?php } ?>
	<hr />
	</div>
	
<?php include("../template/footer_h2h.php");?>