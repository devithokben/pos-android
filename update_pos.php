<?php
ini_set('display_errors', '1');
ini_set('display_startup_errors', '1');
error_reporting(0);
?>
<!DOCTYPE html>
<html dir="ltr" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
   
    <title>Halaman Login POS</title>
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="assets/images/favicon.png">
    <!-- chartist CSS -->
    <link href="assets/plugins/chartist-js/dist/chartist.min.css" rel="stylesheet">
    <link href="assets/plugins/chartist-js/dist/chartist-init.css" rel="stylesheet">
    <link href="assets/plugins/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.css" rel="stylesheet">
    <!--This page css - Morris CSS -->
    <link href="assets/plugins/c3-master/c3.min.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="css/style.min.css" rel="stylesheet">
	<link href="<?php echo  $base_url ?>sweetalert2/dist/sweetalert2.min.css" rel="stylesheet">
	 
	<!-- keyboard widget css & script (required) -->
	<link href="Keyboard/css/keyboard.css" rel="stylesheet">
	<style>
		.ui-keyboard{
			background : #eee;
		}
		.ui-keyboard-button{
			height: 4em;
			min-width: 4em;
			margin: .2em;
			cursor: pointer;
			overflow: hidden;
			line-height: 4em;
			-moz-user-focus: ignore;
			
		}
		.ui-keyboard-text{
			font-size:20px;
		}
	</style>
</head>

<body class="btn-warning">
<div id="main-wrapper" data-layout="vertical"  data-sidebartype="full"
	data-sidebar-position="absolute" data-header-position="absolute" data-boxed-layout="full">
	<!-- ============================================================== -->
	<!-- Topbar header - style you can find in pages.scss -->
	<!-- ============================================================== -->
	<header class="topbar btn-danger" data-navbarbg="">
		<nav class="navbar top-navbar navbar-expand-md navbar-dark btn-danger">
			<div class="navbar-header" data-logobg="">
				<!-- ============================================================== -->
				<!-- Logo -->
				<!-- ============================================================== -->
				<a class="navbar-brand ml-4" href="index.html">
				   
					<!--End Logo icon -->
					<!-- Logo text -->
					<span class="logo-text" style="color:#fff">
					   <h2>System Update</h2>

					</span>
				</a>
				<!-- ============================================================== -->
				<!-- End Logo -->
				<!-- ============================================================== -->
				<!-- ============================================================== -->
				<!-- toggle and nav items -->
				<!-- ============================================================== -->
				<a class="nav-toggler waves-effect waves-light text-white d-block d-md-none"
					href="javascript:void(0)"><i class="ti-menu ti-close"></i></a>
			</div>
			<!-- ============================================================== -->
			<!-- End Logo -->
		  
		</nav>
	</header>
	<div class="container-fluid">
		<!-- ============================================================== -->
		<!-- Start Page Content -->
		<!-- ============================================================== -->
		<!-- Row -->
		<div class="row">
			<div class="col-lg-4 col-xlg-3 col-md-5">
					
				</div>
				<!-- Column -->
				<!-- Column -->
				<div class="col-lg-12 col-xlg-12 col-md-12">
					<div class="card" style="margin-top:40px;">
						<div class="card-body">
							<div id="information"></div>
							<hr />
							<div id="btn-akses" class="col-lg-12 col-xlg-12 col-md-12" style="text-align:center;"></div>
							<hr />
							<div id="log"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
	$qry_data	=  urldecode($_GET['qry']);
	
	include("dbconnect.php");
	ini_set('max_execution_time', 0); 
	$ctx = stream_context_create();
	stream_context_set_params($ctx, array("notification" => "stream_notification_callback"));
	
	$mb_download = file_put_contents( 'update.zip', fopen( 'https://online.hokben.net/digital_system/uploads/update.zip', 'r', null, $ctx) );
	
	function stream_notification_callback($notification_code, $severity, $message, $message_code, $bytes_transferred, $bytes_max) {
		static $filesize = null;
		switch($notification_code) {
		case STREAM_NOTIFY_FILE_SIZE_IS:
			$filesize = $bytes_max;
			break;
		case STREAM_NOTIFY_CONNECT:
			//echo "Connected...\n";
			break;
		case STREAM_NOTIFY_PROGRESS:
			 $length = (int)(($bytes_transferred/$filesize)*100);
			$data = "[".str_repeat("=", $length). ">] ".$length."% (".($bytes_transferred)."/".$filesize ." kb)";
			
			echo '<script>
			parent.document.getElementById("information").innerHTML="<div style=\"text-align:center; font-weight:bold\">'.$data.'</div>";</script>';
			break;
			
		}
		
	}
	//echo dirname(__FILE__);
	$zip = new \ZipArchive;
	//if ($zip->open($packageFileName,  (ZipArchive::CREATE | ZipArchive::OVERWRITE) ))
    $res = $zip->open('update.zip');
    if ($res === TRUE) {
		for ($i = 0; $i < $zip->numFiles; $i++) {
			$filename = $zip->getNameIndex($i);
			unlink($filename);
		}
		
		$zip->extractTo(dirname(__FILE__));
        $zip->close();
		foreach(json_decode($qry_data) as $data_update){
			mysqli_query($conpos,$data_update);
			echo $data_update."<br />";
		}
		//exit;
	   mysqli_query($conpos,"UPDATE ap_setup SET value = '".$_GET['ver']."' where ID = 'VER'");
	   //echo '<script>location.href="dashboard.php";</script>';
	   echo '<script>parent.document.getElementById("btn-akses").innerHTML="<a href=\"http://localhost:8080\" class=\"btn btn-danger\" style=\"text-align:center;font-size:18px;\">UPDATE BERHASIL SILAHKAN KLIK DISINI</a>";</script>';
	   echo '<script>parent.document.getElementById("log").innerHTML="
								<object data=\"changelog.txt\" width=\"100%\" height=\"400\" style=\"overflow:scroll\">
								Not supported
								</object>";</script>';
    } else {
        echo "gagal extract";
    }
	
?>
