<?php 
session_start();
	date_default_timezone_set("Asia/Bangkok");
	include("dbconnect.php");
	include("template/header_h2h.php");
	
	//getuser
	$getuser	= mysqli_query($conpos,"select * from tbluser where USER_1STLOGIN = 1" );
	$datauser	= mysqli_fetch_object($getuser);
	
	$get_eod 	= mysqli_query($conpos,"select * from trx_ctl where TRX_STATUS = '0'");
	$dataeod	= mysqli_fetch_object($get_eod);
	if(isset($dataeod)){
		echo "<script>
			Swal.fire({
			  title: 'ERROR!',
			  text: 'Start Of Day Sudah Dilakukan Atau End Of Day Belum Dilakukan',
			  icon: 'error',
			  confirmButtonText: 'CLOSE'
			}).then((result) => {
			  if (result.isConfirmed) {
				location.href='master_pos.php';
			  }
			});
		</script>";
		//exit;
	}
	

	$getId 		= mysqli_query($conpos,"select * from trx_ctl order by trx_date desc LIMIT 1");
	$dataTrx	= mysqli_fetch_object($getId);

	$last_trx 	= $dataTrx->TRX_DATE;
	$last_trxud = $dataTrx->TRX_ID+1;
	$status		= $dataTrx->TRX_STATUS; 
	
	if($dataTrx){
		if($status == 0){
			echo "<script>
				Swal.fire({
				  title: 'Error!',
				  text: 'Start Of Day Sudah Dilakukan Atau End Of Day Belum Dilakukan',
				  icon: 'error',
				  confirmButtonText: 'CLOSE'
				}).then((result) => {
					if (result.isConfirmed) {
						location.href='master_pos.php';
					}
				}
				});
			</script>";
			//exit;
		}
	}
?>
	<style>
		.ui-datepicker table {
			font-size: 20px;
		}
		.ui-datepicker {
			width: 300px;
		}
		.ui-widget {
			font-family: Arial,Helvetica,sans-serif;
			font-size: 20px;
		}
	</style>
	<div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="page-breadcrumb">
                <div class="row align-items-center">
					<div class="col-xs-12 col-md-12 col-sm-12">
						<form method="POST" action="<?php $_SERVER['PHP_SELF'] ?>">
							<!-- <h3>Tanggal Transaksi Sebelumnya : <?php echo  date("d-m-Y",strtotime($last_trx)) ?></h3> -->
							<h3>Tanggal Transaksi Sebelumnya : <?php $no = date("d-m-Y",strtotime($last_trx));  if ($no == "01-01-1970"){echo "Belum ada transaksi";}else{echo  date("d-m-Y",strtotime($last_trx));} ?></h3>	
							<input type="hidden" name="prior_date" value="<?php echo  $last_trx ?>" />	
							<input type="hidden" name="pos_id" value="<?php echo  $pos_id ?>" />	
							<input type="hidden" name="user_id" value="<?php echo  $datauser->USER_ID?>" />	
							<h3>Tanggal Transaksi Berikutnya : <input type="text" name="trx_date" placeholder="FORMAT : yyyy-mm-dd"  id="datepicker"/>
							</h3>
							<h4>Format tanggal Transaksi Berikutnya adalah Tahun-Bulan-Tanggal</h4>
							<hr />
							<input type="submit" value="OK" class="btn btn-warning" />
						</form>
					</div>
				</div>
			</div>
		</div>
<?php include("template/footer_h2h.php");?>
<?php
	if($_POST){
		
		if(!$_POST['trx_date'] || ($_POST['trx_date'] == '00-00-0000') || ($_POST['trx_date'] == '0000-00-00')){
			//echo "masuk";
			echo "<script>
			Swal.fire({
			  title: 'ERROR!',
			  text: 'Start Of Day Tidak dapat Dilakukan,Belum Memilih Tanggal Berjalan',
			  icon: 'error',
			  confirmButtonText: 'CLOSE'
			}).then((result) => {
			  if (result.isConfirmed) {
				location.href='master_pos.php';
			  }
			});
		</script>";
		exit;
		}else if(date("Y-m-d",strtotime($_POST['trx_date'])) == date("Y-m-d",strtotime($last_trx))){
			echo "<script>
				Swal.fire({
				  title: 'ERROR!',
				  text: 'Start Of Day Tidak dapat Dilakukan, Tanggal Terpilih Sama Dengan  Tanggal Berjalan',
				  icon: 'error',
				  confirmButtonText: 'CLOSE'
				}).then((result) => {
				  if (result.isConfirmed) {
					location.href='master_pos.php';
				  }
				});
			</script>";
			exit;
		}else if(date("Y-m-d",strtotime($_POST['trx_date'])) < date("Y-m-d",strtotime($last_trx))){
			echo "<script>
				Swal.fire({
				  title: 'ERROR!',
				  text: 'Start Of Day Tidak dapat Dilakukan, Tanggal Terpilih Lebih Kecil Dari Tanggal Berjalan',
				  icon: 'error',
				  confirmButtonText: 'CLOSE'
				}).then((result) => {
				  if (result.isConfirmed) {
					location.href='master_pos.php';
				  }
				});
			</script>";
			exit;
		}else{
		
			if($_POST['prior_date']){
				$priordate = $_POST['prior_date'];
			}else{
				$priordate = "1970-01-01 00:00:00";
			}
			$sql	=	"Insert into trx_ctl (
							TRX_ID,
							TRX_DATE,
							PRIOR_TRX_DATE,
							TRX_STATUS,
							COUNTER_NO,
							OPENING_USER,
							OPENING_DATE,						
							POS_ID)
						values (
							'".$last_trxud."',
							'".$_POST['trx_date']."',
							'".$priordate."',
							0,
							0,
							'".$_POST['user_id']."',
							'".date("Y-m-d h:i:s")."',						
							'".substr($_POST['pos_id'],1,5)."')";
			//echo $sql;exit;
			
			//echo "aduh";
			$myfile = fopen("./datafiles/TRXCTL_".str_replace(".","_",$last_trxud).".txt", "w") or die("Unable to open file!");
			fwrite($myfile, $sql);
			fclose($myfile);
			
			if(mysqli_query($conpos,$sql)){
				echo "<script>					
						Swal.fire({
						  title: 'SUCCESS!',
						  text: 'Start Off Day Berhasil',
						  icon: 'success',
						  confirmButtonText: 'CLOSE'
						}).then((result) => {
						  if (result.isConfirmed) {
							location.href='master_pos.php';
						  }
						});
						//alert('Start Off Day Berhasil');
					</script>";
			}else{
				echo $sql;
			//	echo mysqli_error();
			}
		}			
		
	}
?>