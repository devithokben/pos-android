
<div id="modalVoidLogin" class="pay-modal">

<!-- Modal content -->
 <div class="pay-modal-content" width="500px">
          
      <div class="col-xs-12 col-md-12 col-sm-12"> 
          <div class="row">
              <form id="frmAksesRoot">
                  <div class="row">
                      <h3>Login Supervisor Store</h3>
                      <hr />
                      <div class="col-xs-4 col-md-4 col-sm-4"> 
                          User ID
                      </div>
                      <div class="col-xs-6 col-md-6 col-sm-6"> 
                           <input type="text" name="userid" id="userid_void"  class="character"/>
                      </div>
                      <div class="col-xs-12 col-md-12 col-sm-12">
                          <br />
                      </div>
                      <div class="col-xs-4 col-md-4 col-sm-4"> 
                          Password
                      </div>
                      <div class="col-xs-6 col-md-6 col-sm-6"> 
                           <input type="password" name="password"   id="password_void" class="character" />
                           <input type="hidden" name="strukid"  id="strukidvoid" />
                      </div>
                      
                          
                      
                  </div>
                  <hr />
                      
                          <input type="button" onclick="voidpos_close()" value="CLOSE" class="btn btn-warning" style="padding:10px; margin-bottom:5px;width:50%;font-size:14px;font-weight:bold;float:left" />
                          <input type="button" onclick="voidpos()" value="OK" class="btn btn-danger" style="padding:10px; margin-bottom:5px;width:50%;font-size:14px;font-weight:bold;float:left" />
              </form>
          </div>
      </div>
      <div style="clear:both;"></div>
  </div>

</div>

<div id="modalVoidTransaction" class="pay-modal">

<!-- Modal content -->
 <div class="pay-modal-content" style="width:50%">
      <div class="col-xs-12 col-md-12 col-sm-12"> 
          <div class="row">
              
              <div class="panel panel-danger">
                  <div class="panel-heading">Detail Void Order</div>
                  <div class="panel-body">
                      <form id="frm_void">
                          <table width='100%'>
                              <tr>
                                  <td width='60%' >
                                      <div id="detailTransactionVoid"></div>
                                  </td>
                                  <td>
                                      <?php				
                                          $getButton	= mysqli_query($conpos,"select * from tbl_lst_void_reason");
                                          while($dataBtn	= mysqli_fetch_object($getButton)){
                                              if($dataBtn->ID == 1){
                                                  $selec = "CHECKED";
                                              }else{
                                                  $selec = '';
                                              }
                                              echo '	<input type="radio" id="male" name="radioReason" value="'.$dataBtn->ID.'" '.$selec.'>
                                                      <label for="male">'.$dataBtn->DESCRIPTION.'</label><hr />';
                                          }
                                          echo "<Textarea width='100%' id='reason_void' name='desc_reason' class='character'> </textarea><hr />";
                                          echo "<input type='button' class='btn btn-danger' id='btn-void-ok' value='VOID' onclick= 'proc_void()' />&nbsp;&nbsp;&nbsp;";
                                          echo "<input type='hidden' name='struk_id_void'  value='VOID' id='txtStrukVoid'  />";
                                          echo "<input type='hidden' name='void_username'   id='voidusername' value='xx'  />";
                                          echo "<input type='button' class='btn btn-warning'  id='btn-void-cancel' onclick= 'modalVoidTransaction_close()' value='CANCEL' />";
                                      ?>
                                  </td>
                              </tr>
                          </table>	
                      </form>
                  </div>
              </div>
          </div>
      </div>
      <div style="clear:both;"></div>
  </div>
</div>
<link href="Keyboard/css/keyboard.css" rel="stylesheet">
      <script src="Keyboard/js/jquery.keyboard.js"></script>

      <!-- keyboard extensions (optional) -->
      <script src="Keyboard/js/jquery.mousewheel.js"></script>
      <script>
          $(function(){
              $('.numeric').keyboard({layout : 'num' });
              $('.character').keyboard({layout: 'qwerty'});
          });
      </script>