
<div id="modalItemDisc" class="pay-modal">

  <!-- Modal content -->
  <div class="pay-modal-content">
	<span class="pay-close" onclick="closedisc()">&times;</span>
	<p>
		<div id="loadItemDiscount"></div>
	</p>
  </div>

</div>

<div id="modalItemDisc_detail" class="pay-modal">

  <!-- Modal content -->
  <div class="pay-modal-content">
	<span class="pay-close" onclick="closedisc_detail()">&times;</span>
	<p>
		<div id="loadItemDiscount_detail"></div>
	</p>
  </div>

</div>

<div id="reset_discount" class="pay-modal">

  <!-- Modal content -->
  <div class="pay-modal-content">
	<div class="col-lg-12 col-xlg-12 col-md-12">
		<div id="reset_disc_desc"></div>
	</div>
	<div class="col-lg-12 col-xlg-12 col-md-12 row">
		<hr />
		<input type="button" onclick="resetDiscYes()" value="Yes" class="btn btn-success"  style="font-size:18px;font-weight:bold;width:50%;float:left;" />
		<input type="button" onclick="resetDiscNo()" value="No" class="btn btn-danger"  style="font-size:18px;font-weight:bold;width:50%;float:left;" />
	</div>
	<div style="clear:both;"></div>
  </div>
	
</div>
<div id="modalItemDisc_popupnik" class="pay-modal">

  
  <div class="pay-modal-content">
	<span class="pay-close" onclick="close_modalItemDisc_popupnik()">&times;</span>
	<p>
		<div class="col-lg-12 col-xlg-12 col-md-12">
			<div class="row">
				<div class="col-lg-3 col-xlg-3 col-md-3">
					<span style="font-size:16px;font-weight:bold">NIK</span>
				</div>
				<div class="col-lg-9 col-xlg-9 col-md-9">
				
					<input type="hidden" id="val_nilaidisc_kar"  style="font-size:16px;font-weight:bold" class="form"/>
					<input type="hidden" id="val_type_kar"  style="font-size:16px;font-weight:bold" class="form"/>
					<input type="hidden" id="val_discname_kar"  style="font-size:16px;font-weight:bold" class="form"/>
					<input type="hidden" id="val_discid_kar"  style="font-size:16px;font-weight:bold" class="form"/>
					<input type="hidden" id="val_disctype_kar"  style="font-size:16px;font-weight:bold" class="form"/>
					<input type="text" id="val_nik_kar"  style="font-size:16px;font-weight:bold" class="form numeric"/>
				</div>
				<!-- <div class="col-lg-3 col-xlg-3 col-md-3  mt-2">
					<span style="font-size:16px;font-weight:bold">NAMA</span>
				</div> -->
				<div class="col-lg-9 col-xlg-9 col-md-9  mt-2">
					<span id="val_nama_kar"  style="font-size:16px;font-weight:bold"></span>
				</div>
				<div class="col-lg-12 col-xlg-12 col-md-12">
					<hr />
					<!-- <input type="submit" onclick="cek_nik()" value="Cek" class="btn btn-warning"  style="font-size:18px;font-weight:bold" /> -->
					<input type="button" onclick="submit_nik()" value="Proccess" class="btn btn-success"  style="font-size:18px;font-weight:bold"/>
				</div>
			</div>
		</div>
	</p>
	<div style="clear:both;"></div>
  </div>
</div>
<div id="modalItemDisc_popupnik_child" class="pay-modal">

  <!-- Modal content -->
  <div class="pay-modal-content">
	<span class="pay-close" onclick="close_modalItemDisc_popupnikchild()">&times;</span>
	<p>
		<div class="col-lg-12 col-xlg-12 col-md-12">
			<div class="row">
				<div class="col-lg-5 col-xlg-5 col-md-5">
					<span style="font-size:16px;font-weight:bold">NIK</span>
				</div>
				<div class="col-lg-7 col-xlg-7 col-md-7">
				
					<input type="hidden" id="val_nilaidisc_kar_anak"  style="font-size:16px;font-weight:bold" class="form"/>
					<input type="hidden" id="val_type_kar_anak"  style="font-size:16px;font-weight:bold" class="form"/>
					<input type="hidden" id="val_discname_kar_anak"  style="font-size:16px;font-weight:bold" class="form"/>
					<input type="hidden" id="val_discid_kar_anak"  style="font-size:16px;font-weight:bold" class="form"/>
					<input type="hidden" id="val_disctype_kar_anak"  style="font-size:16px;font-weight:bold" class="form"/>
					<input type="text" id="val_nik_kar_child_anak"  style="font-size:16px;font-weight:bold" class="form numeric"/>
				</div>
				<div class="col-lg-5 col-xlg-5 col-md-5">
					<span style="font-size:16px;font-weight:bold">NAMA ANAK</span>
				</div>
				<div class="col-lg-7 col-xlg-7 col-md-7">
					<input type="text" id="val_anak_kar_child"  style="font-size:16px;font-weight:bold" class="form character"/>
				</div>
				<div class="col-lg-12 col-xlg-12 col-md-12">
					<hr />
					<input type="button" onclick="submit_nik_child()" value="Proccess" class="btn btn-success"  style="font-size:18px;font-weight:boldl" />
				</div>
			</div>
		</div>
	</p>
	<div style="clear:both;"></div>
  </div>

</div>


<div id="modalResetDiscLogin" class="pay-modal">

<!-- Modal content -->
 <div class="pay-modal-content" width="500px">
		  
	  <div class="col-xs-12 col-md-12 col-sm-12"> 
		  <div class="row">
			  <form id="frmAksesRoot">
				  <div class="row">
					  <h3>Login Supervisor Store</h3>
					  <hr />
					  <div class="col-xs-4 col-md-4 col-sm-4"> 
						  User ID
					  </div>
					  <div class="col-xs-6 col-md-6 col-sm-6"> 
						   <input type="text" name="userid" id="userid_disc"  class="character"/>
					  </div>
					  <div class="col-xs-12 col-md-12 col-sm-12">
						  <br />
					  </div>
					  <div class="col-xs-4 col-md-4 col-sm-4"> 
						  Password
					  </div>
					  <div class="col-xs-6 col-md-6 col-sm-6"> 
						   <input type="password" name="password"   id="password_disc" class="character" />
						   <!-- <input type="hidden" name="strukid"  id="strukidvoid" /> -->
					  </div>
					  
						  
					  
				  </div>
				  <hr />
					  
						  <input type="button" onclick="closeLoginDisc()" value="CLOSE" class="btn btn-warning" style="padding:10px; margin-bottom:5px;width:50%;font-size:14px;font-weight:bold;float:left" />
						  <input type="button" onclick="resetDiscYes()" value="OK" class="btn btn-danger" style="padding:10px; margin-bottom:5px;width:50%;font-size:14px;font-weight:bold;float:left" />
			  </form>
		  </div>
	  </div>
	  <div style="clear:both;"></div>
  </div>

</div>

