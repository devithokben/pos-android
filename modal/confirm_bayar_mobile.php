
<style>
.form-control{
	height:20px;
	margin-bottom:5px;
	text-align:right;
}
</style>
<div id="confirm_mypayModal" class="pay-modal" style="padding:10px;">

	  <!-- Modal content -->
	<div class="pay-modal-content" style="width:100%">
		<input type="hidden" id="promo_id_bayar" />
		<input type="hidden" id="promo_value_bayar" />
		<div class="col-xs-12 col-md-12 col-sm-12"> 
			<div class="row">
				<div class="col-xs-12 col-md-12 col-sm-12"> 
					<div class="row">	
						
						<div class="panel panel-success">
							<div class="panel-body">
								<table class="table-striped" width="100%">
									<tbody>
										<tr>
											<td>
												<b>GROSS:</b>
											</td>
											<td>
												<input type="text" class="form-control" id="gross_bayar" readonly>
											</td>
										</tr>
										<tr id="item_disc">
											<td>
												<b>ITEM DISCOUNT:</b>
											</td>
											<td >
												<input type="text" class="form-control" id="item_discount" value="0" readonly>
											</td>
										</tr>
										<tr id="sales_disc">
											<td>
												<b>SALES DISCOUNT:</b>
											</td>
											<td >
												<input type="text" class="form-control" id="sales_discount" value="0" readonly>
											</td>
										</tr>
										<tr>
											<td>
												
											</td>
											<td style="border-top: 2px solid #000;">
												
												<input type="text" class="form-control" id="gross_saldisc" readonly>
											</td>
										</tr>
										<tr>
											<td>
												<b>Service Charge:</b>
											</td>
											<td  style="border-bottom: 2px solid #000;">
												<input type="text" class="form-control" id="sales_servicecharge" readonly>
											</td>
										</tr>
										<tr>
											<td>
												
											</td>
											<td>
												<input type="text" class="form-control" id="grsl_svc" readonly>
											</td>
										</tr>
										<tr>
											<td>
												<b>TA Charge:</b>
											</td>
											<td style="">
												<input type="text" class="form-control" id="sales_tacharge" readonly>
											</td>
										</tr>
										<tr>
											<td>
												<b>Down Payment:</b>
											</td>
											<td style="">
												<input type="text" class="form-control" id="dp_sales" readonly>
											</td>
										</tr>
										<tr>
											<td>
												<b>PJK Resto:</b>
											</td>
											<td style="border-bottom: 2px solid #000;">
												<input type="text" class="form-control" id="sales_pajakresto" readonly>
											</td>
										</tr>
										<tr>
											<td>
												<b>SUBTOTAL</b>
											</td>
											<td>
												<input type="text" class="form-control" id="sales_subtotal" readonly>
											</td>
										</tr>
										<tr>
											<td>
												<b>Pembulatan</b>
											</td>
											<td>
												<input type="text" class="form-control" name="pembulatan" id="pembulatan2" readonly>
											</td>
										</tr>
										<tr>
											<td>
												<b>Grand Total</b>
											</td>
											<td>
												<input type="text" class="form-control" name="grand_total" id="subtotal2" readonly>
											</td>
										</tr>
										<tr>
											<td>
												<b>Non Tunai</b>
											</td>
											<td>
												<input type="text" class="form-control" name="non_nu" id="non_tunai" value="0"readonly>
											</td>
										</tr>
										<!--
										<tr>
											<td>
												<b>Total:</b>
											</td>
											<td style="border-bottom: 2px solid #000;">
												<input type="text" class="form-control" id="sales_total" readonly>
											</td>
										</tr>
										-->
										<tr>
											<td>
												<b>Tunai</b>
											</td>
											<td>
												<br/>
												<input type="text" class="form-control numeric" id="tunai"  placeholder="Enter Nominal Payment" data-inputmask="'mask': '9999 9999 9999 9999'" >
												<br />
												<input type="button" class="btn btn-warning col-sm-12" id="btn-cash" onclick="btncash(0)" value="CASH"/>
											</td>
										</tr>
										<tr>
											<td colspan="2" style="padding:10px 0px 10px 30px;">
												<a href="#" class="btn btn-danger" style="width:30%" onclick="btncash(10000)">10,000</a>
												<a href="#" class="btn btn-danger" style="width:30%"  onclick="btncash(20000)">20,000</a>
												<a href="#" class="btn btn-danger" style="width:30%"  onclick="btncash(30000)">30,000</a>
												<a href="#" class="btn btn-danger" style="width:30%"  onclick="btncash(40000)">40,000</a>
												<a href="#" class="btn btn-danger" style="width:30%"  onclick="btncash(50000)">50,000</a>
												<a href="#" class="btn btn-danger" style="width:30%"  onclick="btncash(100000)">100,000</a>
											</td>
										</tr>
										<tr>
											<td>
												<b>Kembali</b>
											</td>
											<td>
												<input type="text" class="form-control" name="pembulatan" value="0" id="kembali" readonly>
											</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-md-12 col-sm-12"> 
					<div class="panel panel-info">
						<div class="panel-body">
							<button class="btn btn-danger " onclick="confirm_mypayModal_close()" style="width:100%;font-size:14px;font-weight:bold;line-height:50px; margin-bottom:10px;" id="cancel_payment">BATAL</button>
							
							<div id="loadButtonbayar"></div>
							
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-md-12 col-sm-12"> 
					<div class="row">	
						<div id="promouse" class="alert alert-info"></div>
						<div class="panel panel-info">
							<div class="panel-heading">Voucher Information</div>
							<div class="panel-body">
								<table class="table-striped" width="90%">
									<thead>
										<tr>
											<th>
												No
											</th>
											<th>
												Voucher No
											</th>
											<th>
												Amount
											</th>
											<th>
											</th>
											<th>
											</th>
										</tr>
									</thead>
									<tbody id="app_voucher">
										<tr>
											<td>
												1
											</td>
											<td>
												<input type="text" id="voucher_number_1" class="voucher_number character" name="voucher_number[]" />
											</td>
											<td>
												<input type="text" id="voucher_amount_1" class="voucher_amount numeric" name="voucher_amount[]" onchange="upd_nontunai()" />
											</td>
											<td>
												<a href="#" class="btn btn-danger" class="del_voucher" onclick="btn_del_voucher(1)" id="del_voucher_1">
													DEL
												</a>
											</td>
											<td>
												
												<a href="#" class="btn btn-success" class="add_voucher" onclick="btn_add_voucher(1)" id="add_voucher_1">
													ADD
												</a>
											</td>
											
										</tr>
									</tbody>
								</table>
							</div>
						</div>
						
						
					</div>
				</div>
			</div>
		</div>
		<div style="clear:both;"></div>
	</div>
</div>


<div id="modal_list_btn_bayar" class="pay-modal" >

  <!-- Modal content -->
  <div class="pay-modal-content" style="width:100%">
	<strong style="font-size:25px;">PILIH METODE PEMBAYARAN</strong><span class="pay-close" onclick="close_list_btn_bayar()">&times;</span>
	<hr />
	<div id="load_list_btn_bayar" class="row"></div>
	
  </div>

</div>
<div id="note_payment_method" class="pay-modal" >
	  <!-- Modal content -->
	  <div class="pay-modal-content">
		<span class="pay-close" onclick="close_note_payment_method()">&times;</span>
		<p>
			<h4 style="font-weight:bold;"><span id="itemname">Masukan ID Referensi</span></h4>
			<div class="col-lg-12 col-xlg-12 col-md-12">
				<div class="row">
					<div class="col-lg-3 col-xlg-3 col-md-3">
						
					</div>
					<div class="col-lg-9 col-xlg-9 col-md-9">
						<input type="hidden" id="idbtn_pay"  style="font-size:16px;font-weight:bold" class="form"/>
						<input type="hidden" id="title_pay"  style="font-size:16px;font-weight:bold" class="form"/>
						<input type="hidden" id="note_pay"  style="font-size:16px;font-weight:bold" class="form"/>
						<input type="text" id="val_id_referensi"  style="width:100%;font-size:20px;font-weight:bold" class="form numeric"/>
					</div>
					<div class="col-lg-6 col-xlg-6 col-md-6">
						<hr />
						<input type="button" onclick="submitReferensi()" value="Proccess" class="btn btn-success"  style="font-size:18px;font-weight:bold;width:100%" />
					</div>
					<div class="col-lg-6 col-xlg-6 col-md-6">
						<hr />					
						<input type="button" onclick="close_note_payment_method()" value="Cancel" class="btn btn-danger"  style="font-size:18px;font-weight:bold;width:100%" />
					</div>
				</div>
			</div>
		</p>
		<div style="clear:both;"></div>
	  </div>
</div>

<!-- keyboard widget css & script (required) -->
		<link href="Keyboard/css/keyboard.css" rel="stylesheet">
		<script src="Keyboard/js/jquery.keyboard.js"></script>

		<!-- keyboard extensions (optional) -->
		<script src="Keyboard/js/jquery.mousewheel.js"></script>
		<script>
			$(function(){
				$('.numeric').keyboard({layout : 'num' });
				$('.character').keyboard({layout: 'qwerty'});
			});
		</script>
		
