<div id="mypayModal" class="pay-modal" >

	  <!-- Modal content -->
	  <div class="pay-modal-content" style="width:800px;">
		<span class="pay-close" onclick="spanpay()">&times;</span>
		<p>
			<h3 style="font-weight:bold;">PROMO</h3>
			<div align="center" style="font-size:16px; margin-bottom:5px;">
				<table width="100%">
					<tr>
						<td width="80%">
							<table width="100%">
								<tr>
									<td>SUB TOTAL </td><td align="right" style="font-weight:bold">Rp <span id="ttlpay"></span></td>
								</tr>
								<tr>
									<td>PAJAK RESTO 10%</td><td align="right" style="font-weight:bold">Rp <span id="pjkpay"></span></td>
								</tr>
								<tr>
									<td>TOTAL </td><td align="right" style="font-weight:bold">Rp <span id="grtotal"></span></td>
								</tr>
							</table>
						</td>
						<td>
							<table width="100%">
								<tr>
									<td>SUB TOTAL </td><td align="right" style="font-weight:bold">Rp <span id="ttlpay"></span></td>
								</tr>
								<tr>
									<td>PAJAK RESTO 10%</td><td align="right" style="font-weight:bold">Rp <span id="pjkpay"></span></td>
								</tr>
								<tr>
									<td>TOTAL </td><td align="right" style="font-weight:bold">Rp <span id="grtotal"></span></td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</div>
			<div id="loadpayment"></div>
			<div>
				<button style="background:#0099FF; width:100%; font-size:14px; color:#FFF; padding:10px; font-weight:bold;" onclick="processpay()">SUBMIT</button>
			</div>
		</p>
	  </div>

	</div>