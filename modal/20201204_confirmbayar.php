	<!-- jQuery & jQuery UI + theme (required) -->
	<script src="Keyboard/docs/js/jquery-ui-custom.min.js"></script>
	<script src="Keyboard/docs/js/bootstrap.min.js"></script>

	<!-- keyboard widget css & script (required) -->
	<script src="Keyboard/js/jquery.keyboard.js"></script>

	<!-- keyboard extensions (optional) -->
	<script src="Keyboard/js/jquery.mousewheel.js"></script>
	
	<script>
		
	</script>
	<style>
		#tunai_keyboard{
			background : #000;
		}
		.form-control{
			font-size:11px;
			border: none;
			height:12px;
			padding:0px;
			
		}
		.form-group{
			margin:0px;
		}
	</style>

<div id="confirm_mypayModal" class="pay-modal" style="padding:10px;">

	  <!-- Modal content -->
	  <div class="pay-modal-content" style="width:90%">
		<input type="hidden" id="promo_id_bayar" />
			<input type="hidden" id="promo_value_bayar" />
			<div class="col-xs-12 col-md-12 col-sm-12"> 
				<div class="row">
					<div class="col-xs-7 col-md-7 col-sm-7"> 
						<div class="row">	
							
							<div class="panel panel-success">
								<div class="panel-heading">Confirmasi Order</div>
								<div class="panel-body">
									<div class =" col-sm-6 row">
										<div class="form-group row">
											<label class="control-label col-sm-6" for="email">GROSS:</label>
											<div class="col-sm-6">
												<input type="text" class="form-control" id="gross_bayar" readonly>
											</div>
										</div>
										<div class="form-group  row">
											<label class="control-label col-sm-6" for="email">Sales Discount:</label>
											<div class="col-sm-6">
											  <input type="text" class="form-control" id="sales_discount" readonly>
											</div>
										</div>
										<div class="form-group row">
											<label class="control-label col-sm-6" for="email">SVC Charge:</label>
											<div class="col-sm-6">
											  <input type="text" class="form-control" id="sales_servicecharge" readonly>
											</div>
										</div>
										<div class="form-group row">
											<label class="control-label col-sm-6" for="email">DP:</label>
											<div class="col-sm-6">
											  <input type="text" class="form-control" id="email" value="0" readonly>
											</div>
										</div>	
										<div class="form-group row">
											<label class="control-label col-sm-6" for="email">Pajak Resto :</label>
											<div class="col-sm-6">
											  <input type="text" class="form-control" id="sales_pajakresto" readonly>
											</div>
										</div>
									</div>
									<div class =" col-sm-6">
										<div class="form-group row">
											<label class="control-label col-sm-6" for="email">None Tunai :</label>
											<div class="col-sm-6">
											  <input type="text" class="form-control" id="email" value="0" readonly>
											</div>
										</div>
										<div class="form-group row">
											<label class="control-label col-sm-6" for="email">Pembulatan :</label>
											<div class="col-sm-6">
											  <input type="text" class="form-control" name="pembulatan" id="pembulatan2" readonly>
											</div>
										</div>
										<div class="form-group row">
											<label class="control-label col-sm-6" for="email">Grand Total :</label>
											<div class="col-sm-6">
											  <input type="text" class="form-control" id="subtotal2" readonly>
											</div>
										</div>
										<div class="form-group row">
											<label class="control-label col-sm-6" for="email">Non Tunai :</label>
											<div class="col-sm-6">
											  <input type="text" class="form-control" id="non_tunai" readonly>
											</div>
										</div>
										<div class="form-group row">
											<label class="control-label col-sm-6" for="email" style="font-size:12px;font-weight:bold;height:14px;"> Tunai :</label>
											<div class="col-sm-6">
											  <input type="text" class="form-control" id="tunai" placeholder="Enter Nominal Payment" style="font-size:14px;font-weight:bold;height:14px;">
											  <br />
											  <input type="button" class="btn btn-warning col-sm-12" id="btn-cash" onclick="btncash()" value="CASH"/>
											</div>
										</div>
										<div class="form-group row">
											<br />
											<label class="control-label col-sm-6" for="email" style="font-size:12px;font-weight:bold;height:14px;"> Kembali :</label>
											<div class="col-sm-6">
											  <input type="text" class="form-control" id="kembali" style="font-size:14px;font-weight:bold;">
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="panel panel-warning">
								<div class="panel-heading">Choose Payment Type</div>
								<div class="panel-body">
									<label class="container col-sm-3">
									<input type="radio" name="radio_ptype" checked value="1">&nbsp;CASH
									</label>
									
									<label class="container col-sm-3">
									<input type="radio" name="radio_ptype" value="2">&nbsp;BCA
									</label>
									
									<label class="container col-sm-3">
									<input type="radio" name="radio_ptype" value="2">&nbsp;MANDIRI
									</label>
									
									<label class="container col-sm-3">
									<input type="radio" name="radio_ptype" value="2">&nbsp;CIMB NIAGA
									</label>
									<label class="container col-sm-3">
									 <input type="radio" name="radio_ptype" value="2">&nbsp;KARTUKU TCASH
									</label></label>
									<label class="container col-sm-3">
									 <input type="radio" name="radio_ptype" value="2">&nbsp;KARTUKU MANDIRI
									</label></label>
									<label class="container col-sm-3">
									  <input type="radio" name="radio_ptype" value="2">&nbsp;KARTUKU GOPAY
									</label>
									<label class="container col-sm-3">
									<input type="radio" name="radio_ptype" value="2">&nbsp;BRI
									</label>
									<label class="container col-sm-3">
									<input type="radio" name="radio_ptype" value="2">&nbsp;OVO
									</label>
									<label class="container col-sm-3">
									<input type="radio" name="radio_ptype" value="2">&nbsp;DANA
									</label>
									<label class="container col-sm-3">
									<input type="radio" name="radio_ptype" value="2">&nbsp;GRAB
									</label>
									<label class="container col-sm-3">
									<input type="radio" name="radio_ptype" value="2">&nbsp;GOBIZ
									</label>
								</div>
							</div>
						</div>
						
					</div>
					<div class="col-xs-5 col-md-5 col-sm-5">
						<div id="promouse" class="alert alert-info"></div>
						<div class="panel panel-info">
								<div class="panel-heading">Voucher Information</div>
								<div class="panel-body">
									<table class="table-striped" width="100%">
										<thead>
											<tr>
												<th>
													No
												</th>
												<th>
													Voucher No
												</th>
												<th>
													Amount
												</th>
												<th>
												</th>
												<th>
												</th>
											</tr>
										</thead>
										<tbody id="app_voucher">
											<tr>
												<td>
													1
												</td>
												<td>
													<input type="text" id="voucher_number_1" />
												</td>
												<td>
													<input type="text" id="voucher_amount_1" />
												</td>
												<td>
													<a href="#" class="btn btn-danger" class="del" onclick="btn_del_voucher(1)">
														DEL
													</a>
												</td>
												<td>
													
													<a href="#" class="btn btn-success" class="add" onclick="btn_add_voucher(1)">
														ADD
													</a>
												</td>
												
											</tr>
										</tbody>
									</table>
								</div>
							</div>
						<div class="panel panel-warning">
							<div class="panel-heading">Detail Order</div>
							<div class="panel-body">
								<table class="table">
									<thead>
										<tr>
											<th>
												PLU
											</th>
											<th>
												NAME
											</th>
											<th>
												QTY
											</th>
											<th>
												PRICE
											</th>
											<th>
												TYPE
											</th>
											<th>
												DISC
											</th>
										</tr>
									</thead>
									<tbody id="temp_order"></tbody>
								</table>
									
							</div>
						</div>
						
					</div>
				
					<div class="col-xs-12 col-md-12 col-sm-12">
						
						<button class="btn btn-success" onclick="processpay()" style="margin-bottom:5px;width:50%;font-size:14px;font-weight:bold;float:left">BAYAR</button>
						<button class="btn btn-danger btn-cancel-payment" onclick="confirm_mypayModal_close()" style="margin-bottom:5px;width:50%;font-size:14px;font-weight:bold;float:left">BATAL</button>
					</div>
				</div>
			</div>
		
		<div style="clear:both;"></div>
	  </div>

</div>