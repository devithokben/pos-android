<style>
.blinking{
    animation:blinkingText 1.2s infinite;
}
@keyframes blinkingText{
    0%{     color: #000;    }
    49%{    color: #000; }
    60%{    color: transparent; }
    99%{    color:transparent;  }
    100%{   color: #000;    }
}

</style>
<div id="modal_list_other" class="pay-modal">

  <!-- Modal content -->
  <div class="pay-modal-content" style="width:60%">
	<strong style="font-size:25px;">PILIH CHANNEL</strong><span class="pay-close" onclick="close_list_other()">&times;</span>
	<hr />
	<div  class="col-lg-4 col-xlg-4 col-md-4 btn btn-danger" style="align:right;font-size:40px;line-height:70px;background-color:#F0AD4E;" onclick="cek_point()" >MEMBER</div>
	<div  class="col-lg-4 col-xlg-4 col-md-4 btn btn-danger" style="align:right;font-size:40px;line-height:70px;background-color:#F0AD4E;" onclick="cek_point()" >QRIS</div>
	<div style="clear:both;"></div>
	<hr />
	
  </div>
	
</div>
<div id="modal_form_cek_point" class="pay-modal">

   <div class="pay-modal-content">
	<strong style="font-size:25px;">Masukan No TLP / Email</strong><span class="pay-close" onclick="close_modal_form_cek_point()">&times;</span>
	<p>
		<div class="col-lg-12 col-xlg-12 col-md-12">
			<div class="row">
				
				<div class="col-lg-12 col-xlg-12 col-md-12">
					<input type="text" id="areacode" value="62" style="font-size:30px;font-weight:bold;width:20%" readonly /><input type="text" id="phone_number"  style="font-size:30px;font-weight:bold;width:80%" placeholder="Tanpa 0 / 62. example : 856887766776" class="form character"/>
				</div>
				<div class="col-lg-12 col-xlg-12 col-md-12">
					
						<div id="dtl_member"></div>
				</div>
				
				<div class="col-lg-12 col-xlg-12 col-md-12">
					<hr />
					<input type="button" id="btnCheck" onclick="check_membership()" value="Check Now" class="btn btn-success"  style="font-size:18px;font-weight:boldl" />
					<div id="waiting" class="alert alert-warning blinking" style="display:none;">CHECKING SERVER .........</div>
					
				</div>
			</div>
		</div>
	</p>
	 <div style="clear:both;"></div>
  </div>
	
</div>
<div id="modal_form_cek_voucher" class="pay-modal" >

   <div class="pay-modal-content" style="width:80%">
	<strong style="font-size:25px;">PILIH REDEEM VOUCHER</strong><span class="pay-close" onclick="close_modal_form_cek_voucher()">&times;</span>
	<p>
		<div class="col-lg-12 col-xlg-12 col-md-12">
			<div class="row">
				
				<div class="col-lg-12 col-xlg-12 col-md-12">
					<hr />
						<button class="btn-warning col-xs-12 col-md-12 col-sm-12" style="align:right;font-size:25px;line-height:50px;margin-bottom:10px;" onclick = "proses_redeem_v('810046',10)" value="">FREE ES OGURA :: POINT 10 </button>
						<button class="btn-warning col-xs-12 col-md-12 col-sm-12" style="align:right;font-size:25px;line-height:50px;margin-bottom:10px;" onclick = "proses_redeem_v('812040',100)" value="">FREE HOKA HEMAT 1 :: POINT 100 </button>
				</div>
				
			</div>
		</div>
	</p>
	 <div style="clear:both;"></div>
  </div>
	
</div>