<div id="modal_ref_void" class="pay-modal">

  <!-- Modal content -->
   <div class="pay-modal-content" style="width:50%">
		<div class="col-xs-12 col-md-12 col-sm-12"> 
			<div class="row">
				
				<div class="panel panel-danger">
					<div class="panel-heading">Reference Void</div>
					<div class="panel-body">
						<!-- <form id="frm_void"> -->
							<table width='100%'>
								<tr>
									<td>
										<div id="data-void"></div> <hr />
										
										<button onclick="closeRefVoid()" style="background-color:red;color:#fff;border:0px;padding:10px;">Close</button>
									</td>
								</tr>
							</table>	
						<!-- </form> -->
					</div>
				</div>
			</div>
		</div>
		<div style="clear:both;"></div>
	</div>
</div>
<link href="Keyboard/css/keyboard.css" rel="stylesheet">
		<script src="Keyboard/js/jquery.keyboard.js"></script>

		<!-- keyboard extensions (optional) -->
		<script src="Keyboard/js/jquery.mousewheel.js"></script>
		<script>
			$(function(){
				$('.numeric').keyboard({layout : 'num' });
				$('.character').keyboard({layout: 'qwerty'});
			});
		</script>