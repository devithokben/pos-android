<div id="modalUpgrade" class="pay-modal">
	<form id="frmupgrade">
	  <!-- Modal content -->
	  <div class="pay-modal-content">
		<span class="pay-close" onclick="closeUpgrade()">&times;</span>
		<p>
			<h4 style="font-weight:bold;"><span id="itemname"></span> : 
				<input type='hidden' name="itemcore" readonly id="qtycore" />
				<input type='hidden' name="parent" readonly id="parent" />
				<input type='hidden' name="layanan" readonly id="coretype" />
			</h4>
			<div>
				<div id="loadyupgrade"></div>
				<table class="table table-striped">
					<tr>
						<th >
							PLU
						</th>
						<th >
							Name
						</th>
						<th width="10%">
							QTY
						</th>
					</tr>
				</table>
				
				<table class="table table-striped">
					<tbody id="resultupgrade"></tbody>
				</table>
			</div>
			<div>
				<hr />
				<a href="#" style="background:#0099FF; width:100%; font-size:14px; color:#FFF; padding:10px; font-weight:bold;" onclick="processupgrade()">SUBMIT</a>
			</div>
		</p>
	  </div>
	</form>
</div>