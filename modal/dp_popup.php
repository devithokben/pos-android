<link href="<?php echo  $base_url ?>css/jquery-ui.css" rel="stylesheet">
<style>
.form-control{
	height:20px;
	margin-bottom:5px;
	text-align:right;
}
</style>
<div id="list_dp" class="pay-modal" style="padding:10px;padding-top:20px;">
	
	  <!-- Modal content -->
	<div class="pay-modal-content" style="width:80%";>
		<div class="col-xs-12 col-md-12 col-sm-12"> 
			<div class="row">
				<h3>List DP <span class="pay-close" onclick="close_info_dp()">&times;</span></h3><hr />
				<div class="col-xs-12 col-md-12 col-sm-12"> 
					<div id="lst_dp"></div>
				</div>
				
				
				<button class="btn btn-success col-xs-6 col-md-6 col-sm-6" style="font-size:20px;width:100%"  onClick="close_info_dp()">Close</button>
			</div>
		</div>
		<div style="clear:both;"></div>
	</div>
</div>
<div id="list_dp_release" class="pay-modal" style="padding:10px;padding-top:20px;">
	
	  <!-- Modal content -->
	<div class="pay-modal-content" style="width:80%";>
		<div class="col-xs-12 col-md-12 col-sm-12"> 
			<div class="row">
				<h3>List DP <span class="pay-close" onclick="close_info_dp_release()">&times;</span></h3><hr />
				<div class="col-xs-12 col-md-12 col-sm-12"> 
					<div id="lst_dp_release"></div>
				</div>
				<button class="btn btn-success col-xs-6 col-md-6 col-sm-6" style="font-size:20px;width:50%"  onClick="proc_list_dp_release()">OK</button>
				<button class="btn btn-warning col-xs-12 col-md-12 col-sm-12" style="font-size:20px;width:50%"  onClick="close_info_dp_release()">Close</button>
			</div>
		</div>
		<div style="clear:both;"></div>
	</div>
</div>
<div id="dp_popup" class="pay-modal" style="padding:10px;padding-top:20px;">
	
	  <!-- Modal content -->
	<div class="pay-modal-content">
		
		<div class="col-xs-12 col-md-12 col-sm-12"> 
			<div class="row">
				<h3>Birthday Party <span class="pay-close" onclick="close_dp_popup()">&times;</span></h3><hr />
				<div class="col-xs-6 col-md-6 col-sm-6"> 
					<button class="btn btn-success col-xs-12 col-md-12 col-sm-12" style="font-size:20px;width:100%" onClick="new_dp()">New DP</button>
				</div>
				
				<div class="col-xs-6 col-md-6 col-sm-6"> 
					<button class="btn btn-success col-xs-12 col-md-12 col-sm-12" style="font-size:20px;width:100%"  onClick="release_dp('<?php echo date('Y-m-d')?>')">Release DP</button>
				</div>
			</div>
		</div>
		<div style="clear:both;"></div>
	</div>
</div>
<div id="amount_dp" class="pay-modal" style="padding:10px;padding-top:20px;">
	
	  <!-- Modal content -->
	<div class="pay-modal-content">
		<div class="col-xs-12 col-md-12 col-sm-12"> 
			<div class="row">
				<h3>Amount DP <span class="pay-close" onclick="close_amount_dp()">&times;</span></h3><hr />
				<div class="col-xs-4 col-md-4 col-sm-4"> 
					<span style="font-weight:bold;font-size:24px;">DP : </span>
				</div>
				<div class="col-xs-8 col-md-8 col-sm-8"> 
					<input type="text" style="font-weight:bold;font-size:24px;width:100%;" id="val_amount_dp" class="numeric"/>
					<input type="hidden" style="font-weight:bold;font-size:24px;width:100%;" id="dp_prodcode" value="<?php echo $plu_dp?>" />
				</div>
				<div class="col-xs-12 col-md-12 col-sm-12"> 
				<hr />
				</div>
				<button class="btn btn-success col-xs-12 col-md-12 col-sm-12" style="font-size:20px;width:100%"  onClick="prc_amount_dp()">Process DP</button>
			</div>
		</div>
		<div style="clear:both;"></div>
	</div>
</div>
<div id="detail_dp" class="pay-modal" style="padding:10px;padding-top:20px;">
	
	  <!-- Modal content -->
	<div class="pay-modal-content">
		<div class="col-xs-12 col-md-12 col-sm-12"> 
			<div class="row">
				<h3>Detail Ultah <span class="pay-close" onclick="close_detail_dp()">&times;</span></h3><hr />
				<table>
					<tr class="form-group">
						<td>
							Nama
						</td>
						<td>
							<input type="text" class="form-control character" style="font-size:20px;width:100%;height:25px;" id="val_name_dp" />
						</td>
					</tr>
					<tr class="form-group">
						<td>
							Tanggal &nbsp;&nbsp;&nbsp;
						</td>
						<td>
							<input type="text" class="form-control" style="font-size:20px;width:100%;height:25px;" id="val_date_dp" />
						</td>
					</tr>
				</table>
				<div class="col-xs-12 col-md-12 col-sm-12"> 
				<hr />
				</div>
				<button class="btn btn-success col-xs-12 col-md-12 col-sm-12" style="font-size:20px;width:100%"  onClick="submit_dp()">Process DP</button>
			</div>
		</div>
		<div style="clear:both;"></div>
	</div>
</div>


<div id="dp_cancel" class="pay-modal" style="padding:10px;padding-top:20px;">
	
	  <!-- Modal content -->
	<div class="pay-modal-content"  style="width:80%">
		<form id="frm-cancel-dp">
		<div class="col-xs-12 col-md-12 col-sm-12"> 
			<div class="row">
				<h3>Cancel DP <span class="pay-close" onclick="close_cancel_dp()">&times;</span></h3><hr />
				<div class="col-xs-4 col-md-4 col-sm-4"> 
					<span style="font-weight:bold;font-size:24px;">NO Struk</span>
				</div>
				<div class="col-xs-8 col-md-8 col-sm-8"> 
					 <input type="text" style="font-weight:bold;font-size:24px;width:100%;" id="val_cancel_dp_id" name="val_cancel_dp_id" readonly disable />
				</div>
				<div class="col-xs-12 col-md-12 col-sm-12"> 
				<hr />
				</div>
				<div class="col-xs-4 col-md-4 col-sm-4"> 
					<span style="font-weight:bold;font-size:24px;">Tanggal Ultah</span>
				</div>
				<div class="col-xs-8 col-md-8 col-sm-8"> 
					<input type="text" style="font-weight:bold;font-size:24px;width:100%;" name="val_cancel_dp_tglultah"  id="val_cancel_dp_tglultah" disable readonly />
				</div>
				<div class="col-xs-12 col-md-12 col-sm-12"> 
				<hr />
				</div>
				<div class="col-xs-4 col-md-4 col-sm-4"> 
					<span style="font-weight:bold;font-size:24px;">Nama</span>
				</div>
				<div class="col-xs-8 col-md-8 col-sm-8"> 
					<input type="text" style="font-weight:bold;font-size:24px;width:100%;" name="val_cancel_dp_nama" id="val_cancel_dp_nama" disable readonly />
				</div>
				<div class="col-xs-12 col-md-12 col-sm-12"> 
				<hr />
				</div>
				<div class="col-xs-4 col-md-4 col-sm-4"> 
					<span style="font-weight:bold;font-size:24px;">Alasan </span>
				</div>
				<div class="col-xs-8 col-md-8 col-sm-8"> 
					<input type="text" style="font-weight:bold;font-size:24px;width:100%;" id="val_cancel_dp_alasan" name="val_cancel_dp_alasan" class="character" />
					
					<input type="hidden" id="val_cancel_dp_user" name="val_cancel_dp_user"/>
				</div>
				<div class="col-xs-12 col-md-12 col-sm-12"> 
				<hr />
				</div>
				<a href="#" class="btn btn-success col-xs-12 col-md-12 col-sm-12" style="font-size:20px;width:100%"  onClick="prc_cancel_dp()">Process Cancel DP</a>
			</div>
		</div>
		<div style="clear:both;"></div>
		</form>
	</div>
</div>

<style>
	.ui-datepicker table {
		font-size: 20px;
	}
	.ui-datepicker {
		width: 300px;
	}
	.ui-widget {
		font-family: Arial,Helvetica,sans-serif;
		font-size: 20px;
	}
</style>

<div id="dp_reschedule" class="pay-modal" style="padding:10px;padding-top:20px;">
	
	  <!-- Modal content -->
	<div class="pay-modal-content"  style="width:80%">
		<form id="frm-reschedule-dp">
		<div class="col-xs-12 col-md-12 col-sm-12"> 
			<div class="row">
				<h3>Reschedule DP <span class="pay-close" onclick="close_reschedule_dp()">&times;</span></h3><hr />
				<div class="col-xs-4 col-md-4 col-sm-4"> 
					<span style="font-weight:bold;font-size:24px;">NO Struk</span>
				</div>
				<div class="col-xs-8 col-md-8 col-sm-8"> 
					 <input type="text" style="font-weight:bold;font-size:24px;width:100%;" id="val_reschedule_dp_id" name="val_reschedule_dp_id" readonly disable />
				</div>
				<div class="col-xs-12 col-md-12 col-sm-12"> 
				<hr />
				</div>
				<div class="col-xs-4 col-md-4 col-sm-4"> 
					<span style="font-weight:bold;font-size:24px;">Nama</span>
				</div>
				<div class="col-xs-8 col-md-8 col-sm-8"> 
					<input type="text" style="font-weight:bold;font-size:24px;width:100%;" name="val_reschedule_dp_nama" id="val_reschedule_dp_nama" disable readonly />
				</div>
				<div class="col-xs-12 col-md-12 col-sm-12"> 
				<hr />
				</div>
				<div class="col-xs-4 col-md-4 col-sm-4"> 
					<span style="font-weight:bold;font-size:24px;">Tanggal Ultah Sebelumnya</span>
				</div>
				<div class="col-xs-8 col-md-8 col-sm-8"> 
					<input type="text" style="font-weight:bold;font-size:24px;width:100%;" name="val_reschedule_dp_tglultah"  id="val_reschedule_dp_tglultah" disable readonly />
				</div>
				<div class="col-xs-12 col-md-12 col-sm-12"> 
				<hr />
				</div>
				<div class="col-xs-4 col-md-4 col-sm-4"> 
					<span style="font-weight:bold;font-size:24px;">Tanggal Ultah Berikutnya</span>
				</div>
				<div class="col-xs-8 col-md-8 col-sm-8"> 
					<input type="text" style="font-weight:bold;font-size:24px;width:100%;" name="val_reschedule_dp_tglultah_new"  id="val_reschedule_dp_tglultah_new" disable  placeholder="FORMAT : yyyy-mm-dd"  id="datepicker" />
				</div>
				<div class="col-xs-12 col-md-12 col-sm-12"> 
				<hr />
				</div>
				
				<div class="col-xs-4 col-md-4 col-sm-4"> 
					<span style="font-weight:bold;font-size:24px;">Alasan </span>
				</div>
				<div class="col-xs-8 col-md-8 col-sm-8"> 
					<input type="text" style="font-weight:bold;font-size:24px;width:100%;" id="val_reschedule_dp_alasan" name="val_reschedule_dp_alasan" class="character" />
					<input type="hidden" id="val_reschedule_dp_user" name="val_reschedule_dp_user"/>
				</div>
				<div class="col-xs-12 col-md-12 col-sm-12"> 
				<hr />
				</div>
				<a href="#" class="btn btn-success col-xs-12 col-md-12 col-sm-12" style="font-size:20px;width:100%"  onClick="prc_reschedule_dp()">Process Reschedule DP</a>
			</div>
		</div>
		<div style="clear:both;"></div>
		</form>
	</div>
</div>

<div id="modalDpRescheduleLogin" class="pay-modal">

<!-- Modal content -->
 <div class="pay-modal-content" width="500px">
		  
	  <div class="col-xs-12 col-md-12 col-sm-12"> 
		  <div class="row">
			  <form id="frmAksesRoot">
				  <div class="row">
					  <h3>Login Supervisor Store</h3>
					  <hr />
					  <div class="col-xs-4 col-md-4 col-sm-4"> 
						  User ID
					  </div>
					  <div class="col-xs-6 col-md-6 col-sm-6">
							<input type="hidden" name="keydp" id="keydp"/>
						   <input type="text" name="userid" id="userid_reschedule"  class="character"/>
					  </div>
					  <div class="col-xs-12 col-md-12 col-sm-12">
						  <br />
					  </div>
					  <div class="col-xs-4 col-md-4 col-sm-4"> 
						  Password
					  </div>
					  <div class="col-xs-6 col-md-6 col-sm-6"> 
						   <input type="password" name="password"   id="password_reschedule" class="character" />
						   <input type="hidden" name="strukid"  id="strukiddp" />
					  </div>
					  
						  
					  
				  </div>
				  <hr />
					  
						  <input type="button" onclick="rescheduledpval_close()" value="CLOSE" class="btn btn-warning" style="padding:10px; margin-bottom:5px;width:50%;font-size:14px;font-weight:bold;float:left" />
						  <input type="button" onclick="rescheduledpval()" value="OK" class="btn btn-danger" style="padding:10px; margin-bottom:5px;width:50%;font-size:14px;font-weight:bold;float:left" />
			  </form>
		  </div>
	  </div>
	  <div style="clear:both;"></div>
  </div>

</div>

<div id="modalDpCancelLogin" class="pay-modal">

<!-- Modal content -->
 <div class="pay-modal-content" width="500px">
		  
	  <div class="col-xs-12 col-md-12 col-sm-12"> 
		  <div class="row">
			  <form id="frmAksesRoot">
				  <div class="row">
					  <h3>Login Supervisor Store</h3>
					  <hr />
					  <div class="col-xs-4 col-md-4 col-sm-4"> 
						  User ID
					  </div>
					  <div class="col-xs-6 col-md-6 col-sm-6">
							<input type="hidden" name="keydp" id="keydp"/>
						   <input type="text" name="userid" id="userid_cancel"  class="character"/>
					  </div>
					  <div class="col-xs-12 col-md-12 col-sm-12">
						  <br />
					  </div>
					  <div class="col-xs-4 col-md-4 col-sm-4"> 
						  Password
					  </div>
					  <div class="col-xs-6 col-md-6 col-sm-6"> 
						   <input type="password" name="password"   id="password_cancel" class="character" />
						   <input type="hidden" name="strukid"  id="strukiddp" />
					  </div>
					  
						  
					  
				  </div>
				  <hr />
					  
						  <input type="button" onclick="canceldpval_close()" value="CLOSE" class="btn btn-warning" style="padding:10px; margin-bottom:5px;width:50%;font-size:14px;font-weight:bold;float:left" />
						  <input type="button" onclick="canceldpval()" value="OK" class="btn btn-danger" style="padding:10px; margin-bottom:5px;width:50%;font-size:14px;font-weight:bold;float:left" />
			  </form>
		  </div>
	  </div>
	  <div style="clear:both;"></div>
  </div>

</div>



<!-- keyboard widget css & script (required) -->
	<link href="Keyboard/css/keyboard.css" rel="stylesheet">
		<script src="Keyboard/js/jquery.keyboard.js"></script>

		<!-- keyboard extensions (optional) -->
		<script src="Keyboard/js/jquery.mousewheel.js"></script>
		<script src="<?php echo  $base_url ?>js/jquery-ui.js"></script>
		<script>
			$(function(){
				$( "#val_date_dp" ).datepicker({
					dateFormat:"yy-mm-dd"
				});
				$( "#val_reschedule_dp_tglultah_new" ).datepicker({
					dateFormat:"yy-mm-dd"
				});
			});
			
		</script>
		