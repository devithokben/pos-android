<?php 
	error_reporting(E_ALL);
	session_start();
	date_default_timezone_set("Asia/Bangkok");
	include("dbconnect.php");
	include("template/header_h2h.php");
?>

	<link href="css/jquery.dataTables.min.css" rel="stylesheet">
	<div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="page-breadcrumb">
                <div class="row align-items-center">
					
					<div class="col-xs-12 col-md-12 col-sm-12" style="font-size:16px;">
						<hr />
						<form method="POST" action="<?php echo $_SERVER['PHP_SELF'] ?>?act=update_promo&pr=promo">
						
						<h4>Silahkan Tekan Tombol Dibawah Untuk Mengupdate Promo</h4>
						<input type="submit" value="UPDATE NOW" name="submit" class="btn btn-warning" />
						
						</form>
						<br />
					</div>
					<div class="col-xs-12 col-md-12 col-sm-12" id="information" >
						<div class="card">
							<div class="card-body">
								<div class="card-title">Find By Name and ID</div>
							</div>
							<div>
								<hr class="m-t-0 m-b-0">
							</div>
							<div class="card-body">
						
								<form class="form-horizontal form-material" method="POST" action="update_promo.php?act=find_name&pr=promo">
									<div class="row">
										<div class="form-group col-md-2">
											<label>Promo ID / Name</label>
										</div>
										<div class="form-group col-md-6">
											<input type="text" name="promoname" 
												class="form-control form-control-line pl-0 character" placeholder="Masukan Promo ID atau Promo Name yang akan di cari">
										</div>
										<div class="form-group col-md-2">
											<input type="submit" value="Cari" class="btn btn-warning col-xs-12 col-md-12 col-sm-12" />
										</div>
										<div class="form-group col-md-2">
											<a href="update_promo.php" class="btn btn-danger col-xs-12 col-md-12 col-sm-12">Clear</a>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
					
					<div class="col-xs-12 col-md-12 col-sm-12" >
						<table id="table_id" class="table table-striped table-bordered" style="width:100%">
							<thead>
								<tr>
									<th>Promo ID</th>
									<th>Promo Name</th>
									<th>ACTION</th>
								</tr>
							</thead>
							<tbody>
						<?php
							if($_GET['act'] != 'find_name'){
								$get_prod 	= mysqli_query($conpos,"select * from tbl_promoh order by PromoId ASC");
							}else{
								//echo "select * from tbl_promoh where PromoName like '%".$_POST['promoname']."%' order by PromoId ASC";
								$get_prod 	= mysqli_query($conpos,"select * from tbl_promoh where PromoName like '%".$_POST['promoname']."%' or PromoId like '%".$_POST['promoname']."%' order by PromoId ASC");
							}
							while($dataProd	= mysqli_fetch_object($get_prod)){
						?>
								<tr>
									<td><?php echo $dataProd->PromoId; ?></td>
									<td><?php echo $dataProd->PromoName; ?></td>
									<td><a class="btn btn-success" href="update_promo_detail.php?id=<?php echo $dataProd->PromoId; ?>&pr=promo">Detail</a></td>
								</tr>
						<?php
							}
						?>
							</tbody>
						</table>
					 </div>
				</div>
			</div>
		</div>
<?php include("template/footer_h2h.php");?>

<?php
	function doXMLCurl($url,$postXML){
		$headers = array(             
			"Content-type: text/xml;charset=\"utf-8\"", 
			"Accept: text/xml", 
			"Cache-Control: no-cache", 
			"Pragma: no-cache", 
			"SOAPAction: \"http://tempuri.org/Tbl_promo\"", 
			"Content-length: ".strlen($xml),
		); 
		$CURL = curl_init();

		curl_setopt($CURL, CURLOPT_URL, $url); 
		curl_setopt($CURL, CURLOPT_HTTPAUTH, CURLAUTH_BASIC); 
		curl_setopt($CURL, CURLOPT_POST, 1); 
		curl_setopt($CURL, CURLOPT_POSTFIELDS, $postXML); 
		curl_setopt($CURL, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($CURL, CURLOPT_RETURNTRANSFER, true);
		$xmlResponse = curl_exec($CURL); 
		
		echo "<pre>";
		print_r($xmlResponse);
		echo "</pre>";
		return $xmlResponse;
	}
	if($_GET['act'] == 'update_promo'){
		
        if(mysqli_query($conpos,"CALL sp_clearpromo")){
			//echo $url_websvc.'promo.php?storeid='.$kd_pemakai;exit;
			$ch = curl_init(); 
			curl_setopt($ch, CURLOPT_URL, $url_websvc.'promo.php?storeid='.$kd_pemakai);
			curl_setopt($ch,CURLOPT_USERAGENT,'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);     
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
			curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
			$return_out = curl_exec($ch); 
			curl_close($ch); 
			if($return_out === false)
			{
				$output	= 'Curl error: ' . curl_error($ch);
			}
			else
			{
				$output = $return_out; 
			}
			$result[0]	= json_decode($output);
			echo "<pre>";
			
		
			foreach ($result[0] as $key=>$val) {
					if($temtable != $key){
						$temtable = $key;
						//echo  "<span class='alert alert-info'>".$key." :: ";
						
						
						
						foreach ($result[0]->$key as $val2) {
							
							$newArray	= (array)$val2;	
							
							$field	= array_keys($newArray);
							$value	= array();
						
							//print_r($field);
							//print_r(array_values($newArray));
						
							for($loop = 0; $loop <= count($field) - 1;$loop++){
								if(is_object(array_values($newArray)[$loop])){
									//echo "masul";
									$value[$loop] = '';
								}else{
									if(
										(array_keys($newArray)[$loop] == 'StartDate') ||
										(array_keys($newArray)[$loop] == 'EndDate') ||
										(array_keys($newArray)[$loop] == 'StartTime') ||
										(array_keys($newArray)[$loop] == 'EndTime') ||
										(array_keys($newArray)[$loop] == 'CreateDate') || 
										(array_keys($newArray)[$loop] == 'UpdateDate') 
									){
										//echo "masukdate";
										$value[$loop] = date("Y-m-d H:i:s",strtotime(array_values($newArray)[$loop]));
									}else{
										$value[$loop] = array_values($newArray)[$loop];
									}
								}
								
								//print_r($value);
							}
							
							
							
						
							$sql = "insert into ".$temtable." (".implode(",",array_keys($newArray)).") values ('".implode("','",$value)."')";
							//echo $sql;
							mysqli_query($conpos,$sql);			
						}
						//print_r($result[0]->$key);
						//echo  "Sukses Update :: </span>";
					}
                }
					echo "</pre>";
				//echo $sql."<br />";
              //  print_r($result[0]);
				
			   
			echo "<script>					
					Swal.fire({
					  title: 'SUCCESS!',
					  text: 'Proses Update Promo Berhasil',
					  icon: 'success',
					  confirmButtonText: 'CLOSE'
					}).then((result) => {
					  if (result.isConfirmed) {
						$(document).ready(function() {
							location.href='update_promo.php?pr=promo';
						} );
					  }
					});
					//alert('Start Off Day Berhasil');
				</script>";
		}else{
			echo "<script>					
					Swal.fire({
					  title: 'GAGAL!',
					  text: 'Proses Update Promo GAGAL',
					  icon: 'error',
					  confirmButtonText: 'CLOSE'
					}).then((result) => {
					  if (result.isConfirmed) {
					//	location.href='dashboard.php';
					  }
					});
					//alert('Start Off Day Berhasil');
				</script>";
		}
		echo "mmm";exit;
	}
	
	
?>


<script src="<?php echo  $base_url ?>js/jquery.dataTables.min.js" type="text/javascript"></script>
<script>
	$.extend( true, $.fn.dataTable.defaults, {
		"searching": false
	} );
	$(document).ready(function() {
		$('#table_id').DataTable();
	} );
</script>
<script src="Keyboard/js/jquery.keyboard.js"></script>
<!-- keyboard extensions (optional) -->
<script src="Keyboard/js/jquery.mousewheel.js"></script>
<script>
	$(function(){
		$('.numeric').keyboard({layout : 'num' });
		$('.character').keyboard({layout: 'qwerty'});
	});
</script>