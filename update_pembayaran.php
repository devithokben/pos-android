<?php 
	error_reporting(E_ALL);
	session_start();
	date_default_timezone_set("Asia/Bangkok");
	include("dbconnect.php");
	include("template/header_h2h.php");
?>


<?php include("template/footer_h2h.php");?>

<?php
	function doXMLCurl($url,$postXML){
		$headers = array(             
			"Content-type: text/xml;charset=\"utf-8\"", 
			"Accept: text/xml", 
			"Cache-Control: no-cache", 
			"Pragma: no-cache", 
			"SOAPAction: \"http://tempuri.org/Tbl_promo\"", 
			"Content-length: ".strlen($xml),
		); 
		$CURL = curl_init();

		curl_setopt($CURL, CURLOPT_URL, $url); 
		curl_setopt($CURL, CURLOPT_HTTPAUTH, CURLAUTH_BASIC); 
		curl_setopt($CURL, CURLOPT_POST, 1); 
		curl_setopt($CURL, CURLOPT_POSTFIELDS, $postXML); 
		curl_setopt($CURL, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($CURL, CURLOPT_RETURNTRANSFER, true);
		$xmlResponse = curl_exec($CURL); 
		
		echo "<pre>";
		print_r($xmlResponse);
		echo "</pre>";
		return $xmlResponse;
	}
	
	if(mysqli_query($conpos,"CALL sp_clearlstbayard")){
		$ch = curl_init(); 
		curl_setopt($ch, CURLOPT_URL, $url_websvc.'tbl_lst_bayar.php?storeid='.$kd_pemakai);
		curl_setopt($ch,CURLOPT_USERAGENT,'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);     
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
		$return_out = curl_exec($ch); 
		curl_close($ch); 
		if($return_out === false)
		{
			$output	= 'Curl error: ' . curl_error($ch);
		}
		else
		{
			$output = $return_out; 
		}
		$result[0]	= json_decode($output);
		echo "<pre>";
		
	
		foreach ($result[0] as $key=>$val) {
				if($temtable != $key){
					$temtable = $key;
					//echo  "<span class='alert alert-info'>".$key." :: ";
					
					
					
					foreach ($result[0]->$key as $val2) {
						
						$newArray	= (array)$val2;	
						
						$field	= array_keys($newArray);
						$value	= array();
					
						//print_r($field);
						//print_r(array_values($newArray));
					
						for($loop = 0; $loop <= count($field) - 1;$loop++){
							if(is_object(array_values($newArray)[$loop])){
								//echo "masul";
								$value[$loop] = '';
							}else{
								if(
									(array_keys($newArray)[$loop] == 'StartDate') ||
									(array_keys($newArray)[$loop] == 'EndDate') ||
									(array_keys($newArray)[$loop] == 'StartTime') ||
									(array_keys($newArray)[$loop] == 'EndTime') ||
									(array_keys($newArray)[$loop] == 'CreateDate') || 
									(array_keys($newArray)[$loop] == 'UpdateDate') 
								){
									//echo "masukdate";
									$value[$loop] = date("Y-m-d H:i:s",strtotime(array_values($newArray)[$loop]));
								}else{
									$value[$loop] = array_values($newArray)[$loop];
								}
							}
							
							//print_r($value);
						}
						
						
						
					
						$sql = "insert into ".$temtable." (".implode(",",array_keys($newArray)).") values ('".implode("','",$value)."')";
						//echo $sql;
						//exit;
						mysqli_query($conpos,$sql);			
					}
					//print_r($result[0]->$key);
					//echo  "Sukses Update :: </span>";
				}
			}
				echo "</pre>";
			//echo $sql."<br />";
		  //  print_r($result[0]);
			
		   
		echo "<script>					
				Swal.fire({
				  title: 'SUCCESS!',
				  text: 'Proses Update Pembayaran Berhasil',
				  icon: 'success',
				  confirmButtonText: 'CLOSE'
				}).then((result) => {
				  if (result.isConfirmed) {
					$(document).ready(function() {
						location.href='master_pembayaran.php';
					} );
				  }
				});
				//alert('Start Off Day Berhasil');
			</script>";
	}else{
		echo "<script>					
				Swal.fire({
				  title: 'GAGAL!',
				  text: 'Proses Update Pembayaran GAGAL',
				  icon: 'error',
				  confirmButtonText: 'CLOSE'
				}).then((result) => {
				  if (result.isConfirmed) {
				//	location.href='master_pembayaran.php';
				  }
				});
				//alert('Start Off Day Berhasil');
			</script>";
	}
	
	
	
?>
