<?php 
	
	session_start();
	date_default_timezone_set("Asia/Bangkok");
	include("dbconnect.php");
	include("template/header_h2h.php");
	//mysqli_query($conpos,"DELETE FROM ztemp_products");
	
?>
	<link href="css/jquery.dataTables.min.css" rel="stylesheet">
	<div class="page-wrapper">
		<!-- ============================================================== -->
		<!-- Bread crumb and right sidebar toggle -->
		<!-- ============================================================== -->
		<div class="page-breadcrumb">
			<div class="row align-items-center">
					
				<div class="card col-xs-12 col-md-12 col-sm-12" >
					<!-- Nav tabs -->
					<ul class="nav nav-tabs profile-tab" role="tablist">
						<li class="nav-item"> <a class="nav-link <?php if($_GET['active'] == 'master_product') { echo "active";}?>" data-toggle="tab" href="#master_product"
								role="tab">Master Product</a>
						</li>
						<li class="nav-item"> <a class="nav-link <?php if($_GET['active'] == 'online_harga') { echo "active";}?>" data-toggle="tab" href="#online_harga"
								role="tab">Online Harga</a> </li>
						<li class="nav-item"> <a class="nav-link <?php if($_GET['active'] == 'online_plu') { echo "active";}?>" data-toggle="tab" href="#online_plu"
								role="tab">Online PLU</a>
						</li>
						<li class="nav-item"> <a class="nav-link <?php if($_GET['active'] == 'online_plu_type') { echo "active";}?>" data-toggle="tab" href="#online_plu_type"
								role="tab">Online PLU TYPE</a>
						</li>
					</ul>
					<div class="tab-content">
						<div class="tab-pane <?php if($_GET['active'] == 'master_product') { echo "active";}?>" id="master_product" role="tabpanel">
							<div class="row align-items-center">
								<table width="100%" class="table table-striped table-bordered">
									<tr>
										<th>
											<a href="master_product_detail.php?act=add&pr=product" class="btn btn-success">Add Product</a>
											<a href="master_product_updharga.php?act=upd&pr=product" class="btn btn-danger">Update Harga</a>
											<a href="master_product_print.php?act=add&pr=product&halaman=1" class="btn btn-success" TARGET="BLANK">Print Product</a>
                                            <a href="master_product_updmayo.php?act=upd&pr=product" class="btn btn-warning">Update Mayonaise</a>
										</th>
									</tr>
								</table>
								<div class="col-xs-12 col-md-12 col-sm-12" id="information" >
									<div class="card">
										<div class="card-body">
											<div class="card-title">Find By Name and PLU</div>
										</div>
										<div>
											<hr class="m-t-0 m-b-0">
										</div>
										<div class="card-body">
									
											<form class="form-horizontal form-material" method="POST" action="master_product.php?act=find_name&pr=product&active=master_product">
												<div class="row">
													<div class="form-group col-md-2">
														<label>PLU / PRODCODE</label>
													</div>
													<div class="form-group col-md-6">
														<input type="text" name="prodname" 
															class="form-control form-control-line pl-0 character" placeholder="Masukan PLU atau Product Name yang akan di cari">
													</div>
													<div class="form-group col-md-2">
														<input type="submit" value="Cari" class="btn btn-warning col-xs-12 col-md-12 col-sm-12" />
													</div>
													<div class="form-group col-md-2">
														<a href="master_product.php" class="btn btn-danger col-xs-12 col-md-12 col-sm-12">Clear</a>
													</div>
												</div>
											</form>
										</div>
									</div>
								</div>
								 <div class="table-responsive">
									<table class="table user-table" id="table_id">
										<thead>
											<tr>
												<th>PLU</th>
												<th>Product Name</th>
												<th>Active</th>
												<th>Action</th>
											</tr>
										</thead>
										<tbody>
									<?php
										if($_GET['act'] != 'find_name'){
											$get_prod 	= mysqli_query($conpos,"select * from products");
										}else{
											//echo "select * from products where PRODCODE like '%".$_POST['promoname']."%' or PRODNAME like '%".$_POST['promoname']."%' order by ENT_DATE DESC";
											$get_prod 	= mysqli_query($conpos,"select * from products where PRODCODE like '%".$_POST['prodname']."%' or PRODNAME like '%".$_POST['prodname']."%' order by STATUS DESC");
										}
										
										while($dataProd	= mysqli_fetch_object($get_prod)){
											if($dataProd->STATUS == 1){
												$checked = "ACTIVE";
												$act	 = "disable";
											}else{
												$checked = "NOT ACTIVE";
												$act	 = "enable";
											}
									?>
											<tr>
												<td><?php echo $dataProd->PRODCODE; ?></td>
												<td><?php echo $dataProd->PRODNAME; ?></td>
												<td><?php echo $checked; ?></td>
												<td>
													<a href="master_product_detail.php?id=<?php echo $dataProd->PRODCODE; ?>&pr=product&act=edit" class="btn btn-primary">edit</a>
													<a href="act_master_product.php?act=<?php echo $act?>" class="btn btn-danger"><?php echo $act?></a>
												</td>
												<!--
												<td>
													<input type="checkbox" id="vehicle3" name="status[]" value="<?php echo $dataProd->STATUS ?>" <?php echo $checked ?>>
												</td>
												-->
											</tr>
									<?php
										}
									?>
										</tbody>
									</table>
								</div>
							</div>
						</div>
						<div class="tab-pane <?php if($_GET['active'] == 'online_harga') { echo "active";}?>" id="online_harga" role="tabpanel">
							<div class="row align-items-center">
								<script> 
								   function clickAndDisable(link) {
									 // disable subsequent clicks
									 link.onclick = function(event) {
										event.preventDefault();
									 }
								   }   
								</script>
								<table width="100%" class="table table-striped table-bordered">
									<tr>
										<th>
											<a href="master_product_online_harga.php?act=get_online_harga&pr=product&proc=clear" class="btn btn-danger">CLEAR DATA</a>
											<a href="master_product_online_harga.php?act=get_online_harga&pr=product&proc=getdata" class="btn btn-success" onclick="clickAndDisable(this);">GET DATA</a>
										</th>
									</tr>
									<tr>
										<td>
											<div class="col-xs-12 col-md-12 col-sm-12" id="information" >
												<div class="card">
													<div class="card-body">
														<div class="card-title">Find By Name and PLU</div>
													</div>
													<div>
														<hr class="m-t-0 m-b-0">
													</div>
													<div class="card-body">												
														<form class="form-horizontal form-material" method="POST" action="master_product.php?act=find_harga&pr=product&active=online_harga">
															<div class="row">
																<div class="form-group col-md-2">
																	<label>PLU / PRODCODE</label>
																</div>
																<div class="form-group col-md-6">
																	<input type="text" name="prodname_find_harga" 
																		class="form-control form-control-line pl-0 character" placeholder="Masukan PLU atau Product Name yang akan di cari">
																</div>
																<div class="form-group col-md-2">
																	<input type="submit" value="Cari" class="btn btn-warning col-xs-12 col-md-12 col-sm-12" />
																</div>
																<div class="form-group col-md-2">
																	<a href="master_product.php" class="btn btn-danger col-xs-12 col-md-12 col-sm-12">Clear</a>
																</div>
															</div>
														</form>
													</div>
												</div>
											</div>
											<table class="table table-striped">												
											<?php
												$prodcode_temp = '';
												if($_GET['act'] == 'find_harga'){
													$get_master_plu	= mysqli_query($conpos,"Select * from ztemp_products where PRODCODE like '%".$_POST['prodname_find_harga']."%' or PRODNAME like '%".$_POST['prodname_find_harga']."%' order by prodcode ASC");
												}else{
													$get_master_plu	= mysqli_query($conpos,"Select * from ztemp_products order by prodcode ASC");
												}
												while($data	= mysqli_fetch_object($get_master_plu)){
													//print_r($data);
													$filter = mysqli_query($conpos,"select * from products_dtl where PRODCODE = '".$data->PRODCODE."' and TRX_CODE = '".$data->TRX_CODE."' and UNIT_PRICE != '".$data->UNIT_PRICE."'");	
													if(mysqli_num_rows($filter) > 0){
														if($prodcode_temp != $data->PRODCODE){
												?>
															<tr style="background:#000;color:#fff;">
																<th><?php echo $data->PRODCODE ?></th>
																<th ><?php echo $data->PRODNAME ?></th>
																<th ><a href="master_product_online_harga_upd.php?prodcode=<?php echo $data->PRODCODE ?>" class="btn btn-danger">UPDATE</a></th>
															</tr>
															<tr>
																<th>TYPE</th>
																<th>BEFORE PRICE</th>
																<th>UPDATE PRICE</th>
															</tr>
												<?php
														}
														$prodcode_temp = $data->PRODCODE;													
															
															$data_filter = mysqli_fetch_object($filter);
															
												?>
															<tr>
																<th><?php echo $data_filter->TRX_CODE ?></th>
																<th><?php echo number_format($data_filter->UNIT_PRICE) ?></th>
																<th><?php echo number_format($data->UNIT_PRICE) ?></th>
															</tr>
												<?php
													}
												}
											?>
											</table>
										</td>
									</tr>
								</table>
							</div>
							
						</div>
						<div class="tab-pane <?php if($_GET['active'] == 'online_plu') { echo "active";}?>" id="online_plu" role="tabpanel">
							<div class="row align-items-center">
								<table width="100%" class="table table-striped table-bordered">
									<tr>
										<th>
											<a href="master_product_online_plu.php?act=get_online_plu&pr=product&proc=clear" class="btn btn-danger">CLEAR DATA</a>
											<a href="master_product_online_plu.php?act=get_online_plu&pr=product&proc=getdata" class="btn btn-success">GET DATA</a>
											<a href="master_product_updharga_all.php?act=get_online_plu&pr=product&proc=getdata" class="btn btn-success">UPDATE ALL PRODUCTS</a>
										</th>
									</tr>
									<tr>
										<td>
											<div class="col-xs-12 col-md-12 col-sm-12" id="information" >
												<div class="card">
													<div class="card-body">
														<div class="card-title">Find By Name and PLU</div>
													</div>
													<div>
														<hr class="m-t-0 m-b-0">
													</div>
													<div class="card-body">												
														<form class="form-horizontal form-material" method="POST" action="master_product.php?act=find_plu&pr=product&active=online_plu">
															<div class="row">
																<div class="form-group col-md-2">
																	<label>PLU / PRODCODE</label>
																</div>
																<div class="form-group col-md-6">
																	<input type="text" name="prodname_find_plu" 
																		class="form-control form-control-line pl-0 character" placeholder="Masukan PLU atau Product Name yang akan di cari">
																</div>
																<div class="form-group col-md-2">
																	<input type="submit" value="Cari" class="btn btn-warning col-xs-12 col-md-12 col-sm-12" />
																</div>
																<div class="form-group col-md-2">
																	<a href="master_product.php" class="btn btn-danger col-xs-12 col-md-12 col-sm-12">Clear</a>
																</div>
															</div>
														</form>
													</div>
												</div>
											</div>
											<table class="table table-striped">												
											<?php
												$prodcode_temp = '';
												if($_GET['act'] == 'find_plu'){
													$get_master_plu	= mysqli_query($conpos,"Select * from ztemp_products where PRODCODE like '%".$_POST['prodname_find_plu']."%' or PRODNAME like '%".$_POST['prodname_find_plu']."%' order by prodcode ASC");
												}else{
													$get_master_plu	= mysqli_query($conpos,"Select * from ztemp_products order by prodcode ASC");
												}
												while($data	= mysqli_fetch_object($get_master_plu)){
													//print_r($data);
													$filter = mysqli_query($conpos,"select * from products where PRODCODE = '".$data->PRODCODE."'");	
													if(mysqli_num_rows($filter) == 0){
														if($prodcode_temp != $data->PRODCODE){
												?>
															<tr style="background:#000;color:#fff;">
																<th><?php echo $data->PRODCODE ?></th>
																<th ><?php echo $data->PRODNAME ?></th>
																<th ><a href="master_product_online_plu_upd.php?prodcode=<?php echo $data->PRODCODE ?>" class="btn btn-danger">ADD</a></th>
															</tr>
												<?php
														}
														$prodcode_temp = $data->PRODCODE;													
															
															$data_filter = mysqli_fetch_object($filter);
															
												?>
															<tr>
																<th><?php echo $data->TRX_CODE ?></th>
																<th><?php echo number_format($data->UNIT_PRICE) ?></th>
															</tr>
												<?php
													}
												}
											?>
											</table>
										</td>
									</tr>
								</table>
							</div>
						</div>
						<div class="tab-pane <?php if($_GET['active'] == 'online_plu_type') { echo "active";}?>" id="online_plu_type" role="tabpanel">
							<div class="row align-items-center">
								<table width="100%" class="table table-striped table-bordered">
									<tr>
										<th>
											<a href="master_product_online_plu_type.php?act=get_online_plu_type&pr=product&proc=clear" class="btn btn-danger">CLEAR DATA</a>
											<a href="master_product_online_plu_type.php?act=get_online_plu_type&pr=product&proc=getdata" class="btn btn-success">GET DATA</a>
											<a href="master_product_online_plu_type_upd.php?act=get_online_plu_type&pr=product&proc=getdata" class="btn btn-success">UPDATE ALL</a>
										</th>
									</tr>
									<tr>
										<td>
											<div class="col-xs-12 col-md-12 col-sm-12" id="information" >
												<div class="card">
													<div class="card-body">
														<div class="card-title">Find By Name and PLU</div>
													</div>
													<div>
														<hr class="m-t-0 m-b-0">
													</div>
													<div class="card-body">												
														<form class="form-horizontal form-material" method="POST" action="master_product.php?act=find_plu&pr=product&active=online_plu_type">
															<div class="row">
																<div class="form-group col-md-2">
																	<label>PLU / PRODCODE</label>
																</div>
																<div class="form-group col-md-6">
																	<input type="text" name="find_plu_type" 
																		class="form-control form-control-line pl-0 character" placeholder="Masukan PLU atau Product Name yang akan di cari">
																</div>
																<div class="form-group col-md-2">
																	<input type="submit" value="Cari" class="btn btn-warning col-xs-12 col-md-12 col-sm-12" />
																</div>
																<div class="form-group col-md-2">
																	<a href="master_product.php" class="btn btn-danger col-xs-12 col-md-12 col-sm-12">Clear</a>
																</div>
															</div>
														</form>
													</div>
												</div>
											</div>
											<table class="table table-striped">												
											<?php
												$prodcode_temp = '';
												if($_GET['act'] == 'find_plu_type'){
													$get_master_plu	= mysqli_query($conpos,"Select ztemp_products.* from ztemp_products where PRODCODE like '%".$_POST['find_plu_type']."%' or PRODNAME like '%".$_POST['find_plu_type']."%' order by prodcode ASC");
												}else{
													$get_master_plu	= mysqli_query($conpos,"select PRODUCTS_DTL.PRODCODE as PRODCODE_LOCAL,
																	PRODUCTS_DTL.TRX_CODE as TRX_LOCAL,
																	ztemp_products.PRODCODE,ztemp_products.TRX_CODE,ztemp_products.UNIT_PRICE from ztemp_products
																	LEFT JOIN PRODUCTS_DTL on
																	(PRODUCTS_DTL.PRODCODE = ztemp_products.PRODCODE
																	and
																	PRODUCTS_DTL.TRX_CODE = ztemp_products.TRX_CODE) 
																	JOIN PRODUCTS on PRODUCTS.PRODCODE = ztemp_products.PRODCODE 
																	where PRODUCTS_DTL.PRODCODE is null");   
												}
												while($data	= mysqli_fetch_object($get_master_plu)){
													///print_r($data);
												?>	
													<tr style="background:#000;color:#fff;">
														<th><?php echo $data->PRODCODE ?></th>
														<th ><?php echo $data->PRODNAME ?></th>
													</tr>
													<?php
														//getDetail
														$sql_detail_plu_type	= "select PRODUCTS_DTL.PRODCODE as PRODCODE_LOCAL,
																	PRODUCTS_DTL.TRX_CODE as TRX_LOCAL,
																	ztemp_products.PRODCODE,ztemp_products.TRX_CODE,ztemp_products.UNIT_PRICE from ztemp_products
																	LEFT JOIN PRODUCTS_DTL on
																	(PRODUCTS_DTL.PRODCODE = ztemp_products.PRODCODE
																	and
																	PRODUCTS_DTL.TRX_CODE = ztemp_products.TRX_CODE)
																	WHERE  ztemp_products.PRODCODE = '".$data->PRODCODE."'
																	and PRODUCTS_DTL.PRODCODE is null ";
														$qry_detail_plu_type = mysqli_query($conpos,$sql_detail_plu_type);
														while($data_detail_plu_type	= mysqli_fetch_object($qry_detail_plu_type)){
														
													?>
															<tr>
																<th><?php echo $data_detail_plu_type->TRX_CODE ?></th>
																<th><?php echo number_format($data_detail_plu_type->UNIT_PRICE) ?></th>
															</tr>
												<?php
														}
												}
											?>
											</table>
										</td>
									</tr>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php include("template/footer_h2h.php");?>
	<script src="<?php echo  $base_url ?>js/jquery.dataTables.min.js" type="text/javascript"></script>
	<script>
	$.extend( true, $.fn.dataTable.defaults, {
		"searching": false
	} );
	$(document).ready(function() {
		$('#table_id').DataTable();
	} );
</script>
<script src="Keyboard/js/jquery.keyboard.js"></script>
<!-- keyboard extensions (optional) -->
<script src="Keyboard/js/jquery.mousewheel.js"></script>
<script>
	$(function(){
		$('.numeric').keyboard({layout : 'num' });
		$('.character').keyboard({layout: 'qwerty'});
	});
</script>