<?php 
	session_start();
	date_default_timezone_set("Asia/Bangkok");
	include("dbconnect.php");
	include("template/header_h2h.php");	
?>
	<div class="page-wrapper" >
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="page-breadcrumb">
                <div class="align-items-center">
				<div class="row">
					<div class=" col-lg-12">
						<div class="row">
							<div class="col-lg-12 col-xlg-12 col-md-12 alert alert-warning" >
								<?php
								echo $base_url."database";
									$html = file_get_contents($base_url."database");
									$count = preg_match_all('/<li><a href="([^"]+)">[^<]*<\/a><\/li>/i', $html, $files);
									$file	= array();
									for ($i = 0; $i < $count; ++$i) {
										$file[] = $files[1][$i];
									}
									sort($file);
									
									$value = "/";
									if (($key = array_search($value, $file)) !== false) {
										unset($file[$key]);
									}
									echo "<pre>";
									print_r($file);
									echo "</pre>";
								?>
							</div>
						</div>
						
						
					</div>
				</div>
				</div>
			</div>
<?php include("template/footer_h2h.php");?>
<script src="Keyboard/js/jquery.keyboard.js"></script>
<!-- keyboard extensions (optional) -->
<script src="Keyboard/js/jquery.mousewheel.js"></script>
<script>
	$(function(){
		$('.numeric').keyboard({layout : 'num' });
		$('.character').keyboard({layout: 'qwerty'});
	});
</script>
<script src="js/jquery.canvasjs.min.js"></script>
<?php
	//data transaksi 7 hari sebelum hari ini	
	$qry_lst_branch 	= mysqli_query($conpos,"SELECT DATE_FORMAT(trx_date, '%d-%m-%Y') as label,COUNT(*) AS y FROM sales_hdr WHERE trx_date <= DATE_SUB(CURDATE(), INTERVAL 7 DAY)
	GROUP BY trx_date ORDER BY trx_date ASC ");
	while($data_lst_branch	= mysqli_fetch_object($qry_lst_branch)){
		$data_transaksi[]	= $data_lst_branch;
	}
	
	//data transaksi go food 7 hari sebelum hari ini	
	$qry_lst_delivery_gf 	= mysqli_query($conpos,"SELECT DATE_FORMAT(trx_date, '%d-%m-%Y') as label,COUNT(*) AS y FROM sales_hdr WHERE trx_date <= DATE_SUB(CURDATE(), INTERVAL 7 DAY) and DELIVERY_ID = 1
	GROUP BY trx_date ORDER BY trx_date desc");
	while($data_lst_delivery_gf	= mysqli_fetch_object($qry_lst_delivery_gf)){
		$data_delivery_gf[]	= $data_lst_delivery_gf;
	}	
	
	//data transaksi grab food 7 hari sebelum hari ini	
	$qry_lst_delivery_g 	= mysqli_query($conpos,"SELECT DATE_FORMAT(trx_date, '%d-%m-%Y') as label,COUNT(*) AS y FROM sales_hdr WHERE trx_date <= DATE_SUB(CURDATE(), INTERVAL 7 DAY) and DELIVERY_ID = 2
	GROUP BY trx_date ORDER BY trx_date desc ");
	while($data_lst_delivery_g	= mysqli_fetch_object($qry_lst_delivery_g)){
		$data_delivery_g[]	= $data_lst_delivery_g;
	}	
?>
<script>
$(function() {	
	var options = {
		title: {
			text: "Transaksi Delivery < 7 hari"              
		},
		axisX: {
			title: "Tanggal Transaksi"
		},
		axisY: {
			title: "Total Transaksi"
		},
		data: [              
		{
			// Change type to "doughnut", "line", "splineArea", etc.
			type: "line",
			name: "Go Food",
			showInLegend: true,
			indexLabel: "{y}",
			yValueFormatString: "#,## Transaksi",
			dataPoints: <?php echo json_encode($data_delivery_gf, JSON_NUMERIC_CHECK); ?>
		},
		{
			type: "line",
			name: "Grab Food",
			showInLegend: true,
			indexLabel: "{y}",
			yValueFormatString: "#,## Transaksi",
			dataPoints: <?php echo json_encode($data_delivery_g, JSON_NUMERIC_CHECK); ?>
		}
		]
	};
	$("#chart_delivery").CanvasJSChart(options);
	
	var options = {
		title: {
			text: "Sales < 7 hari"              
		},
		axisX: {
			title: "Tanggal Transaksi"
		},
		axisY: {
			title: "Total Transaksi"
		},
		data: [              
		{
			// Change type to "doughnut", "line", "splineArea", etc.
			type: "column",
			indexLabel: "{y}",
			dataPoints: <?php echo json_encode($data_transaksi, JSON_NUMERIC_CHECK); ?>
		}
		]
	};
	$("#chart_sales").CanvasJSChart(options);
});
</script>
