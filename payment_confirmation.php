<style>
	h3 {
		line-height : 0px;
		font-size:4;
	}
	#conf-order {
		font-size:14px;
	}
	#conf-order input{
		height:25px;
	}
	#conf-order button{
		height:25px;
	}
	
	/* On screens that are 600px or less, set the background color to olive */
	@media screen and (max-width: 600px) {
		#conf-order {
			font-size:12px;
			font-weight:bold;
		}
		#conf-order input{
			height:18px;
		}
		#conf-order button{
			height:20px;
		}
	}
</style>
<div class="col-xs-12 col-md-12 col-sm-12" id="conf-order"> 	
	<div class="row">		
		<div class="col-lg-4 col-xs-4">
			<!-- <form class="form-inline"> -->
				<div class="form-group">
					<label for="email">ID DP</label>
					<input type="text" style="width:100%" id="dp_val_use" readonly />
				</div>	
				<div class="form-group">
					<label for="email">
						<button class="btn btn-primary" onclick="btn_ref_void()">Ref. Void</button>
					</label>
					<input type="text" style="width:100%" id="id_ref_void"  readonly />
				</div>	
				<div class="form-group">
					<label for="email">
						<button class="btn btn-primary" onclick="validDiscLogin()">Reset Disc</button>
					</label>
				</div>
			<!-- </form> -->
		</div>
		<div class="col-lg-8 col-xs-8">
			<div class="row">		
				<div class="row">
					<div class="col-xs-12 col-md-12 col-sm-12 row"> 
						<div class="col-xs-6 col-md-6 col-sm-6">Sub Total</div>
						<div class="col-xs-6 col-md-6 col-sm-6 align-right"> : <span id="totalpay">0</span></div>
					</div>
					<div class="col-xs-12 col-md-12 col-sm-12 row"> 
						<div class="col-xs-6 col-md-6 col-sm-6">Discount</div>
						<div class="col-xs-6 col-md-6 col-sm-6 align-right" style="border-bottom:1px solid #000;"> : <span id="amountdisc_vw">0</span></div>
						<input type="hidden" id="val_discmax" value="0"/>
					</div>
					<div class="col-xs-12 col-md-12 col-sm-12 row">
						<div class="col-xs-6 col-md-6 col-sm-6"></div>
						<div class="col-xs-6 col-md-6 col-sm-6 align-right" style="background:#000;color:#fff;"> :
						<span id="amounttotal_disc">  0</span></div>
					</div>
					<div class="col-xs-12 col-md-12 col-sm-12 row"> 
						<div class="col-xs-6 col-md-6 col-sm-6">SVC Charge</div>
						<div class="col-xs-6 col-md-6 col-sm-6 align-right"> : <span id="servicechargeval">0</span></div>
					</div>
					<div class="col-xs-12 col-md-12 col-sm-12 row">
						<div class="col-xs-6 col-md-6 col-sm-6 align-right">TA Charge</div>
						<div class="col-xs-6 col-md-6 col-sm-6"> : <span id="tacharge">0</span></div>	
					</div>
					<div class="col-xs-12 col-md-12 col-sm-12 row"> 
						<div class="col-xs-6 col-md-6 col-sm-6">PJK Resto</div>
						<div class="col-xs-6 col-md-6 col-sm-6 align-right"> : <span id="taxresto">0</span></div>
					</div>
					
					
					<div class="col-xs-12 col-md-12 col-sm-12 row"> 
						<div class="col-xs-6 col-md-6 col-sm-6">Pembulatan</div>
						<div class="col-xs-6 col-md-6 col-sm-6 align-right"> : <span id="pembulatanval">0</span></div>
					</div>
					<div class="col-xs-12 col-md-12 col-sm-12 row"> 
						<div class="col-xs-6 col-md-6 col-sm-6">Total</div>
						<div class="col-xs-6 col-md-6 col-sm-6 align-right" style="background:#000;color:#fff;"> : <span id="subtotal">0</span></div>
					</div>
				</div>				
			</div>
		</div>
	</div>
</div>	
