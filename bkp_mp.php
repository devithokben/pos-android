<?php
	
	
	require 'dbconnect.php';
	date_default_timezone_set("Asia/Jakarta");
	error_reporting(0);
	ini_set('display_errors', 1);
	if($_GET['keyresponse'] == 1){
		$_SESSION['keyresponse'] = 1;
	}
	$get_eod 	= mysqli_query($conpos,"select * from trx_ctl where TRX_STATUS = '0'");
	$dataeod	= mysqli_fetch_object($get_eod);
	if(!isset($dataeod)){
	//	print_r($_SESSION);exit;
		
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="refresh" content="18000">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>Untitled Document</title>
		<link href="css/style-pos.css" rel="stylesheet" type="text/css">
		<link href="css/bootstrap.css" rel="stylesheet" type="text/css">
		<link href="<?php echo  $base_url ?>sweetalert2/dist/sweetalert2.min.css" rel="stylesheet">
		<link href="<?php echo  $base_url ?>css/jquery-ui.css" rel="stylesheet">
		
		<script src="js/jquery.js" type="text/javascript"></script>
		<script src="js/bootstrap.js" type="text/javascript"></script>
		<script>
			var strukid 		= "<?php echo $dataTrx->POS_ID.".".$dataTrx->TRX_ID."."?>";
			var min_order		= "<?php echo $mindlamount ?>";
			var default_type	= "<?php echo $default_type ?>";
			var base_url		= "<?php echo $base_url ?>";
			var screenWidth		= screen.width;
			if(screenWidth > 600){
				location.href = "system_pos.php";
			}
		</script>
		
		<script src="js/layanan.js" type="text/javascript"></script>
		<script src="js/pembayaran.js" type="text/javascript"></script>
		<script src="js/posting_menu.js" type="text/javascript"></script>
		<script src="js/posting_discount.js" type="text/javascript"></script>
		<script src="js/posting_promo.js" type="text/javascript"></script>
		<script src="js/function.js" type="text/javascript"></script>
		<script src="js/proc_dp.js" type="text/javascript"></script>
		<script src="<?php echo  $base_url ?>sweetalert2/dist/sweetalert2.all.min.js" type="text/javascript"></script>				
		<script src="<?php echo  $base_url ?>js/jquery-ui.js"></script>
		<style>
		*:fullscreen
			*:-ms-fullscreen,
			*:-webkit-full-screen,
			*:-moz-full-screen {
			   overflow: auto !important;
			   background:#fff;
			}
			body {
			   background:#fff;
			  color: #000; 
			  font-size:12px;}

			html:-webkit-full-screen-ancestor {
			  background-color: #fff; }

			html:-moz-full-screen-ancestor {
			  background-color: #fff; }
			  .nav-tabs > li.active > a, .nav-tabs > li.active > a:hover, .nav-tabs > li.active > a:focus{
				  background:red;color:#fff;
			  }
			  #content-isi {
				  display:block;
			  }
				
			html {
				background-color: #ffffff;
				/* Or any color / image you want */
			}
			.btn {
				font-size:14px;
			}
			#swal2-content{
				font-size:15px;
			}
			.swal2-styled{
				font-size:15px;
			}
			#modal_list_tacharge a,
			#load_list_layanan a{
				font-size:15px;
			}
			
			
			
		</style>
		
		<!-- keyboard widget css & script (required) -->
		<link href="Keyboard/css/keyboard.css" rel="stylesheet">
		<script src="Keyboard/js/jquery.keyboard.js"></script>

		<!-- keyboard extensions (optional) -->
		<script src="Keyboard/js/jquery.mousewheel.js"></script>
		<script>
			$(function(){
				$('.numeric').keyboard({layout : 'num' });
				$('.character').keyboard({layout: 'qwerty'});
			});
		</script>
		
	</head>
	<body>
	
	<?php 

include "modal/confirm_entry.php";
error_reporting(0);

	if($_GET['act'] == 'err_print'){
		echo "<SCRIPT>
		Swal.fire({
		  title: 'Error!',
		  text: 'Start Of Day Belum Dilakukan',
		  icon: 'error',
		  confirmButtonText: 'CLOSE'
		}).then((result) => {
			if (result.isConfirmed) {
				location.href='index.php';
			}
		});
		</SCRIPT>";
	}

	$getId 		= mysqli_query($conpos,"select * from trx_ctl where trx_status = 0");
	$dataTrx	= mysqli_fetch_object($getId);

	$newcounter = $dataTrx->COUNTER_NO + 1;
	//echo exit;
	
	//get date now
	$datenow	= date('Y-m-d');
	$datepos	= date('Y-m-d',strtotime($dataTrx->TRX_DATE));
	
	if($_GET['status'] != 1){
		if($datenow != $datepos){
			if(empty($_SESSION['keyresponse'])){
				echo '	
					<script>
					Swal.fire({
					  title: "CONFIRM!",
					  text: "Tanggal Berjalan dengan tanggal start of day tidak sama / Lakukan End Of Day ! \n Apakah Transaksi akan di lanjutkan ?",
					  icon: "question",
					  type: "warning",
					  showCancelButton: true,
					  confirmButtonText: "YES",
					  cancelButtonText: "NO"
					}).then((result) => {
						if (result.isConfirmed) {
							var modalsession = document.getElementById("confirm_entryModal");
							modalsession.style.display = "block";
						}else{
							location.href="index.php";
						}
					});
					</script>';
			}
		}
	}
	
	
?>
	<input type="hidden" id="tgl_berjalan" value="<?php echo $_SESSION['keyresponse']?>" />
	
		<div class="fluid-container" style="background: #fff;">
			<div class="">
				<div class="col-xs-12 col-md-12 col-sm-12"> 
				<?php
				if(!$dataTrx){
					if($_SESSION['ROLE_ID'] == '01'){
						echo "<script>
							Swal.fire({
							  title: 'Error!',
							  text: 'Start Of Day Belum Dilakukan',
							  icon: 'error',
							  confirmButtonText: 'CLOSE'
							}).then((result) => {
								if (result.isConfirmed) {
									location.href='index.php';
								}
							});
							</script>";
							
					}else{
						echo "
							<script>
							Swal.fire({
							  title: 'Error!',
							  text: 'Start Of Day Belum Dilakukan',
							  icon: 'error',
							  confirmButtonText: 'CLOSE'
							}).then((result) => {
								if (result.isConfirmed) {
									location.href='dashboard.php';
								}
							});
							</script>";
							
						
					}
					exit;
				}
				require 'cek_kasir.php';

				?>
				</div>
				<div class="col-xs-12 col-md-12 col-sm-12"> 
					<div class="row">
						<div class="col-xs-12 col-md-6 col-sm-6"> 
							<div class="row">
								<div class="col-xs-12 col-md-12 col-sm-12">
									<div class="row">
										<div class="col-lg-10 col-xs-10">
											<div class="row">
												<input type="hidden" id="seq_no" value="1" />
												<?php include "lstmenu_left.php";?>												
											</div>
										</div>
										<div class="col-lg-2 col-xs-2" 
										style="
												font-size:12px;
												font-weight:bold;
												padding-top:10px;
												padding-bottom:10px;">
											<div class="row">
												<?php include "btn_lstmenu_right.php";?>												
											</div>
										</div>
										<div class="col-xs-12 col-md-12 col-sm-12">
											<div class="row">
												<?php include "payment_confirmation.php"; ?>
											</div>
										</div>
										
										<div class="col-xs-12 col-md-12 col-sm-12">
											<div class="row">
												<div class="col-lg-12 col-xs-12"> 
													<hr />
													<div class="row">
														<div id="result" class="col-xs-6 col-md-6 col-sm-6" style="font-size:17px;font-weight:bold;background:#efefef;">&nbsp;</div>
														<div id="layanan" class="col-xs-2 col-md-2 col-sm-2" style="font-size:17px;font-weight:bold;background:#000;color:#fff;"></div>															
														<div id="servicecharge" class="btn btn-primary" style="display:none;"></div>
														<div id="salesDisc" class="btn btn-success" style="display:none;"></div>
														<input id="unik_dlcharge" type="hidden" style="font-size:12px;">
														<button class="col-xs-4 col-md-4 col-sm-4" onclick="pilih_layanan()" style="font-size:12px;font-weight:bold;" >
															Pilih Layanan
														</button>
													</div>
												</div>
											</div>
										</div>
										
										<div class="col-xs-12 col-md-12 col-sm-12">
											<div class="row">
												<div class="col-xs-3 col-md-3 col-sm-3">
													<div class="row">
														<?php include "btn_keypad_leftside.php";?>
													</div>
												</div>												
												<div class="col-xs-6 col-md-6 col-sm-6">
													<div class="row">
														<?php include "keypad.php";?>
													</div>
												</div>
												<div class="col-xs-3 col-md-3 col-sm-3">
													<div class="row">
														<?php include "btn_keypad_right.php";?>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								
							</div>
						</div>
						<div class="col-xs-12 col-md-6 col-sm-6"> 
							<div class="row">
								<div class="col-lg-12 col-xlg-12 col-md-12 row" style="height:80px">
									<div id="user-content">
										<?php 
										$cat = mysqli_query($conthr, "SELECT * FROM tblgmnu");
										while($ct = mysqli_fetch_object($cat)){
										?>
										<a class=" col-lg-3 col-xs-3" onclick="catproduct('<?=$ct->ID?>')" style="background:#333; font-size:15px; line-height:50px; font-family:Tahoma, Geneva, sans-serif;font-weight:bold; margin:0px;border : 2px solid #fff;color:#fff;">
											<?=$ct->DESCRIPTION?>
										</a>
										<?php }?>
										
									</div>
								</div>
								<div class="col-lg-12 col-xlg-12 col-md-12 row" style="height:600px">
									<span id="rtvmenu">
										<div style="margin:20% auto; width:100px;">
											<img src="loading.gif" width="100px">
										</div>
									</span>
								</div>
								
							</div>
						</div>
					</div>					
				</div>
			</div>
				
		</div>
		
		<?php include "modal/bayar.php";?>
		<?php include "modal/item_disc.php";?>
		<?php include "modal/service_charge.php";?>
		<?php include "modal/upgrade.php";?>
		<?php include "modal/confirm_bayar.php";?>
		<?php include "modal/exitpos.php";?>
		<?php include "modal/close_session.php";?>
		<?php include "modal/layanan.php";?>
		<?php include "modal/voidpos.php";?>
		<?php include "modal/release.php";?>
		<?php include "modal/reference_void.php";?>
		<?php include "modal/dp_popup.php";?>
		<style>
			.ui-keyboard{
				background : #eee;
			}
			.ui-keyboard-button{
				height: 4.2em;
				min-width: 4.2em;
				margin: .2em;
				cursor: pointer;
				overflow: hidden;
				line-height: 4.2em;
				-moz-user-focus: ignore;
				
			}
			.ui-keyboard-text{
				font-size:20px;
			}
			.ui-keyboard-preview-wrapper{
				height:30px;
			}
			.ui-keyboard-preview{
				font-size:20px;
			}
		</style>
	</body>
</html>
