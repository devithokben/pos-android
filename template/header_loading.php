<?php 
	error_reporting(0);
	//print_r($_SESSION);
	date_default_timezone_set("Asia/Bangkok");
?>
<!DOCTYPE html>
<html dir="ltr" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
   
    <title>DashBoard</title>
    <link rel="canonical" href="https://www.wrappixel.com/templates/materialpro-lite/" />
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo  $base_url ?>assets/images/favicon.png">
	 <link href="<?php echo  $base_url ?>css/jquery-ui.css" rel="stylesheet">
	<link href="<?php echo  $base_url ?>sweetalert2/dist/sweetalert2.min.css" rel="stylesheet">
   <script src="<?php echo  $base_url ?>sweetalert2/dist/sweetalert2.all.min.js" type="text/javascript"></script>
</head>

<body>
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <div class="lds-ripple">
            <div class="lds-pos"></div>
            <div class="lds-pos"></div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper" data-layout="vertical" data-navbarbg="skin6" data-sidebartype="full"
        data-sidebar-position="absolute" data-header-position="absolute" data-boxed-layout="full">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <header class="topbar" data-navbarbg="skin6">
            <nav class="navbar top-navbar navbar-expand-md navbar-dark">
                <div class="navbar-header" data-logobg="">
                    <ul class="navbar-nav">
                        <!-- ============================================================== -->
                        <!-- User profile and search -->
                        <!-- ============================================================== -->
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle text-muted waves-effect waves-dark" href=""
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img
                                    src="<?php echo $base_url ?>assets_front/img/logocart.png" alt="user" class="profile-pic m-r-10">POS DashBoard</a>
                        </li>
                    </ul>
                </div>
                
            </nav>
        </header>
        <!-- ============================================================== -->
        <!-- End Topbar header -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <aside class="left-sidebar" data-sidebarbg="skin6">
            <!-- Sidebar scroll-->
            <div class="scroll-sidebar">
                <!-- Sidebar navigation-->
                <nav class="sidebar-nav">
                    <ul id="sidebarnav">
                        <!-- User Profile-->
                        <li class="sidebar-item"> 
							<a class="sidebar-link waves-effect waves-dark sidebar-link"
                                href="#" aria-expanded="false">
									<span class="hide-menu">Welcome, <?php echo $_SESSION['username'] ?></span>
							</a>
						</li> 
						<li class="sidebar-item"> 
							<a class="sidebar-link waves-effect waves-dark sidebar-link"
                                href="<?php echo  $base_url ?>dashboard.php" aria-expanded="false">
									<span class="hide-menu">Dashboard</span>
							</a>
						</li>
						<li class="sidebar-item"> 
							<a class="sidebar-link waves-effect waves-dark sidebar-link <?php if($_GET['pr'] == 'pos'){ echo 'active'; } ?>"
                                href="<?php echo  $base_url ?>master_pos.php" aria-expanded="false">
									<span class="hide-menu">Ponit Of Sale (POS)</span>
							</a>
						</li>
						<li class="sidebar-item"> 
							<a class="sidebar-link waves-effect waves-dark sidebar-link <?php if($_GET['pr'] == 'report'){ echo 'active'; } ?>"
                                href="<?php echo  $base_url ?>report.php" aria-expanded="false">
									<span class="hide-menu">Report POS</span>
							</a>
						</li>
						<li class="sidebar-item"> 
							<a class="sidebar-link waves-effect waves-dark sidebar-link <?php if($_GET['pr'] == 'user'){ echo 'active'; } ?>"
                                href="<?php echo  $base_url ?>master_user.php" aria-expanded="false">
									<span class="hide-menu">Master USER</span>
							</a>
						</li>
						<li class="sidebar-item"> 
							<a class="sidebar-link waves-effect waves-dark sidebar-link <?php if($_GET['pr'] == 'product'){ echo 'active'; } ?>"
                                href="<?php echo  $base_url ?>master_product.php" aria-expanded="false">
									<span class="hide-menu">Master Product</span>
							</a>
						</li>
						<li class="sidebar-item"> 
							<a class="sidebar-link waves-effect waves-dark sidebar-link <?php if($_GET['pr'] == 'promo'){ echo 'active'; } ?>"
                                href="<?php echo  $base_url ?>update_promo.php" aria-expanded="false">
									<span class="hide-menu">Master Promo</span>
							</a>
						</li>
						<li class="sidebar-item"> 
							<a class="sidebar-link waves-effect waves-dark sidebar-link <?php if($_GET['pr'] == 'discount'){ echo 'active'; } ?>"
                                href="<?php echo  $base_url ?>update_discount.php" aria-expanded="false">
									<span class="hide-menu">Master Discount</span>
							</a>
						</li>
						<li class="text-center p-20 upgrade-btn">
                            <a href="<?php echo  $base_url ?>index.php"
                                class="btn btn-warning text-white mt-4" target="_blank">LOGOUT</a>
                        </li>
                    </ul>

                </nav>
                <!-- End Sidebar navigation -->
            </div>
            <!-- End Sidebar scroll-->
            <div class="sidebar-footer">
                <div class="row">
                    <div class="col-12 link-wrap">
                       IT HokBen @ 2021 
                    </div>
                </div>
            </div>
        </aside>
        <!-- ============================================================== -->
        <!-- End Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->