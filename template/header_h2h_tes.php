<?php 
	error_reporting(0);
	//print_r($_SESSION);
	date_default_timezone_set("Asia/Bangkok");
	function availableUrl($host, $timeout=5) { 
		$fp = fSockOpen($host, 6969, $errno, $errstr, $timeout); 
		return $fp!=false;
	}
?>
<!DOCTYPE html>
<html dir="ltr" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
   
    <title>DashBoard</title>
    <link rel="canonical" href="https://www.wrappixel.com/templates/materialpro-lite/" />
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo  $base_url ?>assets/images/favicon.png">
    <!-- chartist CSS -->
    <link href="<?php echo  $base_url ?>assets/plugins/chartist-js/dist/chartist.min.css" rel="stylesheet">
    <link href="<?php echo  $base_url ?>assets/plugins/chartist-js/dist/chartist-init.css" rel="stylesheet">
    <link href="<?php echo  $base_url ?>assets/plugins/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.css" rel="stylesheet">
    <!--This page css - Morris CSS -->
    <link href="<?php echo  $base_url ?>assets/plugins/c3-master/c3.min.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="<?php echo  $base_url ?>css/style.min.css" rel="stylesheet">
	 <link href="<?php echo  $base_url ?>css/jquery-ui.css" rel="stylesheet">
	<link href="<?php echo  $base_url ?>sweetalert2/dist/sweetalert2.min.css" rel="stylesheet">
   <script src="<?php echo  $base_url ?>sweetalert2/dist/sweetalert2.all.min.js" type="text/javascript"></script>

   <script>
        // for php demo call
        function ajax_print(url, btn) {
            b = $(btn);
            b.attr('data-old', b.text());
            b.text('wait');
            $.get(url, function (data) {
                window.location.href = data;  // main action
            }).fail(function () {
                alert("ajax error");
            }).always(function () {
                b.text(b.attr('data-old'));
            })
        }
    </script>
   
   <!-- keyboard widget css & script (required) -->
		<link href="Keyboard/css/keyboard.css" rel="stylesheet">
		<!-- keyboard widget css & script (required) -->
		<link href="Keyboard/css/keyboard.css" rel="stylesheet">
		<style>
			@media only screen and (min-width: 600px) {
				.ui-keyboard{
					background : #eee;
				}
				.ui-keyboard-button{
					height: 4em;
					min-width: 4em;
					margin: .2em;
					cursor: pointer;
					overflow: hidden;
					line-height: 4em;
					-moz-user-focus: ignore;
					
				}
				.ui-keyboard-text{
					font-size:20px;
				}
				.ui-keyboard-preview-wrapper{
					height:30px;
				}
				.ui-keyboard-preview{
					font-size:20px;
					height:40px;
				}
				header{
					display:none;
				}
			}
			.left-sidebar{
				padding-top:0px;
			}
			.sidebar-nav ul .sidebar-item .sidebar-link{
				padding:0px;
			}

			.page-wrapper {
				background-image:url('assets/images/back.jpg');
				background-size:100%;
				background-position:bottom;
				background-repeat:no-repeat;
				height:780px;
				overflow:scroll;
			}
			.page-breadcrumb {
				background : none;
			}
			table{
				background-color:#fff;
				}
		</style>
</head>

<body style="">
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <div class="lds-ripple">
            <div class="lds-pos"></div>
            <div class="lds-pos"></div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper" data-layout="vertical" data-navbarbg="skin6" data-sidebartype="full"
        data-sidebar-position="absolute" data-header-position="absolute" data-boxed-layout="full">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
		<header class="topbar btn-danger" >
            <nav class="navbar top-navbar navbar-expand-md navbar-dark btn-danger">
                <div class="navbar-header" data-logobg="">
                    <!-- ============================================================== -->
                    <!-- Logo -->
                    <!-- ============================================================== -->
                    <a class="navbar-brand ms-4" href="index.html">
                        <!--End Logo icon -->
                        <!-- Logo text -->
                        <span class="logo-text" style="color:#fff;font-weight:bold;margin-left:10px;line-height:30px;">
                            <!-- dark Logo text -->
                            <h3>HokBen POS	<h3>
                        </span>
                    </a>
                    <!-- ============================================================== -->
                    <!-- End Logo -->
                    <!-- ============================================================== -->
                    <!-- ============================================================== -->
                    <!-- toggle and nav items -->
                    <!-- ============================================================== -->
                    <a class="nav-toggler waves-effect waves-light text-white d-block d-md-none"
                        href="javascript:void(0)"><i class="ti-menu ti-close"></i></a>
                </div>
                <!-- ============================================================== -->
                <!-- End Logo -->
                <!-- ============================================================== -->
                <div class="navbar-collapse collapse" id="navbarSupportedContent" data-navbarbg="skin5">
                    <ul class="navbar-nav d-lg-none d-md-block ">
                        <li class="nav-item">
                            <a class="nav-toggler nav-link waves-effect waves-light text-white "
                                href="javascript:void(0)"><i class="ti-menu ti-close"></i></a>
                        </li>
                    </ul>
                    <!-- ============================================================== -->
                    <!-- toggle and nav items -->
                    <!-- ============================================================== -->
                    <ul class="navbar-nav me-auto mt-md-0 ">
                        <!-- ============================================================== -->
                        <!-- Search -->
                        <!-- ============================================================== -->

                        <li class="nav-item search-box">
						
                        </li>
                    </ul>

                    <!-- ============================================================== -->
                    <!-- Right side toggle and nav items -->
                    <!-- ============================================================== -->
                    <ul class="navbar-nav">
                        <!-- ============================================================== -->
                        <!-- User profile and search -->
                        <!-- ============================================================== -->
                        <li class="nav-item dropdown">
                           
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
		
		
		
		
        <!-- ============================================================== -->
        <!-- End Topbar header -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <aside class="left-sidebar" data-sidebarbg="skin6">
            <!-- Sidebar scroll-->
            <div class="scroll-sidebar">
                <!-- Sidebar navigation-->
                <nav class="sidebar-nav">
                    <ul id="sidebarnav">
                        <!-- User Profile-->
                        <li class="sidebar-item"> 
							<img class="media-object"  src="<?php echo $base_url ?>/assets/images/logo.png" width="150px" style="
								  display: block;
								  margin-left: auto;
								  margin-right: auto;
								  width: 40%;
								  margin-bottom:20px;
							" />
						</li> 
						<li class="sidebar-item"> 
							<a class="sidebar-link waves-effect waves-dark sidebar-link"
                                href="<?php echo  $base_url ?>dashboard.php" aria-expanded="false">
									<span class="hide-menu">Dashboard</span>
							</a>
						</li>
						<li class="sidebar-item"> 
							<a class="sidebar-link waves-effect waves-dark sidebar-link <?php if($_GET['pr'] == 'pos'){ echo 'active'; } ?>"
                                href="<?php echo  $base_url ?>master_pos.php" aria-expanded="false">
									<span class="hide-menu">Point Of Sale (POS)</span>
							</a>
						</li>
						<li class="sidebar-item"> 
							<a class="sidebar-link waves-effect waves-dark sidebar-link <?php if($_GET['pr'] == 'report'){ echo 'active'; } ?>"
                                href="<?php echo  $base_url ?>report.php" aria-expanded="false">
									<span class="hide-menu">Report POS</span>
							</a>
						</li>
						<li class="sidebar-item"> 
							<a class="sidebar-link waves-effect waves-dark sidebar-link <?php if($_GET['pr'] == 'user'){ echo 'active'; } ?>"
                                href="<?php echo  $base_url ?>master_user.php" aria-expanded="false">
									<span class="hide-menu">Master USER</span>
							</a>
						</li>
						<li class="sidebar-item"> 
							<a class="sidebar-link waves-effect waves-dark sidebar-link <?php if($_GET['pr'] == 'product'){ echo 'active'; } ?>"
                                href="<?php echo  $base_url ?>master_product.php?pr=product&active=master_product" aria-expanded="false">
									<span class="hide-menu">Master Product</span>
							</a>
						</li>
						<li class="sidebar-item"> 
							<a class="sidebar-link waves-effect waves-dark sidebar-link <?php if($_GET['pr'] == 'promo'){ echo 'active'; } ?>"
                                href="<?php echo  $base_url ?>update_promo.php" aria-expanded="false">
									<span class="hide-menu">Master Promo</span>
							</a>
						</li>
						<li class="sidebar-item"> 
							<a class="sidebar-link waves-effect waves-dark sidebar-link <?php if($_GET['pr'] == 'discount'){ echo 'active'; } ?>"
                                href="<?php echo  $base_url ?>update_discount.php" aria-expanded="false">
									<span class="hide-menu">Master Discount</span>
							</a>
						</li>
						<li class="sidebar-item"> 
							<a class="sidebar-link waves-effect waves-dark sidebar-link <?php if($_GET['pr'] == 'pembayaran'){ echo 'active'; } ?>"
                                href="<?php echo  $base_url ?>master_pembayaran.php" aria-expanded="false">
									<span class="hide-menu">Master Pembayaran</span>
							</a>
						</li>
						<li class="sidebar-item"> 
							<a class="sidebar-link waves-effect waves-dark sidebar-link <?php if($_GET['pr'] == 'config'){ echo 'active'; } ?>"
                                href="<?php echo  $base_url ?>master_config.php" aria-expanded="false">
									<span class="hide-menu">POS Configuration</span>
							</a>
						</li>
						<li class="sidebar-item"> 
							<a class="sidebar-link waves-effect waves-dark sidebar-link <?php if($_GET['pr'] == 'setup'){ echo 'active'; } ?>"
                                href="<?php echo  $base_url ?>setup_app.php" aria-expanded="false">
									<span class="hide-menu">Setup Application</span>
							</a>
						</li>
						<li class="sidebar-item"> 
							<a class="sidebar-link waves-effect waves-dark sidebar-link <?php if($_GET['pr'] == 'transpos'){ echo 'active'; } ?>"
                                href="<?php echo  $base_url ?>transfer_pos.php" aria-expanded="false">
									<span class="hide-menu">Transfer POS</span>
							</a>
						</li>
						<!--
						<li class="sidebar-item"> 
							<a class="sidebar-link waves-effect waves-dark sidebar-link <?php if($_GET['pr'] == 'transpos'){ echo 'active'; } ?>"
                                href="<?php echo  $base_url ?>reset_periodic.php" aria-expanded="false">
									<span class="hide-menu">Reset Periodic</span>
							</a>
						</li>
						-->
						<li class="">
                            <button onclick="ajax_print('rawbt-receipt.php?id=41801.1.2',this)">RECEIPT</button>
                        </li>
						<li class="">
                            <a href="<?php echo  $base_url ?>escpos-php/example/rawbt-receipt.html"
                                class="btn btn-danger text-white" style="font-size:14px;line-height:25px;font-weight:bold;">RawBt Print</a>
                        </li>
						<li class="">
                            <a href="<?php echo  $base_url ?>logout.php"
                                class="btn btn-danger text-white" style="font-size:14px;line-height:25px;font-weight:bold;">LOGOUT</a>
                        </li>
                    </ul>

                </nav>
                <!-- End Sidebar navigation -->
            </div>
            <!-- End Sidebar scroll-->
          
        </aside>
        <!-- ============================================================== -->
        <!-- End Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->