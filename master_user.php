<?php 
	session_start();
	date_default_timezone_set("Asia/Bangkok");
	include("dbconnect.php");
	include("template/header_h2h.php");
?>
	<link href="css/jquery.dataTables.min.css" rel="stylesheet">
	<div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="page-breadcrumb">
                <div class="row align-items-center">
					<table width="100%" class="table table-striped table-bordered">
						<tr>
							<th>
								<a href="master_user_detail.php?act=add&pr=user" class="btn btn-success">Add USER</a>
							</th>
						</tr>
					</table>
					 <div class="table-responsive">
						<table class="table user-table" id="table_id">
							<thead>
								<tr>
									<th>USER </th>
									<th>ROLE</th>
									<th>Active</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
						<?php
							$get_prod 	= mysqli_query($conpos,"select * from tbluser ");
							while($dataProd	= mysqli_fetch_object($get_prod)){
								if($dataProd->USER_STATUS == 1){
									$checked = "ACTIVE";
									$act	 = "disable";
								}else{
									$checked = "NOT ACTIVE";
									$act	 = "enable";
								}
						?>
								<tr>
									<td><?php echo $dataProd->USER_NAME; ?></td>
									<td><?php echo $dataProd->ROLE_ID; ?></td>
									<td><?php echo $checked; ?></td>
									<td>
										<a href="master_user_detail.php?id=<?php echo $dataProd->USER_ID; ?>&pr=user&act=edit" class="btn btn-primary">edit</a>
										<a href="act_master_product.php?act=delete&id=<?php echo $dataProd->USER_ID; ?>" class="btn btn-danger">Del</a>
									</td>
									<!--
									<td>
										<input type="checkbox" id="vehicle3" name="status[]" value="<?php echo $dataProd->STATUS ?>" <?php echo $checked ?>>
									</td>
									-->
								</tr>
						<?php
							}
						?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
<?php include("template/footer_h2h.php");?>
	<script src="<?php echo  $base_url ?>js/jquery.dataTables.min.js" type="text/javascript"></script>
	<script>
		$.extend( true, $.fn.dataTable.defaults, {
		"searching": false
	} );
	$(document).ready(function() {
		$('#table_id').DataTable();
	} );
	</script>