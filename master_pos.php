<?php 
	session_start();
	error_reporting(E_ALL);
	date_default_timezone_set("Asia/Bangkok");
	include("dbconnect.php");
	include("template/header_h2h.php");
?>
	<style>
		@media only screen and (min-width: 600px) {
			.col-xs-6 : 50%;
		}
	</style>
	<div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="page-breadcrumb">
                <div class="row align-items-center">
					<div class="col-xs-12 col-md-12 col-sm-12">
						<div class="row">
							<div class="col-xs-6 "> 
								<a href="system_pos.php" class="btn btn-default" style="border:1px solid #888888; box-shadow: 5px 10px #888888;background:#fff;color:#000;margin-bottom:20px;padding-top:10px;font-size:14px;;">
									<img src="img/cashreg.bmp"  height="120px; "  width="110px;" />
									<p>Point Of Sale</p>
								</a>
							</div>
							<div class="col-xs-6 ">
								<a href="start_of_day.php?pr=pos" class="btn btn-default" style="border:1px solid #888888; box-shadow: 5px 10px #888888;background:#fff;color:#000;margin-bottom:20px;padding-top:10px;font-size:14px;;">
									<img src="img/STARTUP.bmp"  height="120px; "  width="110px;" />
									<p>Start Of Day</p>
								</a>
							</div>
							<div class="col-xs-6 ">
								<a href="end_of_day.php?pr=pos" class="btn btn-default" style="border:1px solid #888888;box-shadow: 5px 10px #888888;background:#fff;color:#000;margin-bottom:20px;padding-top:10px;font-size:14px;;">
									<img src="img/NIGHT.bmp"  height="120px; "  width="110px;" />
									<p>End Of Day</p>
								</a>
							</div>
							<div class="col-xs-6 ">
								<a href="void_backdate.php?pr=pos" class="btn btn-default" style="border:1px solid #888888;box-shadow: 5px 10px #888888;background:#fff;color:#000;margin-bottom:20px;padding-top:10px;font-size:14px;;">
									<img src="img/void.jpg"  height="120px; "  width="110px;" />
									<p>VOID Back Date</p>
								</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
<?php include("template/footer_h2h.php");?>