<div class="container">
  <div class="row">
	<h2>Mohon Tunggu Sedang Transfer Data</h2>
    <div class="col-md-12">
		<p>&nbsp;</p>
	    <p>&nbsp;</p>
		<div id="progressbar" style="border:1px solid #ccc; border-radius: 5px; "></div>
  
		<!-- Progress information -->
		<br>
		<div id="information" ></div>
	</div>
  </div>
</div>
<?php
ini_set('max_execution_time', 0); // to get unlimited php script execution time
date_default_timezone_set("Asia/Bangkok");
include("dbconnect.php");


if($_GET['proc'] == 'clear'){
	mysqli_query($conpos,"DELETE FROM ztemp_products");
	echo '<script>location.href="master_product.php?pr=product&active=online_harga";</script>';
	exit;
}

$data 		= array('company_id' => 'EKA','hhb' => $kd_pemakai, 'tanggal' => date("Ymd"));
$ch = curl_init(); 
curl_setopt($ch, CURLOPT_URL,  $url_webonline.'/store/getMasterPricePLU');
curl_setopt($ch,CURLOPT_USERAGENT,'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);     
curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
$return_out = curl_exec($ch); 
curl_close($ch); 
if($return_out === false)
{
	$output	= 'Curl error: ' . curl_error($ch);
}
else
{
	$output = $return_out; 
}
$return	= json_decode($output);
mysqli_query($conpos,"DELETE FROM ztemp_products");
$i = 0;
foreach ($return as $key2) {
	$percent = intval($i/count($return) * 100)."%";   
	
    
	
	$insert_temp = "INSERT INTO ztemp_products 
		(PRODCODE,PRODNAME,STATUS,PROMO_ID,PROMO_COUNT,PROD_GROUP_ID,TRX_CODE,UNIT_PRICE,LAST_UPD,LAST_UPD_USER,PRICE_AT,KET,AUDDATE,AUDUSER)
		VALUES 
		(
		'".$key2->NOPLU."',
		'".$key2->NMPLU."',
		'".$key2->STATUS."',
		'".$key2->PROMO_ID."',
		'".$key2->PROMO_COUNT."',
		'".$key2->PROD_GROUP_ID."',
		'".$key2->PLU_TYPE."',
		'".$key2->HARGA."',
		'".date("Y-m-d",strtotime($key2->LAST_DATE))."',
		'".$key2->LAST_USER."',
		'".$key2->HARGA."',
		'".$key2->KDHARGA."',
		'".date("Y-m-d")."',
		'POS WEB UPDATE'
		)";
		//echo $insert_temp;
		if(mysqli_query($conpos,$insert_temp)){
		
			
			echo '<script>
			parent.document.getElementById("progressbar").innerHTML="<div style=\"width:'.$percent.';background:linear-gradient(to left, #FD0101, #FFBB01); ;height:35px;\">&nbsp;</div>";
			parent.document.getElementById("information").innerHTML="<div style=\"text-align:center; font-weight:bold\">'.$percent.' is processed.</div>";</script>';

			ob_flush(); 
			flush(); 
		}
		//exit;
		$i++;
		//exit;
}
echo '<script>location.href="master_product.php?pr=product&active=online_harga";</script>';
?>