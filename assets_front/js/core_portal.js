
var portal = {}
//Declare variable global Jika ada kalo ngak ada kosongin aja;
{portal.utils=function(){
	return {
		attach:function (){
			
			$(document).ready(function(){
				//runtime Proses
				portal.utils.showSlider();
				portal.utils.showCalender();		
				
				
				$("#procLoginMember").click(function(){portal.utils.loginmember();})
				$("#procLoginMemberClose").click(function(){portal.utils.reloadpage();})
				$("#procLogoutmember").click(function(){ portal.utils.logout();})
				$("#getStatus").click(function(){ portal.utils.getStatusPO();})
			});
		},
		reloadpage:function(){
			location.href = base_url_fix;
		},
		logout:function(){
			location.href = base_url+"home/logut";
		},
		showCalender:function(){
			$('#mycalendar').monthly({
				mode: 'event',
				jsonUrl: 'events.json',
				dataType: 'json'
				//xmlUrl: 'events.xml'
			});


			switch(window.location.protocol) {
				case 'http:':
				case 'https:':
				// running on a server, should be good.
				break;
				case 'file:':
				alert('Just a heads-up, events will not work when run locally.');
			}
		},
		showSlider:function(){
			$('.slider').bxSlider(
			{
				  auto: true,
				  autoControls: false,
				  stopAutoOnClick: false,
				  pager: false,
				  slideWidth: 710	
			});
		},
		loginmember:function(){
			//alert(base_url + "home/login_proc");
			$.ajax({
				method: "POST",
				url: base_url + "home/login_proc",
				data: { username: $("#username").val(), password: $("#pwd").val() },
				beforeSend:function() { 
					portal.utils.loadingContent("formLogin");
				},
				complete:function(msg) {	
					var obj = jQuery.parseJSON( msg.responseText );
					//console.log(obj.error);
					///return;
					if(obj.error == "1"){
						portal.utils.loadingContentError("formLogin");
					}else{						
						location.href=base_url+"?sts=sukses";
					}
					
				}
			})
			
		},
		loadingContent:function(idDivHide){
			
			var content = "<div style='margin: 0 auto;width: 200px;'> " +
							"<img src = './assets_front/img/loading.gif' width='200px' />" +
							"</div>";
							
			$("#"+idDivHide).html(content);
		},
		loadingContentError:function(idDivHide){
			
			var content = "<div style='margin: 0 auto;width: 90%;text-align:center'> " +
							"The <b>username</b> is incorrect <h2>or</h2> The <b>password</b> is incorrect" +
							"</div>";
							
			$("#"+idDivHide).html(content);
		},
		updateField:function(fieldname){
			alert(fieldname);
		},
		clear:function(){
			//$(“#nama”).val(“”);
			//$(“#pass”).val(“”);
		},
		
		/***Cek status PO***/
		getStatusPO:function(){
			alert($("#fromdate").val());
		}
		
		/***end***/
	}
}()}

/****
local function
***/
function change_field(userid,fieldname,value){
	$(document).ready(function(){
		$.ajax({
			method: "POST",
			url: base_url + "home/change_profile",
			data: { id: userid, fieldname: fieldname, value: value },
			beforeSend:function() { 
				//portal.utils.loadingContent("formLogin");
			},
			complete:function(msg) {	
				//alert(msg.responseText);
				location.href=base_url_fix;
			}
		})
	});
}