
var finance = {}
//Declare variable global Jika ada kalo ngak ada kosongin aja;
{finance.utils=function(){
	return {
		attach:function (){			
			$(document).ready(function(){
				$(".detailTukarFaktur").hide();
				$('.dateproc').datepicker({
					dateFormat: 'yy-mm-dd'
				});
				$("#getStatusBytf").click(function(){ finance.utils.getHeaderTukarFaktur();})
				$("#kodeVendor").keyup(function(){ finance.utils.getAutoCompleteVendor($(this).val());})
			});
		},
		reloadpage:function(){
			location.href = base_url_fix;
		},
		logout:function(){
			location.href = base_url+"home/logut";
		},
		getAutoCompleteVendor:function(val){
			var inpString		= val;
			var datadropdown 	= '';
			var n = inpString.length; 
			if(n >= 3){
				$.ajax({
					type: "GET",
					url: base_url  + "finance/lap_status_po_bytf/getAllVendor",
					data:'keyword='+val,
					beforeSend: function(){
						$("#search-box").css("background","#FFF url(LoaderIcon.gif) no-repeat 165px");
					},
					success: function(data){
						//console.log(data);
						//alert('sukses');
						var result = JSON.parse(data);
						datadropdown += '<div class="list-group">';

						$.each(result, function(i, item) {
							//alert(item.purchid);
							var link  = "setDDVendor('"+item.accountnum+"')" ;
							datadropdown += '<a href="#" onClick = '+link+' class="list-group-item list-group-item-info">'+
												'<h4 class="list-group-item-heading">'+item.accountnum+'</h4>'+
												'<p class="list-group-item-text">'+item.NAME+'</p>'+
											'</a>';
						});
						$("#suggesstion-box").show();
						$("#suggesstion-box").html(datadropdown);
						$("#search-box").css("background","#FFF");
						
					}
				});
			}	
		},
		getHeaderTukarFaktur:function(){
			var vendid 		= $("#kodeVendor").val();
			var fromdate	= $("#fromdate").val();
			var todate		= $("#todate").val();
			var	xx			= 0;
			$.ajax({
				type: "POST",
				url: base_url  + "finance/lap_status_po_bytf/getHeaderTukarFaktur",
				data:'vendid='+vendid+'&fromdate='+fromdate+'&todate='+todate,
				beforeSend: function(){
					$("#search-box").css("background","#FFF url(LoaderIcon.gif) no-repeat 165px");
				},
				success: function(data){
					//console.log(data);
					dataDetailPO = 	"<table class='table table-striped' border='1' style='background-color:#efefef;'>"+
										" <thead >"+
											"<tr  style='background:#000000;color:#fff;'> "+
												"<td>ID Tukar Faktur</td> "+
												"<td>TGL. Tukar Faktur</td> "+
												"<td>Estimasi Tanggal Bayar</td> "+
												"<td>ID Vendor</td> "+
												"<td>Nama Vendor</td> "+
												"<td>Description</td> "+
											"</tr>";
										"</thead>";
										"<tbody> ";
					$.each(JSON.parse(data), function(i, item) {
						dataDetailPO 	+= 									
							"<tr style='background:orange;color:#000;'> "+													
								"<td>"+item.wcs_tftukarfakturid+"</td> "+
								"<td>"+item.wcs_tftukarfakturdate.replace("00:00:00.000", "")+"</td> "+
								"<td>"+item.wcs_tfduedate.replace("00:00:00.000", "")+"</td> "+
								"<td>"+item.WCS_TFINVOICEACCOUNT+"</td> "+
								"<td>"+item.name+"</td> "+
								"<td>"+item.wcs_tfDescription+"</td> "+
							"</tr>";
						dataDetailPO	+=
							'<tr> '+	
								'<td colspan=6>'+
									'<div class="col-lg-12" >'+
										'<div class="card">'+
										  '<div class="card-title">'+
											'<a href="#" onclick="btnShowDetail('+xx+');" style="color:red;">Show / Hide</a>'+
										  '</div>'+
										  '<div class="card-body">'+
												'<div id="detailTukarFaktur'+xx+'" class="detailTukarFaktur"></div>'+
										  '</div>'+
										'</div>'+
									'</div>';
									finance.utils.getDetailTukarFaktur(item.recid,xx,item.wcs_tftukarfakturid);
						dataDetailPO	+=		
								"</td> "+
							"</tr>";
						xx++;				
					});
					dataDetailPO += 	"</tbody></table> ";
					
					$("#headerTukarFaktur").html(dataDetailPO);				
							
				}
			});
		},
		getDetailTukarFaktur:function(recid,xx,irv){
			
			
			var curr = new Intl.NumberFormat('en-id', {
			  style: 'currency',
			  currency: 'IDR',
			  minimumFractionDigits: 0,
			  maximumFractionDigits: 0
			});
			$.ajax({
				type: "POST",
				url: base_url  + "finance/lap_status_po_bytf/getDetailTukarFaktur",
				data:'recid='+recid,
				beforeSend: function(){
					$("#search-box").css("background","#FFF url(LoaderIcon.gif) no-repeat 165px");
				},
				success: function(data){
					//console.log(data);return;
					dataDetailPO = 	"<table class='table table-striped' border='1' style='color:#000;'>"+
										" <thead>"+
											"<tr> "+
												"<td>NO PO</td> "+
												"<td>No Receipt</td> "+
												"<td>Tanggal Receipt</td> "+
												"<td>Voucher Pembayaran</td> "+
												"<td>Journal Pembayaran</td> "+
												"<td>Description</td> "+
												"<td>Amount</td> "+
												
											"</tr>";
										"</thead>";
										"<tbody> ";
					var bpvNo;
					var totalAmount = 0;
					$.each(JSON.parse(data), function(i, item) {
						//getBpvVoucher
						totalAmount 	= parseInt(totalAmount) + parseInt(item.WCS_TFAMOUNT);
						console.log(totalAmount);
						dataDetailPO += 									
							"<tr> "+													
								"<td>"+item.ponya+"</td> "+
								"<td>"+item.WCS_TFDOCUMENTID+"</td> "+
								"<td>"+item.invoicedate+"</td> "+
								//"<td>"+item.wcs_tftukarfakturdate.replace("00:00:00.000", "")+"</td> "+
								"<td>"+item.VOUCHER+"</td> "+
								"<td>"+item.journal+"</td> "+
								"<td>"+item.journal+"</td> "+
								"<td align='right'>"+curr.format(item.WCS_TFAMOUNT)+"</td> "+
								
							"</tr>";
										
					});
					dataDetailPO += 	"<tr> "+
										"<td></td> "+
										"<td></td> "+
										"<td></td> "+
										"<td></td> "+
										"<td><b>Total</b></td> "+
										"<td align='right'><b>"+curr.format(parseInt(totalAmount))+"</b></td> "+
										
									"</tr>";
					dataDetailPO += 	"</tbody></table> ";
					
					$("#detailTukarFaktur"+xx).html(dataDetailPO);				
				}
			});
			
		}
	}
}()}

/****
local function
***/
function change_field(userid,fieldname,value){
	$(document).ready(function(){
		$.ajax({
			method: "POST",
			url: base_url + "home/change_profile",
			data: { id: userid, fieldname: fieldname, value: value },
			beforeSend:function() { 
				//portal.utils.loadingContent("formLogin");
			},
			complete:function(msg) {	
				//alert(msg.responseText);
				location.href=base_url_fix;
			}
		})
	});
}
function setDDVendor(vendid){
	$(document).ready(function(){
		//alert("masuk");
		$("#kodeVendor").val(vendid);
		$("#suggesstion-box").hide();
	});
}