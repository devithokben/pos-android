
var host2host = {}
//Declare variable global Jika ada kalo ngak ada kosongin aja;
{host2host.utils=function(){
	return {
		attach:function (){
			
			$(document).ready(function(){				
				$("#sendToken").click(function(){ host2host.utils.sendToken();})
				$("#login").click(function(){ host2host.utils.login();})
				$(".idH2HData").click(function(){ host2host.utils.procChecked($(this).find(".valId").val(),$(this).find(".phoneid").val(),$(this).find(".level").val());})
				$("#frmToken").hide();
				$("#sendToBca").click(function(){ host2host.utils.transferToApi();})
				$("#journalnum").keyup(function(){ host2host.utils.getAutoCompleteVendor($(this).val());})
			});
		},		
		transferToApi:function(){
			var form = $('#frm-example');
			//console.log();

			$.ajax({
				method: "POST",
				url: base_url + "home/sendToBCAHost",
				data : form.serialize(),
				beforeSend:function() { 
					//$("#sendToken").attr("disabled", true);
				},
				complete:function(msg) {	
					//console.log(msg.responseText);
					if(msg.responseText == "error"){
						alert("Masuk Error");
					}else{
						console.log(msg.responseText);
						//alert(msg.responseText);
						//window.location.reload();
					}
					
				}
			})
			
		},
		procChecked:function(id,phoneid,level){
			
			$.ajax({
				method: "POST",
				url: base_url + "home/updateApprove",
				data: { id: id, phone: phoneid, level: level },
				beforeSend:function() { 
					//$("#sendToken").attr("disabled", true);
				},
				complete:function(msg) {	
					console.log(msg.responseText);
					if(msg.responseText == "error"){
						alert("Approve Tidak Berhasil");
					}else{
						alert("Approve Sudah Berhasil");
						location.href=base_url+"home/home";
					}
					
				}
			})
			
		},
		sendToken:function(){
			//alert($("#email").val());
			$.ajax({
				method: "GET",
				url: base_url + "home/createToken",
				data: { email: $("#email").val()},
				beforeSend:function() { 
					$("#sendToken").attr("disabled", true);
				},
				complete:function(msg) {	
					console.log(msg.responseText);
					if(msg.responseText == "error"){
						alert("Email Tidak Ditemukan");
					}else{
						$("#sendToken").attr("disabled", false);
						//$("#frmEmail").hide("slow");
						//$("#frmToken").show("slow");
					}
					
				}
			})
			
		},
		login:function(){	
			$.ajax({
				method: "POST",
				url: base_url + "home/login_proc",
				data: { email: $("#email").val(), otp: $("#otp").val() },
				beforeSend:function() { 
					$('#login').attr('disabled','disabled');
					$("#login").text("Please Wait");
				},
				complete:function(msg) {	
					///alert(msg.responseText);
					if(msg.responseText == "gagal"){
						alert('ScreetKey Tidak Sesuai');
						$('#login').removeAttr('disabled');
						$("#login").text("Login");
					}else{
						location.href=base_url + "home/home";
					}
					
				}
			})			
		},
		getAutoCompleteVendor:function(val){
			var inpString		= val;
			var datadropdown 	= '';
			var n = inpString.length; 
			if(n >= 3){
				$.ajax({
					type: "GET",
					url: base_url  + "home/getAllJournal",
					data:'keyword='+val,
					beforeSend: function(){
						$("#search-box").css("background","#FFF url(LoaderIcon.gif) no-repeat 165px");
					},
					success: function(data){
						//console.log(data);
						//alert('sukses');
						var result = JSON.parse(data);
						datadropdown += "<div class='row'>";
						$.each(result, function(i, item) {
							//alert(item.purchid);
							var link  = "setDDVendor('"+item.JOURNALNUM+"')" ;
							datadropdown += '<div class="col-lg-4 linkdd"><a href="#" class="" onClick = '+link+'>'+
												'<h4>'+item.JOURNALNUM+'</h4>'+
												'<p>'+item.NAME+'</p>'+
											'</a></div>';
						});
						datadropdown += "</div>";
						$("#suggesstion-box").show();
						$("#suggesstion-box").html(datadropdown);
						$("#search-box").css("background","#FFF");
						
					}
				});
			}	
		}
	}
}()}

function setDDVendor(journalid){
	$(document).ready(function(){
		//alert("masuk");
		$("#journalnum").val(journalid);
		$("#suggesstion-box").hide();
	});
}