<?php 
	session_start();
	date_default_timezone_set("Asia/Bangkok");
	include("dbconnect.php");
	include("template/header_h2h.php");
	//print_r($_SESSION);
	if($_GET['act'] == 'update_footnote'){
		$ch = curl_init(); 
			curl_setopt($ch, CURLOPT_URL, $url_websvc.'footnote.php?storeid='.$kd_pemakai);
			curl_setopt($ch,CURLOPT_USERAGENT,'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);     
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
			curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
			$return_out = curl_exec($ch); 
			curl_close($ch); 
			if($return_out === false)
			{
				$output	= 'Curl error: ' . curl_error($ch);
			}
			else
			{
				$output = $return_out; 
			}
			$result[0]	= json_decode($output);
			//echo "<pre>";
			//echo $url_websvc.'footnote.php?storeid='.$kd_pemakai;
			//print_r($result[0]);
			//exit;
			
		
			foreach ($result[0] as $key=>$val) {
					if($temtable != $key){
						$temtable = $key;
						//echo  "<span class='alert alert-info'>".$key." :: ";
						
						//print_r($result[0]->$key);
							// $sql = "update cprofile set ".$key." = '".$val."'";
							// if(mysqli_query($conpos,$sql)){
							// 	echo "sukses";
							// }else{
							// 	echo "gagal";
							// }
						
						foreach ($result[0]->$key as $key2=>$val2) {
							$sql = "update cprofile set ".$key2." = '".$val2."'";
							if(mysqli_query($conpos,$sql)){
								echo "sukses";
							}else{
								echo "gagal";
							}
						}
						//print_r($result[0]->$key);
						//echo  "Sukses Update :: </span>";
					}
                }
					echo "</pre>";
			echo "<script>					
					Swal.fire({
					  title: 'SUCCESS!',
					  text: 'Proses Update Foot Note Berhasil',
					  icon: 'success',
					  confirmButtonText: 'CLOSE'
					}).then((result) => {
					  if (result.isConfirmed) {
						$(document).ready(function() {
							location.href='master_config.php';
						} );
					  }
					});
					//alert('Start Off Day Berhasil');
				</script>";
	}
	else if ($_GET['act'] == 'update_profile'){
		$sql = " UPDATE	
				cprofile
			SET
				BRANCH_ID = '".$_POST['C_BRANCH_ID']."',
				BRANCH_NAME = '".$_POST['C_BRANCH_NAME']."',
				CPHONE = '".$_POST['C_PHONE']."',
				CFAX = '".$_POST['C_FAX']."',
				CADDR1 = '".$_POST['CADDR1']."',
				MSG_STROOK_1 = '".$_POST['C_MSG_STROOK_1']."',
				MSG_STROOK_2 = '".$_POST['C_MSG_STROOK_2']."',
				MSG_STROOK_3 = '".$_POST['C_MSG_STROOK_3']."',
				MSG_STROOK_4 = '".$_POST['C_MSG_STROOK_4']."'";
		//echo $sql;
		if(mysqli_query($conpos,$sql)){	
			echo "<script>					
						Swal.fire({
						  title: 'SUCCESS!',
						  text: 'Proses Update Profile Berhasil',
						  icon: 'success',
						  confirmButtonText: 'CLOSE'
						}).then((result) => {
						  if (result.isConfirmed) {
							$(document).ready(function() {
								location.href='master_config.php';
							} );
						  }
						});
						//alert('Start Off Day Berhasil');
					</script>";
		}else{
			echo "<script>					
						Swal.fire({
						  title: 'GAGAL!',
						  text: 'Proses Update Profile Gagal',
						  icon: 'error',
						  confirmButtonText: 'CLOSE'
						}).then((result) => {
						  if (result.isConfirmed) {
							$(document).ready(function() {
								location.href='master_config.php';
							} );
						  }
						});
						//alert('Start Off Day Berhasil');
					</script>";
		}
	}
	else if ($_GET['act'] == 'update_branch'){
				
		$sql = " UPDATE	
				tbl_lst_branch
			SET
				BRANCH_ID = '".$_POST['BRANCH_ID']."',
				BRANCH_NAME = '".$_POST['BRANCH_NAME']."',
				ADDRESS1 = '".$_POST['ADDRESS1']."',
				CITY = '".$_POST['CITY']."',
				PHONE = '".$_POST['PHONE']."',
				EMAIL = '".$_POST['EMAIL']."'";
		//echo $sql;
		mysqli_query($conpos,$sql);		
		echo "<script>					
					Swal.fire({
					  title: 'SUCCESS!',
					  text: 'Proses Update Profile Berhasil',
					  icon: 'success',
					  confirmButtonText: 'CLOSE'
					}).then((result) => {
					  if (result.isConfirmed) {
						$(document).ready(function() {
							location.href='master_config.php';
						} );
					  }
					});
					//alert('Start Off Day Berhasil');
				</script>";
	}
	else if ($_GET['act'] == 'upload_product'){
		$target_dir = "./cust_display/product/";
		$target_file = $target_dir . basename($_FILES["fileImageToUpload"]["name"]);
		
		if (move_uploaded_file($_FILES["fileImageToUpload"]["tmp_name"], $target_file)) {
			echo "<script>					
				Swal.fire({
				  title: 'SUCCESS!',
				  text: 'The file ". htmlspecialchars( basename( $_FILES["fileImageToUpload"]["name"])).  "has been uploaded.',
				  icon: 'success',
				  confirmButtonText: 'CLOSE'
				}).then((result) => {
				  if (result.isConfirmed) {
					$(document).ready(function() {
						location.href='master_config.php';
					} );
				  }
				});
				//alert('Start Off Day Berhasil');
			</script>";
		} else {
			echo "Sorry, there was an error uploading your file.";
		}
	}
	else if ($_GET['act'] == 'upload_promo'){
		
		/*
$filename   = uniqid() . "-" . time(); // 5dab1961e93a7-1571494241
$extension  = pathinfo( $_FILES["file"]["name"], PATHINFO_EXTENSION ); // jpg
$basename   = $filename . "." . $extension; // 5dab1961e93a7_1571494241.jpg

$source       = $_FILES["file"]["tmp_name"];
$destination  = "../img/imageDirectory/{$basename}";

move_uploaded_file( $source, $destination )
*/
		 
		
		$filename   = "mainpromo"; // 5dab1961e93a7-1571494241
		$extension  = pathinfo( $_FILES["filePromoToUpload"]["name"], PATHINFO_EXTENSION ); // jpg
		$basename   = $filename . "." . $extension; // 5dab1961e93a7_1571494241.jpg
		$target_file  = "./cust_display/product/".$basename;
		
		if (move_uploaded_file($_FILES["filePromoToUpload"]["tmp_name"], $target_file)) {
			echo "<script>					
				Swal.fire({
				  title: 'SUCCESS!',
				  text: 'The file ". htmlspecialchars( basename( $_FILES["filePromoToUpload"]["name"])).  "has been uploaded.',
				  icon: 'success',
				  confirmButtonText: 'CLOSE'
				}).then((result) => {
				  if (result.isConfirmed) {
					$(document).ready(function() {
						location.href='index.php';
					} );
				  }
				});
				//alert('Start Off Day Berhasil');
			</script>";
		} else {
			echo "Sorry, there was an error uploading your file.".$target_file;
		}
	}
	
?>
	<div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="page-breadcrumb">
                <div class="row align-items-center">
					
					<div class="col-xs-12 col-md-12 col-sm-12">
						<div class="row">
							<div class="col-lg-6 col-md-6">
								<?php
									$qry_lst_profile 	= mysqli_query($conpos,"select * from cprofile");
									$data_lst_profile	= mysqli_fetch_object($qry_lst_profile)
								?>
								<div class="card">
									<div class="card-body">
										<h3 class="card-title">Konfigurasi PROFILE </h3>
									</div>
									<div>
										<hr class="m-t-0 m-b-0">
									</div>
									<div class="card-body row">
										<form class="form-horizontal form-material" method="POST" action="master_config.php?act=update_profile">
											<div class="row">
												<div class="form-group col-md-4">
													<label class="col-md-12">ID</label>
													<div class="col-md-12">
														<input type="text" name="C_BRANCH_ID" value="<?php echo $data_lst_profile->BRANCH_ID ?>"
															class="form-control character form-control-line pl-0">
													</div>
												</div>
											
												<div class="form-group col-md-4">
													<label class="col-md-12">NAME</label>
													<div class="col-md-12">
														<input type="text" name="C_BRANCH_NAME" value="<?php echo $data_lst_profile->BRANCH_NAME ?>"
															class="form-control character form-control-line pl-0">
													</div>
												</div>
												<div class="form-group col-md-4">
													<label class="col-md-12">PHONE</label>
													<div class="col-md-12">
														<input type="text" name="C_PHONE" value="<?php echo $data_lst_profile->CPHONE ?>"
															class="form-control character form-control-line pl-0">
													</div>
												</div>
												<div class="form-group col-md-6">
													<label class="col-md-12">FAX</label>
													<div class="col-md-12">
														<input type="text"  name="C_FAX" value="<?php echo $data_lst_profile->CFAX ?>"
															class="form-control character form-control-line pl-0">
													</div>
												</div>
												<div class="form-group col-md-6">
													<label class="col-md-12">ADDRESS</label>
													<div class="col-md-12">
														<input type="text"  name="CADDR1" value="<?php echo $data_lst_profile->CADDR1 ?>"
															class="form-control character form-control-line pl-0">
													</div>
												</div>
											</div>											
											
											<div class="form-group">
                                                <label class="col-md-12">FOOTER</label>
                                                <div class="col-md-12">
                                                    <input type="text" name="C_MSG_STROOK_1" value="<?php echo $data_lst_profile->MSG_STROOK_1 ?>" class="form-control character form-control-line pl-0"/>
													<input type="text" name="C_MSG_STROOK_2" value="<?php echo $data_lst_profile->MSG_STROOK_2 ?>" class="form-control character form-control-line pl-0"/>
													<input type="text" name="C_MSG_STROOK_3" value="<?php echo $data_lst_profile->MSG_STROOK_3 ?>" class="form-control character form-control-line pl-0"/>
													<input type="text" name="C_MSG_STROOK_4" value="<?php echo $data_lst_profile->MSG_STROOK_4 ?>" class="form-control character form-control-line pl-0"/>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-12">
                                                    <input type="submit" value="UPDATE PROFILE" class="btn btn-warning" />
													<a href="master_config.php?act=update_footnote" class="btn btn-danger">Update FootNote</a>
                                                </div>
                                            </div>
                                        </form>
									</div>
								</div>
							</div>
							<div class="col-lg-6 col-md-6">
								<?php
									$qry_lst_branch 	= mysqli_query($conpos,"select * from tbl_lst_branch");
									$data_lst_branch	= mysqli_fetch_object($qry_lst_branch)
								?>
								<div class="card">
									<div class="card-body">
										<h3 class="card-title">Konfigurasi BRANCH_ID </h3>
									</div>
									<div>
										<hr class="m-t-0 m-b-0">
									</div>
									<div class="card-body row">
										<form class="form-horizontal form-material" method="POST" action="master_config.php?act=update_branch">
											<div class="row">
												<div class="form-group col-md-6">
													<label class="col-md-12">Kode</label>
													<div class="col-md-12">
														<input type="text" value="<?php echo $data_lst_branch->BRANCH_ID;?>" name="BRANCH_ID"
															class="form-control character form-control-line pl-0">
													</div>
												</div>
												 <div class="form-group col-md-6">
													<label class="col-md-12">Nama</label>
													<div class="col-md-12">
														<input type="text" value="<?php echo $data_lst_branch->BRANCH_NAME;?>" name="BRANCH_NAME"
															class="form-control character form-control-line pl-0">
													</div>
												</div>
											</div>
											<div class="form-group">
                                                <label class="col-md-12">Alamat</label>
                                                <div class="col-md-12">
                                                    <input type="text" value="<?php echo $data_lst_branch->ADDRESS1;?>" name="ADDRESS1"
                                                        class="form-control character form-control-line pl-0">
                                                </div>
                                            </div>
											<div class="row">
												<div class="form-group col-md-6">
													<label class="col-md-12">Kota</label>
													<div class="col-md-12">
														<input type="text" value="<?php echo $data_lst_branch->CITY;?>" name="CITY"
															class="form-control character form-control-line pl-0">
													</div>
												</div>
												<div class="form-group col-md-6">
													<label class="col-md-12">Phone</label>
													<div class="col-md-12">
														<input type="text" value="<?php echo $data_lst_branch->PHONE;?>" name="PHONE"
															class="form-control character form-control-line pl-0">
													</div>
												</div>
											</div>
											<div class="form-group">
                                                <label class="col-md-12">Email</label>
                                                <div class="col-md-12">
                                                    <input type="text" value="<?php echo $data_lst_branch->EMAIL;?>" name="EMAIL"
                                                        class="form-control character form-control-line pl-0">
                                                </div>
                                            </div>
                                          
                                            <div class="form-group">
                                                <div class="col-sm-12">
                                                    <input type="submit" class="btn btn-success text-white" value="Update Konfigurasi">
                                                </div>
                                            </div>
                                        </form>
									</div>
								</div>
							</div>
							<div class="col-lg-12 col-md-12 row">
								<?php
									$qry_lst_branch 	= mysqli_query($conpos,"select * from tbl_lst_branch");
									$data_lst_branch	= mysqli_fetch_object($qry_lst_branch)
								?>
								<div class=" col-lg-4">
									<div class="card">
										<div class="card-body">
											<h3 class="card-title">UPLOAD GAMBAR PRODUCT </h3>
										</div>
										<div>
											<hr class="m-t-0 m-b-0">
										</div>
										<div class="card-body ">
											<form class="form-horizontal form-material" method="POST" enctype="multipart/form-data" action="master_config.php?act=upload_product">
												<div class="row">
													<div class="form-group col-md-12">
														<div class="form-group">
															<div class="col-sm-12">
																<div class="alert alert-danger">
																	Pastikan penamaan gambar adalah : {plucode}.jpg ::: cth : 901585.jpg
																</div>
																<input type="file" name="fileImageToUpload" id="fileImageToUpload" class="form-control form-control-line pl-0" />
																<input type="submit" value="UPLOAD GAMBAR PRODUCT" class="btn btn-warning" />
															</div>
														</div>
													</div>
												</div>
											</form>
										</div>
									</div>
								</div>
								<div class=" col-lg-4">
									<div class="card">
										<div class="card-body">
											<h3 class="card-title">UPLOAD GAMBAR PROMO </h3>
										</div>
										<div>
											<hr class="m-t-0 m-b-0">
										</div>
										<div class="card-body">
											<form class="form-horizontal form-material" method="POST" enctype="multipart/form-data" action="master_config.php?act=upload_promo">
												<div class="row">
													<div class="form-group col-md-12">
														<div class="form-group">
															<div class="col-sm-12">
															<div class="alert alert-danger">Pastikan penamaan gambar adalah : mainpromo.jpg</div>
															<input type="file" name="filePromoToUpload" id="filePromoToUpload" class="form-control form-control-line pl-0">
																<input type="submit" value="UPLOAD GAMBAR PROMO" class="btn btn-warning" />
															</div>
														</div>
													</div>
												</div>
											</form>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
<?php include("template/footer_h2h.php");?>
<script src="Keyboard/js/jquery.keyboard.js"></script>
<!-- keyboard extensions (optional) -->
<script src="Keyboard/js/jquery.mousewheel.js"></script>
<script>
	$(function(){
		$('.numeric').keyboard({layout : 'num' });
		$('.character').keyboard({layout: 'qwerty'});
	});
</script>