<?php
	error_reporting(E_ALL);
	ini_set('error_reporting', 1);
	ini_set('display_errors',1);
	session_start();

	ini_set('max_execution_time', 0); // to get unlimited php script execution time

	
	date_default_timezone_set("Asia/Bangkok");
	include("dbconnect.php");
?>
<!DOCTYPE html>
<html dir="ltr" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
   
    <title>DashBoard</title>
    <link rel="canonical" href="https://www.wrappixel.com/templates/materialpro-lite/" />
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo  $base_url ?>assets/images/favicon.png">

    <!-- Custom CSS -->
    <link href="<?php echo  $base_url ?>css/style.min.css" rel="stylesheet">
	 <link href="<?php echo  $base_url ?>css/jquery-ui.css" rel="stylesheet">
	<style>
	.card{
		margin-bottom : 10px;
	}
	</style>
 
</head>

<body>
<div id="main-wrapper" data-layout="vertical"  data-sidebartype="full"
	data-sidebar-position="absolute" data-header-position="absolute" data-boxed-layout="full">
		<header class="topbar btn-danger" data-navbarbg="">
			<nav class="navbar top-navbar navbar-expand-md navbar-dark btn-danger">
				<div class="navbar-header" data-logobg="">
					<!-- ============================================================== -->
					<!-- Logo -->
					<!-- ============================================================== -->
					<a class="navbar-brand ml-4" href="index.html">
					   
						<!--End Logo icon -->
						<!-- Logo text -->
						<span class="logo-text" style="color:#fff">
						   <h2>System Transfer POS .... Please Wait ......</h2>

						</span>
					</a>
					<!-- ============================================================== -->
					<!-- End Logo -->
					<!-- ============================================================== -->
					<!-- ============================================================== -->
					<!-- toggle and nav items -->
					<!-- ============================================================== -->
					<a class="nav-toggler waves-effect waves-light text-white d-block d-md-none"
						href="javascript:void(0)"><i class="ti-menu ti-close"></i></a>
				</div>
				<!-- ============================================================== -->
				<!-- End Logo -->
			  
			</nav>
		</header>
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="page-breadcrumb">
                <div class="row align-items-center">
					<div class="col-xs-12 col-md-12 col-sm-12 row">						
						<div class="col-xs-6 col-md-6 col-sm-6 card ">	
							<div class="card-body  btn-success row">
								<div class="col-md-12">
									<div id="progressbar_hdr" style="border:1px solid #ccc; border-radius: 5px; "></div>
							  		<div id="information_hdr" ></div>
								</div>
							</div>
						</div>
						<div class="col-xs-6 col-md-6 col-sm-6 card">	
							<div class="card-body">
								<div class="col-md-12">
									<div id="progressbar_dtl" style="border:1px solid #ccc; border-radius: 5px; "></div>							  
									<div id="information_dtl" ></div>
								</div>
							</div>
						</div>
						<div class="col-xs-6 col-md-6 col-sm-6 card">	
							<div class="card-body">
								<div class="col-md-12">
									<div id="progressbar_payment" style="border:1px solid #ccc; border-radius: 5px; "></div>							  
									<div id="information_payment" ></div>
								</div>
							</div>
						</div>
						<div class="col-xs-6 col-md-6 col-sm-6 card">	
							<div class="card-body   btn-success row">
								<div class="col-md-12">
									<div id="progressbar_sales_cancel" style="border:1px solid #ccc; border-radius: 5px; "></div>							  
									<div id="information_sales_cancel" ></div>
								</div>
							</div>
						</div>
						<div class="col-xs-6 col-md-6 col-sm-6 card">	
							<div class="card-body   btn-success row">
								<div class="col-md-12">
									<div id="progressbar_sales_dp" style="border:1px solid #ccc; border-radius: 5px; "></div>							  
									<div id="information_sales_dp" ></div>
								</div>
							</div>
						</div>
						<div class="col-xs-6 col-md-6 col-sm-6 card">	
							<div class="card-body">
								<div class="col-md-12">
									<div id="progressbar_sales_dp_cancel" style="border:1px solid #ccc; border-radius: 5px; "></div>							  
									<div id="information_sales_dp_cancel" ></div>
								</div>
							</div>
						</div>
						<!--
						<div class="col-xs-6 col-md-6 col-sm-6 card">	
							<div class="card-body ">
								<div class="col-md-12">
									<div id="progressbar_sales_print" style="border:1px solid #ccc; border-radius: 5px; "></div>							  
									<div id="information_sales_print" ></div>
								</div>
							</div>
						</div>
						-->
						<div class="col-xs-6 col-md-6 col-sm-6 card">	
							<div class="card-body  row">
								<div class="col-md-12">
									<div id="progressbar_sales_vch" style="border:1px solid #ccc; border-radius: 5px; "></div>							  
									<div id="information_sales_vch" ></div>
								</div>
							</div>
						</div>
						<div class="col-xs-6 col-md-6 col-sm-6 card">	
							<div class="card-body   btn-success row">
								<div class="col-md-12">
									<div id="progressbar_sales_void" style="border:1px solid #ccc; border-radius: 5px; "></div>							  
									<div id="information_sales_void" ></div>
								</div>
							</div>
						</div>
						<div class="col-xs-6 col-md-6 col-sm-6 card">	
							<div class="card-body row">
								<div class="col-md-12">
									<div id="progressbar_trx_ctl" style="border:1px solid #ccc; border-radius: 5px; "></div>							  
									<div id="information_trx_ctl" ></div>
								</div>
							</div>
						</div>
						<div class="col-xs-12 col-md-12 col-sm-12">
							<div id="btn_success" ></div>
						</div>
					</div>
						
						<?php 
							if($_POST){
								if(empty($_SESSION['idhdr'])){
									$_SESSION['idhdr'] = 0;
								}
								if(empty($_SESSION['idtl'])){
									$_SESSION['idtl'] = 0;
								}
								if(empty($_SESSION['ipay'])){
									$_SESSION['ipay'] = 0;
								}
								if(empty($_SESSION['isale_cancel'])){
									$_SESSION['isale_cancel'] = 0;
								}
								if(empty($_SESSION['isale_dp'])){
									$_SESSION['isale_dp'] = 0;
								}
								if(empty($_SESSION['isale_dp_cancel'])){
									$_SESSION['isale_dp_cancel'] = 0;
								}
								if(empty($_SESSION['sales_print'])){
									$_SESSION['sales_print'] = 0;
								}
								if(empty($_SESSION['sales_vch'])){
									$_SESSION['sales_vch'] = 0;
								}
								if(empty($_SESSION['sales_void'])){
									$_SESSION['sales_void'] = 0;
								}
								if(empty($_SESSION['trx_ctl'])){
									$_SESSION['trx_ctl'] = 0;
								}
								$total_hdr 	= 0;
								
								$sql_hdr	= " select ID from sales_hdr where TRX_DATE = '".$_POST['tgl_trans']."' order by ENT_DATE ASC";
								$get_hdr 	= mysqli_query($conpos,$sql_hdr);
								$total = mysqli_num_rows($get_hdr);
								while($dat_hdr	= mysqli_fetch_array($get_hdr)){
									
									$id[] = $dat_hdr['ID'];
								}
								
								$idhdr			= 0;
								$sql_hdr	= " select * from sales_hdr where ID IN ('".implode("','",$id)."') order by ID ASC";
									//echo $sql_dtl;
									$get_hdr	= mysqli_query($conpos,$sql_hdr);
									$totalhdr	= mysqli_num_rows($get_hdr);
									while($dat_hdr	= mysqli_fetch_object($get_hdr)){
										//print_r($dat_hdr);
										$_SESSION['idhdr'] = $idhdr;
										$percent = intval($idhdr/$totalhdr * 100)."%"; 
										//echo $percent;
										usleep( 250000 );; // Here send pusat
										
										$ch = curl_init(); 
										curl_setopt($ch, CURLOPT_URL, 'https://online.hokben.net/digital_system/index.php/transferdata/sales_hdr');
										curl_setopt($ch,CURLOPT_USERAGENT,'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');
										curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
										curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
										curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);     
										curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
										curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($dat_hdr));
										$return_out = curl_exec($ch); 
										curl_close($ch); 
										if($return_out === false)
										{
											$output	= 'Curl error: ' . curl_error($ch).$url;
										}
										else
										{
											$output = $return_out; 
										}
										

										echo '<script>
										parent.document.getElementById("progressbar_hdr").innerHTML="<div style=\"width:'.$percent.';background:linear-gradient(to bottom, rgba(125,126,125,1) 0%,rgba(14,14,14,1) 100%); ;height:20px;\">&nbsp;</div>";
										parent.document.getElementById("information_hdr").innerHTML="<div style=\"text-align:center; font-weight:bold\">Sales Header :'.$dat_hdr->ID.' is processed.</div>";</script>';
										ob_flush(); 
										flush();
										$idhdr = $idhdr + 1;
									}
								
								//exit;
								echo '<script>
										parent.document.getElementById("progressbar_hdr").innerHTML="<div style=\"width:100%;background:linear-gradient(to bottom, rgba(125,126,125,1) 0%,rgba(14,14,14,1) 100%); ;height:20px;\">&nbsp;</div>";
										parent.document.getElementById("information_hdr").innerHTML="<div style=\"text-align:center; font-weight:bold\">Sales Header Proses Complete.</div>";</script>';
								
								$idtl			= 0;
								$sql_dtl	= " select * from sales_dtl where ID IN ('".implode("','",$id)."') order by ID ASC";
									//echo $sql_dtl;
									$get_dtl 	= mysqli_query($conpos,$sql_dtl);
									$totaldtl = mysqli_num_rows($get_dtl);
									while($dat_dtl	= mysqli_fetch_object($get_dtl)){
										$_SESSION['idtl'] = $idtl;
										$percent = intval($idtl/$totaldtl * 100)."%"; 
										usleep( 200000 ); // Here send pusat
										
										$ch = curl_init(); 
										curl_setopt($ch, CURLOPT_URL, 'https://online.hokben.net/digital_system/index.php/transferdata/sales_dtl');
										curl_setopt($ch,CURLOPT_USERAGENT,'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');
										curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
										curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
										curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);     
										curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
										curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($dat_dtl));
										$return_out = curl_exec($ch); 
										curl_close($ch); 
										if($return_out === false)
										{
											$output	= 'Curl error: ' . curl_error($ch).$url;
										}
										else
										{
											$output = $return_out; 
										}
										print_r($output);
										echo '<script>
										parent.document.getElementById("progressbar_dtl").innerHTML="<div style=\"width:'.$percent.';background:linear-gradient(to bottom, rgba(125,126,125,1) 0%,rgba(14,14,14,1) 100%); ;height:20px;\">&nbsp;</div>";
										parent.document.getElementById("information_dtl").innerHTML="<div style=\"text-align:center; font-weight:bold\">Sales Detail '.$dat_dtl->ID.' Prodcode : '.$dat_dtl->PRODCODE.' is processed.</div>";</script>';
										ob_flush(); 
										flush();
										$idtl = $idtl + 1;
									}
								
								echo '<script>
										parent.document.getElementById("progressbar_dtl").innerHTML="<div style=\"width:100%;background:linear-gradient(to bottom, rgba(125,126,125,1) 0%,rgba(14,14,14,1) 100%); ;height:20px;\">&nbsp;</div>";
										parent.document.getElementById("information_dtl").innerHTML="<div style=\"text-align:center; font-weight:bold\">Sales Detail Proses Complete.</div>";</script>';
								
								$sql_payment	= " select * from sales_payment where ID IN ('".implode("','",$id)."') order by ID ASC";
									//echo $sql_payment;
									$get_payment 	= mysqli_query($conpos,$sql_payment);
									$total_pay	 = mysqli_num_rows($get_payment);
									$ipay		= 0;
									while($dat_pay	= mysqli_fetch_object($get_payment)){
										$_SESSION['ipay'] = $ipay;
										$percent_pay = intval($ipay/$total_pay * 100)."%"; 
										//echo "(".$ipay."/".$total_pay ."* 100)"."%";
										usleep( 250000 ); // Here send pusat
										
										$ch = curl_init(); 
										curl_setopt($ch, CURLOPT_URL, 'https://online.hokben.net/digital_system/index.php/transferdata/sales_payment');
										curl_setopt($ch,CURLOPT_USERAGENT,'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');
										curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
										curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
										curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);     
										curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
										curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($dat_pay));
										$return_out = curl_exec($ch); 
										curl_close($ch); 
										if($return_out === false)
										{
											$output	= 'Curl error: ' . curl_error($ch).$url;
										}
										else
										{
											$output = $return_out; 
										}
										print_r($output);

										echo '<script>
										parent.document.getElementById("progressbar_payment").innerHTML="<div style=\"width:'.$percent_pay.';background:linear-gradient(to bottom, rgba(125,126,125,1) 0%,rgba(14,14,14,1) 100%); ;height:20px;\">&nbsp;</div>";
										parent.document.getElementById("information_payment").innerHTML="<div style=\"text-align:center; font-weight:bold\">Sales Payment '.$dat_pay->ID.' is processed.</div>";</script>';
										ob_flush(); 
										flush();
										$ipay = $ipay + 1;
									}
								
								echo '<script>
										parent.document.getElementById("progressbar_payment").innerHTML="<div style=\"width:100%;background:linear-gradient(to bottom, rgba(125,126,125,1) 0%,rgba(14,14,14,1) 100%); ;height:20px;\">&nbsp;</div>";
										parent.document.getElementById("information_payment").innerHTML="<div style=\"text-align:center; font-weight:bold\">Sales Payment  Proses Complete.</div>";</script>';
										
								$sql_salecancel	= " select * from sales_cancel where ID IN ('".implode("','",$id)."') order by ID ASC";
							//	echo $sql_salecancel;
								$get_salecancel 	= mysqli_query($conpos,$sql_salecancel);
								$total_salecancel	= mysqli_num_rows($get_salecancel);
								$isale_cancel		= 0;
								while($dat_sales_cancel	= mysqli_fetch_object($get_salecancel)){
									$_SESSION['isale_cancel'] = $isale_cancel;
									$percent_pay = intval($isale_cancel/$total_salecancel * 100)."%"; 
									//echo "(".$ipay."/".$total_pay ."* 100)"."%";
									usleep( 250000 ); // Here send pusat

									$ch = curl_init(); 
									curl_setopt($ch, CURLOPT_URL, 'https://online.hokben.net/digital_system/index.php/transferdata/sales_cancel');
									curl_setopt($ch,CURLOPT_USERAGENT,'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');
									curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
									curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
									curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);     
									curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
									curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($dat_sales_cancel));
									$return_out = curl_exec($ch); 
									curl_close($ch); 
									if($return_out === false)
									{
										$output	= 'Curl error: ' . curl_error($ch).$url;
									}
									else
									{
										$output = $return_out; 
									}
									
									print_r($output);
									
									echo '<script>
									parent.document.getElementById("progressbar_sales_cancel").innerHTML="<div style=\"width:'.$percent_pay.';background:linear-gradient(to bottom, rgba(125,126,125,1) 0%,rgba(14,14,14,1) 100%); ;height:20px;\">&nbsp;</div>";
									parent.document.getElementById("information_sales_cancel").innerHTML="<div style=\"text-align:center; font-weight:bold\">Sales Cancel '.$dat_sales_cancel->ID.' is processed.</div>";</script>';
									ob_flush(); 
									flush();
									$isale_cancel = $isale_cancel + 1;
								}
							
							echo '<script>
									parent.document.getElementById("progressbar_sales_cancel").innerHTML="<div style=\"width:100%;background:linear-gradient(to bottom, rgba(125,126,125,1) 0%,rgba(14,14,14,1) 100%); ;height:20px;\">&nbsp;</div>";
									parent.document.getElementById("information_sales_cancel").innerHTML="<div style=\"text-align:center; font-weight:bold\">Sales Cancel  Proses Complete.</div>";</script>';
							
							$sql_saledp	= " select * from sales_dp where ID IN ('".implode("','",$id)."') order by ID ASC";
								//echo $sql_payment;
								$get_saledp	= mysqli_query($conpos,$sql_saledp);
								$total_saledp	= mysqli_num_rows($get_saledp);
								$isale_dp		= 0;
								while($dat_sale_dp	= mysqli_fetch_object($get_saledp)){
									$_SESSION['isale_cancel'] = $isale_dp;
									$percent_sale_dp = intval($isale_dp/$total_saledp * 100)."%"; 
									//echo "(".$ipay."/".$total_pay ."* 100)"."%";
									usleep( 250000 ); // Here send pusat
									
									$ch = curl_init(); 
									curl_setopt($ch, CURLOPT_URL, 'https://online.hokben.net/digital_system/index.php/transferdata/sales_dp');
									curl_setopt($ch,CURLOPT_USERAGENT,'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');
									curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
									curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
									curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);     
									curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
									curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($dat_sale_dp));
									$return_out = curl_exec($ch); 
									curl_close($ch); 
									if($return_out === false)
									{
										$output	= 'Curl error: ' . curl_error($ch).$url;
									}
									else
									{
										$output = $return_out; 
									}
									print_r($output);

									echo '<script>
									parent.document.getElementById("progressbar_sales_dp").innerHTML="<div style=\"width:'.$percent_sale_dp.';background:linear-gradient(to bottom, rgba(125,126,125,1) 0%,rgba(14,14,14,1) 100%); ;height:20px;\">&nbsp;</div>";
									parent.document.getElementById("information_sales_dp").innerHTML="<div style=\"text-align:center; font-weight:bold\">Sales DP '.$dat_sale_dp->ID.' is processed.</div>";</script>';
									ob_flush(); 
									flush();
									$isale_cancel = $isale_cancel + 1;
								}
							
							echo '<script>
									parent.document.getElementById("progressbar_sales_dp").innerHTML="<div style=\"width:100%;background:linear-gradient(to bottom, rgba(125,126,125,1) 0%,rgba(14,14,14,1) 100%); ;height:20px;\">&nbsp;</div>";
									parent.document.getElementById("information_sales_dp").innerHTML="<div style=\"text-align:center; font-weight:bold\">Sales DP  Proses Complete.</div>";</script>';
							
							$sql_saledp_cancel	= " select * from sales_dp_cancel where ID IN ('".implode("','",$id)."') order by ID ASC";
								//echo $sql_payment;
								$get_saledp_cancel	= mysqli_query($conpos,$sql_saledp_cancel);
								$total_saledp_cancel	= mysqli_num_rows($get_saledp_cancel);
								$isale_dp_cancel		= 0;
								while($dat_sale_dp_cancel	= mysqli_fetch_object($get_saledp_cancel)){
									$_SESSION['isale_dp_cancel'] = $isale_dp_cancel;
									$percent_sale_dp_cancel = intval($isale_dp_cancel/$total_saledp_cancel * 100)."%"; 
									//echo "(".$ipay."/".$total_pay ."* 100)"."%";
									usleep( 250000 ); // Here send pusat
									
									$ch = curl_init(); 
									curl_setopt($ch, CURLOPT_URL, 'https://online.hokben.net/digital_system/index.php/transferdata/sales_dp_cancel');
									curl_setopt($ch,CURLOPT_USERAGENT,'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');
									curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
									curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
									curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);     
									curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
									curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($dat_sale_dp_cancel));
									$return_out = curl_exec($ch); 
									curl_close($ch); 
									if($return_out === false)
									{
										$output	= 'Curl error: ' . curl_error($ch).$url;
									}
									else
									{
										$output = $return_out; 
									}
									print_r($output);

									echo '<script>
									parent.document.getElementById("progressbar_sales_dp_cancel").innerHTML="<div style=\"width:'.$percent_sale_dp_cancel.';background:linear-gradient(to bottom, rgba(125,126,125,1) 0%,rgba(14,14,14,1) 100%); ;height:20px;\">&nbsp;</div>";
									parent.document.getElementById("information_sales_dp_cancel").innerHTML="<div style=\"text-align:center; font-weight:bold\">Sales DP Cancel'.$dat_sale_dp_cancel->ID.' is processed.</div>";</script>';
									ob_flush(); 
									flush();
									$isale_dp_cancel = $isale_dp_cancel + 1;
								}
							
							echo '<script>
									parent.document.getElementById("progressbar_sales_dp_cancel").innerHTML="<div style=\"width:100%;background:linear-gradient(to bottom, rgba(125,126,125,1) 0%,rgba(14,14,14,1) 100%); ;height:20px;\">&nbsp;</div>";
									parent.document.getElementById("information_sales_dp_cancel").innerHTML="<div style=\"text-align:center; font-weight:bold\">Sales DP  Cancel Proses Complete.</div>";</script>';
							/*		
							$sql_sales_print	= " select * from sales_print where ID IN ('".implode("','",$id)."') order by ID ASC";
								//echo $sql_payment;
								$get_sales_print	= mysqli_query($conpos,$sql_sales_print);
								$total_sales_print	= mysqli_num_rows($get_sales_print);
								$sales_print		= 0;
								while($dat_sales_print	= mysqli_fetch_object($get_sales_print)){
									$_SESSION['sales_print'] = $dat_sales_print;
									$percent_sale_print = intval($sales_print/$total_sales_print * 100)."%"; 
									//echo "(".$ipay."/".$total_pay ."* 100)"."%";
									usleep( 250000 ); // Here send pusat
									
									$ch = curl_init(); 
									curl_setopt($ch, CURLOPT_URL, 'https://online.hokben.net/digital_system/index.php/transferdata/sales_print');
									curl_setopt($ch,CURLOPT_USERAGENT,'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');
									curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
									curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
									curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);     
									curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
									curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($dat_sales_print));
									$return_out = curl_exec($ch); 
									curl_close($ch); 
									if($return_out === false)
									{
										$output	= 'Curl error: ' . curl_error($ch).$url;
									}
									else
									{
										$output = $return_out; 
									}
									print_r($output);

									echo '<script>
									parent.document.getElementById("progressbar_sales_print").innerHTML="<div style=\"width:'.$percent_sale_print.';background:linear-gradient(to bottom, rgba(125,126,125,1) 0%,rgba(14,14,14,1) 100%); ;height:20px;\">&nbsp;</div>";
									parent.document.getElementById("information_sales_print").innerHTML="<div style=\"text-align:center; font-weight:bold\">Sales Print '.$dat_sales_print->ID.' is processed.</div>";</script>';
									ob_flush(); 
									flush();
									$sales_print = $sales_print + 1;
								}
							
							echo '<script>
									parent.document.getElementById("progressbar_sales_print").innerHTML="<div style=\"width:100%;background:linear-gradient(to bottom, rgba(125,126,125,1) 0%,rgba(14,14,14,1) 100%); ;height:20px;\">&nbsp;</div>";
									parent.document.getElementById("information_sales_print").innerHTML="<div style=\"text-align:center; font-weight:bold\">Sales Print  Proses Complete.</div>";</script>';
							*/
							
							$sql_sales_vch	= " select * from sales_vch where ID IN ('".implode("','",$id)."') order by ID ASC";
								//echo $sql_payment;
								$get_sales_vch	= mysqli_query($conpos,$sql_sales_vch);
								$total_sales_vch	= mysqli_num_rows($get_sales_vch);
								$sales_vch		= 0;
								while($dat_sales_vch	= mysqli_fetch_object($get_sales_vch)){
									$_SESSION['sales_print'] = $dat_sales_vch;
									$percent_sale_vch = intval($sales_vch/$total_sales_vch * 100)."%"; 
									//echo "(".$ipay."/".$total_pay ."* 100)"."%";
									usleep( 250000 ); // Here send pusat
									
									$ch = curl_init(); 
									curl_setopt($ch, CURLOPT_URL, 'https://online.hokben.net/digital_system/index.php/transferdata/sales_vch');
									curl_setopt($ch,CURLOPT_USERAGENT,'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');
									curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
									curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
									curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);     
									curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
									curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($dat_sales_vch));
									$return_out = curl_exec($ch); 
									curl_close($ch); 
									if($return_out === false)
									{
										$output	= 'Curl error: ' . curl_error($ch).$url;
									}
									else
									{
										$output = $return_out; 
									}
									
									echo '<script>
									parent.document.getElementById("progressbar_sales_vch").innerHTML="<div style=\"width:'.$percent_sale_vch.';background:linear-gradient(to bottom, rgba(125,126,125,1) 0%,rgba(14,14,14,1) 100%); ;height:20px;\">&nbsp;</div>";
									parent.document.getElementById("information_sales_vch").innerHTML="<div style=\"text-align:center; font-weight:bold\">Sales Voucher '.$dat_sales_vch->ID.' is processed.</div>";</script>';
									ob_flush(); 
									flush();
									$sales_vch = $sales_vch + 1;
								}
							
							echo '<script>
									parent.document.getElementById("progressbar_sales_vch").innerHTML="<div style=\"width:100%;background:linear-gradient(to bottom, rgba(125,126,125,1) 0%,rgba(14,14,14,1) 100%); ;height:20px;\">&nbsp;</div>";
									parent.document.getElementById("information_sales_vch").innerHTML="<div style=\"text-align:center; font-weight:bold\">Sales Voucher  Proses Complete.</div>";</script>';
									
							$sql_sales_void	= " select * from sales_void where ID IN ('".implode("','",$id)."') order by ID ASC";
								//echo $sql_payment;
								$get_sales_void	= mysqli_query($conpos,$sql_sales_void);
								$total_sales_void	= mysqli_num_rows($get_sales_void);
								$sales_void	= 0;
								while($dat_sales_void	= mysqli_fetch_object($get_sales_void)){
									$_SESSION['sales_print'] = $dat_sales_void;
									$percent_sale_void = intval($sales_void/$total_sales_void * 100)."%"; 
									//echo "(".$ipay."/".$total_pay ."* 100)"."%";
									usleep( 250000 ); // Here send pusat
									
									$ch = curl_init(); 
									curl_setopt($ch, CURLOPT_URL, 'https://online.hokben.net/digital_system/index.php/transferdata/sales_void');
									curl_setopt($ch,CURLOPT_USERAGENT,'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');
									curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
									curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
									curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);     
									curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
									curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($dat_sales_void));
									$return_out = curl_exec($ch); 
									curl_close($ch); 
									if($return_out === false)
									{
										$output	= 'Curl error: ' . curl_error($ch).$url;
									}
									else
									{
										$output = $return_out; 
									}

									echo '<script>
									parent.document.getElementById("progressbar_sales_void").innerHTML="<div style=\"width:'.$percent_sale_void.';background:linear-gradient(to bottom, rgba(125,126,125,1) 0%,rgba(14,14,14,1) 100%); ;height:20px;\">&nbsp;</div>";
									parent.document.getElementById("information_sales_void").innerHTML="<div style=\"text-align:center; font-weight:bold\">Sales VOID '.$dat_sales_void->ID.' is processed.</div>";</script>';
									ob_flush(); 
									flush();
									$sales_void = $sales_void + 1;
								}
							
							echo '<script>
									parent.document.getElementById("progressbar_sales_void").innerHTML="<div style=\"width:100%;background:linear-gradient(to bottom, rgba(125,126,125,1) 0%,rgba(14,14,14,1) 100%); ;height:20px;\">&nbsp;</div>";
									parent.document.getElementById("information_sales_void").innerHTML="<div style=\"text-align:center; font-weight:bold\">Sales VOID  Proses Complete.</div>";</script>';
									
							$sql_trx_ctl	= " select * from trx_ctl where TRX_DATE = '".$_POST['tgl_trans']."'";
								//echo $sql_payment;
								$get_trx_ctl	= mysqli_query($conpos,$sql_trx_ctl);
								$total_trx_ctl	= mysqli_num_rows($get_trx_ctl);
								$trx_ctl	= 0;
								while($dat_trx_ctl	= mysqli_fetch_object($get_trx_ctl)){
									$_SESSION['trx_ctl'] = $dat_trx_ctl;
									$percent_trx_ctl = intval($trx_ctl/$total_trx_ctl * 100)."%"; 
									//echo "(".$ipay."/".$total_pay ."* 100)"."%";
									usleep( 250000 ); // Here send pusat
									
									$ch = curl_init(); 
									curl_setopt($ch, CURLOPT_URL, 'https://online.hokben.net/digital_system/index.php/transferdata/trx_ctl');
									curl_setopt($ch,CURLOPT_USERAGENT,'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');
									curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
									curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
									curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);     
									curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
									curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($dat_trx_ctl));
									$return_out = curl_exec($ch); 
									curl_close($ch); 
									if($return_out === false)
									{
										$output	= 'Curl error: ' . curl_error($ch).$url;
									}
									else
									{
										$output = $return_out; 
									}
									
									echo $output;

									echo '<script>
									parent.document.getElementById("progressbar_trx_ctl").innerHTML="<div style=\"width:'.$percent_trx_ctl.';background:linear-gradient(to bottom, rgba(125,126,125,1) 0%,rgba(14,14,14,1) 100%); ;height:20px;\">&nbsp;</div>";
									parent.document.getElementById("information_trx_ctl").innerHTML="<div style=\"text-align:center; font-weight:bold\">TRX CTL '.$dat_trx_ctl->TRX_ID.' is processed.</div>";</script>';
									ob_flush(); 
									flush();
									$trx_ctl = $trx_ctl + 1;
								}
							
							echo '<script>
									parent.document.getElementById("progressbar_trx_ctl").innerHTML="<div style=\"width:100%;background:linear-gradient(to bottom, rgba(125,126,125,1) 0%,rgba(14,14,14,1) 100%); ;height:20px;\">&nbsp;</div>";
									parent.document.getElementById("information_trx_ctl").innerHTML="<div style=\"text-align:center; font-weight:bold\">TRX CTL  Proses Complete.</div>";</script>';
							
								session_destroy(); 
							}
							
								echo '<script>
									parent.document.getElementById("btn_success").innerHTML="<a href=\"dashboard.php\" class=\"btn btn-warning\" style=\"text-align:center; font-weight:bold;width:100%;font-size:20px;margin:0 auto;\">All Transfer Success Click to Back</a>";</script>';
						?>
					
					</div>
				</div>
			</div>
		</div>
	</body>
</html>

