<?php 
	session_start();
	date_default_timezone_set("Asia/Bangkok");
	include("dbconnect.php");
	include("template/header_h2h.php");
	
	
?>
	<div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="page-breadcrumb">
                <div class="row align-items-center">
					<div class="col-xs-12 col-md-12 col-sm-12">
						<div class="card">
							<div class="card-body">
								<div class="card-title">Setup POS Android</div>
							</div>
							<div>
								<hr class="m-t-0 m-b-0">
							</div>
							<div class="card-body">
								<table class="table table-striped">
									<tr>
										<th>
											Description
										</th>
										<th>
											Value
										</th>
										<th>
											Action
										</th>
									</tr>
									<tr>
										<td>
											<?php echo $data_ippos->DESCRIPTION ?>
										</td>
										<td>
											<?php echo $data_ippos->VALUE ?>
										</td>
										<td>
											<a href = '<?php echo  $base_url; ?>setup_app_edit.php?id=<?php echo $data_ippos->ID ?>' class="btn btn-danger" >Edit</a>
										</td>
									</tr>
									<tr>
										<td>
											<?php echo $data_amtdl->DESCRIPTION ?>
										</td>
										<td>
											<?php echo number_format($data_amtdl->VALUE) ?>
										</td>
										<td>
											<a href = '<?php echo  $base_url; ?>setup_app_edit.php?id=<?php echo $data_amtdl->ID ?>' class="btn btn-danger" >Edit</a>
										</td>
									</tr>
									<tr>
										<td>
											<?php echo $data_idpos->DESCRIPTION ?>
										</td>
										<td>
											<?php echo $data_idpos->VALUE ?>
										</td>
										<td>
											<a href = '<?php echo  $base_url; ?>setup_app_edit.php?id=<?php echo $data_idpos->ID ?>' class="btn btn-danger" >Edit</a>
										</td>
									</tr>
									<tr>
										<td>
											<?php echo $data_tr->DESCRIPTION ?>
										</td>
										<td>
											<?php echo $data_tr->VALUE ?>
										</td>
										<td>
											<a href = '<?php echo  $base_url; ?>setup_app_edit.php?id=<?php echo $data_tr->ID ?>' class="btn btn-danger" >Edit</a>
										</td>
									</tr>
									<tr>
										<td>
											<?php echo $data_deplu->DESCRIPTION ?>
										</td>
										<td>
											<?php echo $data_deplu->VALUE ?>
										</td>
										<td>
											<a href = '<?php echo  $base_url; ?>setup_app_edit.php?id=<?php echo $data_deplu->ID ?>' class="btn btn-danger" >Edit</a>
										</td>
									</tr>
									<tr>
										<td>
											<?php echo $data_urlsrv->DESCRIPTION ?>
										</td>
										<td>
											<?php echo $data_urlsrv->VALUE ?>
										</td>
										<td>
											<a href = '<?php echo  $base_url; ?>setup_app_edit.php?id=<?php echo $data_urlsrv->ID ?>' class="btn btn-danger" >Edit</a>
										</td>
									</tr>
									<tr>
										<td>
											<?php echo $data_pludp->DESCRIPTION ?>
										</td>
										<td>
											<?php echo $data_pludp->VALUE ?>
										</td>
										<td>
											<a href = '<?php echo  $base_url; ?>setup_app_edit.php?id=<?php echo $data_pludp->ID ?>' class="btn btn-danger" >Edit</a>
										</td>
									</tr>
									<tr>
										<td>
											<?php echo $data_urlnewcounter->DESCRIPTION ?>
										</td>
										<td>
											<?php echo $data_urlnewcounter->VALUE ?>
										</td>
										<td>
											<a href = '<?php echo  $base_url; ?>setup_app_edit.php?id=<?php echo $data_urlnewcounter->ID ?>' class="btn btn-danger" >Edit</a>
										</td>
									</tr>
									<tr>
										<td>
											<?php echo $data_kp->DESCRIPTION ?>
										</td>
										<td>
											<?php echo $data_kp->VALUE ?>
										</td>
										<td>
											<a href = '<?php echo  $base_url; ?>setup_app_edit.php?id=<?php echo $data_kp->ID ?>' class="btn btn-danger" >Edit</a>
										</td>
									</tr>
									<tr>
										<td>
											<?php echo $data_urlapi->DESCRIPTION ?>
										</td>
										<td>
											<?php echo $data_urlapi->VALUE ?>
										</td>
										<td>
											<a href = '<?php echo  $base_url; ?>setup_app_edit.php?id=<?php echo $data_urlapi->ID ?>' class="btn btn-danger" >Edit</a>
										</td>
									</tr>
								</table>
							</div>
						</div>
						
					</div>
				</div>
			</div>
		</div>
<?php include("template/footer_h2h.php");?>
<?php
	if($_POST){
		
	
		
	}
?>