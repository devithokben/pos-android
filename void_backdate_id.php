<?php 
	date_default_timezone_set("Asia/Bangkok");
	include("dbconnect.php");
	include("template/header_h2h.php");
?>
	<style>
		.ui-datepicker table {
			font-size: 20px;
		}
		.ui-datepicker {
			width: 300px;
		}
		.ui-widget {
			font-family: Arial,Helvetica,sans-serif;
			font-size: 20px;
		}
	</style>
	<script>
		var base_url		= "<?php echo $base_url ?>";
	</script>
	<script src="js/function.js" type="text/javascript"></script>
	<div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="page-breadcrumb">
                <div class="row align-items-center">
					<div class="col-xs-12 col-md-12 col-sm-12">
						<h3 class="alert alert-warning">Masukan Id Transaksi Yang Akan Di VOID</h3>
						<hr />
						<form method="POST" action="<?php $_SERVER['PHP_SELF'] ?>">
							<div class="form-group">
								<label for="exampleInputPassword1"><h4>TRX ID</h4></label>
								<input type="text" class="form-control character" name="trx_id" placeholder="Masukan ID Transaksi disini">
							</div>
							<div class="form-group">
								<button type="submit" class="btn btn-primary">PROCESS</button>
								<a href="void_backdate_newbill.php" class="btn btn-warning">New Bill</a>
							</div>
						</form>
					</div>
					<div class="col-xs-12 col-md-12 col-sm-12">
						<h3 class="alert alert-danger">Detail Transaksi</h3>
						<hr />
						<?php
							if($_POST['trx_id']){
						?>
						<form id="frm_void">
							<table width='100%'>
								<tr>
									<td width='60%' >
										<?php
											$getTrans	= mysqli_query($conpos,"select * from sales_hdr where ID = '".$_POST['trx_id']."'");
											$dataTrans	= mysqli_fetch_object($getTrans);
											//cek void
											$getTransVoid	= mysqli_query($conpos,"select * from sales_void where ID = '".$_POST['trx_id']."'");
											$dataTransVoid	= mysqli_fetch_object($getTransVoid);
											if($dataTrans && !$dataTransVoid){
												
												$getBranch	= mysqli_query($conpos,"select * from tbl_lst_branch");
												$dataBranch	= mysqli_fetch_object($getBranch);
												
														echo 	"
																<div style='height:700px;width:90%;overflow:scroll'>
																<table width='90%'>
																	<tr>
																		<td colspan='3' align='center'> JAPANESE RESTAURANT </td>
																	</tr>
																	<tr>
																		<td colspan='3' align='center'> ".$dataBranch->BRANCH_NAME." </td>
																	</tr>
																	<tr>
																		<td colspan='3' align='center'> TLP : ".$dataBranch->PHONE." </td>
																	</tr>
																	<tr>
																		<td colspan='3'> <b>BILL NO :</b> ".$_POST['trx_id']." </td>
																	</tr>
																	<tr>
																		<td colspan='3'> ".$dataTrans->ENT_USER.", ".$dataTrans->ENT_DATE. " GUEST ".$dataTrans->GUEST."<hr /></td>
																	</tr>";
																
																$getdetail	= mysqli_query($conpos,"select ITEM_TYPE from sales_dtl where ID = '".$_POST['trx_id']."' GROUP BY ITEM_TYPE");
																while($dataDetail	= mysqli_fetch_object($getdetail)){
																	echo "<tr>
																			<td colspan='3'> <b>".$dataDetail->ITEM_TYPE."</b></td>
																		</tr>";
																	$total_item = 0;
																	$total_amount = 0;
																	$getlistdetail	= mysqli_query($conpos,"select sales_dtl.*,products.PRODNAME as nmProd from sales_dtl left join products on sales_dtl.PRODCODE = products.PRODCODE where ID = '".$_POST['trx_id']."' and ITEM_TYPE = '".$dataDetail->ITEM_TYPE."' order by ENT_DATE DESC");
																	while($dataListDetail	= mysqli_fetch_object($getlistdetail)){
																		echo "<tr>
																			<td> ".number_format($dataListDetail->QTY)."</td>
																			<td> ".$dataListDetail->nmProd."</td>
																			<td align='right'> ".number_format($dataListDetail->AMOUNT)."</td>
																		</tr>";
																		$total_item 	=	$total_item + $dataListDetail->QTY;
																		$total_amount	= 	$total_amount + $dataListDetail->AMOUNT;
																	}
																};
																
																//get sales_payment
																$getPayment	= mysqli_query($conpos,"select sales_payment.*,tbl_lst_bayard.DESCRIPTION from sales_payment left join tbl_lst_bayard on sales_payment.EDC_VDR = tbl_lst_bayard.PARAM where sales_payment.ID ='".$_POST['trx_id']."'");
																$dataPayement	= mysqli_fetch_object($getPayment);
																
																echo "	<tr>
																			<td colspan='3'><hr /></td>
																		</tr>";
																echo "	<tr>
																			<td> ".number_format($total_item)." Item</td>
																			<td> <b>SUB TOTAL </b></td>
																			<td align='right'> <b>".number_format($dataPayement->GROSS_SALES)."</b></td>
																		</tr>
																		<tr>
																			<td></td>
																			<td>ITEM DISCOUNT %</td>
																			<td align='right'> <b>".number_format($dataPayement->DISC_ITEM)."</b></td>
																		</tr>
																		<tr>
																			<td></td>
																			<td>SERVICE CHARGE</td>
																			<td align='right'> <b>".number_format(0)."</b></td>
																		</tr>
																		<tr>
																			<td></td>
																			<td>TA CHARGE</td>
																			<td align='right'> <b>".number_format($dataPayement->TACHARGE)."</b></td>
																		</tr>
																		<tr>
																			<td></td>
																			<td>PJK RESTO 10%</td>
																			<td align='right'> <b>".number_format($dataPayement->TAX)."</b></td>
																		</tr>
																		<tr>
																			<td></td>
																			<td>PEMBULATAN</td>
																			<td align='right'> <b>".number_format($dataPayement->ROUNDING)."</b></td>
																		</tr>
																		<tr>
																			<td></td>
																			<td><b>TOTAL</b></td>
																			<td align='right'> <b>".number_format($dataPayement->SUB_TOTAL)."</b></td>
																		</tr>
																		<tr>
																			<td></td>
																			<td>NON TUNAI</td>
																			<td align='right'> <b>".number_format($dataPayement->NON_CASH)."</b></td>
																		</tr>
																		<tr>
																			<td></td>
																			<td><b>".$dataPayement->DESCRIPTION."</b></td>
																			<td align='right'> <b>".number_format($dataPayement->CASH)."</b></td>
																		</tr>";
																		if($dataPayement->edc_vdr == 0){
																			$pay_amount = $dataPayement->CASH;
																		}elseif($dataPayement->edc_vdr == 1){
																			$pay_amount = $dataPayement->FBCA;
																		}else{
																			$pay_amount = $dataPayement->CARD_AMT;
																		}
																		
																echo"	<tr>
																			<td></td>
																			<td>KEMBALI</td>
																			<td align='right'> <b>".number_format($dataPayement->SUB_TOTAL - $pay_amount)."</b></td>
																		</tr>
																		<tr>
																			<td colspan='3' align='center'><hr /> TOKYO CURRY !! </td>
																		</tr>
																		<tr>
																			<td colspan='3' align='center'> THE TASTE OF JAPANESE CURRY </td>
																		</tr>
																		<tr>
																			<td colspan='3' align='center'> SARAN DAN KRITIK HUBUNGIN XXXX </td>
																		</tr>
																		<tr>
																			<td colspan='3' align='center'> <h2> COPY STRUK </h2> </td>
																		</tr>
																		";
																	
														echo"	</table></div>";
											
										?>
									<td>
									<?php
										//getuser
										$getuser	= mysqli_query($conpos,"select * from tbluser where USER_1STLOGIN = 1" );
										$datauser	= mysqli_fetch_object($getuser);	

										$getButton	= mysqli_query($conpos,"select * from tbl_lst_void_reason");
										while($dataBtn	= mysqli_fetch_object($getButton)){
											if($dataBtn->ID == 1){
												$selec = "CHECKED";
											}else{
												$selec = '';
											}
											echo '	<input type="radio" id="male" name="radioReason" value="'.$dataBtn->ID.'" '.$selec.'>
													<label for="male">'.$dataBtn->DESCRIPTION.'</label><hr />';
										}
										echo "<Textarea width='100%' id='reason_void' name='desc_reason' class='character'> </textarea><hr />";
										echo "<input type='button' class='btn btn-danger' id='btn-void-ok' value='VOID' onclick= 'proc_void_backdate()' />&nbsp;&nbsp;&nbsp;";
										echo "<input type='hidden' name='void_username'   id='voidusername' value='$datauser->USER_ID'  />";
										echo "<input type='hidden' name='struk_id_void'  value='".$_POST['trx_id']."' id='txtStrukVoid'  />";
									}else{
										echo "
										<h3 class='alert alert-danger'>Maaf, Struk Sudah Pernah Divoid</h3>";
									}
									?>
								</td>
									</td>
								</tr>
							</table>
						</form>
						<?php
							}
						?>
					</div>
				</div>
			</div>
		</div>
<?php include("template/footer_h2h.php");?>
<script>
	$(document).ready(function() {
		$('#table_id').DataTable();
	} );
</script>
<link href="Keyboard/css/keyboard.css" rel="stylesheet">
<script src="Keyboard/js/jquery.keyboard.js"></script>

<!-- keyboard extensions (optional) -->
<script src="Keyboard/js/jquery.mousewheel.js"></script>
<script>
	$(function(){
		$('.numeric').keyboard({layout : 'num' });
		$('.character').keyboard({layout: 'qwerty'});
	});
</script>
