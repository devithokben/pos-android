<?php
/*
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);*/
error_reporting(0);
session_start();

$ESC = chr(27);
$bolds 		= $ESC.'G'.chr(1);
$bolde 		= $ESC.'G'.chr(0);
$bigfont	= $ESC."!".chr(16);
$bigfontend	= $ESC.'!'.chr(8);

$arr_layanan = 0;
	date_default_timezone_set("Asia/Bangkok");
	require '../../dbconnect.php';
	//$_GET['id'] = '11501.1.1';
	$getTrans	= mysqli_query($conpos,"select * from sales_hdr where ID = '".$_GET['id']."'");
	//$getTrans	= mysqli_query($conpos,"select * from sales_hdr where ID = '11501.1.4'");
	$dataTrans	= mysqli_fetch_object($getTrans);
	if($dataTrans){
		
		$getBranch	= mysqli_query($conpos,"select * from tbl_lst_branch");
		$dataBranch	= mysqli_fetch_object($getBranch);
		
		//getCprofile
		$getProfile	= mysqli_query($conpos,"select * from cprofile");
		$dataProf	= mysqli_fetch_object($getProfile);	
		
	function encryp($plaintext){	
		$key 		= '05788993F8E4CE6A';
		//$plaintext 	= $_POST['text'];

		$ivlen = openssl_cipher_iv_length($cipher="AES-128-CBC");
		$iv = openssl_random_pseudo_bytes($ivlen);
		$ciphertext_raw = openssl_encrypt($plaintext, $cipher, $key, $options=OPENSSL_RAW_DATA);
		//$hmac = hash_hmac('sha256', $ciphertext_raw, $key, $as_binary=true);
		$ciphertext = base64_encode( $ciphertext_raw );
		return $ciphertext;
	}
	//echo $dataBranch->BRANCH_ID.":::".encryp($dataBranch->BRANCH_ID)
?>

<a href="#" onclick="printQrCode('<?php echo encryp($dataBranch->BRANCH_ID)?>','<?php echo encryp($_GET['id']) ?>')" style="font-size:16px;">Print Report</a>
<?php		
echo"<pre id='pre_print' style='width:80mm;'>";

echo"
&nbsp; 
".str_pad("JAPANESE RESTAURANT",50," ",STR_PAD_BOTH).str_pad($dataBranch->BRANCH_NAME,45," ",STR_PAD_BOTH).str_pad("TLP: ".$dataBranch->PHONE,50," ",STR_PAD_BOTH).chr(2)."	
BILL NO :</b> ".$bolds.$_GET['id'].$bolde."  
".$dataTrans->ENT_USER.", ".$dataTrans->ENT_DATE. " GUEST ".$dataTrans->GUEST."
------------------------------------------------";
$total_item = 0;
$total_amount = 0;
$getdetail	= mysqli_query($conpos,"select ITEM_TYPE from sales_dtl where ID = '".$_GET['id']."' GROUP BY ITEM_TYPE");
while($dataDetail	= mysqli_fetch_object($getdetail)){
	//echo "masuk";
if($dataDetail->ITEM_TYPE == 'DI'){
	$layanan = 'DINE IN';
}else if($dataDetail->ITEM_TYPE == 'TA'){
	$arr_layanan = 1;
	$layanan = 'TAKE AWAY';
}else if($dataDetail->ITEM_TYPE == 'DL'){
	$arr_layanan = 1;
	$layanan = 'DELIVERY';
}else if($dataDetail->ITEM_TYPE == 'GR'){
	$arr_layanan = 1;
	$layanan = 'GRAB FOOD';
}else if($dataDetail->ITEM_TYPE == 'GF'){
	$arr_layanan = 1;
	$layanan = 'GO FOOD';
}else if($dataDetail->ITEM_TYPE == 'SF'){
	$arr_layanan = 1;
	$layanan = 'SHOPEE FOOD';
}else{
	$layanan = $dataDetail->ITEM_TYPE;
}

if($dataTrans->DELIVERY_NO){
	if(substr($dataTrans->DELIVERY_NO,0,2) == 'GR'){
	$layanan = "GRAB - NO : ".str_replace("#",'#',$dataTrans->DELIVERY_NO)."";
	}else if(substr($dataTrans->DELIVERY_NO,0,2) == 'SF'){
	$layanan = "SHOPEE FOOD - NO : ".str_replace("#",'#',$dataTrans->DELIVERY_NO)."";
	}else if(substr($dataTrans->DELIVERY_NO,0,1) == 'G'){
	$layanan =  "GO FOOD - NO : ".str_replace("#",'#',$dataTrans->DELIVERY_NO)."";
	}else{
		$layanan =  "DELIVERY : ".str_replace("#",'#',$dataTrans->DELIVERY_NO)."";
	}
}

echo "
".$bolds.$layanan.$bolde."\n";

$getlistdetail	= mysqli_query($conpos,"select sales_dtl.*,products.PRODNAME as nmProd from sales_dtl left join products on sales_dtl.PRODCODE = products.PRODCODE where ID = '".$_GET['id']."' and ITEM_TYPE = '".$dataDetail->ITEM_TYPE."' order by SEQ_NO ASC");
while($dataListDetail	= mysqli_fetch_object($getlistdetail)){
	if($dataListDetail->DISC_AMOUNT){
		$ket = 'DSC';
	}else{
		$ket = '   ';
	}
echo "
&nbsp;&nbsp;&nbsp;".number_format($dataListDetail->QTY)."&nbsp;&nbsp;".str_pad(substr($dataListDetail->nmProd,0,20),27," ",STR_PAD_RIGHT).$ket.str_pad(number_format($dataListDetail->PRICE * $dataListDetail->QTY),10," ",STR_PAD_LEFT);
$total_item 	=	$total_item + $dataListDetail->QTY;
$total_amount	= 	$total_amount + $dataListDetail->PRICE;
}
}
$getPayment	= mysqli_query($conpos,"select sales_payment.*,tbl_lst_bayard.DESCRIPTION from sales_payment left join
tbl_lst_bayard on sales_payment.EDC_VDR = tbl_lst_bayard.PARAM where sales_payment.ID ='".$_GET['id']."'");
$dataPayement	= mysqli_fetch_object($getPayment);
//print_r($dataPayement);
echo "	
------------------------------------------------
".$bolds.number_format($total_item)." Item ".$bolde;
echo "
\n&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".str_pad("SUB TOTAL",31," ",STR_PAD_RIGHT).str_pad(number_format($dataPayement->GROSS_SALES),10," ",STR_PAD_LEFT);
if($dataPayement->ITEM_DISCOUNT != 0){
echo "
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".str_pad("ITEM DISCOUNT ".number_format($dataPayement->DISCVALUE)."%",31," ",STR_PAD_RIGHT).str_pad(number_format($dataPayement->ITEM_DISCOUNT),10," ",STR_PAD_LEFT);
}
if($dataPayement->SALES_DISCOUNT != 0){
echo "
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".str_pad("SALES DISCOUNT ".number_format($dataPayement->DISCVALUE)."%",31," ",STR_PAD_RIGHT).str_pad(number_format($dataPayement->SALES_DISCOUNT),10," ",STR_PAD_LEFT);
}
echo"
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".str_pad("SERVICE CHARGE",31," ",STR_PAD_RIGHT).str_pad(number_format($dataPayement->TOTAL_COST),10," ",STR_PAD_LEFT)."
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".str_pad("TA CHARGE",31," ",STR_PAD_RIGHT).str_pad(number_format($dataPayement->TACHARGE),10," ",STR_PAD_LEFT)."
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".str_pad("PJK RESTO 10%",31," ",STR_PAD_RIGHT).str_pad(number_format($dataPayement->TAX),10," ",STR_PAD_LEFT)."
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".str_pad("PEMBULATAN",31," ",STR_PAD_RIGHT).str_pad(number_format("-".$dataPayement->ROUNDING),10," ",STR_PAD_LEFT)."
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$bolds.str_pad("TOTAL",31," ",STR_PAD_RIGHT).str_pad(number_format($dataPayement->SUB_TOTAL),10," ",STR_PAD_LEFT).$bolde."
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".str_pad("NON TUNAI",31," ",STR_PAD_RIGHT).str_pad(number_format($dataPayement->NON_CASH),10," ",STR_PAD_LEFT);
if($dataPayement->edc_vdr == 0){
	$pay_amount = $dataPayement->CASH;
}elseif($dataPayement->edc_vdr == 1){
	$pay_amount = $dataPayement->FBCA;
}else{
	$pay_amount = $dataPayement->CARD_AMT;
}
echo "
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$bolds.str_pad($dataPayement->DESCRIPTION,31," ",STR_PAD_RIGHT).str_pad(number_format($pay_amount),10," ",STR_PAD_LEFT).$bolde;
//echo $dataPayement->SUB_TOTAL ."-". $pay_amount ."-". $dataPayement->ROUNDING;
echo "
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".str_pad("KEMBALI",31," ",STR_PAD_RIGHT).str_pad(number_format($dataPayement->REFUND ),10," ",STR_PAD_LEFT);
echo "
------------------------------------------------
".str_pad($dataProf->MSG_STROOK_1,50," ",STR_PAD_BOTH).str_pad($dataProf->MSG_STROOK_2,45," ",STR_PAD_BOTH).str_pad($dataProf->MSG_STROOK_3,50," ",STR_PAD_BOTH).str_pad($dataProf->MSG_STROOK_4,50," ",STR_PAD_BOTH);
echo "
------------------------------------------------
".base64_decode($dataPayement->PROMODESC)."NOTE :  ".str_replace("#","::",$dataPayement->NOTE);
if ($_GET['act'] == 'void'){
echo"
----------------------------------------------
".$bigfont.str_pad("VOID ",50," ",STR_PAD_BOTH).$bigfontend."";
}if ($_GET['act'] == 'reprint'){
echo"
----------------------------------------------
".$bigfont.str_pad("COPY STRUK ",50," ",STR_PAD_BOTH).$bigfontend."";
}if ($arr_layanan == 1){
echo"
Diterima dalam keadaan baik & Lengkap
Oleh :";
echo"





----------------------------------------------";
}
echo "</pre>";	
					
	
	}else{
		echo "select * from sales_hdr where ID = '".$_GET['id']."'";
	}
?>
 <script type="text/javascript">
	function showAndroidToast(toast) {
		Android.showToast(encodeURI(toast));
	}
</script>

<script src="app.js" charset="utf-8" async></script>
<script>

window.onload = function() {
	
	var textEncoded = encodeURI(document.getElementById('pre_print').innerText);
    Android.showToast(textEncoded);
	
	printQrCode('<?php echo encryp($dataBranch->BRANCH_ID)?>','<?php echo encryp($_GET['id']) ?>');
	
	//location.href = "<?php echo  $base_url; ?>system_pos.php";
	
}

function chr(x){
	return String.fromCharCode(x);
}
// symbolic
var ESC = chr(27);
var LF = chr(10);

// user friendly command name
var PrnAlignLeft = ESC+'a'+chr(0);
var PrnAlignCenter = ESC+'a'+chr(1);
var PrnAlignRight = ESC+'a'+chr(2);
var PrnBoldOn = ESC+'G'+chr(1);
var PrnBoldOff = ESC+'G'+chr(0);


function BtPrint(prn){
        var S = "#Intent;scheme=rawbt;";
        var P =  "package=ru.a402d.rawbtprinter;end;";
        var textEncoded = encodeURI(prn);
        window.location.href="intent:"+textEncoded+S+P;

}
 function QrCode(s){
        var q = ESC+'Z'+chr(3)+chr(3)+chr(6);
        var s_len = s.length;
        q += chr(s_len%256)+chr(s_len/256);
		alert(q+s);
        return q+s;
}

// demo document
function slip(){
	
	// собираем чек
	var prn = '';
	prn += PrnAlignCenter;
	prn += PrnBoldOn+'Nasi Goreng'+PrnBoldOff+LF;
	prn += '--------------------------------'+LF;
	prn += PrnAlignRight+PrnBoldOn+'ИТОГ: 7300.00'+PrnBoldOff+LF;
	prn += LF;
	prn += PrnAlignLeft;
	prn += 'Зав.№ККТ : 0149060506089651'+LF;
	prn += 'ФН №     : 0149060506089651'+LF;
	prn += 'ФД №     : 3'+LF;
	prn += 'ФПД      : 846945255'+LF;

	prn += LF;

	prn += PrnAlignCenter;
	prn += QrCode('t=20171016T201109&s=7300.00&fn=0149060506089651&i=3&fp=846945255&n=1')+LF;

	prn += LF;
	prn += LF;
	// передаем на печать
	BtPrint(prn);
}
    
</script>