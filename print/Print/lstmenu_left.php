<div class="col-xs-12 col-md-12 col-sm-12"> 
	<div class="row">
		<?php
			$newcounter	= $dataTrx->COUNTER_NO + 1;
		?>
		<div class="col-xs-6 col-md-6 col-sm-6">
			No : <?php echo $newcounter ?>
		</div>
		<div class="col-xs-6 col-md-6 col-sm-6">
			TGL : <?php echo date('d-m-Y h:s') ?>
		</div>
		<div class="col-xs-6 col-md-6 col-sm-6">
			ID : <?php echo $dataTrx->POS_ID.".".$dataTrx->TRX_ID.".".$newcounter ?>
		</div>
		<div class="col-xs-6 col-md-6 col-sm-6">
			TRX : <?php echo date('d-m-Y',strtotime($dataTrx->TRX_DATE)) ?>
		</div>
	</div>
</div>
<table width="100%"  cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="2" valign="top">
			<table width="100%" cellpadding="0" cellspacing="0" class="table-list" border="1">
				<tr>
					<th width="35%" style="display:none;">PLU</th>
					<th width="35%">PLU</th>
					<th width="10%" align="left">SVC</th>
					<th width="10%"  align="left">QTY</th>
					<th width="10%"  align="left">%</th>
					<th width="15%">HARGA</th>
					<th width="15%">JUMLAH</th>
					<th width="5%" style="display:none;"></th>
				</tr>
			</table>
			<form id="frmtrans" method="post" action="save_order.php">
				<div  style="overflow:scroll;height:180px" >
					<table width="100%" cellpadding="0" cellspacing="0" id="table-list" border="1">
						<tbody id="list"></tbody>
					</table>
				</div>
				
				<!--Detail Variable-->
				<input type="hidden" name="strukid" id="strukid" value="<?php echo $dataTrx->POS_ID.".".$dataTrx->TRX_ID.".".$newcounter ?>"/>
				<input type="hidden" name="trx_id" id="" value="<?php echo $dataTrx->TRX_ID + 1?>"/>
				<input type="hidden" name="trx_date" id="" value="<?php echo $dataTrx->TRX_DATE?>"/>
				<input type="hidden" name="branch_id" id="" value="<?php echo substr($dataTrx->POS_ID,0,3)?>"/>
				<input type="hidden" name="pos_id" id="" value="<?php echo $dataTrx->POS_ID?>"/>
				<input type="hidden" name="en_date" id="" value="<?php echo date("Y-m-d H:i:s");?>"/>
				<input type="hidden" name="en_user" id="" value="ROOT"/>
				
				<input type="hidden" name="pajakresto" id="pajakresto" value="0"/>
				<input type="hidden" name="subtotalval" id="subtotalval" value="0"/>
				<input type="hidden" name="pembulatan" id="pembulatan" value="0"/>
				
				<input type="hidden" name="service_charge" id="service_charge" value="0"/>
				<input type="hidden" name="svc_type" id="svc_type" value="0"/>
				<input type="hidden" name="svc_val" id="svc_val" value="0"/>
				
				
				<input type="hidden" name="discount" id="discount" value="0"/>
				<input type="hidden" name="tacharge" id="valtacharge" value="0"/>
				<input id="val_disc_type" name="val_disc_type" type="hidden" placeholder="menampung jenis discount sales / item" value="">
				<input id="val_disc_val" name="val_disc_val" type="hidden" placeholder="menampung jenis discount sales / item" value="">
				<input type="hidden" name="promo_id" id="promo_id" value="0"/>
				<input type="hidden" name="counter" id="counter" value="<?php echo  $newcounter ?>"/>
				<input type="hidden" name="totalpayval" id="totalpayval" placeholder="subtotalterakhir" value="0"/>
				<input type="hidden" name="tmpvalue" id="tmpvalue" placeholder="menampung id unik masing item" value="0"/>
				
				<input id="idrider" name="idrider" type="hidden" value="">
				<input id="layananVal" name="layananVal" type="hidden" value="">
				<input id="layanan_pref" name="layanan_pref" type="hidden" value="">
				<input id="delivery_id" name="delivery_id" type="hidden" value="">
				<input id="mask_delivery_id" name="mask_delivery_id" type="hidden" value="">
				<input id="referensi_pay" name="referensi_pay" type="hidden" value="">
				<input id="nik_kar" name="nik_kar" type="hidden" value="">
				<input id="nik_kar_child" name="nik_kar_child" type="hidden" value="">
				<input id="nama_kar_child" name="nik_kar_child" type="hidden" value="">
				
				<!-- End Detail Variable -->
			</form>
		</td>
	</tr>
</table>
