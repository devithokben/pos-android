 <script>
 function PosPrinterJob(driver, transport) {
        /**
         * @type EscPosDriver
         */
        this.driver = driver;

        /**
         * @type RawBtTransport
         */
        this.transport = transport;


        this.buffer = [];


        // ----------------------------------
        //  CONFIGURE
        // ----------------------------------
        this.encoding = 'CP437';
        this.characterTable = 0;

        this.setEncoding = function (encoding) {
            this.encoding = encoding;
            this.buffer.push(this.driver.setDefaultCharacterTable(encoding.toUpperCase()));
            return this;
        };
        this.setCharacterTable = function (number) {
            this.characterTable = number;
            this.buffer.push(this.driver.setCharacterTable(number));
            return this;
        };


        // ----------------------------------
        //  SEND TO PRINT
        // ----------------------------------

        this.execute = function () {
            this.transport.send(this.buffer.join(''));
            return this;
        };

        // ----------------------------------
        //  HIGH LEVEL FUNCTION
        // ----------------------------------

        this.initialize = function () {
            this.buffer.push(this.driver.initialize());
            return this;
        };


        /**
         *
         * @param {string} string
         */
        this.print = function (string, encoding) {
            let bytes = iconv.encode(string, encoding || this.encoding);
            let s = '';
            let self = this;
            bytes.forEach(function (b) {
                s = s + self.driver.encodeByte(b);
            });
            this.buffer.push(s);
            return this;
        };

        /**
         *
         * @param {string} string
         */
        this.printLine = function (string, encoding) {
            this.print(string, encoding);
            this.buffer.push(this.driver.lf());
            return this;
        };


        this.printText = function (text, aligment, size) {
            if (aligment === undefined) {
                aligment = this.ALIGNMENT_LEFT;
            }
            if (size === undefined) {
                size = this.FONT_SIZE_NORMAL;
            }
            this.setAlignment(aligment);
            this.setPrintMode(size);
            this.printLine(text);
            return this;
        };


        // ----------------------------------
        //  FONTS
        // ----------------------------------

        // user friendly names
        this.FONT_SIZE_SMALL = 1;
        this.FONT_SIZE_NORMAL = 0;
        this.FONT_SIZE_MEDIUM1 = 33;
        this.FONT_SIZE_MEDIUM2 = 16;
        this.FONT_SIZE_MEDIUM3 = 49;
        this.FONT_SIZE_BIG = 48; // BIG

        // bits for ESC !
        this.FONT_A = 0; // A
        this.FONT_B = 1; // B
        this.FONT_EMPHASIZED = 8;
        this.FONT_DOUBLE_HEIGHT = 16;
        this.FONT_DOUBLE_WIDTH = 32;
        this.FONT_ITALIC = 64;
        this.FONT_UNDERLINE = 128;

        this.setPrintMode = function (mode) {
            this.buffer.push(this.driver.setPrintMode(mode));
            return this;
        };


        this.setEmphasis = this.emphasis = function (mode) {
            this.buffer.push(this.driver.emphasis(mode));
            return this;
        };

        this.bold = function (on) {
            if (on === undefined) {
                on = true;
            }
            this.buffer.push(this.driver.emphasis(on));
            return this;
        };

        this.UNDERLINE_NONE = 0;
        this.UNDERLINE_SINGLE = 1;
        this.UNDERLINE_DOUBLE = 2;

        this.underline = function (mode) {
            if (mode === true || mode === undefined) {
                mode = this.UNDERLINE_SINGLE;
            } else if (mode === false) {
                mode = this.UNDERLINE_NONE;
            }
            this.buffer.push(this.driver.underline(mode));
            return this;
        };


        // ----------------------------------
        //  ALIGNMENT
        // ----------------------------------

        this.ALIGNMENT_LEFT = 0;
        this.ALIGNMENT_CENTER = 1;
        this.ALIGNMENT_RIGHT = 2;

        this.setAlignment = function (aligment) {
            if (aligment === undefined) {
                aligment = this.ALIGNMENT_LEFT;
            }

            this.buffer.push(this.driver.alignment(aligment));
            return this;
        };


        

        /**
         * Make a full cut, when used with Printer::cut
         */
        this.CUT_FULL = 65;
        /**
         * Make a partial cut, when used with Printer::cut
         */
        this.CUT_PARTIAL = 66;

        this.cut = function (mode, lines = 3) {
            if (mode === undefined) {
                mode = this.CUT_FULL;
            }
            if (lines === undefined) {
                lines = 3;
            }
            this.buffer.push(this.driver.cut(mode, lines));
            return this;
        };

        /**
         * Print and feed line / Print and feed n lines.
         *
         */
        this.feed = this.lf = function (lines) {
            this.buffer.push(this.driver.lf(lines));
            return this;
        };

        /**
         * Some printers require a form feed to release the paper. On most printers, this
         * command is only useful in page mode, which is not implemented in this driver.
         */
        this.feedForm = function () {
            this.buffer.push(this.driver.feedForm());
            return this;
        };


        /**
         * Some slip printers require `ESC q` sequence to release the paper.
         */
        this.release = function () {
            this.buffer.push(this.driver.relese());
            return this;
        };

        /**
         * Print and reverse feed n lines.
         */
        this.feedReverse = function (lines) {
            if (lines === undefined) {
                lines = 1;
            }
            this.buffer.push(this.driver.feedReverse(lines));
            return this;
        };

        // ---------------------------------
        //  SHORT SYNTAX
        // ---------------------------------

        // alignment

        this.left = function () {
            this.buffer.push(this.driver.alignment(this.ALIGNMENT_LEFT));
            return this;
        };

        this.right = function () {
            this.buffer.push(this.driver.alignment(this.ALIGNMENT_RIGHT));
            return this;
        };

        this.center = function () {
            this.buffer.push(this.driver.alignment(this.ALIGNMENT_CENTER));
            return this;
        };


        return this;
    }

// ==================================================
    // MAIN
    // ===================================================


    function getCurrentTransport() {
       return new RawBtTransport();
    }

    function getCurrentDriver() {
        return new EscPosDriver();
    }
	
	   function RawBtTransport() {
        this.send = function (prn) {
            let S = "#Intent;scheme=rawbt;";
            let P = "package=ru.a402d.rawbtprinter;end;";
            let textEncoded = "base64," + btoa(unescape(prn));
            window.location.href = "intent:" + textEncoded + S + P;
        };

        return this;
    }


function EscPosDriver() {
        this.defaultCodePages = {
            'CP437': 0,
            'CP932': 1,
            'CP850': 2,
            'CP860': 3,
            'CP863': 4,
            'CP865': 5,
            'CP857': 13,
            'CP737': 14,
            'ISO_8859-7': 15,
            'CP1252': 16,
            'CP866': 17,
            'CP852': 18,
            'CP858': 19,
            'ISO88596': 22,
            'WINDOWS1257': 25,
            'CP864': 28,
            'WINDOWS1255': 32,
            'CP861': 56,
            'CP855': 60,
            'CP862': 62,
            'CP869': 66,
            'WINDOWS1250': 72,
            'WINDOWS1251': 73,
            'WINDOWS1253': 90,
            'WINDOWS1254': 91,
            'WINDOWS1256': 92,
            'WINDOWS1258': 94,
            'CP775': 95,
            'CP874': 255,
            'GBK': -1
        };

        this.goojprtCodePages = {
            "CP437":"0",
            "CP932":"1",
            "CP850":"2",
            "CP860":"3",
            "CP863":"4",
            "CP865":"5",
            "CP1251":"6",
            "CP866":"7",
            "CP775":"9",
            "CP862":"15",
            "CP1252":"16",
            "WINDOWS1253":"17",
            "CP852":"18",
            "CP858":"19",
            "CP864":"22",
            "CP737":"24",
            "WINDOWS1257":"25",
            "CP85":"29",
            "WINDOWS1256":"34",
            "CP874":"47",
            'GBK': "-1"
        };
}


 function printDemoText() {
            let text = "JAVANESE RESTAURANT";
            var c = new PosPrinterJob(getCurrentDriver(), getCurrentTransport());
            c.initialize();
            c.setEncoding(defaultCP[userLang]);

            let align = document.querySelector('input[name="align"]:checked').value;
            c.setAlignment(align);
            let font_flags  = document.querySelectorAll('input[name="font"]');

            let i;
            let font = 0;
            for (i = 0; i < font_flags.length; i++) {
                if (font_flags[i].checked) {
                    font = font + 1*font_flags[i].value;
                }
            }
            c.setPrintMode(font);

            c.printLine(text);
            c.feed(2);
            c.execute();
        }
</script>
 <button class="green" style="width:100%;margin:0" onclick="printDemoText(); return false;">Print</button>