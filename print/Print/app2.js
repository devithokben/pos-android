    var CodePageType = 'GENERAL'; // 'PT210'

    // ***************************************************
    //  LIBs
    // ***************************************************

    function RawBtTransport() {
        /*
		this.send = function (prn) {
            let S = "#Intent;scheme=rawbt;";
            let P = "package=ru.a402d.rawbtprinter;end;";
            let textEncoded = "base64," + btoa(unescape(prn));
            window.location.href = "intent:" + textEncoded + S + P;
        };
		*/
		this.send = function (prn) {
            let textEncoded = "base64," + btoa(unescape(prn));
			Android.showToast(textEncoded);
			
        };
        return this;
    }


    function EscPosDriver() {
        this.defaultCodePages = {
            'CP437': 0,
            'CP932': 1,
            'CP850': 2,
            'CP860': 3,
            'CP863': 4,
            'CP865': 5,
            'CP857': 13,
            'CP737': 14,
            'ISO_8859-7': 15,
            'CP1252': 16,
            'CP866': 17,
            'CP852': 18,
            'CP858': 19,
            'ISO88596': 22,
            'WINDOWS1257': 25,
            'CP864': 28,
            'WINDOWS1255': 32,
            'CP861': 56,
            'CP855': 60,
            'CP862': 62,
            'CP869': 66,
            'WINDOWS1250': 72,
            'WINDOWS1251': 73,
            'WINDOWS1253': 90,
            'WINDOWS1254': 91,
            'WINDOWS1256': 92,
            'WINDOWS1258': 94,
            'CP775': 95,
            'CP874': 255,
            'GBK': -1
        };

        this.goojprtCodePages = {
            "CP437":"0",
            "CP932":"1",
            "CP850":"2",
            "CP860":"3",
            "CP863":"4",
            "CP865":"5",
            "CP1251":"6",
            "CP866":"7",
            "CP775":"9",
            "CP862":"15",
            "CP1252":"16",
            "WINDOWS1253":"17",
            "CP852":"18",
            "CP858":"19",
            "CP864":"22",
            "CP737":"24",
            "WINDOWS1257":"25",
            "CP85":"29",
            "WINDOWS1256":"34",
            "CP874":"47",
            'GBK': "-1"
        };


        function intval (mixedVar, base) {
            var tmp, match

            var type = typeof mixedVar

            if (type === 'boolean') {
                return +mixedVar
            } else if (type === 'string') {
                if (base === 0) {
                    match = mixedVar.match(/^\s*0(x?)/i)
                    base = match ? (match[1] ? 16 : 8) : 10
                }
                tmp = parseInt(mixedVar, base || 10)
                return (isNaN(tmp) || !isFinite(tmp)) ? 0 : tmp
            } else if (type === 'number' && isFinite(mixedVar)) {
                return mixedVar < 0 ? Math.ceil(mixedVar) : Math.floor(mixedVar)
            } else {
                return 0
            }
        }

        function chr(x) {
            x = intval(x);
            hexString = x.toString(16);
            if (hexString.length % 2) {
                hexString = '0' + hexString;
            }
            return "%" + hexString;
        }

        this.encodeByte = function (b) {
            return chr(b);
        };

        const LF = chr(10);
        const CR = chr(13);
        const ESC = chr(27);
        const FS = chr(28);
        const GS = chr(29);
        const ON = '1';
        const OFF = '0';

        this.lf = function (lines) {
            if (lines === undefined || lines < 2) {
                return LF + CR;
            } else {
                return ESC + "d" + chr(lines);
            }
        };

        this.alignment = function (aligment) {
            return ESC + "a" + chr(aligment);
        };

        this.cut = function (mode, lines) {
            return GS + "V" + chr(mode) + chr(lines);
        };

        this.feedForm = function () {
            return chr(12);
        };

        /**
         * Some slip printers require `ESC q` sequence to release the paper.
         */
        this.release = function () {
            return ESC + chr(113);
        };


        this.feedReverse = function (lines) {
            return ESC + 'e' + chr(1 * lines);
        };

        this.setPrintMode = function (mode) {
            return ESC + "!" + chr(1 * mode);
        };

        this.barcode = function (content, type) {
            return GS + "k" + chr(1 * type) + chr(content.length) + content;
        };

        this.setBarcodeHeight = function (height) {
            return GS + "h" + chr(1 * height);
        };

        this.setBarcodeWidth = function (width) {
            return GS + "w" + chr(1 * width);
        };

        this.setBarcodeTextPosition = function (position) {
            return GS + "H" + chr(1 * position);
        };

        this.emphasis = function (mode) {
            return ESC + "E" + (mode ? ON : OFF);
        };

        this.underline = function (mode) {
            return ESC + "-" + chr(1 * mode);
        };

        this.initialize = function () {
            return ESC + '@';
        };

        this.setCharacterTable = function (number) {
            if(number<0){
              return FS+ '&';
            }
            return FS+ '.'+ ESC + "t" + chr(1 * number);

        };

        this.setDefaultCharacterTable = function (cpname) {
            if(CodePageType=='PT210'){
             return this.setCharacterTable(this.goojprtCodePages[cpname]);
            }
            return this.setCharacterTable(this.defaultCodePages[cpname]);
        };


        this.wrapperSend2dCodeData = function(fn, cn, data, m ){
            if(data===undefined){
                data='';
            }
            if(m===undefined){
                m='';
            }
            let n=data.length+m.length+2;
            header = chr(n%256)+chr(n/256);
            return GS+"(k"+header+cn+fn+m+data;
        };

        this.qrCode = function(code,ec,size,model){
            let r = '';
            let cn = '1'; // Code type for QR code
            // Select model: 1, 2 or micro.
            r +=  this.wrapperSend2dCodeData(String.fromCharCode(65), cn, String.fromCharCode(48 + 1*model) + String.fromCharCode(0));
            // Set dot size.
            r += this.wrapperSend2dCodeData(String.fromCharCode(67), cn, String.fromCharCode(1*size));
            // Set error correction level: L, M, Q, or H
            r += this.wrapperSend2dCodeData(String.fromCharCode(69), cn, String.fromCharCode(48 + 1*ec));
            // Send content & print
            r += this.wrapperSend2dCodeData(String.fromCharCode(80), cn, code, '0');
            r += this.wrapperSend2dCodeData(String.fromCharCode(81), cn, '', '0');

            return r;
        };

        return this;
    }

    function PosPrinterJob(driver, transport) {
        /**
         * @type EscPosDriver
         */
        this.driver = driver;

        /**
         * @type RawBtTransport
         */
        this.transport = transport;


        this.buffer = [];


        // ----------------------------------
        //  CONFIGURE
        // ----------------------------------
        this.encoding = 'CP437';
        this.characterTable = 0;

        this.setEncoding = function (encoding) {
            this.encoding = encoding;
            this.buffer.push(this.driver.setDefaultCharacterTable(encoding.toUpperCase()));
            return this;
        };
        this.setCharacterTable = function (number) {
            this.characterTable = number;
            this.buffer.push(this.driver.setCharacterTable(number));
            return this;
        };


        // ----------------------------------
        //  SEND TO PRINT
        // ----------------------------------

        this.execute = function () {
            this.transport.send(this.buffer.join(''));
            return this;
        };

        // ----------------------------------
        //  HIGH LEVEL FUNCTION
        // ----------------------------------

        this.initialize = function () {
            this.buffer.push(this.driver.initialize());
            return this;
        };


        /**
         *
         * @param {string} string
         */
        this.print = function (string, encoding) {
            let bytes = iconv.encode(string, encoding || this.encoding);
            let s = '';
            let self = this;
            bytes.forEach(function (b) {
                s = s + self.driver.encodeByte(b);
            });
            this.buffer.push(s);
            return this;
        };

        /**
         *
         * @param {string} string
         */
        this.printLine = function (string, encoding) {
            this.print(string, encoding);
            this.buffer.push(this.driver.lf());
            return this;
        };


        this.printText = function (text, aligment, size) {
            if (aligment === undefined) {
                aligment = this.ALIGNMENT_LEFT;
            }
            if (size === undefined) {
                size = this.FONT_SIZE_NORMAL;
            }
            this.setAlignment(aligment);
            this.setPrintMode(size);
            this.printLine(text);
            return this;
        };


        // ----------------------------------
        //  FONTS
        // ----------------------------------

        // user friendly names
        this.FONT_SIZE_SMALL = 1;
        this.FONT_SIZE_NORMAL = 0;
        this.FONT_SIZE_MEDIUM1 = 33;
        this.FONT_SIZE_MEDIUM2 = 16;
        this.FONT_SIZE_MEDIUM3 = 49;
        this.FONT_SIZE_BIG = 48; // BIG

        // bits for ESC !
        this.FONT_A = 0; // A
        this.FONT_B = 1; // B
        this.FONT_EMPHASIZED = 8;
        this.FONT_DOUBLE_HEIGHT = 16;
        this.FONT_DOUBLE_WIDTH = 32;
        this.FONT_ITALIC = 64;
        this.FONT_UNDERLINE = 128;

        this.setPrintMode = function (mode) {
            this.buffer.push(this.driver.setPrintMode(mode));
            return this;
        };


        this.setEmphasis = this.emphasis = function (mode) {
            this.buffer.push(this.driver.emphasis(mode));
            return this;
        };

        this.bold = function (on) {
            if (on === undefined) {
                on = true;
            }
            this.buffer.push(this.driver.emphasis(on));
            return this;
        };

        this.UNDERLINE_NONE = 0;
        this.UNDERLINE_SINGLE = 1;
        this.UNDERLINE_DOUBLE = 2;

        this.underline = function (mode) {
            if (mode === true || mode === undefined) {
                mode = this.UNDERLINE_SINGLE;
            } else if (mode === false) {
                mode = this.UNDERLINE_NONE;
            }
            this.buffer.push(this.driver.underline(mode));
            return this;
        };


        // ----------------------------------
        //  ALIGNMENT
        // ----------------------------------

        this.ALIGNMENT_LEFT = 0;
        this.ALIGNMENT_CENTER = 1;
        this.ALIGNMENT_RIGHT = 2;

        this.setAlignment = function (aligment) {
            if (aligment === undefined) {
                aligment = this.ALIGNMENT_LEFT;
            }

            this.buffer.push(this.driver.alignment(aligment));
            return this;
        };


        // ----------------------------------
        //  BARCODE
        // ----------------------------------

        this.BARCODE_UPCA = 65;
        this.BARCODE_UPCE = 66;
        this.BARCODE_JAN13 = 67;
        this.BARCODE_JAN8 = 68;
        this.BARCODE_CODE39 = 69;
        this.BARCODE_ITF = 70;
        this.BARCODE_CODABAR = 71;
        this.BARCODE_CODE93 = 72;
        this.BARCODE_CODE128 = 73;

        this.printBarCode = function (content, type) {
            if (type === undefined) {
                type = this.BARCODE_CODE39;
            }
            this.buffer.push(this.driver.barcode(content, type));
            return this;
        };

        /**
         * Set barcode height.
         *
         * height Height in dots. If not specified, 8 will be used.
         */
        this.setBarcodeHeight = function (height) {
            if (height === undefined) {
                height = 30;
            }
            this.buffer.push(this.driver.setBarcodeHeight(height));
            return this;
        };

        /**
         * Set barcode bar width.
         *
         * width Bar width in dots. If not specified, 3 will be used.
         *  Values above 6 appear to have no effect.
         */
        this.setBarcodeWidth = function (width) {
            if (width === undefined) {
                width = 3;
            }
            this.buffer.push(this.driver.setBarcodeWidth(width));
            return this;
        };


        /**
         * Indicates that HRI (human-readable interpretation) text should not be
         * printed, when used with Printer::setBarcodeTextPosition
         */
        this.BARCODE_TEXT_NONE = 0;
        /**
         * Indicates that HRI (human-readable interpretation) text should be printed
         * above a barcode, when used with Printer::setBarcodeTextPosition
         */
        this.BARCODE_TEXT_ABOVE = 1;
        /**
         * Indicates that HRI (human-readable interpretation) text should be printed
         * below a barcode, when used with Printer::setBarcodeTextPosition
         */
        this.BARCODE_TEXT_BELOW = 2;


        /**
         * Set the position for the Human Readable Interpretation (HRI) of barcode characters.
         *
         * position. Use Printer::BARCODE_TEXT_NONE to hide the text (default),
         *  or any combination of Printer::BARCODE_TEXT_ABOVE and Printer::BARCODE_TEXT_BELOW
         *  flags to display the text.
         */
        this.setBarcodeTextPosition = function (position) {
            if (position === undefined) {
                position = this.BARCODE_TEXT_NONE;
            }
            this.buffer.push(this.driver.setBarcodeTextPosition(position));
            return this;
        };

        // ----------------------------------
        //  QRCODE
        // ----------------------------------

        this.QR_ECLEVEL_L = 0;
        this.QR_ECLEVEL_M = 1;
        this.QR_ECLEVEL_Q = 2;
        this.QR_ECLEVEL_H = 3;

        this.QR_SIZES_1 = 1;
        this.QR_SIZES_2 = 2;
        this.QR_SIZES_3 = 3;
        this.QR_SIZES_4 = 4;
        this.QR_SIZES_5 = 5;
        this.QR_SIZES_6 = 6;
        this.QR_SIZES_7 = 7;
        this.QR_SIZES_8 = 8;

        this.QR_MODEL_1 = 1;
        this.QR_MODEL_2 = 2;
        this.QR_MICRO = 3;

        this.printQrCode = function (code, ec, size, model) {
            if(ec === undefined){
                ec = this.QR_ECLEVEL_L;
            }
            if(size === undefined){
                size = this.QR_SIZES_3;
            }
            if(model === undefined){
                model = this.QR_MODEL_2;
            }

            this.buffer.push(this.driver.qrCode(code,ec, size, model));
            return this;
        };


        /**
         * Make a full cut, when used with Printer::cut
         */
        this.CUT_FULL = 66;
        /**
         * Make a partial cut, when used with Printer::cut
         */
        this.CUT_PARTIAL = 66;

        this.cut = function (mode, lines = 3) {
            if (mode === undefined) {
                mode = this.CUT_FULL;
            }
            if (lines === undefined) {
                lines = 3;
            }
            this.buffer.push(this.driver.cut(mode, lines));
            return this;
        };

        /**
         * Print and feed line / Print and feed n lines.
         *
         */
        this.feed = this.lf = function (lines) {
            this.buffer.push(this.driver.lf(lines));
            return this;
        };

        /**
         * Some printers require a form feed to release the paper. On most printers, this
         * command is only useful in page mode, which is not implemented in this driver.
         */
        this.feedForm = function () {
            this.buffer.push(this.driver.feedForm());
            return this;
        };


        /**
         * Some slip printers require `ESC q` sequence to release the paper.
         */
        this.release = function () {
            this.buffer.push(this.driver.relese());
            return this;
        };

        /**
         * Print and reverse feed n lines.
         */
        this.feedReverse = function (lines) {
            if (lines === undefined) {
                lines = 1;
            }
            this.buffer.push(this.driver.feedReverse(lines));
            return this;
        };

        // ---------------------------------
        //  SHORT SYNTAX
        // ---------------------------------

        // alignment

        this.left = function () {
            this.buffer.push(this.driver.alignment(this.ALIGNMENT_LEFT));
            return this;
        };

        this.right = function () {
            this.buffer.push(this.driver.alignment(this.ALIGNMENT_RIGHT));
            return this;
        };

        this.center = function () {
            this.buffer.push(this.driver.alignment(this.ALIGNMENT_CENTER));
            return this;
        };


        return this;
    }


    // ==================================================
    // MAIN
    // ===================================================


    function getCurrentTransport() {
       return new RawBtTransport();
    }

    function getCurrentDriver() {
        return new EscPosDriver();
    }




    const countryNames = {
        // 'hy': '?????????',
        'sq': 'Albanian',
//        'ar': 'Arabic',
//        'bn': 'Bengali',
        'bg': 'Bulgarian',
        'ca': 'Catalan',
        'zh': 'Chinese',
        'hr': 'Croatian',
        'cs': 'Czech',
        'da': 'Danish',
        'nl': 'Dutch',
        'en': 'English',
        'et': 'Estonian',
        'fi': 'Finnish (Suomi)',
        'fr': 'French',
        'de': 'Deutsch',
        'el': 'Greek',
//        'hi': 'Hindi',
        'hu': 'Hungarian',
        'it': 'Italian',
        'id': 'Indonesian',
        'lt': 'Lithuanian',
        'ms': 'Malay',
        'no': 'Norwegian',
        'pl': 'Polish',
        'pt': 'Portuguese',
        'ru': 'Russian',
        'sk': 'Slovak',
        'sl': 'Slovenian',
        'es': 'Spanish',
        'sv': 'Swedish',
        'th': 'Thai',
        'tr': 'Turkish',
        'uk': 'Ukrainian'
    };

    const defaultCP = {
           // 'hy': 'ArmSCII8',
            'zh': 'GBK',
            'sq': 'cp858',
            'bg': 'cp866',
            'ca': 'cp437',
            'hr': 'cp1252',
            'cs': 'cp1252',
            'da': 'cp858',
            'nl': 'cp858',
            'et': 'cp858',
            'fi': 'cp858',
            'fr': 'cp858',
            'de': 'cp858',
            'el': 'windows1253',
            'hu': 'cp852',
            'id': 'cp437',
            'it': 'cp858',
            'lt': 'cp852',
            'ms': 'cp437',
            'no': 'cp858',
            'pl': 'cp852',
            'pt': 'cp858',
            'ru': 'cp866',
            'sk': 'cp852',
            'sl': 'cp852',
            'es': 'cp858',
            'sv': 'cp858',
            'th': 'cp874',
            'tr': 'cp857',
            'uk': 'cp866',
            'en': 'cp437'
    };

    // ------------------------------------------------------
    // DEMO
    // ------------------------------------------------------

    function alignDemo() {

        var c = new PosPrinterJob(getCurrentDriver(), getCurrentTransport());
        c.initialize();

        c.printText("Text align:", c.ALIGNMENT_LEFT, c.FONT_SIZE_BIG);
        c.printText("Left aligned", c.ALIGNMENT_LEFT);
        c.printText("Center aligned", c.ALIGNMENT_CENTER);
        c.printText("Right aligned", c.ALIGNMENT_RIGHT);
        c.center().print('center()').lf().right().print('right()').lf().left().print('left()').lf();

        c.feed(2);
        c.execute();
    }

    function decorDemo() {
        var c = new PosPrinterJob(getCurrentDriver(), getCurrentTransport());
        c.initialize();

        c.printText("Font decoration:", c.ALIGNMENT_LEFT, c.FONT_SIZE_BIG);
        c.printText("underline text as ESC !", c.ALIGNMENT_LEFT, c.FONT_UNDERLINE);
        c.underline(c.UNDERLINE_SINGLE).print(' one dot underline ').underline(c.UNDERLINE_DOUBLE).lf().print(' double dots underline ').underline(c.UNDERLINE_NONE).lf();
        c.printText("emphasized text as ESC !", c.ALIGNMENT_LEFT, c.FONT_EMPHASIZED);
        c.emphasis(true).print('Emphasis TEXT').emphasis(false).printLine(' and bold off text');
        c.feed(2);
        c.execute();
    }

    function fontDemo() {
        var c = new PosPrinterJob(getCurrentDriver(), getCurrentTransport());
        c.initialize();

        c.printText("Font sizes:", c.ALIGNMENT_LEFT, c.FONT_SIZE_BIG);
        c.printText("small FONT", c.ALIGNMENT_LEFT, c.FONT_SIZE_SMALL);
        c.printText("medium 1 FONT", c.ALIGNMENT_LEFT, c.FONT_SIZE_MEDIUM1);
        c.printText("medium 2 FONT", c.ALIGNMENT_LEFT, c.FONT_SIZE_MEDIUM2);
        c.printText("medium 3 FONT", c.ALIGNMENT_LEFT, c.FONT_SIZE_MEDIUM3);
        c.printText("big FONT", c.ALIGNMENT_LEFT, c.FONT_SIZE_BIG);
        c.printText("double WIDTH", c.ALIGNMENT_LEFT, c.FONT_DOUBLE_WIDTH);
        c.printText("double HEIGHT", c.ALIGNMENT_LEFT, c.FONT_DOUBLE_HEIGHT);
        c.setPrintMode(c.FONT_SIZE_BIG + c.FONT_UNDERLINE).printLine("Example");
        c.feed(3);

        c.execute();
    }

    function encodingDemo() {
        var c = new PosPrinterJob(getCurrentDriver(), getCurrentTransport());
        c.initialize();

        c.setEncoding(defaultCP['en']);
        c.printText("Chines", c.ALIGNMENT_LEFT, c.FONT_EMPHASIZED).bold(false);
        c.setEncoding(defaultCP['zh']).printLine("???!\n?????????????????????!", "GBK");

        c.setEncoding(defaultCP['en']);
        c.printText("Indonesian", c.ALIGNMENT_LEFT, c.FONT_EMPHASIZED).bold(false);
        c.setEncoding(defaultCP['id']).printLine("Kamu gimana kabarnya? Terima kasih", "cp437");

        c.setEncoding(defaultCP['en']);
        c.printText("Portugal", c.ALIGNMENT_LEFT, c.FONT_EMPHASIZED).bold(false);
        c.setEncoding(defaultCP['pt']).printLine("Lu�s arg�ia � J�lia que �bra��es, f�, ch�, �xido, p�r, z�ng�o� eram palavras do portugu�s", "cp860");

        c.setEncoding(defaultCP['en']);
        c.printText("Spanish", c.ALIGNMENT_LEFT, c.FONT_EMPHASIZED).bold(false);
        c.setEncoding(defaultCP['es']).printLine("El ping�ino Wenceslao hizo kil�metros bajo exhaustiva lluvia y fr�o, a�oraba a su querido cachorro.", "cp437");


        c.setEncoding(defaultCP['en']);
        c.printText("Danish", c.ALIGNMENT_LEFT, c.FONT_EMPHASIZED).bold(false);
        c.setEncoding(defaultCP['da']).printLine("Quizdeltagerne spiste jordb�r med fl�de, mens cirkusklovnen Wolther spillede p� xylofon.", "cp850");

        c.setEncoding(defaultCP['en']);
        c.printText("German", c.ALIGNMENT_LEFT, c.FONT_EMPHASIZED).bold(false);
        c.setEncoding(defaultCP['de']).printLine("Falsches �ben von Xylophonmusik qu�lt jeden gr��eren Zwerg.", "cp850");

        c.setEncoding(defaultCP['en']);
        c.printText("Greek", c.ALIGNMENT_LEFT, c.FONT_EMPHASIZED).bold(false);
        c.setEncoding(defaultCP['el']).printLine("?es?ep??? t?? ????f???a �de???�?a", "cp737");


        c.setEncoding(defaultCP['en']);
        c.printText("French", c.ALIGNMENT_LEFT, c.FONT_EMPHASIZED).bold(false);
        c.setEncoding(defaultCP['fr']).printLine("Le c�ur d��u mais l'�me plut�t na�ve, Lou�s r�va de crapa�ter en cano� au del� des �les, pr�s du m�lstr�m o� br�lent les nov�.", "cp1252");

//        c.printText("Irish Gaelic", c.ALIGNMENT_LEFT, c.FONT_EMPHASIZED).bold(false);
//        c.setCharacterTable(16).printLine("D'fhuascail �osa, �rmhac na h�ighe Beannaithe, p�r �ava agus �dhaimh.", "cp1252");

        c.setEncoding(defaultCP['en']);
        c.printText("Hungarian", c.ALIGNMENT_LEFT, c.FONT_EMPHASIZED).bold(false);
        c.setEncoding(defaultCP['hu']).printLine("�rv�zturo t�k�rf�r�g�p.", "cp852");

//        c.printText("Icelandic", c.ALIGNMENT_LEFT, c.FONT_EMPHASIZED).bold(false);
//        c.setCharacterTable(2).printLine("K�mi n� �xi h�r ykist �j�fum n� b��i v�l og �drepa.", "cp850");

        c.setEncoding(defaultCP['en']);
        c.printText("Latvian", c.ALIGNMENT_LEFT, c.FONT_EMPHASIZED).bold(false);
        c.setEncoding(defaultCP['lt']).printLine("Gla��kuna ruki�i dzeruma ciepj Baha koncertfligelu vakus.", "cp1257");

        c.setEncoding(defaultCP['en']);
        c.printText("Polish", c.ALIGNMENT_LEFT, c.FONT_EMPHASIZED).bold(false);
        c.setEncoding(defaultCP['pl']).printLine("Pchnac w te l�dz jeza lub osm skrzyn fig.", "cp1257");

        c.setEncoding(defaultCP['en']);
        c.printText("Russian", c.ALIGNMENT_LEFT, c.FONT_EMPHASIZED).bold(false);
        c.setEncoding(defaultCP['ru']).printLine("? ????? ??? ??? ?? ??????? ??, ?? ????????? ?????????!", "cp866");

        c.setEncoding(defaultCP['en']);
        c.printText("Turkish", c.ALIGNMENT_LEFT, c.FONT_EMPHASIZED).bold(false);
        c.setEncoding(defaultCP['tr']).printLine("Pijamali hasta, yagiz sof�re �abucak g�vendi.", "cp857");

        c.feed(3);
        c.execute();
    }

    function pangrams(lang) {
        switch (lang) {
            // case 'hy': return '????? ????? ????? ? ???? ????????? ?????:';
            case 'sq':
                return 'Un� mund t� ha qelq dhe nuk m� gjen gj�.';
            case 'bg':
                return '???? ?? ?? ??????, ?? ?? ?? ?????.';
            case 'ca':
                return 'Puc menjar vidre, que no em fa mal.';
            case 'hr':
                return 'Ja mogu jesti staklo, i to mi ne �teti.';
            case 'cs':
                return 'Mohu j�st sklo, neubl�� mi.';
            case 'da':
                return 'Jeg kan spise glas, det g�r ikke ondt p� mig.';
            case 'nl':
                return 'Ik kan glas eten, het doet m? geen kwaad.';
            case 'et':
                return 'Ma v�in klaasi s��a, see ei tee mulle midagi.';
            case 'ph':
                return '-unknown-';
            case 'fi':
                return 'Voin sy�d� lasia, se ei vahingoita minua.';
            case 'fr':
                return 'Je peux manger du verre, �a ne me fait pas mal.';
            case 'ka':
                return '????? ???? ?? ??? ??????.';
            case 'de':
                return 'Ich kann Glas essen, ohne mir zu schaden.';
            case 'el':
                return '?p??? ?a f?? spas�??a ??a??? ????? ?a p??? t?p?ta.';
            case 'hi':
                return '??? ???? ?? ???? ???, ???? ?? ?? ??? ???? ???? ????.';
            case 'hu':
                return '�rv�zturo t�k�rf�r�g�p.';
            case 'id':
                return 'Cwm fjordbank glyphs vext quiz.';
            case 'it':
                return 'Posso mangiare il vetro e non mi fa male.';
            case 'lv':
                return 'Es varu est stiklu, tas man nekaite.';
            case 'lt':
                return 'A� galiu valgyti stikla ir jis manes ne�eid�ia';
            case 'mk':
                return '????? ?? ????? ??????, ? ?? ?? ?????.';
            case 'ms':
                return 'Saya boleh makan kaca dan ia tidak mencederakan saya.';
            case 'no':
                return 'Eg kan eta glas utan � skada meg. Jeg kan spise glass uten � skade meg.';
            case 'pl':
                return 'Pchnac w te l�dz jeza lub osm skrzyn fig.';
            case 'pt':
                return 'O pr�ximo v�o � noite sobre o Atl�ntico, p�e freq�entemente o �nico m�dico.';
            case 'ro':
                return 'Pot sa man�nc sticla ?i ea nu ma rane?te.';
            case 'ru':
                return '? ????? ??? ??? ?? ??????? ??, ?? ????????? ?????????!';
            case 'sr':
                return '?? ???? ????? ??????, ? ?? ?? ?? ?????. Ja mogu jesti staklo, i to mi ne �teti.';
            case 'sk':
                return 'Star� k�n na hrbe kn�h �uje t�ko pov�dnut� ru�e, na stlpe sa datel uc� kv�kat nov� �du o �ivote.';
            case 'sl':
                return 'Lahko jem steklo, ne da bi mi �kodovalo.';
            case 'es':
                return 'Puedo comer vidrio, no me hace da�o.';
            case 'sv':
                return 'Jag kan �ta glas utan att skada mig.';
            case 'th':
                return '?????????????? ?????????????????????';
            case 'tr':
                return 'Pijamali hasta, yagiz sof�re �abucak g�vendi.';
            case 'uk':
                return '???? ??, ????, ??? ??????? ? ??, ???????? ??? ???????!';
            case 'vi':
                return 'T�i c� th? an th?y tinh m� kh�ng h?i g�.';

            case 'en':
            default:
                return 'A quick brown fox jumps over the lazy dog';

        }
    }
    function loremIpsum(lang) {
        switch (lang) {
            // case 'hy':  return 'Lorem Ipsum-? ??????????? ? ????????? ????????????????? ????? ?????????? ????????? ????? ?: ????? 1500-?????????` Lorem Ipsum-? ?????????? ? ????????? ????????????????? ???????? ????????? ?????, ???? ?? ?????? ???????? ?????? ?????? ????????????? ?????????? ???? ????????? ??????? ???????? ?: ??? ?????? ?? ????? ????????? ? ???????? ???? ?????????, ???? ???????? ? ???????????? ??????????? ???` ?????? ????? ???????: ??? ?????? ? ?????? 1960-????????? Lorem Ipsum ?????????? Letraset ????? ????????? ???????????, ??? ????? ??? ????????????? ??????????? ??????? ???????? ????????? ?????????, ???????? ? Aldus PageMaker-?, ??? ???????? ? Lorem Ipsum-? ????????????:';
            case 'sq':
                return 'Lorem Ipsum �sht� nj� tekst shabllon i industris� s� printimit dhe shtypshkronjave. Lorem Ipsum ka qen� teksti shabllon i industris� q� nga vitet 1500, kur nj� shtyp�s i panjohur morri nj� galeri shkrimesh dhe i ngat�rroi p�r t� krijuar nj� lib�r most�r. Teksti i ka mbijetuar jo vet�m pes� shekujve, por edhe kalimit n� shtypshkrimin elektronik, duke ndenjur n� thelb i pandryshuar. U b� i njohur n� vitet 1960 me l�shimin e letrave \'Letraset\' q� p�rmbanin tekstin Lorem Ipsum, e n� koh�t e fundit me aplikacione publikimi si Aldus PageMaker q� p�rmbajn� versione t� Lorem Ipsum.';
            case 'bg':
                return 'Lorem Ipsum ? ??????????? ???????? ?????, ????????? ? ???????????? ? ????????????? ?????????. Lorem Ipsum ? ???????????? ???????? ?? ????? 1500 ??????, ?????? ?????????? ??????? ????? ??????? ?????????? ????? ? ?? ?????????, ?? ?? ???????? ? ??? ????? ? ???????? ????????. ???? ????? ?? ???? ? ?????? ?????? ?? 5 ????, ?? ? ???????? ? ? ????????????? ?? ?????????? ??????? ???? ? ??????? ????? ??? ???????. ????????????? ? ???? 60?? ?????? ?? 20?? ??? ??? ?????????? ?? Letraset ?????, ????????? Lorem Ipsum ??????, ????????? ? ? ? ???? ??? ??? ??????? ?? ??????? ??????? ???? Aldus PageMaker, ????? ??????? ???????? ?????? ?? Lorem Ipsum.';
            case 'ca':
                return 'Lorem Ipsum �s un text de farciment usat per la ind�stria de la tipografia i la impremta. Lorem Ipsum ha estat el text est�ndard de la ind�stria des de l\'any 1500, quan un impressor desconegut va fer servir una galerada de text i la va mesclar per crear un llibre de mostres tipogr�fiques. No nom�s ha sobreviscut cinc segles, sin� que ha fet el salt cap a la creaci� de tipus de lletra electr�nics, romanent essencialment sense canvis. Es va popularitzar l\'any 1960 amb el llan�ament de fulls Letraset que contenien passatges de Lorem Ipsum, i m�s recentment amb programari d\'autoedici� com Aldus Pagemaker que inclou versions de Lorem Ipsum.';
            case 'hr':
                return 'Lorem Ipsum je jednostavno probni tekst koji se koristi u tiskarskoj i slovoslagarskoj industriji. Lorem Ipsum postoji kao industrijski standard jo� od 16-og stoljeca, kada je nepoznati tiskar uzeo tiskarsku galiju slova i poslo�io ih da bi napravio knjigu s uzorkom tiska. Taj je tekst ne samo pre�ivio pet stoljeca, vec se i vinuo u svijet elektronskog slovoslagarstva, ostajuci u su�tini nepromijenjen. Postao je popularan tijekom 1960-ih s pojavom Letraset listova s odlomcima Lorem Ipsum-a, a u skorije vrijeme sa software-om za stolno izdava�tvo kao �to je Aldus PageMaker koji takoder sadr�i varijante Lorem Ipsum-a.';
            case 'cs':
                return 'Lorem Ipsum je demonstrativn� v�plnov� text pou��van� v tiskarsk�m a kniharsk�m prumyslu. Lorem Ipsum je pova�ov�no za standard v t�to oblasti u� od zac�tku 16. stolet�, kdy dnes nezn�m� tiskar vzal kusy textu a na jejich z�klade vytvoril speci�ln� vzorovou knihu. Jeho odkaz nevydr�el pouze pet stolet�, on pre�il i n�stup elektronick� sazby v podstate beze zmeny. Nejv�ce popularizov�no bylo Lorem Ipsum v �edes�t�ch letech 20. stolet�, kdy byly vyd�v�ny speci�ln� vzorn�ky s jeho pas�emi a pozdeji pak d�ky poc�tacov�m DTP programum jako Aldus PageMaker.';
            case 'da':
                return 'Lorem Ipsum er ganske enkelt fyldtekst fra print- og typografiindustrien. Lorem Ipsum har v�ret standard fyldtekst siden 1500-tallet, hvor en ukendt trykker sammensatte en tilf�ldig spalte for at trykke en bog til sammenligning af forskellige skrifttyper. Lorem Ipsum har ikke alene overlevet fem �rhundreder, men har ogs� vundet indpas i elektronisk typografi uden v�sentlige �ndringer. S�tningen blev gjordt kendt i 1960\'erne med lanceringen af Letraset-ark, som indeholdt afsnit med Lorem Ipsum, og senere med layoutprogrammer som Aldus PageMaker, som ogs� indeholdt en udgave af Lorem Ipsum.';
            case 'nl':
                return 'Lorem Ipsum is slechts een proeftekst uit het drukkerij- en zetterijwezen. Lorem Ipsum is de standaard proeftekst in deze bedrijfstak sinds de 16e eeuw, toen een onbekende drukker een zethaak met letters nam en ze door elkaar husselde om een font-catalogus te maken. Het heeft niet alleen vijf eeuwen overleefd maar is ook, vrijwel onveranderd, overgenomen in elektronische letterzetting. Het is in de jaren \'60 populair geworden met de introductie van Letraset vellen met Lorem Ipsum passages en meer recentelijk door desktop publishing software zoals Aldus PageMaker die versies van Lorem Ipsum bevatten.';
            case 'et':
                return 'Lorem Ipsum on lihtsalt proovitekst, mida kasutatakse printimis- ja ladumist��stuses. See on olnud t��stuse p�hiline proovitekst juba alates 1500. aastatest, mil tundmatu printija v�ttis hulga suvalist teksti, et teha tr�kin�idist. Lorem Ipsum ei ole ainult viis sajandit s�ilinud, vaid on ka edasi kandunud elektroonilisse tr�kiladumisse, j��des sealjuures peaaegu muutumatuks. See sai tuntuks 1960. aastatel Letraset\'i lehtede v�ljalaskmisega, ja hiljuti tekstiredaktoritega nagu Aldus PageMaker, mis sisaldavad erinevaid Lorem Ipsumi versioone.';
            case 'ph':
                return 'Ang Lorem Ipsum ay ginagamit na modelo ng industriya ng pagpriprint at pagtytypeset. Ang Lorem Ipsum ang naging regular na modelo simula pa noong 1500s, noong may isang di kilalang manlilimbag and kumuha ng galley ng type at ginulo ang pagkaka-ayos nito upang makagawa ng libro ng mga type specimen. Nalagpasan nito hindi lang limang siglo, kundi nalagpasan din nito ang paglaganap ng electronic typesetting at nanatiling parehas. Sumikat ito noong 1960s kasabay ng pag labas ng Letraset sheets na mayroong mga talata ng Lorem Ipsum, at kamakailan lang sa mga desktop publishing software tulad ng Aldus Pagemaker ginamit ang mga bersyon ng Lorem Ipsum.';
            case 'fi':
                return 'Lorem Ipsum on yksinkertaisesti testausteksti, jota tulostus- ja ladontateollisuudet k�ytt�v�t. Lorem Ipsum on ollut teollisuuden normaali testausteksti jo 1500-luvulta asti, jolloin tuntematon tulostaja otti kaljuunan ja sekoitti sen tehd�kseen esimerkkikirjan. Se ei ole selvinnyt vain viitt� vuosisataa, mutta my�s loikan elektroniseen kirjoitukseen, j��den suurinpiirtein muuntamattomana. Se tuli kuuluisuuteen 1960-luvulla kun Letraset-paperiarkit, joissa oli Lorem Ipsum p�tki�, julkaistiin ja viel� my�hemmin tietokoneen julkistusohjelmissa, kuten Aldus PageMaker joissa oli versioita Lorem Ipsumista.';
            case 'fr':
                return 'Le Lorem Ipsum est simplement du faux texte employ� dans la composition et la mise en page avant impression. Le Lorem Ipsum est le faux texte standard de l\'imprimerie depuis les ann�es 1500, quand un imprimeur anonyme assembla ensemble des morceaux de texte pour r�aliser un livre sp�cimen de polices de texte. Il n\'a pas fait que survivre cinq si�cles, mais s\'est aussi adapt� � la bureautique informatique, sans que son contenu n\'en soit modifi�. Il a �t� popularis� dans les ann�es 1960 gr�ce � la vente de feuilles Letraset contenant des passages du Lorem Ipsum, et, plus r�cemment, par son inclusion dans des applications de mise en page de texte, comme Aldus PageMaker.';
            case 'ka':
                return 'Lorem Ipsum ??????? ?? ???????????? ?????????? ????????? ???????. ??? ?????????? 1500-???? ???????? ????, ??????? ??????? ????????? ?????? ??????? ?????? ??????? ?????????? ???????. ???? ?????? ???????? 5 ???????? ???????? ???????, ?????? ??? ??????, ??????????? ??????????? ??????? ???????? ???????. ?????????????? ?????????? ??? 1960-??? ?????? ????????? Letraset-?? ???????? ???????????? ???????, ???? ??????????? ?? � Aldus PageMaker-?? ????? ???????????? ???????????, ????????? Lorem Ipsum-?? ?????????? ???????? ??? ??????????.';
            case 'de':
                return 'Lorem Ipsum ist ein einfacher Demo-Text f�r die Print- und Schriftindustrie. Lorem Ipsum ist in der Industrie bereits der Standard Demo-Text seit 1500, als ein unbekannter Schriftsteller eine Hand voll W�rter nahm und diese durcheinander warf um ein Musterbuch zu erstellen. Es hat nicht nur 5 Jahrhunderte �berlebt, sondern auch in Spruch in die elektronische Schriftbearbeitung geschafft (bemerke, nahezu unver�ndert). Bekannt wurde es 1960, mit dem erscheinen von "Letraset", welches Passagen von Lorem Ipsum enhielt, so wie Desktop Software wie "Aldus PageMaker" - ebenfalls mit Lorem Ipsum.';
            case 'el':
                return '?? Lorem Ipsum e??a? ap?? ??a ?e?�e?? ????? ???�a ??a t??? epa??e?�at?e? t?? t?p???af?a? ?a? st???e???es?a?. ?? Lorem Ipsum e??a? t? epa??e?�at??? p??t?p? ?s?? af??? t? ?e?�e?? ????? ???�a, ap? t?? 15? a???a, ?ta? ??a? a????�?? t?p????f?? p??e ??a d???�?? ?a? a?a??te?e t?? ???e?? ??a ?a d?�??????se? ??a de??�a �?�????. ??? �??? ep?�??se p??te a???e?, a??? ????????se st?? ??e?t?????? st???e???es?a, pa?a�????ta? �e ???e t??p? a?a?????t?. ????e d?�?f???? t? de?aet?a t?? \'60 �e t?? ??d?s? t?? de??�?t?? t?? Letraset ?p?? pe??e??��a?a? ap?sp?s�ata t?? Lorem Ipsum, ?a? p?? p??sfata �e t? ????s�??? ??e?t??????? se??d?p???s?? ?p?? t? Aldus PageMaker p?? pe??e??a? e?d???? t?? Lorem Ipsum.';
            case 'hi':
                return 'Lorem Ipsum ???? ?? ????? ???? ?????? ?? ?? ?????? ??? ??? ??. Lorem Ipsum ?? ???? ?? ??? ?? ??? ?? ?? ?????? ?? ???? ??? ??? ?? ???, ?? ?? ?????? ?????? ?? ????? ???? ?? ????? ????? ????. ?? ? ???? ???? ?????? ?? ????? ??? ????? ???? ???????????? ?????? ??? ????? ????? ?? ??? ?? ????? ?????????? ???. ?? 1960 ?? ??? ??? Letraset Lorem Ipsum ??? ????? ???? ?? ????? ?? ??? ???????? ???, ?? ??? ?? ??? Aldus PageMaker Lorem Ipsum ?? ????????? ???? ??? ???????? ??????? ????????? ?? ??? ???? ??????? ???.';
            case 'hu':
                return 'A Lorem Ipsum egy egyszer� sz�vegr�szlete, sz�vegut�nzata a bet�szed� �s nyomdaiparnak. A Lorem Ipsum az 1500-as �vek �ta standard sz�vegr�szletk�nt szolg�lt az iparban; mikor egy ismeretlen nyomd�sz �ssze�ll�totta a bet�k�szlet�t �s egy p�lda-k�nyvet vagy sz�veget nyomott pap�rra, ezt haszn�lta. Nem csak 5 �vsz�zadot �lt t�l, de az elektronikus bet�k�szletekn�l is v�ltozatlanul megmaradt. Az 1960-as �vekben n�pszer�s�tett�k a Lorem Ipsum r�szleteket magukbafoglal� Letraset lapokkal, �s legut�bb softwarekkel mint p�ld�ul az Aldus Pagemaker.';
            case 'id':
                return 'Lorem Ipsum adalah contoh teks atau dummy dalam industri percetakan dan penataan huruf atau typesetting. Lorem Ipsum telah menjadi standar contoh teks sejak tahun 1500an, saat seorang tukang cetak yang tidak dikenal mengambil sebuah kumpulan teks dan mengacaknya untuk menjadi sebuah buku contoh huruf. Ia tidak hanya bertahan selama 5 abad, tapi juga telah beralih ke penataan huruf elektronik, tanpa ada perubahan apapun. Ia mulai dipopulerkan pada tahun 1960 dengan diluncurkannya lembaran-lembaran Letraset yang menggunakan kalimat-kalimat dari Lorem Ipsum, dan seiring munculnya perangkat lunak Desktop Publishing seperti Aldus PageMaker juga memiliki versi Lorem Ipsum.';
            case 'it':
                return 'Lorem Ipsum � un testo segnaposto utilizzato nel settore della tipografia e della stampa. Lorem Ipsum � considerato il testo segnaposto standard sin dal sedicesimo secolo, quando un anonimo tipografo prese una cassetta di caratteri e li assembl� per preparare un testo campione. � sopravvissuto non solo a pi� di cinque secoli, ma anche al passaggio alla videoimpaginazione, pervenendoci sostanzialmente inalterato. Fu reso popolare, negli anni �60, con la diffusione dei fogli di caratteri trasferibili �Letraset�, che contenevano passaggi del Lorem Ipsum, e pi� recentemente da software di impaginazione come Aldus PageMaker, che includeva versioni del Lorem Ipsum.';
            case 'lv':
                return 'Lorem Ipsum � tas ir teksta salikums, kuru izmanto poligrafija un makete�anas darbos. Lorem Ipsum ir kluvis par visparpienemtu teksta aizvietotaju kop� 16. gadsimta sakuma. Taja laika kads nezinams iespiedejs izveidoja teksta fragmentu, lai nodrukatu gramatu ar burtu paraugiem. Tas ir ne tikai pardzivojis piecus gadsimtus, bet bez ieverojamam izmainam saglabajies ari musdienas, parejot uz datorizetu teksta apstradi. Ta popularize�anai 60-tajos gados kalpoja Letraset burtu paraugu publice�ana ar Lorem Ipsum teksta fragmentiem un, nesena pagatne, tadas makete�anas programmas ka Aldus PageMaker, kuras �ablonu paraugos ir izmantots Lorem Ipsum teksts.';
            case 'lt':
                return 'Lorem ipsum - tai fiktyvus tekstas naudojamas spaudos ir grafinio dizaino pasaulyje jau nuo XVI a. prad�ios. Lorem Ipsum tapo standartiniu fiktyviu tekstu, kai ne�inomas spaustuvininkas atsitiktine tvarka i�deliojo raides atspaudu prese ir tokiu budu sukure raid�iu egzemplioriu. �is tekstas i�liko beveik nepasikeites ne tik penkis am�ius, bet ir i�enge i kopiuterinio grafinio dizaino laikus. Jis i�populiarejo XX a. �e�tajame de�imtmetyje, kai buvo i�leisti Letraset lapai su Lorem Ipsum i�traukomis, o veliau -leidybine sistema AldusPageMaker, kurioje buvo ir Lorem Ipsum versija.';
            case 'mk':
                return 'Lorem Ipsum ? ?????????? ????? ?? ????? ??? ?? ???????? ?? ???????????? ??????????. Lorem ipsum ??? ??????????? ???????? ??? ?? ???????? ???? ????? ???? ???? 1500 ??????, ???? ???????? ??????? ??? ?????? ?? ????? ? ?? ?????? ?? ????? ????? ?? ?? ??????? ???????? ?? ?????. ? ?? ???? ??? ???? ????? ???????? ??? ?????? ???? ?????? ?? ?? ??????? ? ?? ????????????? ???????, ??? ?? ???? ?? ? ????????. ?? ????????????? ?? 60-???? ?????? ?? ??????????? ??? ?? ?????????? ?? ?????? ?? ?????? ?? ??? ?? ??????? Lorem Ipsum ??????, ? ????? ?? ???????????? ????? ???? ??? ? Aldus PageMaker ?? ??? ?? ????? ??????? ?? Lorem Ipsum.';
            case 'ms':
                return 'Lorem Ipsum adalah text contoh digunakan didalam industri pencetakan dan typesetting. Lorem Ipsum telah menjadi text contoh semenjak tahun ke 1500an, apabila pencetak yang kurang terkenal mengambil sebuah galeri cetak dan merobakanya menjadi satu buku spesimen. Ia telah bertahan bukan hanya selama lima kurun, tetapi telah melonjak ke era typesetting elektronik, dengan tiada perubahan ketara. Ia telah dipopularkan pada tahun 1960an dengan penerbitan Letraset yang mebawa kangungan Lorem Ipsum, dan lebih terkini dengan sofwer pencetakan desktop seperti Aldus PageMaker yang telah menyertakan satu versi Lorem Ipsum.';
            case 'no':
                return 'Lorem Ipsum er rett og slett dummytekst fra og for trykkeindustrien. Lorem Ipsum har v�rt bransjens standard for dummytekst helt siden 1500-tallet, da en ukjent boktrykker stokket en mengde bokstaver for � lage et pr�veeksemplar av en bok. Lorem Ipsum har t�lt tidens tann usedvanlig godt, og har i tillegg til � best� gjennom fem �rhundrer ogs� t�lt spranget over til elektronisk typografi uten vesentlige endringer. Lorem Ipsum ble gjort allment kjent i 1960-�rene ved lanseringen av Letraset-ark med avsnitt fra Lorem Ipsum, og senere med sideombrekkingsprogrammet Aldus PageMaker som tok i bruk nettopp Lorem Ipsum for dummytekst.';
            case 'pl':
                return 'Lorem Ipsum jest tekstem stosowanym jako przykladowy wypelniacz w przemysle poligraficznym. Zostal po raz pierwszy uzyty w XV w. przez nieznanego drukarza do wypelnienia tekstem pr�bnej ksiazki. Piec wiek�w p�zniej zaczal byc uzywany przemysle elektronicznym, pozostajac praktycznie niezmienionym. Spopularyzowal sie w latach 60. XX w. wraz z publikacja arkuszy Letrasetu, zawierajacych fragmenty Lorem Ipsum, a ostatnio z zawierajacym r�zne wersje Lorem Ipsum oprogramowaniem przeznaczonym do realizacji druk�w na komputerach osobistych, jak Aldus PageMaker';
            case 'pt':
                return 'O Lorem Ipsum � um texto modelo da ind�stria tipogr�fica e de impress�o. O Lorem Ipsum tem vindo a ser o texto padr�o usado por estas ind�strias desde o ano de 1500, quando uma misturou os caracteres de um texto para criar um esp�cime de livro. Este texto n�o s� sobreviveu 5 s�culos, mas tamb�m o salto para a tipografia electr�nica, mantendo-se essencialmente inalterada. Foi popularizada nos anos 60 com a disponibiliza��o das folhas de Letraset, que continham passagens com Lorem Ipsum, e mais recentemente com os programas de publica��o como o Aldus PageMaker que incluem vers�es do Lorem Ipsum.';
            case 'ro':
                return 'Lorem Ipsum este pur si simplu o macheta pentru text a industriei tipografice. Lorem Ipsum a fost macheta standard a industriei �nca din secolul al XVI-lea, c�nd un tipograf anonim a luat o planseta de litere si le-a amestecat pentru a crea o carte demonstrativa pentru literele respective. Nu doar ca a supravietuit timp de cinci secole, dar si a facut saltul �n tipografia electronica practic neschimbata. A fost popularizata �n anii \'60 odata cu iesirea colilor Letraset care contineau pasaje Lorem Ipsum, iar mai recent, prin programele de publicare pentru calculator, ca Aldus PageMaker care includeau versiuni de Lorem Ipsum.';
            case 'ru':
                return 'Lorem Ipsum - ??? ?????-"????", ????? ???????????? ? ?????? ? ???-???????. Lorem Ipsum ???????? ??????????? "?????" ??? ??????? ?? ???????? ? ?????? XVI ????. ? ?? ????? ????? ?????????? ???????? ?????? ??????? ????????? ???????? ? ???? ???????, ????????? Lorem Ipsum ??? ?????????? ????????. Lorem Ipsum ?? ?????? ??????? ??????? ??? ???????? ????????? ???? ?????, ?? ? ?????????? ? ??????????? ??????. ??? ????????????? ? ????? ????? ????????? ?????????? ?????? Letraset ? ????????? Lorem Ipsum ? 60-? ????? ?, ? ????? ???????? ?????, ????????? ??????????? ??????? ???? Aldus PageMaker, ? ???????? ??????? ???????????? Lorem Ipsum.';
            case 'sr':
                return 'Lorem Ipsum ?? ??????????? ????? ?????? ???? ?? ??????? ? ??????????? ? ?????????????? ??????????. Lorem ipsum ?? ??? ???????? ?? ????? ?????? ??? ?? 1500. ??????, ???? ?? ????????? ??????? ???? ?????? ?? ??????? ? ?????? ?? ???? ?? ???????? ?????? ?????. ?? ???? ??? ?? ???? ????? ?????? ??? ??????, ???? ?? ??? ????? ?? ?? ??????? ? ? ???????????? ????????, ???????????? ??. ????????????? ?? ?????????? ?????? ?????????? ???? ??????? ?? ????????? ????????? ???? ?? ???????? Lorem Ipsum ??????, ? ????? ?? ??????????? ??????? ?? ?????? ??? ??? ?? Aldus PageMaker ???? ?? ??????? Lorem Ipsum ???????.';
            case 'sk':
                return 'Lorem Ipsum je fikt�vny text, pou��van� pri n�vrhu tlacov�n a typografie. Lorem Ipsum je �tandardn�m v�plnov�m textom u� od 16. storocia, ked nezn�my tlaciar zobral sadzobnicu pln� tlacov�ch znakov a pomie�al ich, aby tak vytvoril vzorkov� knihu. Pre�il nielen p�t storoc�, ale aj skok do elektronickej sadzby, a pritom zostal v podstate nezmenen�. Spopularizovan� bol v 60-tych rokoch 20.storocia, vydan�m h�rkov Letraset, ktor� obsahovali pas�e Lorem Ipsum, a nesk�r aj publikacn�m softv�rom ako Aldus PageMaker, ktor� obsahoval verzie Lorem Ipsum.';
            case 'sl':
                return 'Lorem Ipsum je slepi tekst, ki se uporablja pri razvoju tipografij in pri pripravi za tisk. Lorem Ipsum je v uporabi �e vec kot petsto let saj je to kombinacijo znakov neznani tiskar zdru�il v vzorcno knjigo �e v zacetku 16. stoletja. To besedilo pa ni zgolj pre�ivelo pet stoletij, temvec se je z malenkostnimi spremembami uspe�no uveljavilo tudi v elektronskem namiznem zalo�ni�tvu. Na priljubljenosti je Lorem Ipsum pridobil v sedemdesetih letih prej�njega stoletja, ko so na trg lansirali Letraset folije z Lorem Ipsum odstavki. V zadnjem casu se Lorem Ipsum pojavlja tudi v priljubljenih programih za namizno zalo�ni�tvo kot je na primer Aldus PageMaker.';
            case 'es':
                return 'Lorem Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto. Lorem Ipsum ha sido el texto de relleno est�ndar de las industrias desde el a�o 1500, cuando un impresor (N. del T. persona que se dedica a la imprenta) desconocido us� una galer�a de textos y los mezcl� de tal manera que logr� hacer un libro de textos especimen. No s�lo sobrevivi� 500 a�os, sino que tambien ingres� como texto de relleno en documentos electr�nicos, quedando esencialmente igual al original. Fue popularizado en los 60s con la creaci�n de las hojas "Letraset", las cuales contenian pasajes de Lorem Ipsum, y m�s recientemente con software de autoedici�n, como por ejemplo Aldus PageMaker, el cual incluye versiones de Lorem Ipsum.';
            case 'sv':
                return 'Lorem Ipsum �r en utfyllnadstext fr�n tryck- och f�rlagsindustrin. Lorem ipsum har varit standard �nda sedan 1500-talet, n�r en ok�nd boks�ttare tog att antal bokst�ver och blandade dem f�r att g�ra ett provexemplar av en bok. Lorem ipsum har inte bara �verlevt fem �rhundraden, utan �ven �verg�ngen till elektronisk typografi utan st�rre f�r�ndringar. Det blev allm�nt k�nt p� 1960-talet i samband med lanseringen av Letraset-ark med avsnitt av Lorem Ipsum, och senare med mjukvaror som Aldus PageMaker.';
            case 'th':
                return 'Lorem Ipsum ??? ????????????????????? ?????????????????????????????????????????? ?????????????????????????????????????????????????????????????????????? 16 ????????????????????????????????????????????????????????????????????????????????????????????? Lorem Ipsum ??????????????????????????????????????? ???????????????????????????????????????????????????????????????????????? ??????????????????????????????????????????? ????????????????????????????? ?.?. 1960 ????????? Letraset ???????????????????????????????? Lorem Ipsum ????????????????? ??????????????????????????????????? (Desktop Publishing) ????? Aldus PageMaker ????????? Lorem Ipsum ?????????????? ??????????????????????';
            case 'tr':
                return 'Lorem Ipsum, dizgi ve baski end�strisinde kullanilan migir metinlerdir. Lorem Ipsum, adi bilinmeyen bir matbaacinin bir hurufat numune kitabi olusturmak �zere bir yazi galerisini alarak karistirdigi 1500\'lerden beri end�stri standardi sahte metinler olarak kullanilmistir. Besy�z yil boyunca varligini s�rd�rmekle kalmamis, ayni zamanda pek degismeden elektronik dizgiye de si�ramistir. 1960\'larda Lorem Ipsum pasajlari da i�eren Letraset yapraklarinin yayinlanmasi ile ve yakin zamanda Aldus PageMaker gibi Lorem Ipsum s�r�mleri i�eren masa�st� yayincilik yazilimlari ile pop�ler olmustur.';
            case 'uk':
                return 'Lorem Ipsum - ?? ?????-"????", ?? ???????????????? ? ?????????? ?? ???????. Lorem Ipsum ?, ????????, ??????????? "?????" ?? ? XVI ????????, ???? ????????? ?????? ???? ???????? ?????? ?? ????? ?? ??? ???????? ??????? ???????. "????" ?? ?????? ??????? ???????? ?\'??? ???????, ??? ? ????????? ? ???????????? ???????????, ??????????? ?? ???? ?????????. ???? ???????????????? ? 60-?? ????? ???????? ???????? ??????? ??????? ??????? ??????? Letraset, ??? ??????? ?????? ? Lorem Ipsum, ? ?????? - ????????? ??????? ????????? ????\'???????? ??????????? ?? ?????? Aldus Pagemaker, ??? ??????????????? ????? ?????? Lorem Ipsum.';
            case 'vi':
                return 'Lorem Ipsum ch? don gi?n l� m?t do?n van b?n gi?, du?c d�ng v�o vi?c tr�nh b�y v� d�n trang ph?c v? cho in ?n. Lorem Ipsum d� du?c s? d?ng nhu m?t van b?n chu?n cho ng�nh c�ng nghi?p in ?n t? nh?ng nam 1500, khi m?t h?a si v� danh gh�p nhi?u do?n van b?n v?i nhau d? t?o th�nh m?t b?n m?u van b?n. �o?n van b?n n�y kh�ng nh?ng d� t?n t?i nam th? k?, m� khi du?c �p d?ng v�o tin h?c van ph�ng, n?i dung c?a n� v?n kh�ng h? b? thay d?i. N� d� du?c ph? bi?n trong nh?ng nam 1960 nh? vi?c b�n nh?ng b?n gi?y Letraset in nh?ng do?n Lorem Ipsum, v� g?n d�y hon, du?c s? d?ng trong c�c ?ng d?ng d�n trang, nhu Aldus PageMaker.';

            case 'en':
            default:
                return 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.';

        }
    }



    // ----------------------------------------------------------------------
    // INIT DEMO
    // ----------------------------------------------------------------------

    let userLangFull = (navigator.language || navigator.userLanguage).split('-', 2);
    let userLang = userLangFull[0];

    function prepareDemo() {
        let sel_lang = document.getElementById('lang');
        for(let entry in countryNames) {
            sel_lang.options[sel_lang.options.length] = new Option(countryNames[entry], entry);
        }
        sel_lang.value = userLang;
    }


        // ----------------------------------------------------------------------
        // DEMO TEXT
        // ----------------------------------------------------------------------
        function printDemoText() {
            let text = document.getElementById('demotext').value;
            var c = new PosPrinterJob(getCurrentDriver(), getCurrentTransport());
            c.initialize();
            c.setEncoding(defaultCP[userLang]);

            let align = document.querySelector('input[name="align"]:checked').value;
            c.setAlignment(align);
            let font_flags  = document.querySelectorAll('input[name="font"]');

            let i;
            let font = 0;
            for (i = 0; i < font_flags.length; i++) {
                if (font_flags[i].checked) {
                    font = font + 1*font_flags[i].value;
                }
            }
            c.setPrintMode(font);

            c.printLine(text);
            c.feed(2);
            c.execute();
        }

        // ----------------------------------------------------------------------
        // DEMO BARCODE
        // ----------------------------------------------------------------------
        function printDemoBarCode() {
            let tip = document.getElementById('bartype').value;
            let text = document.getElementById('demobarcode').value;
            let hri = document.querySelector('input[name="hri"]:checked').value;
            let h = document.getElementById('barcode_h').value;


            let c = new PosPrinterJob(getCurrentDriver(), getCurrentTransport());
            c.initialize();
            c.setBarcodeTextPosition(hri).setBarcodeHeight(h);
            c.printBarCode(text,tip).lf();
            c.feed(2);
            c.execute();
        }

    function democode(s) {
        let o = s.options;
        let i;
        for(i=0;i<o.length;i++){
            if(o[i].value == s.value){
                document.getElementById("demobarcode").value = o[i].attributes['data-example'].value;
            }
        }
    }

        // ----------------------------------------------------------------------
        // DEMO QRCODE
        // ----------------------------------------------------------------------
        function printQrCode(kdstore,strukid) {
            let code = 'https://update.hokben.co.id/satisfaction/store?id='+kdstore+':::'+strukid; 
			//document.getElementById('demoqrcode').value;
			
            let e = 3;
            let s = 8;
            let m = 2;

            let c = new PosPrinterJob(getCurrentDriver(), getCurrentTransport());
            c.initialize();
            c.center();
            c.printQrCode(code,e,s,m).lf();
//            c.left().setPrintMode(c.FONT_SIZE_SMALL).printLine(code);
            c.feed(1);
			c.cut();
            c.execute();
        }
		
		function cut() {
        

            let c = new PosPrinterJob(getCurrentDriver(), getCurrentTransport());
            c.initialize();
            c.center();
         
            c.feed(1);
			c.cut();
            c.execute();
        }

//prepareDemo();
//document.getElementById("demo").style.display="block";