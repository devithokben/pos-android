<?php 
	session_start();
	date_default_timezone_set("Asia/Bangkok");
	error_reporting(1);
	include("dbconnect.php");
	include("template/header_h2h.php");
	$get_prod 	= mysqli_query($conpos,"select * from tbl_lst_disc where discid = '".$_GET['id']."'");
	$dataProd	= mysqli_fetch_object($get_prod);
?>

	<link href="css/jquery.dataTables.min.css" rel="stylesheet">
	<div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="page-breadcrumb">
                <div class="row align-items-center">
					
					<div class="card col-xs-12 col-md-12 col-sm-12" >
						<table width="100%" class="table table-striped table-bordered">
							<tr>
								<th>
									Detail 
								</th>
							</tr>
						</table>
						<table width="100%" class="table table-striped table-bordered">
							<tr>
								<th>
									Id
								</th>
								<th>
									Name
								</th>
								<th>
									Period
								</th>
							</tr>
							<tr>
								<td>
									<input type="text" name="discid" value="<?php echo $dataProd->discid ?>" style="width:50px;" />
								</td>
								<td>
									<input type="text" name="discname" value="<?php echo $dataProd->discname ?>" style="width:350px;" />
								</td>
								<td>
									<input type="text" name="startdate" value="<?php echo $dataProd->startdate ?>" style="width:200px;" /> s/d 
									<input type="text" name="enddate" value="<?php echo $dataProd->enddate ?>" style="width:200px;" />
								</td>
							</tr>
						</table>
						<table width="100%" class="table table-striped table-bordered">
							<tr>
								<th>
									Type
								</th>
								<th>
									Value
								</th>
								<th>
									Max Amount
								</th>
							</tr>
							<tr>
								<td>
									<select name="disctype">
										<option value="1" <?php if($dataProd->disctype == 1){echo "SELECTED";} ?>>Percent</option>
										<option value="2" <?php if($dataProd->disctype == 2){echo "SELECTED";} ?>>Amount</option>
										<option value="3" <?php if($dataProd->disctype == 3){echo "SELECTED";} ?>>Custom</option>
									</select>
									
								<td>
									<input type="text" name="discvalue" value="<?php echo $dataProd->discvalue ?>" style="width:200px;" />
								</td>
								<td>
									<input type="text" name="discvaluemax" value="<?php echo $dataProd->discvaluemax ?>" style="width:200px;" />
								</td>
							</tr>
						</table>
						<table width="100%" class="table table-striped table-bordered">
							<tr>
								<th>
									Target
								</th>
								<th>
									Service Type
								</th>
								<th>
									Reason
								</th>
							</tr>
							<tr>
								<td>
									<select name="disctarget">
										<option value="1" <?php if($dataProd->disctarget == 1){echo "SELECTED";} ?>>Item Discount</option>
										<option value="2" <?php if($dataProd->disctarget == 2){echo "SELECTED";} ?>>Sales Discount</option>
										<option value="3" <?php if($dataProd->disctarget == 3){echo "SELECTED";} ?>>Both</option>
									</select>
								</td>
								<td>
									<input type="text" name="trxcode" value="<?php echo $dataProd->trxcode ?>" style="width:200px;" />
								</td>
								<td>
									<select name="dept">
										<?php
											
											$get_prod_group 	= mysqli_query($conpos,"select * from tbl_lst_disc_reason");
											while($dataProd_group	= mysqli_fetch_object($get_prod_group)){
												if($dataProd_group->id == $dataProd->idreason){
													$select = "SELECTED";
												}else{
													$select = "";
												}
										?>
											
											<option value="<?php echo $dataProd_group->id?>" <?php echo $select ?>><?php echo $dataProd_group->id?> :: <?php echo $dataProd_group->description?></option>
										<?php
											}
										?>
										</select>
								</td>
							</tr>
						</table>
						<table width="100%" class="table table-striped table-bordered">
							<tr>
								<th>
									Pop UP
								</th>
								<th>
									Active
								</th>
							</tr>
							<tr>
								<td>
									<select name="winpopup">
										<option value="0" <?php if($dataProd->winpopup == 0){echo "SELECTED";} ?>>No PopUp</option>
										<option value="1" <?php if($dataProd->winpopup == 1){echo "SELECTED";} ?>>PopUp 1 Line</option>
										<option value="2" <?php if($dataProd->winpopup == 2){echo "SELECTED";} ?>>PopUp 2 Line</option>
									</select>
								</td>
								<td>
									<?php
											if($dataProd->onaktif == 1){
												echo "<input type='checkbox' name='onaktif' Checked> ";
											}else{
												echo "<input type='checkbox' name='onaktif' > ";
											}
										?>
								</td>
							</tr>
						</table>
					</div>
				</div>
			</div>
		</div>
<?php include("template/footer_h2h.php");?>



<script src="<?php echo  $base_url ?>js/jquery.dataTables.min.js" type="text/javascript"></script>
<script>
	$(document).ready(function() {
		$('#table_locd').DataTable();
	} );
</script>