<div id="modal_list_layanan" class="pay-modal">

  <!-- Modal content -->
  <div class="pay-modal-content" style="width:60%">
	<strong style="font-size:25px;">PILIH LAYANAN</strong><span class="pay-close" onclick="close_pilih_layanan()">&times;</span>
	<hr />
	<div id="load_list_layanan"></div>
	<div style="clear:both;"></div>
  </div>
	
</div>
<div id="modal_list_layanan_delivery" class="pay-modal" >

  <!-- Modal content -->
  <div class="pay-modal-content" style="width:60%">
	<strong style="font-size:25px;">PILIH LAYANAN DELIVERY</strong><span class="pay-close" onclick="close_pilih_layanan_delivery()">&times;</span>
	<hr />
	<div id="load_list_layanan_delivery"></div>
	<div style="clear:both;"></div>
  </div>

</div>

<div id="modal_list_srvcharge" class="pay-modal" style="padding:10px;padding-top:20px;">
	
	  <!-- Modal content -->
	<div class="pay-modal-content">
		<div class="col-xs-12 col-md-12 col-sm-12"> 
			<div class="row">
				<h3>Biaya Kirim <span class="pay-close" onclick="close_modal_list_srvcharge()">&times;</span></h3><hr />
				<div class="col-xs-4 col-md-4 col-sm-4"> 
					<span style="font-weight:bold;font-size:24px;">Rp : </span>
				</div>
				<div class="col-xs-8 col-md-8 col-sm-8"> 
					<input type="text" style="font-weight:bold;font-size:24px;width:100%;" id="val_srvcharge" class="form numeric"/>
				</div>
				<div class="col-xs-12 col-md-12 col-sm-12"> 
				<hr />
				</div>
				<button class="btn btn-success col-xs-12 col-md-12 col-sm-12" style="font-size:20px;width:100%"  onClick="prc_srvcharge()">Process</button>
			</div>
		</div>
		<div style="clear:both;"></div>
	</div>
</div>

<div id="modal_list_tacharge" class="pay-modal">

  <!-- Modal content -->
  <div class="pay-modal-content">
	<strong style="font-size:25px;">PILIH TA CHARGE</strong><span class="pay-close" onclick="close_pilih_tacharge()">&times;</span>
	<hr />
		<input type="hidden" id="TaChargeVal" />
		<input type="hidden" id="layananVal" />
		<a href="#" class="btn btn-primary btn-lg col-lg-6 col-xlg-6 col-md-6" role="button" onclick="setTaCharge('1')" style="color:#000;">TA CHARGE</a>
		<a href="#" class="btn btn-success btn-lg col-lg-6 col-xlg-6 col-md-6" role="button" onclick="setTaCharge('0')" style="color:#000;">FREE TA CHARGE</a>
	<div style="clear:both;"></div>
  </div>

</div>

<div id="modal_id_delivery" class="pay-modal" >

  <!-- Modal content -->
  <div class="pay-modal-content">
	<span class="pay-close" onclick="close_modal_id_delivery()">&times;</span>
	<p>
		
		<div class="col-lg-12 col-xlg-12 col-md-12">
			<div class="row">
				<div class="col-lg-3 col-xlg-3 col-md-3">
					<span id="prefix" style="font-size:16px;font-weight:bold"></span>
				</div>
				<div class="col-lg-9 col-xlg-9 col-md-9">
					<input type="text" id="val_id_rider"  style="font-size:16px;font-weight:bold" class="form numeric"/>
				</div>
				<div class="col-lg-12 col-xlg-12 col-md-12">
					<hr />
					<input type="button" onclick="SubmitRider()" value="Proccess" class="btn btn-success"  style="font-size:18px;font-weight:bold" />
				</div>
			</div>
		</div>
	</p>
	<div style="clear:both"></div>
  </div>
	
</div>
<div id="modal_id_delivery_int" class="pay-modal" >

  <!-- Modal content -->
  <div class="pay-modal-content">
	<strong style="font-size:25px;">Hokben Delivery Number</strong><span class="pay-close" onclick="close_modal_id_delivery_internal()">&times;</span>
	<p>
		<div class="col-lg-12 col-xlg-12 col-md-12">
			<div class="row">
				
				<div class="col-lg-12 col-xlg-12 col-md-12">
					<input type="text" id="val_id_rider_int"  style="font-size:16px;font-weight:bold;width:100%" class="form character"/>
					<a href="#" class="btn btn-warning" onCLick="setValInt()">INT </a>
				</div>
				
				<div class="col-lg-12 col-xlg-12 col-md-12">
					<hr />
					<input type="button" onclick="SubmitRider()" value="Proccess" class="btn btn-success"  style="font-size:18px;font-weight:boldl" />
				</div>
			</div>
		</div>
	</p>
	 <div style="clear:both;"></div>
  </div>
 
</div>

<div id="modal_id_mask" class="pay-modal" >

  <!-- Modal content -->
  <div class="pay-modal-content">
	<strong style="font-size:25px;" id="titlemask"></strong><span class="pay-close" onclick="close_modal_id_delivery_internal()">&times;</span>
	<p>
		<div class="col-lg-12 col-xlg-12 col-md-12">
			<div class="row">
				<div class="col-lg-3 col-xlg-3 col-md-3">
					<span id="prefix_mask" style="font-size:16px;font-weight:bold"></span>
				</div>
				<div class="col-lg-9 col-xlg-9 col-md-9">
					<input type="text" id="val_id_mask"  style="font-size:16px;font-weight:bold" class="form numeric"/>
					<input type="hidden" id="promoid"  style="font-size:16px;font-weight:bold" class="form"/>
					<input type="hidden" id="multiply"  style="font-size:16px;font-weight:bold" class="form"/>
					<input type="hidden" id="amount_promo"  style="font-size:16px;font-weight:bold" class="form"/>
				</div>
				<div class="col-lg-12 col-xlg-12 col-md-12">
					<hr />
					<input type="button" onclick="submitMask()" value="Proccess" class="btn btn-success"  style="font-size:18px;font-weight:boldl" />
				</div>
			</div>
		</div>
	</p>
	 <div style="clear:both;"></div>
  </div>
 
</div>


<!-- keyboard widget css & script (required) -->
		<link href="Keyboard/css/keyboard.css" rel="stylesheet">
		<script src="Keyboard/js/jquery.keyboard.js"></script>

		<!-- keyboard extensions (optional) -->
		<script src="Keyboard/js/jquery.mousewheel.js"></script>
		<script>
			$(function(){
				$('.numeric').keyboard({layout : 'num' });
				$('.character').keyboard({layout: 'qwerty'});
				// console.log($(this), "amount")
				// updateTextView($(this));
			});
		</script>
		<!-- <script>
			function updateTextView(_obj){
			var num = getNumber(_obj.val());
			if(num==0){
				_obj.val('');
			}else{
				_obj.val(num.toLocaleString());
			}
			}
			function getNumber(_str){
			var arr = _str.split('');
			var out = new Array();
			for(var cnt=0;cnt<arr.length;cnt++){
				if(isNaN(arr[cnt])==false){
				out.push(arr[cnt]);
				}
			}
			return Number(out.join(''));
			}
			$(document).ready(function(){
			$('input[type=text]').on('keyup',function(){
				updateTextView($(this));
			});
			});
		</script> -->


