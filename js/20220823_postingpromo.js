function add_promoitem(i,qty=1,disc=0){
    var divObj 		= 1;
	var divlayanan	= $("#layanan").text();//'DI';
	var saldesDisc	= 0;
	var servicecharge	= 0;
	var priceDisc	= 0;
	var discount	= disc;
	
	$.ajax({
		type : 'GET',
		url : 'get_product_layanan.php?layanan='+divlayanan+'&plu='+i,
		success : function(data){
			//alert(data);
			var result = JSON.parse(data);
			//console.log(result);
			if(data == 0){
				
				$("#result").html('');
				Swal.fire({
				  title: 'Error!',
				  text: '"Layanan/PLU Tidak Active"',
				  icon: 'error',
				  confirmButtonText: 'CLOSE'
				});
				return false; 
			}
			var pr = result ;
			//alert(discount);
			if(discount != 0){
				var amount_disc	= parseFloat(discount.replace(",",""));
				if(amount_disc){
					if(amount_disc > 100){
						var discVal = (amount_disc/(qty*pr.UNIT_PRICE))* 100;
						//alert(discVal);
					}else{						
						var discVal = 110
					}
				}else{
					var discVal = 0
				}
			}
			realprice = (1 * pr.UNIT_PRICE);
			total = (1 * pr.UNIT_PRICE);
			//console.log(total);
			unique = Date.now();
			seq_no = $("#seq_no").val();
			
			
			$("#seq_no").val(parseInt(seq_no) + 1);
			strukid = '1111';
			$('#list').append(
				'<tr tabindex="'+i+'" id="un_xxx_disc" class="finishclear" onclick=tmpvalue("un_xxx_disc")> '+
					'<td width="35%" class="itemid" align="left" style="display:none;">'+pr.PRODCODE+'</td> '+
					'<td width="35%" class="itemname" align="left">'+pr.PRODNAME+'</td> '+
					'<td width="10%" class="coretype" align="center">'+pr.TRX_CODE+'</td> '+
					'<td width="10%" class="coreqty" align="center">'+qty+'</td> '+
					'<td width="10%" class="discount" align="center">"'+discVal+'"</td> '+
					'<td width="15%" align="right">'+numberWithCommas(realprice)+'</td> '+
					'<td width="15%" align="right" class="totalprice">'+numberWithCommas(total)+'</td> '+
					'<td width="15%" align="right" class="upgrade" style="display:none;">0</td> '+
					'<td width="5%" style="display:none;"> '+
						'<table> '+
							'<tr> '+
								'<td colspan="6"> '+
									'<input type="hidden" name="PRICE[]" value="'+total+'">'+
									'<input type="hidden" class="realprice" value="'+realprice+'">'+
									'<input type="hidden" name="SEQ_NO[]" value="'+seq_no+'">'+
									'<input type="hidden" name="type[]" value="'+pr.TRX_CODE+'">'+
									'<input type="hidden" name="disc[]" value="'+discVal+'" class="discountval">'+
									'<input type="hidden" name="PRODCODE[]" value="'+i+'">'+
									'<input type="hidden" name="QTY[]" value="'+qty+'">'+
									'<input type="hidden" name="PRODNAME[]" value="'+pr.PRODNAME+'">'+
									'<input type="hidden" name="PROMOID[]"  class="promoid">'+
									'<input type="hidden" name="PROMODESC[]"   class="promodesc">'+
									'<input type="hidden" name="DISCTYPE[]" class="disctype">'+
									'<input type="hidden" name="DISCVALUE[]" value="'+discVal+'" class="discvalue">'+
									
								'</td> '+
							'</tr> '+
						'</table> '+
					'</td> '+
					'<td class="amountdisc" style="display:none;">0</td> '+
				'</tr>');
			
			//alert(unique);
			$("#unik_dlcharge").val(unique);
			
			//detail_disc_proc(110.00,1,"","",1,1)
			
			totalpay(total, 0,discount)
			subtotal();

			$("#sales_discount").val('');
			$("#val_disc_type").val('item');
			$('#tmpvalue').val('_xxx_disc');
			$("#item_discount").show();
			
			$('#tmpvalue').val('_xxx_disc')
			detail_disc_proc(discVal,1,"","",1,0)
		//	disc_proc(110.00,1,"","",0)

			
			cleartxt();
		}
	});
}

function submitMask(){
	var pref_mask = $("#prefix_mask").text();
	var id_mask = $("#val_id_mask").val();
	var multiply = $("#multiply").val();
	$("#mask_delivery_id").val(pref_mask+id_mask);
	//mypayBtn();
	
	
	var modalpay = document.getElementById('modal_id_mask');
	modalpay.style.display = "none";
	
	
	
	
	//declare payment 20201031
	id		= $("#promoid").val();
	title	= $("#titlemask").text();
	
	gross_bayar	= $("#totalpay").text();
	$("#gross_bayar").val(gross_bayar);
	
	$sales_discount_bayar	= $("#amountdisc_vw").text();
	//alert("masuk4");
	$("#sales_discount").val($sales_discount_bayar*multiply);
	
	sales_servicecharge	= $("#servicechargeval").text();
	$("#sales_servicecharge").val(sales_servicecharge);
	
	sales_total	= $("#subtotal").text();
	$("#totalpay").val(sales_total);
	
	sales_subtotal	= $("#subtotal").text();
	$("#subtotal2").val(sales_subtotal);
	
	sales_pembulatan	= $("#pembulatanval").text();
	$("#pembulatan2").val(sales_pembulatan);
	
	sales_pajakresto	= $("#taxresto").text();
	$("#sales_pajakresto").val(sales_pajakresto);
	
	//setVoucher
	var prefix_mask 	= $("#prefix_mask").text();
	var amount_promo 	= $("#amount_promo").val();
	if(amount_promo != 0){
		$("#voucher_number_1").val(prefix_mask+id_mask);
		$("#voucher_amount_1").val(amount_promo);
		$("#non_tunai").val(amount_promo);
	}else{
		$("#voucher_number_1").val(prefix_mask+id_mask);
		$("#voucher_amount_1").val(sales_subtotal.replace(",",""));
		$("#non_tunai").val(sales_subtotal.replace(",",""));
	}
	
	$("#cancel_payment").hide();
	
					
	var array_prodname	= $('#frmtrans').getForm2obj();
	//console.log(array_prodname['PRODCODE']);
	var prodcode	= array_prodname['PRODCODE'];
	var prodname	= array_prodname['PRODNAME'];
	var qty			= array_prodname['QTY'];
	var price		= array_prodname['PRICE'];
	var type		= array_prodname['type'];
	var disc		= array_prodname['disc'];
	
	var i;
	var lst_detail = '';
	var amountBeforeTax	= 0;
	$("#temp_order").html('');
	for (i = 0; i < prodcode.length; i++) {
		lst_detail	+= "<tr>" +
							"<td>"+prodcode[i]+"</td>" +
							"<td>"+prodname[i]+"</td>" +
							"<td>"+qty[i]+"</td>" +
							"<td>"+numberWithCommas(price[i])+"</td>" +
							"<td>"+type[i]+"</td>" +
							"<td>"+numberWithCommas(disc[i])+"</td>" +
						"</tr>";
		amountBeforeTax	+= (parseFloat(price[i]) * parseFloat(qty[i])) - parseFloat(disc[i]);
	}
	
	$("#temp_order").append(lst_detail);
	
	cek_lineType_promo(id,amountBeforeTax,btoa(title));
	var sbt_str		= $('#subtotal2').val();
	var amount_sbb	= parseFloat(sbt_str.replace(",",""));
	var kembalian 	= ($('#non_tunai').val() + $('#tunai').val()) - amount_sbb;
	
	$("#kembali").val(kembalian);
	
	$("#del_voucher_1").hide();
	$("#add_voucher_1").hide();
	$('#voucher_number_1').attr('readonly', true);
	$('#voucher_amount_1').attr('readonly', true);
	//alert("mauk");
		
}

function mypayBtnMask_proc(id,mask,title,promoname,amountpromo,multiply=1){
	Swal.fire({
	  title: 'CONFIRM!',
	  text: "Apakah Transaksi Menggunakan Promo " + title + " ..?",
	  icon: 'question',
	  type: "warning",
      showCancelButton: true,
	  confirmButtonText: 'YES',
	  cancelButtonText: 'NO'
	}).then((result) => {
	  if (result.isConfirmed) {
			var modalpay = document.getElementById('mypayModal');
			modalpay.style.display = "none";
			
			$("#titlemask").html(title);
			$("#prefix_mask").html(mask);
			$("#amount_promo").val(amountpromo);
			$("#multiply").val(multiply);
			$("#promoid").val(id);
			
			var modalpay = document.getElementById('modal_id_mask');
			modalpay.style.display = "block";
		
		}else{
			Swal.fire({
			  title: 'Error!',
			  text: 'Promo di batalkan',
			  icon: 'error',
			  confirmButtonText: 'CLOSE'
			});
		}
	});
}
function mypayBtn_proc(id,title,multiply=1){
	//r = confirm("Apakah Transaksi Menggunakan Promo " + atob(title) + " ..?");

	
	r=false;
	Swal.fire({
	  title: 'CONFIRM!',
	  text: "Apakah Transaksi Menggunakan Promo " + title + " ..?",
	  icon: 'question',
	  type: "warning",
      showCancelButton: true,
	  confirmButtonText: 'YES',
	  cancelButtonText: 'NO'
	}).then((result) => {
	  if (result.isConfirmed) {
		var modalpay = document.getElementById('mypayModal');
		modalpay.style.display = "none";
		
		$("#cancel_payment").hide();
		
		
		//declare payment 20201031
		gross_bayar	= $("#totalpay").text();
		$("#gross_bayar").val(gross_bayar);
		
		$sales_discount_bayar	= $("#amountdisc_vw").text();
		//alert("masuk3");
		$("#sales_discount").val($sales_discount_bayar * multiply);
		
		sales_servicecharge	= $("#servicechargeval").text();
		$("#sales_servicecharge").val(sales_servicecharge);
		
		sales_total	= $("#subtotal").text();
		$("#totalpay").val(sales_total);
		
		sales_subtotal	= $("#subtotal").text();
		$("#subtotal2").val(sales_subtotal);
		
		sales_pembulatan	= $("#pembulatanval").text();
		$("#pembulatan2").val(sales_pembulatan);
		
		sales_pajakresto	= $("#taxresto").text();
		$("#sales_pajakresto").val(sales_pajakresto);
		
		
		
						
		var array_prodname	= $('#frmtrans').getForm2obj();
		//console.log(array_prodname['PRODCODE']);
		var prodcode	= array_prodname['PRODCODE'];
		var prodname	= array_prodname['PRODNAME'];
		var qty			= array_prodname['QTY'];
		var price		= array_prodname['PRICE'];
		var type		= array_prodname['type'];
		var disc		= array_prodname['disc'];
		
		var i;
		var lst_detail = '';
		var amountBeforeTax	= 0;
		$("#temp_order").html('');
		for (i = 0; i < prodcode.length; i++) {
			lst_detail	+= "<tr>" +
								"<td>"+prodcode[i]+"</td>" +
								"<td>"+prodname[i]+"</td>" +
								"<td>"+qty[i]+"</td>" +
								"<td>"+numberWithCommas(price[i])+"</td>" +
								"<td>"+type[i]+"</td>" +
								"<td>"+numberWithCommas(disc[i])+"</td>" +
							"</tr>";
			amountBeforeTax	+= (parseFloat(price[i]) * parseFloat(qty[i])) - parseFloat(disc[i]);
		}
		
		$("#temp_order").append(lst_detail);
		
		cek_lineType_promo(id,amountBeforeTax,title,multiply);
		
	  }else
	  {
		Swal.fire({
		  title: 'Error!',
		  text: 'Promo di batalkan',
		  icon: 'error',
		  confirmButtonText: 'CLOSE'
		});
	  }
	});
	
	
}
function cek_lineType_promo(promoid,amountBeforeTax,title,multiply=1){
	//getDetailIfItemGroup
	$(".btn-cancel-payment").hide();
	
	//alert('get_itemGroup.php?promoid='+promoid+'&amount='+amountBeforeTax);
	$.ajax({
		type : 'post',
		url : 'get_itemGroup.php?promoid='+promoid+'&amount='+amountBeforeTax,
		data : $("#frmtrans").serialize(),
		success : function(data){	
		//alert(data);
			var sales_disc	= "0";
			var obj 		= JSON.parse(data);
			
			$("#promo_id_bayar").val(obj.promo_id);
			$("#promo_value_bayar").val(obj.promo_val);
			$("#val_disc_val").val(obj.promo_val);
			//alert(obj.promo_val);

			if(obj.promo_type == 'sales'){
				$("#val_disc_type").val(obj.promo_type);
				//alert(obj.promo_val+"*"+multiply);
				if(obj.promo_val == 1){
					$("#sales_discount").val(1);
				}else{
					//alert("masuk2");
					$("#sales_discount").val(numberWithCommas(obj.promo_val.replace(",","")*multiply));
				}
				//alert("test1");
				$("#amountdisc_vw").html(obj.promo_val);				
				
				if($("#sales_discount").val() == 1){
					var sales_disc			= "0";
				}else{
					var sales_disc			= $("#sales_discount").val();
				}	
				//alert('masuk2');
				
			}else if(obj.promo_type == 'item'){
				if(obj.promo_val == 1){
					$("#val_disc_type").val(obj.promo_type);
					$("#item_discount").val(numberWithCommas(1*multiply));
				}else{
					$("#val_disc_type").val(obj.promo_type);
					$("#item_discount").val(numberWithCommas(obj.promo_val.replace(",","")*multiply));
				}
			}else if(obj.promo_type == 'tupperware'){
				if(obj.promo_val == 1){
					$("#val_disc_type").val(obj.promo_type);
					$("#item_discount").val(numberWithCommas(1*multiply));
				}else{
					$("#val_disc_type").val(obj.promo_type);
					$("#item_discount").val(numberWithCommas(obj.promo_val.replace(",","")*multiply));
				}
				var sales_disc			= "0";
			}else if(obj.promo_type == 'itemdiscount'){
				if(obj.promo_val == 1){
					$("#val_disc_type").val(obj.promo_type);
					$("#item_discount").val(numberWithCommas(1*multiply));
				}else{
					$("#val_disc_type").val(obj.promo_type);
					$("#item_discount").val(numberWithCommas(obj.promo_val.replace(",","")*multiply));
				}
				
			}			
		
			if(obj.linetype == 3){
				var modalpay = document.getElementById('mypayModal');
				modalpay.style.display = "none";
				
				var modalpay = document.getElementById('promoLineType');
				modalpay.style.display = "block";
				
				$("#loadPromoRadio").html(obj.message);
			}
			else if(obj.linetype == 2)
			{
				$("#sales_disc").hide();
				//$("#item_discount").show();
				
				if(obj.promo_val <= 1){
					if(multiply == 0){
						add_promoitem(obj.promo_type,obj.qty);		
					}else{
						add_promoitem(obj.promo_type,obj.qty*multiply);	
					}
				}
				else{
					//alert("masu");
					if(multiply == 0){
						add_promoitem(obj.item,obj.qty,numberWithCommas(obj.promo_val.replace(",","")));		
					}else{
						add_promoitem(obj.item,obj.qty*multiply,numberWithCommas(obj.promo_val.replace(",","")));	
					}
					$("#item_disc").show();
				}
				$("#promouse").html(obj.message);
				
				var modalpay = document.getElementById('mypayModal');
				modalpay.style.display = "none";
				
				var modalpay = document.getElementById('confirm_mypayModal');
				modalpay.style.display = "block";
				
				
			}
			else if(obj.linetype == 30)
			{
				$("#sales_disc").hide();
				if(obj.promo_val <= 1){
					if(multiply == 0){
						add_promoitem(obj.promo_type,obj.qty,obj.promo_val);		
					}else{
						add_promoitem(obj.promo_type,obj.qty*multiply,obj.promo_val);	
					}
				}
				else{
					if(multiply == 0){
						add_promoitem(obj.item,obj.qty,numberWithCommas(obj.promo_val.replace(",","")),obj.promo_val);		
					}else{
						add_promoitem(obj.item,obj.qty*multiply,numberWithCommas(obj.promo_val.replace(",","")),obj.promo_val);	
					}
					$("#item_disc").show();
				}
				
				
				var modalpay = document.getElementById('mypayModal');
				modalpay.style.display = "none";
				
				var modalpay = document.getElementById('confirm_mypayModal');
				modalpay.style.display = "block";
				
				
				//alert("masuk");
				
			}
			else if(obj.linetype == 40)
			{
				//alert("masuk");
				$("#val_disc_type").val('');
				$("#sales_discount").val('');
				$("#amountdisc_vw").html('');
				
				$("#sales_disc").hide();
				
				$("#promouse").html(obj.message);
				var modalpay = document.getElementById('confirm_mypayModal');
				modalpay.style.display = "block";
			}
			else{
				$("#promouse").html(title);
				var modalpay = document.getElementById('confirm_mypayModal');
				modalpay.style.display = "block";
			}
			
			
			//gross_saldisc
			////
			var gross				= $("#gross_bayar").val();
			var ngr_1					= gross.replace(",","");
			var ngr					= ngr_1.replace(",","");
			
			var nsal_disc			= sales_disc.replace(",","");
			var sub_gross_saldisc	=  parseFloat(ngr) - parseFloat(nsal_disc);
		//	alert(parseFloat(ngr) +"-"+ nsal_disc);
			//alert(sub_gross_saldisc);
			$("#gross_saldisc").val(numberWithCommas(sub_gross_saldisc)); 
			
			//service charge baru
			if($("#svc_type").val() == 'persen'){
				var dec_svc			= sub_gross_saldisc * (parseFloat($("#svc_val").val())/100);
				$("#sales_servicecharge").val(numberWithCommas(dec_svc.toFixed())); //confirmation Order
				$("#service_charge").val(numberWithCommas(dec_svc.toFixed())); //send to Save Order
				$("#servicechargeval").html(numberWithCommas(dec_svc.toFixed())); //send to Save Order
				var svc_val	= dec_svc.toFixed();
			}else{
				var dec_svc			= parseFloat($("#svc_val").val()).toFixed();
				//var dec_svc			= str_svc.replace(/,/g,"");	
				$("#sales_servicecharge").val(numberWithCommas(dec_svc)); //confirmation Order
				$("#service_charge").val(numberWithCommas(dec_svc)); //send to Save Order
				$("#servicechargeval").html(numberWithCommas(dec_svc)); //send to Save Order
				var svc_val	= dec_svc;
			}
				
			//end service charge baru
			
			var svc				= $("#sales_servicecharge").val();
			var nsvc			= svc.replace(",","");
			var grsl_svc		= parseFloat(sub_gross_saldisc) + parseFloat(nsvc);
			$("#grsl_svc").val(numberWithCommas(grsl_svc));
			
			var tacharge			= $("#tacharge").text();
			var ntacharge			= tacharge.replace(",","");
			//console.log(ntacharge);
			$("#sales_tacharge").val(numberWithCommas(ntacharge));
			
			//tax baru
			var tax	= (parseFloat(ngr) + parseFloat(svc_val) + parseFloat(ntacharge))*(10/100);
			$('#taxresto').html(numberWithCommas(tax.toFixed()));
			$("#sales_pajakresto").val(numberWithCommas(tax.toFixed())); //confirmation Order
			$("#pajakresto").val(numberWithCommas(tax.toFixed())); //send to Save Order
			//end tax baru
			
			var sales_pajakresto			= $("#sales_pajakresto").val();
			var nsales_pajakresto			= sales_pajakresto.replace(",","");

		//alert(parseFloat(sub_gross_saldisc) +"+"+ parseFloat(nsvc) +"+"+ parseFloat(ntacharge) +"+"+ parseFloat(nsales_pajakresto));
			var subtotal		= parseFloat(sub_gross_saldisc) + parseFloat(nsvc) + parseFloat(ntacharge) + parseFloat(nsales_pajakresto);
			
			$("#sales_subtotal").val(numberWithCommas(subtotal));
			grandTotal		= subtotal - $("#pembulatan2").val();
			$("#grand_total").val(numberWithCommas(subtotal));
			$("#subtotal2").val(numberWithCommas(grandTotal));
			
			$.ajax({
				type : 'GET',
				url : 'get_button_payment_group.php?promo_id='+promoid,
				success : function(data){
					$("#loadButtonbayar").html(data);
				}
			});
		}
		
	});
	
	
	
	
}
var currentValue = 0;
function changeRadioPromo(myRadio) {
	var layanan = $("#layanan").text();
	document.getElementById("promo_value_bayar").value = myRadio.value;
	var myRadio_val 	= myRadio.value;
	var myRadio_split	= myRadio_val.split("|");
	
	//alert(myRadio_split[1] );
	if(myRadio_split[1] == 0){
		add_promoitem(myRadio_split[0],myRadio_split[1]);
	}else{
		add_promoitem(myRadio_split[0],myRadio_split[1],myRadio_split[2]);
	}
	$.ajax({
		type : 'GET',
		url : 'get_product_layanan.php?layanan='+layanan+'&plu='+myRadio_split[0],
		success : function(data){
			//alert(data);
			var result = JSON.parse(data);
			$("#promouse").html("Anda Mengambil Promo " + result.PRODNAME);
			$(".btn-cancel-payment").hide();
		}
	})
	
	var modaltype = document.getElementById('promoLineType');
	modaltype.style.display = "none";
	
	var modalpay = document.getElementById('confirm_mypayModal');
	modalpay.style.display = "block";
	
	
	//$('#promo_value').val(myRadio.value);
}

function close_promoLineType(){
	var modalpay = document.getElementById('mypayModal');
	modalpay.style.display = "none";
	
	var modalpay = document.getElementById('promoLineType');
	modalpay.style.display = "none";
}
