function pilih_channel(){
	var modalsession = document.getElementById('modal_list_other');
	modalsession.style.display = "Block";
}
function close_list_other(){
	var modalsession = document.getElementById('modal_list_other');
	modalsession.style.display = "none";
}
function cek_point(){
	$('#dtl_member').html('');
	$('#phone_number').val('');
	var modalsession = document.getElementById('modal_list_other');
	modalsession.style.display = "none";
	var modalsession = document.getElementById('modal_form_cek_voucher');
	modalsession.style.display = "none";
	
	var modalsession = document.getElementById('modal_form_cek_point');
	modalsession.style.display = "Block";
}
function check_voucher(){	
	var modalsession = document.getElementById('modal_form_cek_voucher');
	modalsession.style.display = "Block";
}
function close_modal_form_cek_voucher(){	
	var modalsession = document.getElementById('modal_form_cek_voucher');
	modalsession.style.display = "none";
}
function close_modal_form_cek_point(){
	var modalsession = document.getElementById('modal_list_other');
	modalsession.style.display = "Block";
	
	var modalsession = document.getElementById('modal_form_cek_point');
	modalsession.style.display = "none";
}
function check_membership(){
	$("#btnCheck").hide();
	$("#waiting").show();
	$.ajax({
		type : 'GET',
		url : 'get_detail_member.php?no_tlp='+$("#phone_number").val()+'&strukid='+$("#strukid").val(),
		success : function(data){
			$('#dtl_member').html(data);
			$("#btnCheck").show();
			$("#waiting").hide();
		}
	});
}
function set_customer(){
	var cust_id		= $("#cust_id").val();
	var cust_name	= $("#cust_name").val();
	var cust_point	= $("#cust_point").val();
	var cust_phone	= $("#cust_phone").val();
	
	//alert(cust_name);
	
	$("#cust_id_head").val(cust_id);
	$("#cust_name_head").val(cust_name);
	$("#member_cust_id").val(cust_id);
	$("#member_point").val(cust_point);
	$("#member_phone").val(cust_phone);
	var modalsession = document.getElementById('modal_form_cek_point');
	modalsession.style.display = "none";
}
function proses_redeem_v(plu,point){
		Swal.fire({
		  title: 'CONFIRM!',
		  text: "PROSES REDEEM TIDAK BISA DIBATALKAN, APAKAH ANDA YAKIN ..?",
		  icon: 'question',
		  type: "warning",
		  showCancelButton: true,
		  confirmButtonText: 'YES',
		  cancelButtonText: 'NO'
		}).then((result) => {
			if (result.isConfirmed) {
				add_promoitem(plu,1);
				
				var modalsession = document.getElementById('modal_form_cek_voucher');
				modalsession.style.display = "none";
				var modalsession = document.getElementById('modal_form_cek_point');
				modalsession.style.display = "none";
			}
		});
}