window.onload = function() {
	
	
	
	var screen,
		output,
		limit,
		itemid,
		layanan,
		salesDisc
	
    
    screen 		= document.getElementById("result");
    layanan 	= document.getElementById("layanan");
    salesDisc 	= document.getElementById("salesDisc");
	layanan.innerHTML = default_type;
	
	var elem = document.querySelectorAll(".num");
    var len = elem.length;
    
    for(var i = 0; i < len; i++ ) {
        elem[i].addEventListener("click",function() {
			
			 num = this.value;
			 output = screen.innerHTML += num;
			 limit = output.length;
			 if(limit > 16 ) {
				Swal.fire({
				  title: 'Error!',
				  text: "Sorry no more input is allowed",
				  icon: 'error',
				  confirmButtonText: 'CLOSE'
				});
				return false;
			 }
     	},false);
    }
	
	$('#totalpayval').val(0); 
	//toggleFullScreen(document.body);
	
	
	
}

function refresh(){
	location.reload(); 
	//$("#fullscreen").hide();
	toggleFullScreen(document.body);
}

function cleartxt(){
	$(result).html('');
}


function layanan(i){
	var  layanan = document.getElementById("layanan");
	layanan.innerHTML = i;
	var  layanan = document.getElementById("layananVal");
	layanan.value = i;
}
function pembulatan(){
	
	if($("#result").text() == ''){
		var amountPembulatan = 0;
	}else{
		var amountPembulatan = $("#result").text();
	}
	$("#pembulatanval").html(numberWithCommas(amountPembulatan));
	$('#pembulatan').val(parseFloat(amountPembulatan));
	subtotal();
}
function servicecharge(){
	var  servicecharge = document.getElementById("servicecharge");
	servicecharge.innerHTML = 'SVC';
	
	valselect = $('#tmpvalue').val();
	var modalpay = document.getElementById('modalservicecharge');
		modalpay.style.display = "block";
	$.ajax({
		url: 'get_list_servicecharge.php',				
		dataType: 'html',
		success: function (data){
			//alert(data);
			$('#loadItemCharge').html(data);
			
		}
	});
}
function setServiceCharge(value){
	var modalpay = document.getElementById('modalservicecharge');
		modalpay.style.display = "none";
	$("#servicechargeval").html(numberWithCommas(value));
	subtotal();
}

function subtax(){
	
	var strtotal	= $("#totalpay").text();	
	var total		= strtotal.replace(/,/g,"");	
	var total2		= parseFloat(total.replace(/,/g,""));	
	
	var strsc		= $("#servicechargeval").text();
	var sc			= parseFloat(strsc.replace(/,/g,""));
	
	var strta		= $("#tacharge").text();
	var ta			= parseFloat(strta.replace(/,/g,""));
	
	var subtotal	= (total2 + sc + ta)*(10/100);
	//alert (subtotal.floor());
	
	var is_dp			= $("#dp_trx_id").val();
	if(is_dp == ''){
		$('#taxresto').html(numberWithCommas(subtotal.toFixed()));
	}else{
		//getNetAmountDP
		var strdpsales		= $("#dp_sales").val();
		var strdpsales_1	= strdpsales.replace(/,/g,"")
		var strdpsales_2	= parseInt(strdpsales_1.replace(/,/g,""));
		
		var TaxDP			= (strdpsales_2 * 1.1) - strdpsales_2;
		
		$("#sales_pajakresto").val(numberWithCommas(subtotal.toFixed() - TaxDP.toFixed()));
	}
	
	
	$('#pajakresto').val(parseFloat(subtotal.toFixed()));

	
}
function tmpvalue(v){
	$('#tmpvalue').val(v);
	$('.finishclear').attr("style", "background:#fff")
	$('#un'+v).attr("style", "background:#efefef")
	//alert("masuk");
}

function itemvoid(){
	valselect = $('#tmpvalue').val();
	
	//save to sales_cancel
	var prodcode	= $('#un'+valselect+' .itemid').text();
	var qty			= $('#un'+valselect+' .coreqty').text();
	var str_price	= $('#un'+valselect+' .price').text();
	var price 		= parseFloat(str_price.replace(/,/g,""));
	var disc_item	= $('#un'+valselect+' .discount').text();
	var item_type	= $('#un'+valselect+' .coretype').text();
	var str_gross	= $('#un'+valselect+' .totalprice').text();
	var gross 		= parseFloat(str_gross.replace(/,/g,""));
	var userid		= $('#kasir_id').val();
	var auddate		= $('#trx_date').val();
	var id			= $('#trx_id').val();
	var seqno		= $('#un'+valselect+' .seqno').text();
	
	if(seqno != $(".seqno").length){
		Swal.fire({
		  title: 'Error!',
		  text: 'Hanya Dapat Menghapus Product Terakhir',
		  icon: 'error',
		  confirmButtonText: 'CLOSE'
		});
		return false;
	}
	
	$.ajax({
		url: 'save_cancel_order.php?id='+id+'&prodcode='+prodcode+'&qty='+qty+'&price='+price+'&disc_item='+disc_item+'&item_type='+item_type+'&gross='+gross+'&userid='+userid+'&auddate='+auddate+'&seqno='+seqno,				
		dataType: 'html',
		success: function (data){
			if(data == 'gagal'){
				Swal.fire({
				  title: 'Error!',
				  text: 'Hanya Dapat Menghapus 1 Product Terakhir',
				  icon: 'error',
				  confirmButtonText: 'CLOSE'
				});
				return false;
			}else{
				if(parseInt(valselect) !== 0){
		//console.log($('#un'+valselect+' .groupid').text());
				if($('#un'+valselect+' .groupid').text() == 'UP'){
					
					Swal.fire({
					  title: 'Error!',
					  text: 'Cannot Delete Item Upgrade',
					  icon: 'error',
					  confirmButtonText: 'CLOSE'
					});
					return false;
				}else{
					mintotal 	= parseFloat($('#un'+valselect+' .realprice').val());
					linedisc	= parseFloat($('#un'+valselect+' .discount').text());
					disctotal	= parseFloat($('#un'+valselect+' .amountdisc').text());
					
					
					var sum = 0;
					$('.link'+valselect).each(function(){
						var strPrice	= $(this).find('.totalprice').text();
						//alert(strPrice);
						sum += parseFloat(strPrice.replace(/,/g,""));  // Or this.innerHTML, this.innerText
					});
					amountUpgrade 	= parseFloat(sum);	
					//alert(amountUpgrade);
					
					var disc	= $("#amountdisc_vw").text();			
					realdisc	= parseFloat(disc.replace(/,/g,""));
					
					
					noparent = $('#un'+valselect+' .linkparent').val();
					
					if(linedisc > 0)
					{
						totalpay(mintotal, 2,0,amountUpgrade);
						subtotal();
					}else{
						totalpay(mintotal, 2,disctotal,amountUpgrade);
						subtotal();
					}
					
					totalAmountDisc	= realdisc - disctotal;
					
					$("#amountdisc_vw").html( numberWithCommas(totalAmountDisc.toFixed()) );
						
					$('#un'+valselect).html('');
					$('.link'+valselect).html('');
					$('#tmpvalue').val(0);
				}
				
				
				
				//clear tacharge
				var array_prodname	= $('#frmtrans').getForm2obj();
				var	type_order		= array_prodname['type'];
				var arraycontainsturtles = (type_order.indexOf('TA') > -1);
				var arraygf = (type_order.indexOf('GF') > -1);
				var arraygr = (type_order.indexOf('GR') > -1);
				var arraysf = (type_order.indexOf('SF') > -1);
				
				
				if((arraycontainsturtles == false) && (arraygf == false) && (arraygr == false) && (arraysf == false)){
					$("#tacharge").html(0);
				}
				
				
			}
			else{
				Swal.fire({
				  title: 'Error!',
				  text: 'No item select',
				  icon: 'error',
				  confirmButtonText: 'CLOSE'
				});
				return false;
			}
			}
		}
	});	
	
	
	
	
	
	
	$.ajax({
		type : 'post',
		url : 'save_order_perline.php',
		data : $("#frmtrans").serialize(),
		success : function(data){
			
		}
	});
	
	
}


function detail_disc(idreason,type){
	
	if(idreason == 1){ //jika diskon karyawan cek internet dahulu
		$.ajax({
			url: 'api/cek_net.php',				
			dataType: 'html',
			success: function (data){
				// alert(data);
				if(data == 1){
					var valselect 	= 	$('#tmpvalue').val();
					var layanan		=	($('#un'+valselect+' .coretype').text());
					
					
					valselect = $('#tmpvalue').val();
					var modalpay = document.getElementById('modalItemDisc');
						modalpay.style.display = "none";
					
					var modalpay_detail = document.getElementById('modalItemDisc_detail');
						modalpay_detail.style.display = "block";
					
					$.ajax({
						url: 'get_list_discount_detail.php?id='+idreason+'&type='+type+'&layanan='+layanan,				
						dataType: 'html',
						success: function (data){
							//alert(data);
							$('#loadItemDiscount_detail').html(data);
						}
					});

				}else{
					Swal.fire({
					  title: 'Error!',
					  text: "Tidak ada koneksi ke Pusat!",
					  icon: 'error',
					  confirmButtonText: 'CLOSE'
					});
					var modalpay = document.getElementById('modalItemDisc');
						modalpay.style.display = "none";
				}
			}
		});		
	}else{
		var valselect 	= 	$('#tmpvalue').val();
		var layanan		=	($('#un'+valselect+' .coretype').text());
		
		
		valselect = $('#tmpvalue').val();
		var modalpay = document.getElementById('modalItemDisc');
			modalpay.style.display = "none";
		
		var modalpay_detail = document.getElementById('modalItemDisc_detail');
			modalpay_detail.style.display = "block";
		
		$.ajax({
			url: 'get_list_discount_detail.php?id='+idreason+'&type='+type+'&layanan='+layanan,				
			dataType: 'html',
			success: function (data){
				//alert(data);
				$('#loadItemDiscount_detail').html(data);
			}
		});

	}
}
function close_modalItemDisc_popupnik(){
	var modalpay_detail = document.getElementById('modalItemDisc_popupnik');
	modalpay_detail.style.display = "none";
	var modalpay_detail = document.getElementById('modalItemDisc_detail');
		modalpay_detail.style.display = "block";
}
function close_modalItemDisc_ceknik(){
	var modalpaykar = document.getElementById('cek_nik_kar');
	modalpaykar.style.display = "none";
}
function close_modalItemDisc_popupnikchild(){
	var modalpay_detail = document.getElementById('modalItemDisc_popupnik_child');
	modalpay_detail.style.display = "none";
	var modalpay_detail = document.getElementById('modalItemDisc_detail');
		modalpay_detail.style.display = "block";
}
function submit_nik(){
	$("#nik_kar").val($("#val_nik_kar").val());
	var  nilaidisc 	= $("#val_nilaidisc_kar").val();
	var  type 		= $("#val_type_kar").val();
	var  discname	= $("#val_discname_kar").val();
	var  discid 	= $("#val_discid_kar").val();
	var  disctype 	= $("#val_disctype_kar").val();
	//alert(nilaidisc+","+type+","+discname+","+discid+","+disctype);
	disc_proc(nilaidisc,type,discname,discid,disctype);
	var modalpay_detail = document.getElementById('modalItemDisc_popupnik');
	modalpay_detail.style.display = "none";
}
function push_nik(){ //process diskon karyawan online
	//nik yg di input
	var nik = $("#val_nik_kar_disc").val();

	//push tgl sekarang
	var dateObj = new Date();
	var month = dateObj.getUTCMonth() + 1; //months from 1-12
	var day = dateObj.getUTCDate();
	var year = dateObj.getUTCFullYear();	
	transdate = year + "-" + month + "-" + day;

	//discount amount
	var disc_final = parseFloat($('#discount').val().replace(/,/g,""));

	//gross_sales
	var gross_sales = parseFloat($('#gross_bayar').val().replace(/,/g,""));

	//kode store
	var branch_id = $("#branch_id").val();

	$("#val_nik_kar").val(nik);

	
	$.ajax({
		type : 'post',
		url : 'api/push_transaction.php?nik='+nik+'&transdate='+transdate+'&tamount='+gross_sales+'&kdstore='+branch_id,
		success : function(data){		
			console.log(data);	
			try {
				var obj = JSON.parse(data);			 
				if (obj.Status == '200'){	//Insert Success
					var val = 1;
					$("#nik_kar").val(nik);
					$("#valid_disc").val(val);
					var idbtn = $("#idbtn").val();
					var title = $("#title").val();
					var note = $("#note_confirm").val();	
					Swal.fire({
					  title: 'Success!',
					  text: obj.message,
					  icon: 'success',
					  confirmButtonText: 'OK'
					});
					act_prosespay(idbtn, title, note);
				}		 
				else if (obj.status == '400'){	//Data tidak ditemukan
					Swal.fire({
					  title: 'Error!',
					  text: obj.message,
					  icon: 'error',
					  confirmButtonText: 'CLOSE'
					});
					$("#valid_disc").val('0');
				}		 
				else if (obj.status == '302'){	//NIK sudah digunakan di store tertentu
					Swal.fire({
					  title: 'Error!',
					  text: obj.message,
					  icon: 'error',
					  confirmButtonText: 'CLOSE'
					});
					$("#valid_disc").val('0');
				}		 
				else if (obj.status == '303'){	//TC Sudah melebihi maksimum
					Swal.fire({
					  title: 'Error!',
					  text: obj.message,
					  icon: 'error',
					  confirmButtonText: 'CLOSE'
					});
					$("#valid_disc").val('0');
				}else{		
					alert("Tidak berhasil konek".data);
					$("#valid_disc").val('0');				
				}
			}catch(err){
				alert(data);
			}
		}
	});
	var modalpay_detail = document.getElementById('cek_nik_kar');
	modalpay_detail.style.display = "none";
}
function submit_disc(reason){
	// $("#nik_kar").val($("#val_nik_kar").val());
	var  nilaidisc 	= $("#val_nilaidisc_kar").val();
	var  type 		= $("#val_type_kar").val();
	var  discname	= $("#val_discname_kar").val();
	var  discid 	= $("#val_discid_kar").val();
	var  disctype 	= $("#val_disctype_kar").val();
	//alert(nilaidisc+","+type+","+discname+","+discid+","+disctype);
	disc_proc(nilaidisc,type,discname,discid,disctype,reason);
}
function cek_nik(){
	var nik = $("#val_nik_kar_disc").val();
	$.ajax({
		type : 'post',
		url : 'api/cek_nik.php?nik='+nik,
		success : function(data){		
			console.log(data);	
			try {
				var obj = JSON.parse(data);			 
				if (obj.Status == '202'){	
					console.log(obj.profile_employee[0].nama);
					$("#val_nama_kar_disc").html(obj.profile_employee[0].nama);
				}		 
				else if (obj.status == '400'){	
					$("#val_nama_kar_disc").html("NIK TIDAK ADA");
				}else{		
					alert("Tidak berhasil konek".data);				
				}
			}catch(err){
				alert(data);
			}
		}
	});
}
function submit_nik_child(){
	$("#nik_kar_child").val($("#val_nik_kar_child").val());
	$("#nama_kar_child").val($("#val_anak_kar_child").val());
	var  nilaidisc 	= $("#val_nilaidisc_kar").val();
	var  type 		= $("#val_type_kar").val();
	var  discname	= $("#val_discname_kar").val();
	var  discid 	= $("#val_discid_kar").val();
	var  disctype 	= $("#val_disctype_kar").val();
	disc_proc(nilaidisc,type,discname,discid,disctype);
	var modalpay_detail = document.getElementById('modalItemDisc_popupnik_child');
	modalpay_detail.style.display = "none";
}
function disc_proc(nilaidisc,type,discname,discid,disctype,reason){
	//alert(nilaidisc+"="+type+"="+discname+"="+discid+"="+type);
	if(reason == 1){ //Jika Diskon Karyawan
		if(type== 1){ //item
			
			var cek_disc = $('#discount').val();
			cek_disk_val = parseFloat(cek_disc.replace(/,/g,""));
			var maxdisc = $("#val_discmax").val();
			var hasil = maxdisc - cek_disk_val ;
			console.log(hasil, 'sisa discount')
			if(hasil >= 0){ //Jika sisa diskon masih ada
				$("#val_disc_type").val(type);
				valselect 	= $('#tmpvalue').val();
				$('#un'+valselect+' .discount').html(nilaidisc);		
				$('#un'+valselect+' .promoid').val(discid);
				$('#un'+valselect+' .promodesc').val(discname);
				$('#un'+valselect+' .disctype').val(disctype);
				$('#un'+valselect+' .discvalue').val(nilaidisc);
				
				
				
				var str = $('#un'+valselect+' .totalprice').text();
				totalprice  = str.replace(/,/g,"");
				totalprice2  = parseFloat(totalprice.replace(/,/g,""));
				discount	= parseFloat($('#un'+valselect+' .discount').text());
				// alert('#un'+valselect+"==="+totalprice2+"*"+(discount+"/"+100));
				//return false;
		
				var amountdisc = totalprice2*(discount/100);
				// alert(cek_disc_val);
	
				if(amountdisc > hasil){
					amountdisc = hasil;			
					$('#un'+valselect+' .discamount').val(amountdisc);
					$('#un'+valselect+' .discamount').html(amountdisc);
				}else{
					$('#un'+valselect+' .discamount').val(amountdisc.toFixed());
					$('#un'+valselect+' .discamount').html(amountdisc.toFixed());
				}
				
				
				var sum = 0;
		
				$(".discamount").each(function() {
					var val = $.trim( $(this).text() );
		
					if ( val ) {
						val = parseFloat( val.replace( /^\$/, "" ) );
		
						sum += !isNaN( val ) ? val : 0;
					}
				});
				
				$('#discount').val(parseFloat(sum.toFixed()));
				$("#amountdisc_vw").html( numberWithCommas(sum.toFixed() ));		
				discount_f = parseFloat(sum.toFixed() );
				// discount_ff = parseFloat(discount_f);
				console.log(discount_f, 'total discount');
				console.log(maxdisc, 'maksimal discount');
				console.log(amountdisc, 'discount per item');
				if(discount_f > maxdisc){
					$("#amountdisc_vw").html(maxdisc);
					$('#discount').val(maxdisc);
				}
				subtotal();
				
			}
		}
		else{ //sales diskon
			//alert(nilaidisc+","+type+","+discname+","+discid+","+disctype);
			$("#val_disc_type").val(type);
			
			discount 		= parseFloat(nilaidisc)/100;
			strtotalpay		= $('#totalpay').text();
			totalpay_2 		= strtotalpay.replace(/,/g,"");
			totalpayment	= parseFloat(totalpay_2.replace(/,/g,""));
			
			
			$('#discount').val(parseFloat(parseFloat(totalpayment) * discount));
			discount_f = parseFloat(totalpayment) * discount;
			var maxdisc = $("#val_discmax").val();
			// alert(maxdisc);
			$("#amountdisc_vw").html( numberWithCommas((parseFloat(parseFloat(totalpayment) * discount)).toFixed()) );
			if(discount_f > maxdisc){
				$("#amountdisc_vw").html(numberWithCommas(maxdisc));
				$('#discount').val(maxdisc);
			}
	
			// $("#amountdisc_vw").html( numberWithCommas((parseFloat(parseFloat(totalpayment) * discount)).toFixed()) );
			subtotal();	
		}

	}else{
		if(type== 1){ //item
			
			valselect 	= $('#tmpvalue').val();
			$('#un'+valselect+' .discount').html(nilaidisc);		
			$('#un'+valselect+' .promoid').val(discid);
			$('#un'+valselect+' .promodesc').val(discname);
			$('#un'+valselect+' .disctype').val(disctype);
			$('#un'+valselect+' .discvalue').val(nilaidisc);
			
			
			
			var str = $('#un'+valselect+' .totalprice').text();
			totalprice  = str.replace(",","");
			totalprice2  = parseFloat(totalprice.replace(",",""));
			discount	= parseFloat($('#un'+valselect+' .discount').text());
			//alert('#un'+valselect+"==="+totalprice2+"*"+(discount+"/"+100));
			//return false;
			var amountdisc = totalprice2*(discount/100);
			
			$('#un'+valselect+' .amountdisc').html(amountdisc);
			
			var sum = 0;
	
			$(".amountdisc").each(function() {
				var val = $.trim( $(this).text() );
	
				if ( val ) {
					val = parseFloat( val.replace( /^\$/, "" ) );
	
					sum += !isNaN( val ) ? val : 0;
				}
			});
			
			$('#discount').val(parseFloat(sum.toFixed()));
			$("#amountdisc_vw").html( numberWithCommas(sum.toFixed()) );
			$("#val_disc_type").val(type);
			console.log($("#val_disc_type").val(), 'value discount');
			subtotal();
		}else{
			//alert(nilaidisc+","+type+","+discname+","+discid+","+disctype);
			
			discount 		= parseFloat(nilaidisc)/100;
			strtotalpay		= $('#totalpay').text();
			totalpay_2 		= strtotalpay.replace(",","");
			totalpayment	= parseFloat(totalpay_2.replace(",",""));
			
			//alert(parseFloat(totalpay) +"*"+ discount);
			$('#discount').val(parseFloat(parseFloat(totalpayment) * discount));
			$("#amountdisc_vw").html( numberWithCommas(parseFloat(parseFloat(totalpayment) * discount).toFixed()) );
			subtotal();	
		}

	}
	subtax();
	var modalpay_detail = document.getElementById('modalItemDisc_detail');
		modalpay_detail.style.display = "none";
	
}
function detail_disc_proc(nilaidisc,type,discname,discid,disctype,winpopup,discmax,reason){
	//alert(type+"---"+winpopup);	
	if (reason == 1){ // Jika diskon karyawan online
		$("#val_discmax").val(discmax);
	
		if($("#amountdisc_vw").html() == numberWithCommas(discmax)){ //Cek limit diskon
			Swal.fire({
			  title: 'Error!',
			  text: 'Maaf limit diskon telah habis',
			  icon: 'error',
			  confirmButtonText: 'CLOSE'
			});
			var modalpay_detail = document.getElementById('modalItemDisc_detail');
				modalpay_detail.style.display = "none";
				// var modalpay_detail2 = document.getElementById('modalItemDisc_popupnik');
				// modalpay_detail2.style.display = "none";
		}else{			
			$("#val_nilaidisc_kar").val(nilaidisc);
			$("#val_nilaidisc_kar_anak").val(nilaidisc);
			if(disctype == 1){
				var typeDisc	= 'item';
			}else{
				var typeDisc	= 'sales';
			}
			$("#val_type_kar").val(type);
			//$("#val_discname_kar").val(discname);
			//$("#val_discid_kar").val(discid);
			$("#val_disctype_kar").val(disctype);
			
			$("#val_disc_val").val(nilaidisc);		
			$("#val_disc_type").val(typeDisc);		
			$("#promo_id_bayar").val(discid);
			$("#promo_value_bayar").val(discname);
			
			
			$("#val_discname_kar").val(discname);
			$("#val_discid_kar").val(discid);
		
		
			// var modalpay_detail = document.getElementById('modalItemDisc_popupnik');
			// modalpay_detail.style.display = "block";
			submit_disc(reason);
			var modalpay_detail = document.getElementById('modalItemDisc_detail');
			modalpay_detail.style.display = "none";
		}
	}else{
		if(winpopup == 1){
			$("#val_nilaidisc_kar").val(nilaidisc);
			$("#val_nilaidisc_kar_anak").val(nilaidisc);
			if(type == 1){
				var typeDisc	= 'item';
			}else{
				var typeDisc	= 'sales';
			}
			$("#val_type_kar").val(typeDisc);
			$("#val_disctype_kar").val(disctype);
			
			$("#val_disc_val").val(nilaidisc);		
			$("#val_disc_type").val(typeDisc);		
			$("#promo_id_bayar").val(discid);
			$("#promo_value_bayar").val(discname);
			
			
			$("#val_discname_kar").val(discname);
			$("#val_discid_kar").val(discid);		
		
			var modalpay_detail = document.getElementById('modalItemDisc_popupnik');
			modalpay_detail.style.display = "block";			
			submit_disc(reason);
			var modalpay_detail = document.getElementById('modalItemDisc_detail');
			modalpay_detail.style.display = "none";
		}
		else if(winpopup == 2){
			$("#val_nilaidisc_kar").val(nilaidisc);
			$("#val_nilaidisc_kar_anak").val(nilaidisc);
			if(disctype == 1){
				var typeDisc	= 'item';
			}else{
				var typeDisc	= 'sales';
			}
			$("#val_type_kar").val(type);
			$("#val_type_kar_anak").val(type);
			
			$("#val_discname_kar").val(discname);
			$("#val_discid_kar").val(discid);
			
			$("#val_disctype_kar").val(disctype);
			$("#val_disctype_kar_anak").val(disctype);
			
			$("#val_disc_val").val(nilaidisc);		
			$("#val_disc_type").val(typeDisc);		
			$("#promo_id_bayar").val(discid);
			$("#promo_value_bayar").val(discname);
			
			var modalpay_detail = document.getElementById('modalItemDisc_popupnik_child');
			modalpay_detail.style.display = "block";
		}
		else{
			disc_proc(nilaidisc,type,discname,discid,disctype);
			$("#val_disc_type").val(type);		
			$("#promo_id_bayar").val(discid);
			$("#promo_value_bayar").val(discname);			
			$("#val_disc_val").val(nilaidisc);	
		}
	}
}

function closedisc_detail() {
	var modalpay = document.getElementById('modalItemDisc_detail');
	modalpay.style.display = "none";
	var modalpay = document.getElementById('modalItemDisc');
		modalpay.style.display = "block";
}

var screenWidth		= screen.width;
if(screenWidth < 600){
	$.ajax({
		url: 'product_mobile.php?id=01',				
		dataType: 'html',
		success: function (data){
			$('#rtvmenu').html(data);
		}
	});	
			
	function catproduct(id){
		$('#rtvmenu').html('<div style="margin:20% auto; width:100px;"><img src="loading.gif" width="100px"></div>');
		$.ajax({
			url: 'product_mobile.php?id='+id,				
			dataType: 'html',
			success: function (data){
				//alert(data);
				$('#rtvmenu').html(data);
			}
		});	
	}
}else{
	$.ajax({
		url: 'product.php?id=01',				
		dataType: 'html',
		success: function (data){
			$('#rtvmenu').html(data);
		}
	});	
			
	function catproduct(id){
		$('#rtvmenu').html('<div style="margin:20% auto; width:100px;"><img src="loading.gif" width="100px"></div>');
		$.ajax({
			url: 'product.php?id='+id,				
			dataType: 'html',
			success: function (data){
				//alert(data);
				$('#rtvmenu').html(data);
			}
		});	
	}
}
function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}


// When the user clicks the button, open the modal 
		
		
		$.fn.getForm2obj = function() {
		  var _ = {};
		  $.map(this.serializeArray(), function(n) {
			const keys = n.name.match(/[a-zA-Z0-9_]+|(?=\[\])/g);
			if (keys.length > 1) {
			  let tmp = _;
			  pop = keys.pop();
			  for (let i = 0; i < keys.length, j = keys[i]; i++) {
				tmp[j] = (!tmp[j] ? (pop == '') ? [] : {} : tmp[j]), tmp = tmp[j];
			  }
			  if (pop == '') tmp = (!Array.isArray(tmp) ? [] : tmp), tmp.push(n.value);
			  else tmp[pop] = n.value;
			} else _[keys.pop()] = n.value;
		  });
		  return _;
		}
		
		
		function closeSaldoAwal(){
			location.href = "logout.php";
		}
	//$("#processupgrade").click(function(){
		
		//});
		
		
		// When the user clicks on <span> (x), close the modal
		function spanpay() {
			var modalpay = document.getElementById('mypayModal');
			modalpay.style.display = "none";
		}
		
		function exitpos_close() {
			var modalpay = document.getElementById('modalExit');
			modalpay.style.display = "none";
		}
		
		// When the user clicks anywhere outside of the modal, close it
		window.onclick = function(event) {
			var modalpay = document.getElementById('mypayModal');
			if (event.target == modalpay) {
				modalpay.style.display = "none";
			}
		}
		
function toggleFullScreen(elem) {
	  if ((document.fullScreenElement !== undefined && document.fullScreenElement === null) || (document.msFullscreenElement !== undefined && document.msFullscreenElement === null) || (document.mozFullScreen !== undefined && !document.mozFullScreen) || (document.webkitIsFullScreen !== undefined && !document.webkitIsFullScreen)) {
		if (elem.requestFullScreen) {
		  elem.requestFullScreen();
		} else if (elem.mozRequestFullScreen) {
		  elem.mozRequestFullScreen();
		} else if (elem.webkitRequestFullScreen) {
		  elem.webkitRequestFullScreen(Element.ALLOW_KEYBOARD_INPUT);
		} else if (elem.msRequestFullscreen) {
		  elem.msRequestFullscreen();
		}
	  } else {
		if (document.cancelFullScreen) {
		  document.cancelFullScreen();
		} else if (document.mozCancelFullScreen) {
		  document.mozCancelFullScreen();
		} else if (document.webkitCancelFullScreen) {
		  document.webkitCancelFullScreen();
		} else if (document.msExitFullscreen) {
		  document.msExitFullscreen();
		}
	  }
	}
function exitpos(){
	var username 	= $("#userid_exit").val();
	var pass	 	= $("#password_exit").val();
	$.ajax({
		type : 'GET',
		url : 'cek_login.php?user='+username+'&password='+pass,
		success : function(data){
			if(data == 0){
				var modalpay = document.getElementById('modalExit');
				modalpay.style.display = "none";
				
				$("#username").val(username)
				var modalsession = document.getElementById('modalCloseSession');
				modalsession.style.display = "Block";
				
			}else{
				Swal.fire({
				  title: 'Error!',
				  text: "User Tidak Di Izinkan",
				  icon: 'error',
				  confirmButtonText: 'CLOSE'
				});
				return false;
			}
		}
	})
	
}

function closesession(act){	
	if(act == 1){
		var username 	= $("#username").val();
		$.ajax({
		type : 'GET',
			url : 'proc_close_session.php?user='+username,
			success : function(data){
				//alert(data);
				if(screenWidth < 600){
					location.href = base_url+"print/Print/print_endsession_mobile.php?session_id="+data;
				}else{
					location.href = base_url+"print/Print/print_endsession.php?session_id="+data;

					
				}
				
				if(data != 1){
					/*alert("Closing Berhasil Dilakukan");
					
					var modalpay = document.getElementById('modalExit');
					modalpay.style.display = "none";
					
					var modalsession = document.getElementById('modalCloseSession');
					modalsession.style.display = "Block";
					*/
					
					
					
					
				}else{
					Swal.fire({
					  title: 'Error!',
					  text: "Closing Gagal",
					  icon: 'error',
					  confirmButtonText: 'CLOSE'
					});
					return false;
				}
			}
		})
	}else{
		location.href="logout.php";
	}
	
	//;
}
function closesession_close(){
	var modalsession = document.getElementById('modalCloseSession');
	modalsession.style.display = "none";
}
function logout(){
	//location.href="dashboard.php";
	var modalpay = document.getElementById('modalExit');
	modalpay.style.display = "Block";
}
function reprint(){
	if(parseFloat($("#subtotal").text()) != 0){
		Swal.fire({
		  title: 'Error!',
		  text: "Silahkan selesaikan transaksi terlebih dahulu sebelum melakukan Reprint",
		  icon: 'error',
		  confirmButtonText: 'CLOSE'
		});
		return false;
	}
	var divObj 		= document.getElementById("result");	
	if ( divObj.textContent){ // FF
		//console.log("masuk");
		var div = parseInt(divObj.textContent);
	}else{
		divObj = 0;
	}
	if(divObj == 0){
		Swal.fire({
		  title: 'Error!',
		  text: "Silahkan Masukan No Transaksi Yang Akan Di Reprint",
		  icon: 'error',
		  confirmButtonText: 'CLOSE'
		});
		return false;
	}else{
		var last_strukid	= $("#strukid").val();
		var arr_struk		= last_strukid.split(".");
		var strukid			= arr_struk[0]+"."+arr_struk[1]+"."+div;
		
		
		$.ajax({
			type : 'GET',
			url : 'cek_transaction.php?strukid='+strukid,
			success : function(data){
				//alert(data);
				if(data == 1){				
					//location.href=base_url + "/print/Print/print_struk.php?id="+strukid+"&act=reprint";
					/*
					ajax_print('print_reprintstruk.php?id='+strukid+'&act=reprint',this);
					setTimeout(function(){  // Beginning of code that should run AFTER the timeout
						location.href = "system_pos.php";
					}, 1000);  // Put the timeout here
					*/
					
					if(screenWidth < 600){
						//location.href = base_url+"print/Print/print_struk_mobile.php?id="+$("#strukid").val();
						ajax_print('rawbt-receipt_mobile.php?id='+strukid+'&act=reprint',this);
						setTimeout(function(){  // Beginning of code that should run AFTER the timeout
							location.href = "system_pos.php";
						}, 1000);  // Put the timeout here
					}else{
						//location.href = base_url+"print/Print/print_struk.php?id="+$("#strukid").val();
						ajax_print('rawbt-receipt.php?id='+strukid+'&act=reprint',this);
						
						setTimeout(function(){  // Beginning of code that should run AFTER the timeout
							location.href = "system_pos.php";
						}, 1000);  // Put the timeout here

						
					}
					
					$("#result").html('');
					
				}else if(data == 0){
					Swal.fire({
					  title: 'Error!',
					  text: "Receipt No : " + strukid + " Sudah Di Void",
					  icon: 'error',
					  confirmButtonText: 'CLOSE'
					});
					$("#result").html('');
					return false;
				}
			}
		});
		
		
		
	}
}
function ajax_print(url, btn) {
	b = $(btn);
	b.attr('data-old', b.text());
	b.text('wait');
	$.get(url, function (data) {
		//window.location.href = data;  // main action
		//alert(data);
		myArr 	= data.split(",");
		myArr2	= myArr[1];
		myArr3 = myArr2.split("#");
		
		
		let textEncoded = "base64," + myArr3[0];
		Android.showToast(textEncoded);
		
	}).fail(function () {
		alert("ajax error");
	}).always(function () {
		b.text(b.attr('data-old'));
	})
}
function voidtransaction(){
	var divObj 		= document.getElementById("result");	
	if ( divObj.textContent){ // FF
		//console.log("masuk");
		var div = parseInt(divObj.textContent);
	}else{
		divObj = 0;
	}
	if(divObj == 0){
		Swal.fire({
		  title: 'Error!',
		  text: "Silahkan Masukan No Transaksi Yang Akan Divoid",
		  icon: 'error',
		  confirmButtonText: 'CLOSE'
		});
		return false;
	}else{
		var last_strukid	= $("#strukid").val();
		var arr_struk		= last_strukid.split(".");
		var strukid			= arr_struk[0]+"."+arr_struk[1]+"."+div;
		
		
		$.ajax({
			type : 'GET',
			url : 'cek_transaction.php?strukid='+strukid,
			success : function(data){
				if(data == 0){
					Swal.fire({
					  title: 'Error!',
					  text: "Receipt No : " + strukid + " Sudah Di Void",
					  icon: 'error',
					  confirmButtonText: 'CLOSE'
					});
					$("#result").html('');
					var modalpay = document.getElementById('modalVoidLogin');
					modalpay.style.display = "none";
					return false;
				}
				else if(data == 10){
					Swal.fire({
					  title: 'Error!',
					  text: "Receipt No : " + strukid + " Hold Transaksi, Silahkan selesaikan terlebih dahulu",
					  icon: 'error',
					  confirmButtonText: 'CLOSE'
					});
					$("#result").html('');
					var modalpay = document.getElementById('modalVoidLogin');
					modalpay.style.display = "none";
					return false;
				}
				else{
					//alert(strukid);
					$.ajax({
						type : 'GET',
						url : 'cek_void.php?strukid='+strukid,
						success : function(data){
							//alert(data);
							if(data == 1){				
								//alert("masuk");
								$("#strukidvoid").val(strukid);
								// var modalpay = document.getElementById('modalVoidLogin');
								// modalpay.style.display = "Block";				
								var modalpay = document.getElementById('modalVoidLogin');
								modalpay.style.display = "block";
							}else if(data == 0){
								Swal.fire({
								  title: 'Error!',
								  text: "Receipt No : " + strukid + " telah lewat batas waktu void maksimum 60 menit setelah transaksi di posting",
								  icon: 'error',
								  confirmButtonText: 'CLOSE'
								});
								return false;
							}else if(data == 3){
								Swal.fire({
								  title: 'Error!',
								  text: "Tanggal atau Jam Device ini tidak Up to Date, Silahkan di Cek Kembali",
								  icon: 'error',
								  confirmButtonText: 'CLOSE'
								});
								return false;
							}else{
								Swal.fire({
								  title: 'Error!',
								  text: "Receipt No : " + strukid + " tidak ada",
								  icon: 'error',
								  confirmButtonText: 'CLOSE'
								});
								return false;
							}
							$("#result").html("");
						}
					})
					
				}
			}
		});
		
		
		
	}
}
function voidpos(){
	var username 	= $("#userid_void").val();
	var pass	 	= $("#password_void").val();
	var strukidvoid	 	= $("#strukidvoid").val();
	$.ajax({
		type : 'GET',
		url : 'cek_login.php?user='+username+'&password='+pass+'&cekadmin=1',
		success : function(data){
			if(data == 1){
				
				Swal.fire({
				  title: 'Error!',
				  text: "User Tidak Di Izinkan",
				  icon: 'error',
				  confirmButtonText: 'CLOSE'
				});		
				return false;
			}else{				
				var modalpay = document.getElementById('modalVoidLogin');
				modalpay.style.display = "none";
				
				$.ajax({
					type : 'GET',
					url : 'cek_voidtran.php?strukidvoid='+strukidvoid,
					success : function(data){
						if(data == 1){
							Swal.fire({
							  title: 'Error!',
							  text: "User Tidak Di Izinkan",
							  icon: 'error',
							  confirmButtonText: 'CLOSE'
							});		
							return false;
						}else{				
							$("#detailTransactionVoid").html(data);
							$("#txtStrukVoid").val(strukidvoid);
							$("#username").val(username);
							$("#voidusername").val(username);
							var modalsession = document.getElementById('modalVoidTransaction');
							modalsession.style.display = "Block";
						}
					}
				})
				
				
			}
		}
	})
	
}
function proc_void(){
	var valRadioVoid	= $('input[name="radioReason"]:checked').val();	
	
	var comment = $.trim($("#reason_void").val());

	// console.log(uservoid);
    
	if(comment == ""){
		
		Swal.fire({
		  title: 'Error!',
		  text: 'Alasan Void Harus Di isi',
		  icon: 'error',
		  confirmButtonText: 'CLOSE'
		});		
		return false;
	}else{
		
		$.ajax({
			type : 'POST',
			url : 'proc_void.php',
			data : $("#frm_void").serialize(),
			success : function(data){
				if(valRadioVoid == 1){
					//alert("masuksini");
					if(screenWidth < 600){
						//location.href = base_url+"print/Print/print_struk_mobile.php?id="+$("#strukid").val();
						ajax_print('rawbt-receipt_mobile.php?id='+$("#txtStrukVoid").val()+'&act=void',this);
						setTimeout(function(){  // Beginning of code that should run AFTER the timeout
							location.href = "system_pos.php";
						}, 1000);  // Put the timeout here
					}else{
						//location.href = base_url+"print/Print/print_struk.php?id="+$("#strukid").val();
						ajax_print('rawbt-receipt.php?id='+$("#txtStrukVoid").val()+'&act=void',this);
						
						setTimeout(function(){  // Beginning of code that should run AFTER the timeout
							location.href = "system_pos.php";
						}, 1000);  // Put the timeout here

						
					}
					return false;
				}else{
					Swal.fire({
					  title: 'SUCCESS!',
					  text: "Struk Hanya Akan Dicetak Jika Cancel By Customer",
					  icon: 'success',
					  confirmButtonText: 'CLOSE'
					}).then((result) => {
					if (result.isConfirmed) {
						location.href = "system_pos.php";
					}
					});
					//alert()
				}
				/*
				Swal.fire({
				  title: 'SUCCESS!',
				  text: data,
				  icon: 'success',
				  confirmButtonText: 'CLOSE'
				}).then((result) => {
				if (result.isConfirmed) {
					location.href = "system_pos.php";
				}
				});
				*/
			}
		})
		return false;
	}
	
}

function proc_void_backdate(){
	var valRadioVoid	= $('input[name="radioReason"]:checked').val();	
	
	var comment = $.trim($("#reason_void").val());
    
	if(comment == ""){
		
		Swal.fire({
		  title: 'Error!',
		  text: 'Alasan Void Harus Di isi',
		  icon: 'error',
		  confirmButtonText: 'CLOSE'
		});		
		return false;
	}else{
		
		$.ajax({
			type : 'POST',
			url : 'proc_void.php',
			data : $("#frm_void").serialize(),
			success : function(data){
				if(valRadioVoid == 1){
					//alert("masuksini");
					//location.href = base_url+"print/Print/print_struk_backdate.php?act=void&id="+$("#txtStrukVoid").val();
					if(screenWidth < 600){
						//location.href = base_url+"print/Print/print_struk_mobile.php?id="+$("#strukid").val();
						ajax_print('rawbt-receipt_mobile.php?id='+$("#txtStrukVoid").val()+'&act=void',this);
						setTimeout(function(){  // Beginning of code that should run AFTER the timeout
							location.href = "system_pos.php";
						}, 1000);  // Put the timeout here
					}else{
						//location.href = base_url+"print/Print/print_struk.php?id="+$("#strukid").val();
						ajax_print('rawbt-receipt.php?id='+$("#txtStrukVoid").val()+'&act=void',this);
						
						setTimeout(function(){  // Beginning of code that should run AFTER the timeout
							location.href = "system_pos.php";
						}, 1000);  // Put the timeout here

						
					}
					
					return false;
				}else{
					Swal.fire({
					  title: 'SUCCESS!',
					  text: "Struk Hanya Akan Dicetak Jika Cancel By Customer",
					  icon: 'success',
					  confirmButtonText: 'CLOSE'
					}).then((result) => {
					if (result.isConfirmed) {
						location.href = "void_backdate.php";
					}
					});
					//alert()
				}
				/*
				Swal.fire({
				  title: 'SUCCESS!',
				  text: data,
				  icon: 'success',
				  confirmButtonText: 'CLOSE'
				}).then((result) => {
				if (result.isConfirmed) {
					location.href = "system_pos.php";
				}
				});
				*/
			}
		})
		return false;
	}
	
}

function voidpos_close(){
	var modalpay = document.getElementById('modalVoidLogin');
	modalpay.style.display = "none";
}
function modalVoidTransaction_close(){
	var modalpay = document.getElementById('modalVoidTransaction');
	modalpay.style.display = "none";
}
function closeRefVoid(){
	var modalpay = document.getElementById('modal_ref_void');
	modalpay.style.display = "none";
}
function myRefBtn_proc(id){
	$.ajax({
		type : 'GET',
		url : 'cek_transaction_void.php?id='+id,
		success : function(data){
			if(data == 1){
				$("#id_ref_void").val(id);
				var modalpay = document.getElementById('modal_ref_void');
				modalpay.style.display = "none";
				return false;
			}else{
				Swal.fire({
				  title: 'Error!',
				  text: "Maaf No. Referensi Tidak Bisa Diinput!",
				  icon: 'error',
				  confirmButtonText: 'CLOSE'
				});
			}
		}
	});
}
function btn_ref_void(){
	var modalpay = document.getElementById('modal_ref_void');
	modalpay.style.display = "block";
	$.ajax({
		type : 'GET',
		url : 'cek_transaction.php?status=VOID',
		success : function(data){
			$("#data-void").html(data);
		}
	});
	
	
}
function close_btn_ref_void(){
	var modalpay = document.getElementById('modal_ref_void');
	modalpay.style.display = "none";
}

function addGuest(){
	$("#guest_id").val($("#result").text());
	$("#guest_id_view").val($("#result").text());
	$("#result").html('')
}