function setTaCharge(sts){
	var idlayanan	= $("#layanan").text();
	if(sts == 1){
		$.ajax({
			type : 'GET',
			url : 'get_amount_tacharge.php?layanan='+idlayanan,
			success : function(data){
				$("#tacharge").text(numberWithCommas(data));
			}
		});
		
	}
	else{
		$("#tacharge").html(0);
	}	
	var modalsession = document.getElementById('modal_list_tacharge');
	modalsession.style.display = "none";
}
function setSVCCharge(){
	var idlayanan		= $("#layanan").text();
	//get subtotal
	var get_subtotal	= $("#totalpay").text();	
	var sub_total		= get_subtotal.replace(/,/g,"");

	//get subtotaldiscount
	var get_totalDisc	= $("#amountdisc_vw").text();	
	var discount		= get_totalDisc.replace(/,/g,"");

	var svc_charge		= 0;
	$.ajax({
		type : 'GET',
		url : 'get_amount_servicecharge.php?trxcode='+idlayanan,
		success : function(data){
			var result = JSON.parse(data);
			//rumus service charge
			//(subtotal - Discount) * (svc/100)
			if(result.schargetype == 1){
				//jika type sevice charge % 
				svc_charge = (parseFloat(sub_total) - parseFloat(discount)) * (parseFloat(result.scharge)/100)
			}
			else if(result.schargetype == 2){
				//jika type service charge rupiah
				
			}
				
			$("#servicechargeval").text(numberWithCommas(Math.ceil(svc_charge)));
		}
	});	
	
}
function setPajakResto(){
	//get subtotal
	var get_subtotal	= $("#totalpay").text();	
	var sub_total		= get_subtotal.replace(/,/g,"");

	//get tacharge
	var get_tacharge	= $("#tacharge").text();	
	var tacharge		= get_tacharge.replace(/,/g,"");

	//get service Charge
	var get_cal_svccharge	= $("#servicechargeval").text();	
	var cal_svcCharge		= get_cal_svccharge.replace(/,/g,"");

	var get_downpayment	= $("#downpayment").text();	
	var downpayment		= get_downpayment.replace(/,/g,"");

	var taxresto		= 0;
	var idlayanan		= $("#layanan").text();
	//get subtotaldiscount
	var get_totalDisc	= $("#amountdisc_vw").text();	
	var discount		= get_totalDisc.replace(/,/g,"");
	$.ajax({
		type : 'GET',
		url : 'get_amount_servicecharge.php?trxcode='+idlayanan,
		success : function(data){
			var result = JSON.parse(data);
			//rumus service charge
			//(subtotal - Discount) * (svc/100)
			if(result.schargetype == 1){
				//jika type sevice charge % 
				svc_charge = (parseFloat(sub_total) - parseFloat(discount)) * (parseFloat(result.scharge)/100)
				//alert("svcnew"+svc_charge);
				taxresto 			= (parseFloat(sub_total) + parseFloat(tacharge) + parseFloat(svc_charge) +  parseFloat(downpayment)) * (10/100);
			}
			else if(result.schargetype == 2){
				//jika type service charge rupiah
				
			}
			else{
				taxresto 			= (parseFloat(sub_total) + parseFloat(tacharge) +  parseFloat(downpayment)) * (10/100);
			}
		}
	});	

	//alert(sub_total);
	//alert(parseFloat(cal_svcCharge)  + parseFloat(svc_charge));
	//alert(tacharge);
	//alert((parseFloat(sub_total) +"+"+ parseFloat(tacharge) +"+"+ parseFloat(svc_charge) +"+"+  parseFloat(downpayment)) );
	
	$("#taxresto").text(numberWithCommas(Math.ceil(taxresto)));
	
}
/*****
	Calculate Proses
	totalpayval Digunakan untuk menyimpan nilai subtotal terakhir
	var amt_subtotal	= Math.floor(parseFloat(total.replace(/,/g,"")));
	numberWithCommas(Math.floor(subtotal))
	totalpay({qty * price},{})
******/

	function totalpay(ttl, tmin, disc = 0, upgrade = 0){
		var TotalValue 	= 0;
		
		var get_subtotal	= $("#totalpay").text();	
		var sub_total		= get_subtotal.replace(/,/g,"");	

		TotalValue			= parseFloat(sub_total) + parseFloat(ttl) - parseFloat(disc);

		//alert(sub_total+'--'+ttl+'=='+disc);

		if(TotalValue !== 0){
			$('#totalpay').html(numberWithCommas(TotalValue));
			$("#amountdisc_vw").html( numberWithCommas(disc) );
		}
		else{
			$('#totalpay').html('0');
			$("#amountdisc_vw").html('0');
		}

		setSVCCharge();
		setPajakResto();
	}


	function subtotal(){
		$("#item_disc").hide();
		$("#sales_disc").hide();

		setSVCCharge();
		setPajakResto();

		//declare global variable
		var ta_charge 		= 0;
		var totalpay		= 0;
		var subtotal		= 0;
		var discount		= 0;
		var service_charge	= 0;
		var tax 			= 0;
		var rounding		= 0;
		var downpayment		= 0;

		//perhitunmasmsgan ulang ganti layanan
		var str_totalpay	= $("#totalpay").text();	
		totalpay			= str_totalpay.replace(/,/g,"");

		var str_discount	= $("#amountdisc_vw").text();	
		discount			= str_discount.replace(/,/g,"");

		$("#amounttotal_disc").html(numberWithCommas(parseFloat(totalpay) - parseFloat(discount) ));
		
		var str_tacharge	= $("#tacharge").text();	
		ta_charge			= str_tacharge.replace(/,/g,"");	

		var str_svc_charge	= $("#servicechargeval").text();	
		service_charge		= str_svc_charge.replace(/,/g,"");

		var str_tax			= $("#taxresto").text();	
		tax					= str_tax.replace(/,/g,"");

		var str_rounding	= $("#pembulatanval").text();	
		rounding			= str_rounding.replace(/,/g,"");

		var str_downpayment	= $("#downpayment").text();	
		downpayment			= str_downpayment.replace(/,/g,"");

		
		
		$("#gross_saldisc").val(parseFloat(totalpay) - parseFloat(discount));

		
		subtotal			= (parseFloat(totalpay) - parseFloat(discount)) + parseFloat(ta_charge) + parseFloat(service_charge) + parseFloat(tax) - parseFloat(rounding) - parseFloat(downpayment);
		$("#subtotal").html(numberWithCommas(subtotal));

		$("#totalpayval").val(subtotal);
		
		cleartxt();
	}

	function confirmation_order(){

		var str_totalpay			= $("#totalpay").text();	
		totalpay					= str_totalpay.replace(/,/g,"");
		//alert("masms"+totalpay);
		$("#slspayment_gross_sales").val(totalpay);

		var str_discount			= $("#amountdisc_vw").text();	
		discount					= str_discount.replace(/,/g,"");

		var str_amounttotal_disc	= $("#amountdisc_vw").text();	
		amounttotal_disc			= str_amounttotal_disc.replace(/,/g,"");

		var str_svc_charge			= $("#servicechargeval").text();	
		service_charge				= str_svc_charge.replace(/,/g,"");

		$("#slspayment_total_cost").val(service_charge);
		
		//addvalue Conformation Order
		$("#gross_bayar").val(numberWithCommas(totalpay));
		if($("#val_disc_type").val() == 'item'){
			$("#item_disc").show();
			$("#item_discount").val(numberWithCommas(discount));
			$("#slspayment_item_discount").val(discount);
			$("#slspayment_gross").val(parseFloat(totalpay) - parseFloat(discount));
		}else{
			$("#sales_disc").show();
			$("#sales_discount").val(numberWithCommas(discount));
			$("#slspayment_sales_discount").val(discount);
			$("#slspayment_gross").val(parseFloat(totalpay))
		}

		$("#gross_saldisc").val(numberWithCommas(parseFloat(totalpay) - parseFloat(discount)));
		$("#sales_servicecharge").val(numberWithCommas(service_charge));
		$("#grsl_svc").val(numberWithCommas(parseFloat(totalpay) - parseFloat(discount) + parseFloat(service_charge)));

		var str_tacharge	= $("#tacharge").text();	
		ta_charge			= str_tacharge.replace(/,/g,"");

		$("#sales_tacharge").val(numberWithCommas(ta_charge));
		$("#slspayment_tacharge").val(ta_charge);

		var str_downpayment	= $("#downpayment").text();	
		downpayment			= str_downpayment.replace(/,/g,"");

		$("#dp_sales").val(numberWithCommas(downpayment));
		$("#slspayment_dp").val(downpayment);

		var str_tax			= $("#taxresto").text();	
		tax					= str_tax.replace(/,/g,"");
		$("#sales_pajakresto").val(numberWithCommas(tax));
		$("#slspayment_tax").val(tax);

		var subtotal		= (parseFloat(totalpay) - parseFloat(discount)) + parseFloat(ta_charge) + parseFloat(service_charge) + parseFloat(tax) - parseFloat(downpayment);
		$("#sales_subtotal").val(numberWithCommas(subtotal));
		$("#slspayment_subtotal").val(subtotal);

		var str_rounding	= $("#pembulatanval").text();	
		rounding			= str_rounding.replace(/,/g,"");
		$("#pembulatan2").val(numberWithCommas(rounding));
		$("#slspayment_rounding").val(rounding);

		var grand_total		= parseFloat(subtotal) - parseFloat(rounding);
		$("#subtotal2").val(numberWithCommas(grand_total));
		$("#slspayment_net").val(grand_total);
	}


function btncash(nominal){
	
	if(nominal == 0){
		var str_grandtotal	= $("#subtotal2").val();	
		grand_total			= str_grandtotal.replace(/,/g,"");
		var str_nontunai	= $("#non_tunai").val();	
		nontunai			= str_nontunai.replace(/,/g,"");

		//alert(grand_total+"==="+nontunai);
		//console.log(grand_total+"==="+nontunai);

		if(nontunai < 0){
			$("#tunai").val(numberWithCommas(grand_total));
		}else{
			$("#tunai").val(numberWithCommas(parseFloat(grand_total) - parseFloat(nontunai)));
		}	
	}else{

		$("#tunai").val(numberWithCommas(nominal));
		
		var str_grandtotal	= $("#subtotal2").val();	
		grand_total			= str_grandtotal.replace(/,/g,"");
		
		var str_nontunai	= $("#non_tunai").val();	
		nontunai			= str_nontunai.replace(/,/g,"");

		//alert(grand_total+"==="+nontunai);
		//console.log(grand_total+"==="+nontunai);
		var kembali			= parseFloat(nominal) - parseFloat(grand_total) - parseFloat(nontunai);

		if(kembali > 0){
			$("#kembali").val(numberWithCommas(kembali));
			$("#slspayment_refund").val(numberWithCommas(kembali));
		}else{
			Swal.fire({
				title: 'Warning!',
				text: "Nominal Lebih Kecil Dari Transaksi!! ",
				icon: 'warning',
				confirmButtonText: 'CLOSE'
			});;
			$("#tunai").val(numberWithCommas(0));
		}	
	}
	
};

