function itemdiscount(id){
	if(id == 'item'){
		$("#val_disc_type").val('item');
		valselect = $('#tmpvalue').val();
		//alert(id);
		if(valselect == 0){
			alert("Silahkan Pilih Item Yang Akan Di Discount");
			return false;
		}
		var modalpay = document.getElementById('modalItemDisc');
			modalpay.style.display = "block";
		$.ajax({
			url: 'get_list_discount.php?id='+id,				
			dataType: 'html',
			success: function (data){
				//alert(data);
				$('#loadItemDiscount').html(data);
			}
		});
	}
	else
	{
		$("#val_disc_type").val('sales');
		var modalpay = document.getElementById('modalItemDisc');
			modalpay.style.display = "block";
		$.ajax({
			url: 'get_list_discount.php?id='+id,				
			dataType: 'html',
			success: function (data){
				//alert(data);
				$('#loadItemDiscount').html(data);
			}
		});
	}
}

function validDiscLogin(){				
	var modalpay = document.getElementById('modalResetDiscLogin');
	modalpay.style.display = "block";
}

/************
Function Button Close PoPUp
************/
function resetDiscYes(){
	var username 	= $("#userid_disc").val();
	var pass	 	= $("#password_disc").val();
	$.ajax({
		type : 'GET',
		url : 'cek_login.php?user='+username+'&password='+pass+'&cekadmin=1',
		success : function(data){
			if(data == 1){
				
				Swal.fire({
				  title: 'Error!',
				  text: "User Tidak Di Izinkan",
				  icon: 'error',
				  confirmButtonText: 'CLOSE'
				});		
				return false;
			}else{	
				$("#val_disc_type").val('');
				$("#nik_kar").val('');
				$("#nik_kar_child").val('');
				$("#nama_kar_child").val('');
				$("#amountdisc_vw").html(0);
				$("#val_disc_val").val('');
				$("#val_type_kar").val('');
				$("#valid_disc").val('');
				// valselect 	= $('#tmpvalue').val();
				// $('#un'+valselect+' .discount').html('0');		
				// $('#un'+valselect+' .promoid').val('');
				// $('#un'+valselect+' .promodesc').val('');
				// $('#un'+valselect+' .disctype').val('');
				// $('#un'+valselect+' .discvalue').val('');		
										
							$('.discamount').val('0');
							$('.discamount').html('0');
							$('.discount').html('0');
							$('.discval').val('');
							$('.disctype').val('0');
							$('.promodesc').val('');
							$('.promoid').val('');
							$('.discountval').val('0');
							$('.discvalue').val('0');
							$("#item_discount").val('0')
				subtotal();				
				var modalpay = document.getElementById('modalResetDiscLogin');
				modalpay.style.display = "none";
				// alert("Reset Discount Berhasil");	
								Swal.fire({
									title: 'Success!',
									text: 'Reset Discount Berhasil',
									icon: 'success',
									confirmButtonText: 'OK'
								  });
				var modalpay = document.getElementById('reset_discount');
				modalpay.style.display = "none";
				
				
			}
		}
	})
}
function resetDiscNo(){
	var modalpay = document.getElementById('reset_discount');
	modalpay.style.display = "none";
}
function closedisc() {

	$("#val_disc_type").val('');
	var modalpay = document.getElementById('modalItemDisc');
	modalpay.style.display = "none";
}
function closeLoginDisc(){				
	var modalpay = document.getElementById('modalResetDiscLogin');
	modalpay.style.display = "none";
}
