function pilih_layanan(){
	//("masuk");
	$.ajax({
		type : 'POST',
		url : 'get_list_layanan.php',
		success : function(data){
			$("#load_list_layanan").html(data);
			var modalsession = document.getElementById('modal_list_layanan');
			modalsession.style.display = "Block";
		}
	})
}
function close_pilih_layanan(){
	var modalsession = document.getElementById('modal_list_layanan');
	modalsession.style.display = "none";
}

function get_detail_layanan(idlayanan,tacharge,prefix = ""){
	//alert(tacharge);
	var statusLayanan 	= 0;
	var idlayananSel	= [];
	var lopArray		= 0;
	$('#table-list .coretype').each(function() {
		idlayananSel[lopArray]	= $(this).html();
		lopArray++;
	});
	
	console.log(idlayananSel.length);	
	if(idlayananSel.length != 0){
		if(idlayanan == 'GF' || idlayanan == 'GR' || idlayanan == 'DL' || idlayanan == 'SF' || idlayanan == 'TE'){
			if(idlayananSel.includes(idlayanan) == false){
				Swal.fire({
				  title: 'Error!',
				  text: "Layanan "+idlayanan+" tidak bisa digabung dengan yang lain",
				  icon: 'error',
				  confirmButtonText: 'CLOSE'
				});
				return false;
			}
		}else if(idlayanan == 'DI' || idlayanan == 'TA' || idlayanan == 'DT' || idlayanan == 'EX' || idlayanan == 'BP'){
			if(idlayananSel.includes('GF') == true || idlayananSel.includes('GR') == true || idlayananSel.includes('DL') == true || idlayananSel.includes('SF') == true || idlayananSel.includes('TE') == true){
				Swal.fire({
				  title: 'Error!',
				  text: "Layanan "+idlayanan+" tidak bisa digabung dengan yang lain",
				  icon: 'error',
				  confirmButtonText: 'CLOSE'
				});		
				return false;
			}
		}
	}
	
	//
	
	//0 tanpa tacharge, 1 dengan tacharge, 2 Layanan Khusu DL, 3 layanan dengan ta charge dan rider id
	if(tacharge == 1){
		var modalsession = document.getElementById('modal_list_layanan');
		modalsession.style.display = "none";
		
		var  layanan = document.getElementById("layanan");
		layanan.innerHTML = idlayanan;
		
		$.ajax({
			type : 'GET',
			url : 'get_amount_tacharge.php?layanan='+idlayanan,
			success : function(data){
				//console.log(data);
				$('#TaChargeVal').val((parseFloat(data)));
				$('#layananVal').val(idlayanan);
			}
		});
		$("#layanan_pref").html('');
		
		var modalsession = document.getElementById('modal_list_tacharge');
		modalsession.style.display = "block";
	}
	else if(tacharge == 3){
		var modalsession = document.getElementById('modal_list_layanan');
		modalsession.style.display = "none";
		
		var  layanan = document.getElementById("layanan");
		layanan.innerHTML = idlayanan;
		
		$.ajax({
			type : 'GET',
			url : 'get_amount_tacharge.php?layanan='+idlayanan,
			success : function(data){
				//console.log(data);
				$('#TaChargeVal').val((parseFloat(data)));
				$('#layananVal').val(idlayanan);
				$("#layanan_pref").val(prefix);
			}
		});
		
		$.ajax({
			type : 'GET',
			url : 'get_amount_servicecharge.php?trxcode='+idlayanan,
			success : function(data){
				//console.log(data);
				$("#svc_type").val('persen')	;	
				$("#svc_val").val(parseFloat(data));
			}
		});
		
		
		
		var modalsession = document.getElementById('modal_list_tacharge');
		modalsession.style.display = "block";
	}else if(tacharge == 2){
		$.ajax({
			type : 'POST',
			url : 'get_list_layanan_delivery.php',
			success : function(data){
				$("#load_list_layanan_delivery").html(data);
				var modalsession = document.getElementById('modal_list_layanan');
				modalsession.style.display = "none";
				
				var modalsession = document.getElementById('modal_list_layanan_delivery');
				modalsession.style.display = "block";
			}
		})
	}else if(tacharge == 0){
			
		del_deliverycharge();
		var  layanan = document.getElementById("layanan");
		layanan.innerHTML = idlayanan;
		
		$('#layananVal').val(idlayanan);
		
		var array_prodname	= $('#frmtrans').getForm2obj();
		var	type_order		= array_prodname['type'];
		
		if (typeof type_order === 'undefined') {
			$("#tacharge").html(0);
			subtotal();
		}else
		{

			var arraycontainsturtles = (type_order.indexOf('TA') > -1);
			if(arraycontainsturtles == false){
				$("#tacharge").html(0);
			}
		}
		
		
		$("#layanan_pref").html('');
		var modalsession = document.getElementById('modal_list_layanan');
		modalsession.style.display = "none";
	}
}
function get_detail_layanan_delivery(delid,askdlcharge,dlcharge,prefix,layanan,scharge,schargetype){
	//alert(layanan);
	$("#delivery_id").val(delid);
	$("#layanan_pref").val(prefix);
	$("#layanan").html(layanan);
	$.ajax({
		type : 'GET',
		url : 'get_amount_tacharge.php?layanan='+layanan,
		success : function(data){
			//console.log(data);
			$('#TaChargeVal').val((parseFloat(data)));
			$('#layananVal').val(layanan);
		}
	});
	
	if(schargetype == '1'){
		$("#svc_type").val('persen');
		$("#svc_val").val(scharge);
		/*
		var amountSub		= $("#amounttotal_disc").text();
		var intAmountSub	= amountSub.replace(",","");
		var svc		= parseFloat(intAmountSub) * (scharge/100)
		$("#service_charge").val(scharge);*/
	}else{
		$("#svc_type").val('amount');
		$("#svc_val").val(scharge);
		//$("#service_charge").val(scharge);
	}
		
	if(askdlcharge == 0){
		del_deliverycharge();
		add_dlcharge(dlcharge);
		var modalsession = document.getElementById('modal_list_tacharge');
		modalsession.style.display = "block";
		
		var modalsession = document.getElementById('modal_list_layanan_delivery');
		modalsession.style.display = "none";
	}else{
		//r = confirm("Tambahkan PLU Delivery Charge Dalam Transaksi  ..?");
		//if (r == true) {
		Swal.fire({
		  title: 'CONFIRM!',
		  text: "Tambahkan PLU Delivery Charge Dalam Transaksi  ..?",
		  icon: 'question',
		  type: "warning",
		  showCancelButton: true,
		  confirmButtonText: 'YES',
		  cancelButtonText: 'NO'
		}).then((result) => {
			if (result.isConfirmed) {
				//alert(dlcharge);
				add_dlcharge(dlcharge);		
				
				var modalsession = document.getElementById('modal_list_tacharge');
				modalsession.style.display = "block";
				
				var modalsession = document.getElementById('modal_list_layanan_delivery');
				modalsession.style.display = "none";
			}else{
				del_deliverycharge();
				var modalsession = document.getElementById('modal_list_layanan_delivery');
				modalsession.style.display = "none";
				var modalsession = document.getElementById('modal_list_tacharge');
				modalsession.style.display = "block";
			}
		});
				
	}
}
function setTaCharge(sts){
	if(sts == 1){
		var amountcharge	= $("#TaChargeVal").val();
		$("#tacharge").html(numberWithCommas(amountcharge));
	}
	else{
		$("#tacharge").html(0);
	}
	
	subtotal();
	
	var  layanan = document.getElementById("layanan");
	layanan.innerHTML = $("#layananVal").val();
	
	var modalsession = document.getElementById('modal_list_tacharge');
	modalsession.style.display = "none";
}


function close_pilih_layanan_delivery(){
	var modalsession = document.getElementById('modal_list_layanan');
	modalsession.style.display = "block";
	
	var modalsession = document.getElementById('modal_list_layanan_delivery');
	modalsession.style.display = "none";
}

function close_pilih_tacharge(){	
	var modalsession = document.getElementById('modal_list_tacharge');
	modalsession.style.display = "none";
	
	
	var modalsession = document.getElementById('modal_list_layanan');
	modalsession.style.display = "block";
}

function add_dlcharge(i){
	del_deliverycharge();
    var divObj 		= 1;
	var divlayanan	= 'DL';
	var salesDisc	= 0;
	var servicecharge	= 0;
	var priceDisc	= 0;
	var discount	= 0;
	
	$.ajax({
		type : 'GET',
		url : 'get_product_layanan.php?layanan=DL&plu='+i,
		success : function(data){
			//alert(data);
			var result = JSON.parse(data);
			//console.log(result);
			if(data == 0){
				
				$("#result").html('');
				Swal.fire({
				  title: 'Error!',
				  text: "Layanan/PLU Tidak Active",
				  icon: 'error',
				  confirmButtonText: 'CLOSE'
				});	
				
				return false;
			}
			var pr = result ;
			
			total = 1 * pr.UNIT_PRICE;
			//console.log(total);
			unique = Date.now();
			seq_no = $("#seq_no").val();
			
			
			$("#seq_no").val(parseInt(seq_no) + 1);
			strukid = '1111';
		
			
			$('#list').append('<tr tabindex="'+i+'" id="un'+unique+'" class="finishclear" onclick=tmpvalue("'+unique+'")> '+
				'<td width="35%" class="seqno" align="left" style="display:none;">'+seq_no+'</td>'+
				'<td width="35%" class="itemid" align="left" style="display:none;">'+pr.PRODCODE+'</td>'+
				'<td width="35%" class="itemname" align="left">'+pr.PRODNAME+'</td><td width="10%" class="coretype" align="center">'+pr.TRX_CODE+'</td>' +
				'<td width="10%" class="coreqty" align="center">1</td>' +
				'<td width="10%" class="discount" align="center">0 </td>' +
				'<td width="15%" align="right" class="price">'+numberWithCommas(total)+'</td>'+
				'<td width="15%" align="right" class="totalprice">'+numberWithCommas(total)+'</td>' +
				'<td width="15%" align="right" class="upgrade" style="display:none;">0</td>' +
				'<td width="5%" style="display:none;"><table>'+
					'<tr><td colspan="6">'+
						'<input type="hidden" name="PRICE[]" value="'+total+'">'+
						'<input type="hidden" class="realprice" value="'+total+'">'+
						'<input type="hidden" name="SEQ_NO[]" value="'+seq_no+'">'+
						'<input type="hidden" name="type[]" value="DL">'+
						'<input type="hidden" name="disc[]" value="0" class="discountval">'+
						'<input type="hidden" name="PRODCODE[]" value="'+i+'">'+
						'<input type="hidden" name="QTY[]" value="1">'+
						'<input type="hidden" name="PRODNAME[]" value="'+pr.PRODNAME+'">'+
						'<input type="hidden" name="PROMOID[]"  class="promoid">'+
						'<input type="hidden" name="PROMODESC[]"   class="promodesc">'+
						'<input type="hidden" name="DISCTYPE[]" class="disctype">'+
						'<input type="hidden" name="DISCVALUE[]" value="0" class="discvalue">'+
					'</td></tr>'+
				'</table></td>'+
				'<td class="amountdisc" style="display:none;">0</td>' + 
			'</tr>');
			
			//alert(unique);
			$("#unik_dlcharge").val(unique);
			
			totalpay(total, 0,discount)
			subtotal();
			
			$.ajax({
				type : 'post',
				url : 'save_order_perline.php',
				data : $("#frmtrans").serialize(),
				success : function(data){
					divObj.textContent = '';
					servicecharge.textContent = '';
					salesDisc.textContent = '';
				}
			});

			
			cleartxt();
		}
	});
	
	
	

}
function del_deliverycharge(){
	valselect = $("#unik_dlcharge").val();		
	$('#un'+valselect).html('');
	$('.link'+valselect).html('');
	$('#tmpvalue').val(0);
	$("#unik_dlcharge").val('');
}


function close_modal_id_delivery(){
	var modalsession = document.getElementById('modal_id_delivery');
	modalsession.style.display = "none";

}function close_modal_id_delivery_internal(){
	var modalsession = document.getElementById('modal_id_delivery_int');
	modalsession.style.display = "none";
}
function setValInt(){
	$("#val_id_rider_int").val("INT")
}
function SubmitRider(){
	
	//$("#layananVal").val('DL-R');
	/*
	if($("#val_id_rider").val() == ''){
		Swal.fire({
		  title: 'Error!',
		  text: "ID Rider Harus Diisi",
		  icon: 'error',
		  confirmButtonText: 'CLOSE'
		});	
		return false;
	}
	if(($("#val_id_rider_int").val() == '') || ($("#idrider").val() == '')){
		Swal.fire({
		  title: 'Error!',
		  text: "ID Rider Harus Diisi",
		  icon: 'error',
		  confirmButtonText: 'CLOSE'
		});		
		return false;
	}*/
	
	$.ajax({
		type : 'post',
		url : 'cek_promo.php',
		data : $("#frmtrans").serialize(),
		success : function(data){
			var modalpay = document.getElementById('mypayModal');
			modalpay.style.display = "block";
			
			var modaldel = document.getElementById('modal_id_delivery');
			modaldel.style.display = "none";
			
			
			var modalpay = document.getElementById('modal_id_delivery_int');
			modalpay.style.display = "none";
			if($("#val_id_rider").val()){
				$("#idrider").val($("#val_id_rider").val());
			}else{
				$("#idrider").val($("#val_id_rider_int").val());
			}
			$("#loadpromo").html(data);
		}
	});
}