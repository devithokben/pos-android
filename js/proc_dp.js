function addDp(){
	var modaltype = document.getElementById('dp_popup');
	modaltype.style.display = "block";
}
function new_dp(){
	var modaltype 	= document.getElementById('amount_dp');
	modaltype.style.display = "block";
	
	var modaltype 	= document.getElementById('dp_popup');
	modaltype.style.display = "none";
	
	
}
function close_dp_popup(){
	var modaltype 	= document.getElementById('dp_popup');
	modaltype.style.display = "none";
}

function showdp(tgl){
	$.ajax({
		type : 'GET',
		url : 'get_list_dp.php?date='+tgl,
		success : function(data){
			$("#lst_dp").html(data);
			var modaltype 	= document.getElementById('list_dp');
			modaltype.style.display = "block";
		}
	});
}
function close_info_dp(){
	var modaltype 	= document.getElementById('list_dp');
	modaltype.style.display = "none";
}
function prc_amount_dp(){	
	var	dp_prodcode	= $("#dp_prodcode").val();
	$("#dp_prodcode_val").val(dp_prodcode);
	
	var modaltype = document.getElementById('detail_dp');
	modaltype.style.display = "block";
	
	var modaltype = document.getElementById('amount_dp');
	modaltype.style.display = "none";
}
function close_amount_dp(){
	var modaltype = document.getElementById('amount_dp');
	modaltype.style.display = "none";
}
function submit_dp(){
	//val_amount_dp
	var stramount_dp 	= $("#val_amount_dp").val();
	var stramount_dp1	= stramount_dp.replace(",","");
	var amount_dp  		= parseInt(stramount_dp1.replace(",","")) / 1.1;
	
	$("#dp_amount_val").val(parseFloat(amount_dp.toFixed()));
	
	var name 	= $("#val_name_dp").val();
	$("#dp_name_val").val(name);
	
	var date 	= $("#val_date_dp").val();
	$("#dp_date_val").val(date);
	
	//alert(date +"<="+ $("#trx_date").val());
	
	if(date <= $("#trx_date").val()){
		Swal.fire({
		  title: 'Error!',
		  text: 'Tanggal Ultah kurang dari atau sama dengan Tanggal Start Of Day',
		  icon: 'error',
		  confirmButtonText: 'CLOSE'
		}).then((result) => {
			if (result.isConfirmed) {
				location.href='system_pos.php';
			}
		});
	}
	
	var nominal	= parseInt($("#dp_amount_val").val());
	
	menuitem_qty('900902',nominal);
	
	var modaltype = document.getElementById('detail_dp');
	modaltype.style.display = "none";
}
function close_detail_dp(){
	var modaltype = document.getElementById('detail_dp');
	modaltype.style.display = "none";
}


//function release DP
function release_dp(tgl){
	var modaltype 	= document.getElementById('dp_popup');
	modaltype.style.display = "none";
	$.ajax({
		type : 'GET',
		url : 'get_list_dp.php?date='+tgl+'&act=release',
		success : function(data){
			$("#lst_dp_release").html(data);
			var modaltype 	= document.getElementById('list_dp_release');
			modaltype.style.display = "block";
		}
	});	
}
function close_info_dp_release(){
	var modaltype 	= document.getElementById('list_dp_release');
	modaltype.style.display = "none";
}
function addDp_val(id){
	var totalAmt	= 0;
	var getDpuse	=  parseInt($("#use_dp").val());
	var lasdp		= $("#dp_trx_id").val();
	
	$.ajax({
		type : 'GET',
		url : 'get_list_dp.php?id='+id+'&act=insertDP',
		success : function(data){
			totalAmt = getDpuse + parseInt(data);
			$("#use_dp").val(numberWithCommas(totalAmt));
			if(lasdp == ''){
				$("#dp_trx_id").val(id);
			}else{
				$("#dp_trx_id").val(lasdp+','+id);
			}
			
			
		}
	});	
	
}
function proc_list_dp_release(){
	$("#dp_val_use").val($("#use_dp").val());
	var stramount_dp 	= $("#use_dp").val();
	var stramount_dp1	= stramount_dp.replace(",","");
	var amount_dp  		= parseInt(stramount_dp1.replace(",",""));
	$("#dp_amount_val").val(parseInt(amount_dp));
	
	//alert("Amount DP: "+amount_dp)
	
	subtotal();
	
	var modaltype 	= document.getElementById('list_dp_release');
	modaltype.style.display = "none";
}

//action DP
function dp_cancel(id){
	
	var modalpay = document.getElementById('modalDpCancelLogin');
	modalpay.style.display = "Block";
	$("#keydp").val(id);
	
}

function canceldpval(){
	var username 	= $("#userid_cancel").val();
	var pass	 	= $("#password_cancel").val();
	var keydp	 	= $("#keydp").val();
	var strukiddp	 	= $("#strukiddp").val();
	$.ajax({
		type : 'GET',
		url : 'cek_login.php?user='+username+'&password='+pass+'&cekadmin=1',
		success : function(data){
			if(data == 1){
				
				Swal.fire({
				  title: 'Error!',
				  text: "User Tidak Di Izinkan",
				  icon: 'error',
				  confirmButtonText: 'CLOSE'
				});		
				return false;
			}else{			
				var modalpay = document.getElementById('modalDpCancelLogin');
				modalpay.style.display = "none";
				$("#keydp").val(keydp);
				var id = keydp;
				$.ajax({
					type : 'GET',
					url : 'get_list_dp_by_id.php?id='+id,
					success : function(data){
						
						
						var result = JSON.parse(data);
						$("#val_cancel_dp_id").val(result.ID);
						$("#val_cancel_dp_tglultah").val(result.DATE_ULT);
						$("#val_cancel_dp_nama").val(result.CUST_ULT);
						$("#val_cancel_dp_user").val(username);
						
						
						//alert("masuk");
						var modaltype 	= document.getElementById('list_dp');
						modaltype.style.display = "none";
						
						var modalCancel 	= document.getElementById('dp_cancel');
						modalCancel.style.display = "block";
					}
				});	
				
				
			}
		}
	})
	
}

function prc_cancel_dp(){
	// alert($("#frm-cancel-dp").serialize());
	$.ajax({
		type : 'post',
		url : 'proc_batal_dp.php',
		data : $("#frm-cancel-dp").serialize(),
		success : function(data){		
			location.href = 'system_pos.php';
			return false;
		}
	});
	
	
}
function close_cancel_dp(){
	var modalCancel 	= document.getElementById('dp_cancel');
	modalCancel.style.display = "none";
}

function canceldpval_close(){
	var modalpay = document.getElementById('modalDpCancelLogin');
	modalpay.style.display = "none";
}

function dp_reschedule(id, e){
	if(e == 1){
		var modalpay = document.getElementById('modalDpRescheduleLogin');
		modalpay.style.display = "Block";
		$("#strukiddp").val(id);
		$("#keydp").val(e);
	}
	else{
		var modalpay = document.getElementById('modalDpRescheduleLogin');
		modalpay.style.display = "Block";
		$("#strukiddp").val(id);
		$("#keydp").val(e);
	}
	
}

function rescheduledpval(){
	var username 	= $("#userid_reschedule").val();
	var pass	 	= $("#password_reschedule").val();
	var keydp	 	= $("#keydp").val();
	var strukiddp	 	= $("#strukiddp").val();
	$.ajax({
		type : 'GET',
		url : 'cek_login.php?user='+username+'&password='+pass+'&cekadmin=1',
		success : function(data){
			if(data == 1){
				
				Swal.fire({
				  title: 'Error!',
				  text: "User Tidak Di Izinkan",
				  icon: 'error',
				  confirmButtonText: 'CLOSE'
				});		
				return false;
			}else{			
				var modalpay = document.getElementById('modalDpRescheduleLogin');
				modalpay.style.display = "none";
				$("#dp_id").val(strukiddp);
				$.ajax({
					type : 'GET',
					url : 'get_list_dp_by_id.php?id='+strukiddp,
					success : function(data){
						
						
						var result = JSON.parse(data);
						$("#val_reschedule_dp_id").val(result.ID);
						$("#val_reschedule_dp_tglultah").val(result.DATE_ULT);
						$("#val_reschedule_dp_nama").val(result.CUST_ULT);
						$("#val_reschedule_dp_user").val(username);

						// dateultah = $("#val_reschedule_dp_tglultah").val(result.DATE_ULT);
						// var d = dateultah;
						// d.setDate(d.getDate() - 2);
						// month = '0' + (d.getMonth() + 1),
						// day = '0' + d.getDate(),
						// year = d.getFullYear();
						// last = [year, month, day].join('-');
						// console.log(last);	
						
						if(keydp == 1){
						
							var modaltype 	= document.getElementById('list_dp');
							modaltype.style.display = "none";
							
							var modalCancel 	= document.getElementById('dp_reschedule');
							modalCancel.style.display = "block";

						}else{
						
							var modaltype 	= document.getElementById('list_dp');
							modaltype.style.display = "none";
							
							var modalCancel 	= document.getElementById('dp_reschedule');
							modalCancel.style.display = "block";

							var modalpay1 = document.getElementById('confirm_entry_dpModal');
							modalpay1.style.display = "block";

						}
					}
				});	
				
				
			}
		}
	})
	
}
function prc_reschedule_dp(){
	// alert($("#frm-reschedule-dp").serialize());
	$.ajax({
		type : 'post',
		url : 'proc_reschedule.php',
		data : $("#frm-reschedule-dp").serialize(),
		success : function(data){		
			location.href = 'system_pos.php';			
			return false;
		}
	});
	
	
}
function close_reschedule_dp(){
	var modalCancel 	= document.getElementById('dp_reschedule');
	modalCancel.style.display = "none";
}
function rescheduledpval_close(){
	var modalpay = document.getElementById('modalDpRescheduleLogin');
	modalpay.style.display = "none";
}
	