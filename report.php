<?php 
	session_start();
	date_default_timezone_set("Asia/Bangkok");
	error_reporting(1);
	include("dbconnect.php");
	include("template/header_h2h.php");
?>
	<div class="page-wrapper">
		<!-- ============================================================== -->
		<!-- Bread crumb and right sidebar toggle -->
		<!-- ============================================================== -->
		<div class="page-breadcrumb">
			<div class="row align-items-center">
				<div class="col-xs-12 col-md-12 col-sm-12">
					<br />
					<div class="row">
						<!--
						<div class="col-lg-3 col-xs-6" style="width:40%"> 
							<a href="report/endsession.php" class="btn btn-default" style="border:1px solid #888888; box-shadow: 5px 10px #888888;">
								<img src="img/report.png" style="width:80%"  />
								<p>Report End Session</p>
							</a>
						</div>
						-->
						<div class="col-lg-3 col-xs-6" style="width:40%"> 
							<a href="report/discount.php?pr=report" class="btn btn-default" style="border:1px solid #888888; box-shadow: 5px 10px #888888;background:#fff;color:#000;margin-bottom:20px;padding-top:10px;font-size:14px;;">
								<img src="img/report.png" style="width:80%"  />
								<p>Laporan Discount</p>
							</a>
						</div>
						<div class="col-lg-3 col-xs-6" style="width:40%"> 
							<a href="report/hourly.php?pr=report" class="btn btn-default" style="border:1px solid #888888; box-shadow: 5px 10px #888888;background:#fff;color:#000;margin-bottom:20px;padding-top:10px;font-size:14px;;">
								<img src="img/report.png" style="width:80%"  />
								<p>Laporan Hourly</p>
							</a>
						</div>
						
						<div class="col-lg-3 col-xs-6" style="width:40%"> 
							<a href="report/eodfinance.php?pr=report" class="btn btn-default" style="border:1px solid #888888; box-shadow: 5px 10px #888888;background:#fff;color:#000;margin-bottom:20px;padding-top:10px;font-size:14px;;">
								<img src="img/report.png" style="width:80%"  />
								<p>Laporan Finance</p>
							</a>
						</div>
						<div class="col-lg-3 col-xs-6" style="width:40%"> 
							<a href="report/perproduct.php?pr=report" class="btn btn-default" style="border:1px solid #888888; box-shadow: 5px 10px #888888;background:#fff;color:#000;margin-bottom:20px;padding-top:10px;font-size:14px;;">
								<img src="img/report.png" style="width:80%"  />
								<p>Penjualan Per Product</p>
							</a>
						</div>
						<div class="col-lg-3 col-xs-6" style="width:40%"> 
							<a href="report/edcreport.php?pr=report" class="btn btn-default" style="border:1px solid #888888; box-shadow: 5px 10px #888888;background:#fff;color:#000;margin-bottom:20px;padding-top:10px;font-size:14px;;">
								<img src="img/report.png" style="width:80%"  />
								<p>Laporan EDC</p>
							</a>
						</div>
						<div class="col-lg-3 col-xs-6" style="width:40%"> 
							<a href="report/deliveryreport.php?pr=report" class="btn btn-default" style="border:1px solid #888888; box-shadow: 5px 10px #888888;background:#fff;color:#000;margin-bottom:20px;padding-top:10px;font-size:14px;;">
								<img src="img/report.png" style="width:80%"  />
								<p>Delivery Report</p>
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php include("template/footer_h2h.php");?>