<?php
ini_set('display_errors', '1');
ini_set('display_startup_errors', '1');
error_reporting(0);
?>
<!DOCTYPE html>
<html dir="ltr" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
   
    <title>Halaman Login POS</title>
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="assets/images/favicon.png">
    <!-- chartist CSS -->
    <link href="assets/plugins/chartist-js/dist/chartist.min.css" rel="stylesheet">
    <link href="assets/plugins/chartist-js/dist/chartist-init.css" rel="stylesheet">
    <link href="assets/plugins/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.css" rel="stylesheet">
    <!--This page css - Morris CSS -->
    <link href="assets/plugins/c3-master/c3.min.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="css/style.min.css" rel="stylesheet">
	<link href="<?php echo  $base_url ?>sweetalert2/dist/sweetalert2.min.css" rel="stylesheet">
	 
	<!-- keyboard widget css & script (required) -->
	<link href="Keyboard/css/keyboard.css" rel="stylesheet">
	<style>
		.ui-keyboard{
			background : #eee;
		}
		.ui-keyboard-button{
			height: 4em;
			min-width: 4em;
			margin: .2em;
			cursor: pointer;
			overflow: hidden;
			line-height: 4em;
			-moz-user-focus: ignore;
			
		}
		.ui-keyboard-text{
			font-size:20px;
		}
	</style>
</head>

<body class="btn-warning">
<div id="main-wrapper" data-layout="vertical"  data-sidebartype="full"
	data-sidebar-position="absolute" data-header-position="absolute" data-boxed-layout="full">
	<!-- ============================================================== -->
	<!-- Topbar header - style you can find in pages.scss -->
	<!-- ============================================================== -->
	<header class="topbar btn-danger" data-navbarbg="">
		<nav class="navbar top-navbar navbar-expand-md navbar-dark btn-danger">
			<div class="navbar-header" data-logobg="">
				<!-- ============================================================== -->
				<!-- Logo -->
				<!-- ============================================================== -->
				<a class="navbar-brand ml-4" href="index.html">
				   
					<!--End Logo icon -->
					<!-- Logo text -->
					<span class="logo-text" style="color:#fff">
					   <h2>System Update Promo</h2>

					</span>
				</a>
				<!-- ============================================================== -->
				<!-- End Logo -->
				<!-- ============================================================== -->
				<!-- ============================================================== -->
				<!-- toggle and nav items -->
				<!-- ============================================================== -->
				<a class="nav-toggler waves-effect waves-light text-white d-block d-md-none"
					href="javascript:void(0)"><i class="ti-menu ti-close"></i></a>
			</div>
			<!-- ============================================================== -->
			<!-- End Logo -->
		  
		</nav>
	</header>
	<div class="container-fluid">
		<!-- ============================================================== -->
		<!-- Start Page Content -->
		<!-- ============================================================== -->
		<!-- Row -->
		<div class="row">
			<div class="col-lg-4 col-xlg-3 col-md-5">
					
				</div>
				<!-- Column -->
				<!-- Column -->
				<div class="col-lg-12 col-xlg-12 col-md-12">
					<div class="card" style="margin-top:40px;">
						<div class="card-body">
							<div id="information"></div>
							<hr />
							<div style="text-align:center; color:red;"><strong>Update promo sedang berlangsung . . Mohon tunggu hingga muncul tombol dibawah ini :<br/><br/></strong></div>
							<div id="btn-akses" class="col-lg-12 col-xlg-12 col-md-12" style="text-align:center;"></div>
							<hr />
							<div id="log"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
	$qry_data	=  urldecode($_GET['qry']);
	
	include("dbconnect.php");
	ini_set('max_execution_time', 0); 
	if(mysqli_query($conpos,"CALL sp_clearpromo")){
		//echo $url_websvc.'promo.php?storeid='.$kd_pemakai;exit;
		$ch = curl_init(); 
		curl_setopt($ch, CURLOPT_URL, $url_websvc.'promo.php?storeid='.$kd_pemakai);
		curl_setopt($ch,CURLOPT_USERAGENT,'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);     
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
		$return_out = curl_exec($ch); 
		curl_close($ch); 
		if($return_out === false)
		{
			$output	= 'Curl error: ' . curl_error($ch);
		}
		else
		{
			$output = $return_out; 
		}
		$result[0]	= json_decode($output);
		echo "<pre>";
		
	
		foreach ($result[0] as $key=>$val) {
				if($temtable != $key){
					$temtable = $key;
					//echo  "<span class='alert alert-info'>".$key." :: ";
					
					
					
					foreach ($result[0]->$key as $val2) {
						
						$newArray	= (array)$val2;	
						
						$field	= array_keys($newArray);
						$value	= array();
					
						//print_r($field);
						//print_r(array_values($newArray));
					
						for($loop = 0; $loop <= count($field) - 1;$loop++){
							if(is_object(array_values($newArray)[$loop])){
								//echo "masul";
								$value[$loop] = '';
							}else{
								if(
									(array_keys($newArray)[$loop] == 'StartDate') ||
									(array_keys($newArray)[$loop] == 'EndDate') ||
									(array_keys($newArray)[$loop] == 'StartTime') ||
									(array_keys($newArray)[$loop] == 'EndTime') ||
									(array_keys($newArray)[$loop] == 'CreateDate') || 
									(array_keys($newArray)[$loop] == 'UpdateDate') 
								){
									//echo "masukdate";
									$value[$loop] = date("Y-m-d H:i:s",strtotime(array_values($newArray)[$loop]));
								}else{
									$value[$loop] = array_values($newArray)[$loop];
								}
							}
							
							//print_r($value);
						}
						
						
						
					
						$sql = "insert into ".$temtable." (".implode(",",array_keys($newArray)).") values ('".implode("','",$value)."')";
						//echo $sql;
						echo '<script>
						parent.document.getElementById("information").innerHTML="<div style=\"text-align:center; font-weight:bold\">'.$sql.'</div>";</script>';
						mysqli_query($conpos,$sql);			
					}
					//print_r($result[0]->$key);
					//echo  "Sukses Update :: </span>";
					mysqli_query($conpos,"UPDATE ap_setup SET value = '".date('Y-m-d')."' where ID = 'UPD_'");				
				}
			}
			echo '<script>parent.document.getElementById("btn-akses").innerHTML="<a href=\"http://localhost:8080\" class=\"btn btn-danger\" style=\"text-align:center;font-size:18px;\">UPDATE BERHASIL SILAHKAN KLIK DISINI</a>";</script>';
	}
				
	
?>
