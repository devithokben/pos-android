<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

date_default_timezone_set("Asia/Bangkok");
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . 'libraries/REST_Controller.php';
class Payment_ovo extends REST_Controller {	
	
	public function __construct(){
		parent::__construct();
		
		$this->app_id		= "stagingkey";
		$this->key_hb		= "46d6e839771a3ab01779dfd2ab7b1ec2a8397cd92d981ae5620d1580dfbb9084";	
		$this->ctype		= 'application/json';
		$this->url			= 'https://api.byte-stack.net/pos';
		$this->tid			= '05630320';
		$this->mid			= '0563HOKBEN54906';
		$this->merchantId	= '110563';
		$this->storeCode	= 'HOKBEN0103';
		$this->appSource	= 'POS';
		$this->random		= $this->generate_random();
		$this->hmac			= $this->generate_hmac($this->app_id.$this->random,$this->key_hb);
		$this->data_post	= file_get_contents('php://input');
		
	}
	
	
    public function index()
    {
		//echo $random."++++++".$hmac;exit;
		if($_GET['action'] == 'void'){
			$detail_payment	= $this->getDetailPayment($app_id,$random,$hmac);		
			$response_code	= json_decode($detail_payment);
			if($response_code->responseCode == 58){
				$status_void = $this->pushVoidTransaction($app_id,$random,$hmac);
				print_r($status_void);
			}
		}
    }
	
	private function generate_random(){
		$date = new DateTime();
		return $date->getTimestamp();
	}
	private function generate_hmac($app_id,$key){
		$hmac	= hash_hmac('sha256',$app_id,$key);
		return $hmac;
	}
	public function pushPayment_post(){
		$type				=	"0200";
		$processingCode		=	"040000";	
		
		require_once APPPATH.'../HTTP_Request/HTTP/Request2.php';
		$request = new HTTP_Request2();
		$request->setUrl($this->url);
		$request->setMethod(HTTP_Request2::METHOD_POST);
		$request->setConfig(array(
		  'follow_redirects' => TRUE
		));
		$request->setHeader(array(
		  'Content-Type' => $this->ctype,
		  'app-id' => $this->app_id,
		  'random' => $this->random,
		  'hmac' => $this->hmac
		));		
		$arr_data					= json_decode($this->data_post);
		$arr_data->type				= $type;
		$arr_data->processingCode	= $processingCode;
		$arr_data->tid				= $this->tid;
		$arr_data->mid				= $this->mid;			
		$arr_data->merchantId		= $this->merchantId;	
		$arr_data->storeCode		= $this->storeCode;	
		$arr_data->appSource		= $this->appSource;	
		
		$request->setBody(json_encode($arr_data));
		try {
		  $response = $request->send();
		 // print_r($response->getStatus());exit;
		  if (($response->getStatus() == 200)) {
			//return  $response->getBody();
			$response_code	= json_decode($response->getBody());
			$rscode			= $response_code->responseCode;
			
			$this->save_log($this->data_post,$response->getBody(),$response->getStatus(),'PUSH P2P',$this->cek_response_code($rscode));
			
			$pushptp	= 	array(
								'status' => $response->getStatus(),
								'message' => $response->getBody()
							);										
			$this->set_response($pushptp, REST_Controller::HTTP_OK);
			
			
		  }
		  else {
			$msg			=  'Unexpected HTTP status: ' . $response->getStatus() . ' ' .
			$response->getReasonPhrase();
			$msg_ovo		= $response->getBody();
			$response_code	= json_decode($response->getBody());
			$rscode			= $response_code->responseCode;
			
			$this->save_log($this->data_post,$response->getBody(),$response->getStatus(),'PUSH P2P',$this->cek_response_code($rscode));
			
			$this->response(array(
					'status' => $response->getStatus(),
					'message_server' => $msg,
					'message_ovo' => $msg_ovo,
					'responsecode_ovo' => $rscode,
					'responsetext_ovo' => $this->cek_response_code($rscode)
				), REST_Controller::HTTP_NOT_FOUND);
				
			
			exit;
		  }
		}
		catch(HTTP_Request2_Exception $e) {
			$msg	= 'Error: ' . $e->getMessage();
			$response->getReasonPhrase();
			
			$response_code	= json_decode($response->getBody());
			$rscode			= $response_code->responseCode;
			$this->save_log($this->data_post,$msg,$response->getStatus(),'PUSH P2P',$this->cek_response_code($rscode));
			
			$this->response(array(
					'status' => $response->getStatus(),
					'message' => $msg
				), REST_Controller::HTTP_NOT_FOUND);
			
			
			exit;
		}	
	}
	
	public function getDetailPayment_post($local = 0){	
		$type				=	"0100";
		$processingCode		=	"040000";
		require_once APPPATH.'../HTTP_Request/HTTP/Request2.php';
		$request = new HTTP_Request2();
		$request->setUrl($this->url);
		$request->setMethod(HTTP_Request2::METHOD_POST);
		$request->setConfig(array(
		  'follow_redirects' => TRUE
		));
		$request->setHeader(array(
		  'Content-Type' => $this->ctype,
		  'app-id' => $this->app_id,
		  'random' => $this->random,
		  'hmac' => $this->hmac
		));
		
		$arr_data					= json_decode($this->data_post);
		$arr_data->type				= $type;
		$arr_data->processingCode	= $processingCode;
		$arr_data->tid				= $this->tid;
		$arr_data->mid				= $this->mid;			
		$arr_data->merchantId		= $this->merchantId;	
		$arr_data->storeCode		= $this->storeCode;	
		$arr_data->appSource		= $this->appSource;	
		
		$request->setBody(json_encode($arr_data));		
		try {
		  $response = $request->send();
		  if (($response->getStatus() == 200) or ($response->getStatus() == 422)) {
				
				$response_code	= json_decode($response->getBody());
				$rscode			= $response_code->responseCode;
				
				$this->save_log($this->data_post,$response->getBody(),$response->getStatus(),'Check Payment',$this->cek_response_code($rscode));
				
				
				if($local == 1){
					return $response->getBody();
				}else{
					$msg_ovo		= $response->getBody();
					$response_code	= json_decode($response->getBody());
					$rscode			= $response_code->responseCode;
					
					$this->response(array(
						'status' => $response->getStatus(),
						'message_ovo' => $msg_ovo,
						'responsecode_ovo' => $rscode,
						'responsetext_ovo' => $this->cek_response_code($rscode)
					), REST_Controller::HTTP_OK);
												
					$this->set_response($checkpayment, REST_Controller::HTTP_OK);
				}
				
		  }
		  else {
				$response_code	= json_decode($response->getBody());
				$rscode			= $response_code->responseCode;
				
			  
				$this->save_log($this->data_post,$response->getReasonPhrase(),$response->getStatus(),'Check Payment',$this->cek_response_code($rscode));	
				
				$this->response(array(
					'status' => $response->getStatus(),
					'message' => 'Unexpected HTTP status: ' . $response->getStatus() . ' ' .$response->getReasonPhrase()
				), REST_Controller::HTTP_NOT_FOUND);
		  }
		}
		catch(HTTP_Request2_Exception $e) {
			$response_code	= json_decode($response->getBody());
			$rscode			= $response_code->responseCode;
			
		  	$this->save_log($this->data_post,$e->getMessage(),$response->getStatus(),'Check Payment',$this->cek_response_code($rscode));
			$this->response(array(
			
				'status' => $response->getStatus(),
				'message' => $e->getMessage()
			), REST_Controller::HTTP_NOT_FOUND);
		}

	}
	
	public function pushVoidTransaction_post(){
		$getdata	= json_decode($this->getDetailPayment_post(1));
		
		//print_r($getdata);
		
		//exit;
		
		$type				=	"0200";
		$processingCode		=	"020040";
		$arr_data					= json_decode($this->data_post);
		$arr_data->type				= $type;
		$arr_data->processingCode	= $processingCode;
		$arr_data->tid				= $this->tid;
		$arr_data->mid				= $this->mid;			
		$arr_data->merchantId		= $this->merchantId;	
		$arr_data->storeCode		= $this->storeCode;	
		$arr_data->appSource		= $this->appSource;
		
		if($getdata->responseCode == 00){
			
			require_once APPPATH.'../HTTP_Request/HTTP/Request2.php';
			$request = new HTTP_Request2();
			$request->setUrl($this->url);
			$request->setMethod(HTTP_Request2::METHOD_POST);
			$request->setConfig(array(
			  'follow_redirects' => TRUE
			));
			$request->setHeader(array(
			  'Content-Type' => $this->ctype,
			  'app-id' => $this->app_id,
			  'random' => $this->random,
			  'hmac' => $this->hmac
			));
			$request->setBody(json_encode($arr_data));
			try {
			  $response = $request->send();
			  //print_r($response->getStatus());
			  if (($response->getStatus() == 200)) {
				
				$response_code	= json_decode($response->getBody());
				$rscode			= $response_code->responseCode;
				$this->save_log($this->data_post,$response->getBody(),$response->getStatus(),'Void Payment',$this->cek_response_code($rscode));				
				$result =  $response->getBody();
			  }
			  else {
				  
				$response_code	= json_decode($response->getBody());
				$rscode			= $response_code->responseCode;				
				$this->save_log($this->data_post,$response->getBody(),$response->getStatus(),'Void Payment',$this->cek_response_code($rscode));
				$result =  'Unexpected HTTP status: ' . $response->getStatus() . ' ' .
				$response->getReasonPhrase();
			  }
			}
			catch(HTTP_Request2_Exception $e) {
				
				$response_code	= json_decode($response->getBody());
				$rscode			= $response_code->responseCode;			
				$this->save_log($this->data_post,$e->getMessage(),$response->getStatus(),'Void Payment',$this->cek_response_code($rscode));
				$result	= 'Error: ' . $e->getMessage();
			}
			$voidstatus	= 	array(
								'status' => $response->getStatus(),
								'message' => $result
							);										
			$this->set_response($voidstatus, REST_Controller::HTTP_OK);
		}	
		else{
			
			$response_code	= json_decode($response->getBody());
			$rscode			= $response_code->responseCode;			
			$this->save_log($this->data_post,$getdata,$response->getStatus(),'Void Payment',$this->cek_response_code($rscode));
			
			$this->response(array(
					'status' => $response->getStatus(),
					'message' => $getdata
				), REST_Controller::HTTP_NOT_FOUND);
			exit;
		}
	}
	private function save_log($json_sender,$json_response,$response,$transaction,$ovo_response_code){
		$sql_log	= "insert into log_ovo (`json_sender`,`json_response`,`transdate`,`response`,`transaction`,`ovo_response_code`)
					values ('".$json_sender."','".$json_response."','".date("Y-m-d H:i:s")."','".$response."','".$transaction."','".$ovo_response_code."')";//echo $sql_log;exit;
		$this->db->query($sql_log);
	}
	private function cek_response_code($id){
		$idresponse['00'] = "Approved Success :: Approved Transaction";
		$idresponse['14'] = "Invalid Mobile Number / OVO ID :: Phone number / OVO ID not found in OVO System";
		$idresponse['17'] = "Transaction Decline OVO :: User canceled payment using OVO Apps";
		$idresponse['25'] = "Transaction Not Found :: Payment status not found when called by Check Payment Status API";
		$idresponse['26'] = "Transaction Failed :: Failed push payment confirmation to OVO Apps";
		$idresponse['40'] = "Transaction Failed :: Failed Push Payment, Error request: Merchant invoice, batch number & reference number already used from previous transactions";
		$idresponse['54'] = "Transaction Expired (More than 7 days) :: Transaction details already expired when API check payment status called";
		$idresponse['56'] = "Card Blocked :: Please call 1500696 Card is blocked, unable to process card transaction";
		$idresponse['64'] = "Card Blocked :: Please call 1500696 Card is blocked, unable to process card transaction";
		$idresponse['58'] = "Transaction Not Allowed :: Transaction module not registered in OVO Systems";
		$idresponse['61'] = "Exceed Transaction Limit :: Amount / count exceed limit, set by user";
		$idresponse['65'] = "Exceed Transaction Limit :: Amount / count exceed limit, set by user";
		$idresponse['63'] = "Security Violation :: Authentication Failed";
		$idresponse['67'] = "Below Transaction Limit :: The transaction amount is less than the minimum payment";
		$idresponse['68'] = "Transaction Pending / Timeout :: OVO Wallet late to give respond to OVO JPOS";
		$idresponse['73'] = "Transaction has been reversed :: Transaction has been reversed by API Reversal Push to Pay in Check Payment Status API";
		$idresponse['96'] = "Invalid Processing Code  :: Invalid Processing Code inputted during Call API Check Payment Status";
		$idresponse['ER'] = "System Failure  :: There is an error in OVO Systems, Credentials not found in OVO Systems ";
		$idresponse['EB'] = "Terminal Blocked  :: TID and/or MID not registered in OVO Systems";
	
		return $idresponse[$id];
	}
}
