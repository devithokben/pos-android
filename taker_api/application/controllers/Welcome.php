<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->view('welcome_message');
	}
	public function login(){
		$username	= $this->input->post("userid");
		$password 	= $this->encrypt($this->input->post("password"),10);
		
		$sql		= "select * from user where user_id = '".$username."' and password = '".$password."'";
		$getUser	= $this->db->query($sql);
	//print_r($getUser->num_rows());exit;
		if($getUser->num_rows()){
			$datausr = $getUser->row();
			print_r($datausr);
			$this->session->set_userdata('id',$datausr->id);
			$this->session->set_userdata('user',$datausr->user_id);
			
			//print_r($this->session->all_userdata());
			//exit;
			redirect("home");
		}else {
			$this->session->set_flashdata('item',' <div class="alert alert-danger" role="alert">Username and Password Not Match</div>');
			redirect(site_url());
		}
		//echo $sql;
	}
	public function encrypt($string, $key=10) {
		$result = '';
		for($i=0, $k= strlen($string); $i<$k; $i++) {
			$char = substr($string, $i, 1);
			$keychar = substr($key, ($i % strlen($key))-1, 1);
			$char = chr(ord($char)+ord($keychar));
			$result .= $char;
		}
		return base64_encode($result);
	}
	public function create_password($user,$password) {
		$result = '';
		for($i=0, $k= strlen($password); $i<$k; $i++) {
			$char = substr($password, $i, 1);
			$keychar = substr(10, ($i % strlen(10))-1, 1);
			$char = chr(ord($char)+ord($keychar));
			$result .= $char;
		}
		//$this->db->query("insert into user (user_id,password) values ('".$user."','".base64_encode($result)."')");
		echo  "Username : ".$user."==Password : ".base64_encode($result);
	}
	public function decrypt($string, $key=10) {
		$result = '';
		$string = base64_decode($string);
		for($i=0,$k=strlen($string); $i< $k ; $i++) {
			$char = substr($string, $i, 1);
			$keychar = substr($key, ($i % strlen($key))-1, 1);
			$char = chr(ord($char)-ord($keychar));
			$result.=$char;
		}
		return $result;
	}
	public function logout(){
		$this->session->sess_destroy();
		redirect("welcome");
	}
}
