<?php

date_default_timezone_set("Asia/Bangkok");
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . 'libraries/REST_Controller.php';
class configuration extends CI_Controller {	
	
	public function __construct(){
		parent::__construct();
		
		if(!$this->session->userdata('id')){
			redirect('welcome');
		}
		$this->staging_db		= $this->load->database('staging_db',TRUE);
	}
	public function documentation(){
		$qry_gethistory				=	"select * from has_list_api where type = 4 order by name asc";
		$data['list_category']		= 	$this->db->query($qry_gethistory)->result_array();
		$data['ovo']					=   'class="active"';
		$data['ovo_doc']				=   'class="active"';		
		$this->load->view('configuration_doc',$data);
	}
	public function setting(){
		$qry_getkey					=	$this->db->query("select `key` as keyval from `keys` where user_id = '".$this->session->userdata('id')."'")->row();
		$qry_gethistory				=	"select * from has_list_api where type = 4 order by name asc";
		$data['list_category']		= 	$this->db->query($qry_gethistory)->result_array();
		
		$det_qry_gethistory				=	"select * from has_category order by name asc";
		$data['det_list_category']		= 	$this->db->query($det_qry_gethistory)->result_array();
		
		
		$lst_user						=	"select *, case when type = '0' then 'staging' else 'live' end as type_name from user order by id asc";
		$lst_user						= 	$this->db->query($lst_user)->result_array();
		$i = 0;
		foreach($lst_user as $val_u){
			$data['lst_user'][$i]['user_id']		= 	$val_u['user_id'];
			$data['lst_user'][$i]['password']		= 	$val_u['password'];
			$data['lst_user'][$i]['realpass']		= 	$this->decrypt($val_u['password']);
			$data['lst_user'][$i]['hash_key']		= 	$val_u['hash_key'];
			$data['lst_user'][$i]['companyname']	= 	$val_u['companyname'];
			$data['lst_user'][$i]['companydetail']	= 	$val_u['companydetail'];
			$data['lst_user'][$i]['type_name']		= 	$val_u['type_name'];
			$i++;
		}
		
		$data['ovo']					=   'class="active"';
		$data['ovo_testing']			=   'class="active"';
		$data['valueheader']			=   $qry_getkey->keyval;
		$row							= 	$this->staging_db->query("select TOP 1 * from trx_ctl order by trx_date desc",$this->mssql_handle)->row();
		//print_r($row);
		$data['dateNow']				= 	$row->TRX_DATE;
		$this->load->view('form_setting',$data);
	}
	public function proc_data(){
				
		$ins_user['user_id']		= $_POST['name'];
		$ins_user['password']		= $this->encrypt($_POST['password']);
		$ins_user['companyname']	= $_POST['Companyname'];
		$ins_user['companydetail']	= $_POST['companydetail'];
		$ins_user['ent_date']		= date("Y-m-d H:i:s");
		$ins_user['active']			= $_POST['apioption'];
		$ins_user['type']			= $_POST['type'];
		$ins_user['hash_key']		= sha1($_POST['name']);
		
		
		if(!$this->db->insert('user',$ins_user)){
			redirect("home");
		}
		
		$last_id = $this->db->insert_id();
		//$loop	 = 0;
		for ($loop = 0; $loop <= count($_POST['apiaccess']) -  1; $loop++){
			$ins_previlage['user_id']		= $last_id;
			$ins_previlage['category_id']	= $_POST['apiaccess'][$loop];
			
			$this->db->insert('user_previlage_category',$ins_previlage);
			
			$get_controller	= $this->db->query("select * from has_category where id = '".$_POST['apiaccess'][$loop]."'")->row();
			
			if($_POST['type'] == 0 ){
				if($get_controller->controller_staging){
					$ins_access['key']			= sha1($_POST['name']);
					foreach(explode(",",$get_controller->controller_staging) as $ctrl){
						$ins_access['controller']	= $ctrl;
						
						$this->db->insert('access',$ins_access);
					}
				}
			}else{
				if($get_controller->controller_live){
					$ins_access['key']			= sha1($_POST['name']);
					foreach(explode(",",$get_controller->controller_live) as $ctrl){
						$ins_access['controller']	= $ctrl;
						$this->db->insert('access',$ins_access);
					}
				}
			}
			
			
		}
		
		redirect("home");
	}
	
	public function proc_startofday(){
		$this->staging_db->query("update trx_ctl set trx_date = '".date("Y-m-d")."' where TRX_ID = '4220'",$this->mssql_handle);
		//$row	= $this->staging_db->query("select TOP 1 * from trx_ctl order by trx_date desc",$this->mssql_handle)->row();
		//print_r($row);
		redirect("configuration/setting");
	}
	
	public function encrypt($string, $key=10) {
		$result = '';
		for($i=0, $k= strlen($string); $i<$k; $i++) {
			$char = substr($string, $i, 1);
			$keychar = substr($key, ($i % strlen($key))-1, 1);
			$char = chr(ord($char)+ord($keychar));
			$result .= $char;
		}
		return base64_encode($result);
	}
	public function decrypt($string, $key=10) {
		$result = '';
		$string = base64_decode($string);
		for($i=0,$k=strlen($string); $i< $k ; $i++) {
			$char = substr($string, $i, 1);
			$keychar = substr($key, ($i % strlen($key))-1, 1);
			$char = chr(ord($char)-ord($keychar));
			$result.=$char;
		}
		return $result;
	}
	public function save_log_transaction($json_post,$device,$entry_date,$response,$response_code,$usr_id){		
		$sql	= "Insert into log_sales_osds (json_post,device,entry_date,response,response_code,usr_id)
					values ( '".$json_post."','".$device."','".$entry_date."','".$response."','".$response_code."','".$usr_id."')";
		//echo $sql;exit;
		if($this->db->query($sql)){
			return  0;
		}else{
			return 1;
		}
	}
}
