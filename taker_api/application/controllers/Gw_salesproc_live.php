<?php
date_default_timezone_set("Asia/Jakarta");
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . 'libraries/REST_Controller.php';
class gw_salesproc_live extends REST_Controller {	
	
	public function __construct(){
		parent::__construct();		
		$this->ctype		= 'application/json';
		$this->data_post	= file_get_contents('php://input');
		$this->staging_db	= $this->load->database('default',TRUE);
		if(!$this->staging_db){
			$msg["responsecode"] = 401;
			$msg["message"]		 = "Cannot Connect DB";
			return $this->response(json_encode($msg),401);
			exit;
		}
	}
	public function pushTransaction_post(){

		$pushdata	= $this->push_datasales();
		$response 	= json_decode($pushdata, TRUE);			
		$res_server	= json_decode($response['message'], TRUE);	
		if($res_server['statusCode']){
			$status		= $res_server['statusCode'];
			$message	= $res_server['message'];
		}else{
			$status 	= $response['status'];
			$message	= $response['message'];
		}
				
		//$savelog	= $this->save_log_transaction($this->data_post,'staging',date("Y-m-d H:i:s"),$message,$status,$usr_id->user_id);		
		/*
		if($savelog == 0){
			$server['status_server']	= $status;
			$server['msg_server']		= $message;
			$server['save_log']			= "Sukses";
		}else{
			$server['status_server']	= $status;
			$server['msg_server']		= $message;
			$server['save_log']			=  "gagal";
		}
		echo json_encode($server, TRUE);*/
		exit;
	}
	public function push_datasales()
	{
		$data			= json_decode(str_replace("\u0027"," ",$this->data_post));
		
		$this->clearTransaction($data->ID);
		$this->save_database($data);
		
		
	}
	public function clearTransaction($trx_id){
		$this->db->query("DELETE from sales_hdr where ID = '".$trx_id."'");
		$this->db->query("DELETE from sales_dtl where ID = '".$trx_id."'");
		$this->db->query("DELETE from sales_payment where ID = '".$trx_id."'");
	}
	private function save_database($data){
		
		//print_r($data);
		//exit;
		//exit;
		//$newCounter		= 0;
		
		$insert_hdr['ID']					= $data->ID;
		$insert_hdr['RESET_NO']				= $data->RESET_NO;
		$insert_hdr['COUNTER_NO']			= $data->COUNTER_NO;
		$insert_hdr['TRX_ID']				= $data->TRX_ID;
		$insert_hdr['TRX_DATE']				= $data->TRX_DATE;
		$insert_hdr['BRANCH_ID']			= $data->BRANCH_ID;
		$insert_hdr['SESSION_ID']			= $data->SESSION_ID;
		$insert_hdr['POS_ID']				= $data->POS_ID;
		$insert_hdr['ENT_DATE']				= $data->ENT_DATE;
		$insert_hdr['ENT_USER']				= $data->ENT_USER;
		$insert_hdr['HREF']					= $data->HREF;
		//$insert_hdr['TAX_RATE']				= $data->TAX_RATE;
		$insert_hdr['TAX_RATE']				= 10;
		$insert_hdr['GUEST']				= $data->GUEST;
		$insert_hdr['STATUS']				= $data->STATUS;
		$insert_hdr['VALID_STATUS']			= $data->VALID_STATUS;
		$insert_hdr['VALID_DATE']			= $data->VALID_DATE;
		$insert_hdr['PRN_REV_NO']			= 0;//$data->PRN_REV_NO;
		$insert_hdr['CLIENT_ID']			= $data->CLIENT_ID;
		$insert_hdr['TRX_STATUS']			= $data->TRX_STATUS;
		$insert_hdr['KD_PEMAKAI']			= $data->KD_PEMAKAI;
		$insert_hdr['ORDER_NO']				= '';
		//$insert_hdr['SVC_CHANGE_RATE']		= 0;
		$insert_hdr['DELIVERY_NO']			= $data->DELIVERY_NO;//$data->DELIVERY_ID_MASK;
		;
		
		try {	
			if($this->staging_db->insert('sales_hdr',$insert_hdr)){
				//echo $this->staging_db->last_query();
				$gross				= 0;
				$item_discount		= 0;
				$newCounter			= $data->COUNTER_NO;
				$status_disc_item 	= 0; 
				foreach ($data->detail as $key=>$val)
				{
					//<id>|<prodname>|<qty>|<seq_no>|<price>|<type>|<disc>|<promoid>|<promodesc>|<disctype>|<discvalue>|<prodcode>
					//11501.1.2|NASI ADD ON(ALA CARTE)|1|1|5455|DI|0|7|KARYAWAN ( 10% )|1|10
					//"41101.1.11|NEW BENTO SPC 1|1|2|50000|TA|110|||||802089"
					$value				= explode("|",$val);
					//$price_before_tax	= ceil($value[1] / 1.1);
					
					$insert_dtl['SEQ_NO']		= $value[3];
					$insert_dtl['ID']			= $value[0];
					$insert_dtl['PRODCODE']		= $value[11];
					$insert_dtl['PRODMAIN']		= 1;
					$insert_dtl['QTY']			= $value[2];
					$insert_dtl['PRICE']		= $value[4];
					$insert_dtl['DISC_ITEM']	= $value[10];
					$insert_dtl['AMOUNT']		= round($value[4])-round(($value[4]*$value[10])/100);
					$insert_dtl['ITEM_TYPE']	= $value[5];
					$insert_dtl['GROSS']		= $value[4] * $value[2];
					// $insert_dtl['DISC_AMOUNT']	= round((($value[4] * $value[2]) * $value[10])/100);
					$insert_dtl['DISC_AMOUNT']	= round($value[12]);
					$insert_dtl['ID_INDEX']		= $value[3];
					$insert_dtl['PRODNAME']		= '';
					$insert_dtl['PARENTID']		= '';
					$insert_dtl['LINEDESC']		= '';
					if($insert_dtl['DISC_ITEM'] != 0){
						if($data->PROMOID){
							$qryName	= $this->db->query("select * from tbl_promoh where PromoId = '".$data->PROMOID."'")->row();
							if($qryName){
								$insert_dtl['PROMOID']				= $data->PROMOID;
								$insert_dtl['PROMODESC']			= $qryName->PromoName;
							}
						}else{
							$insert_dtl['PROMOID']		= $value[7];
							$insert_dtl['PROMODESC']	= $value[8];
						}
					}else{
						$insert_dtl['PROMOID']		= $value[7];
						$insert_dtl['PROMODESC']	= $value[8];
					}
					$status_disc_item 			= $insert_dtl['PROMOID'];
					$insert_dtl['DISCREASON']	= 0;
					$insert_dtl['DISCTYPE']		= '';
					// $insert_dtl['DISCVALUE']	= ((int)$value[4] * (int)$value[2]) * ($value[10]/100);
					$insert_dtl['DISCVALUE']	= round($value[12]);
					$insert_dtl['KET']			= '';
					$insert_dtl['DISCYES']		= 0;
					$insert_dtl['TRANSRC']		= '';
					
					//print_r($insert_dtl);
					
					$gross			= $insert_dtl['GROSS'] + $gross;
					$item_discount	= $insert_dtl['DISC_AMOUNT'] + $item_discount;
					/*
					echo ((int)$value[4] * (int)$value[2])."*".($value[10]/100)."<hr />";
					echo $insert_dtl['DISC_AMOUNT'] .'+'. $item_discount."<hr />";*/
					
				
					try {
						if(!$this->staging_db->insert('sales_dtl',$insert_dtl)){
							//echo "<hr />";
							//echo $this->staging_db->last_query();
							$data_error			 = sqlsrv_errors();	
							$data_error['loc']	 = "Detail Transaction";
							$msg["responsecode"] = 35;
							$msg["message"]		 = $data_error;
							return $this->response(json_encode($msg),401);
							exit;
						}else{
							//echo "<hr />";
							//echo $this->staging_db->last_query();
						}
					}catch (Exception $e) {
						$msg["responsecode"] = 400;
						$e->loc				 = "Detail Transaction";
						$msg["message"]		 = $e->getMessage();
						return $this->response(json_encode($msg),401);
						exit;
					
					}
				} 
				//exit;
				///insert payment
				$grosstotal = $data->GROSS - round($item_discount);
				$insert_payment['ID']					= $data->ID;
				//$insert_payment['GROSS_SALES']			= $gross;
				$insert_payment['GROSS_SALES']			= $data->GROSS;
				$insert_payment['GROSS']				= $data->GROSS - round($item_discount);
				$insert_payment['TOTAL_COST']			= $data->TOTAL_COST;
				$insert_payment['TAX']					= $data->TAX;
				
				$insert_payment['SUB_TOTAL']			= $data->SUB_TOTAL;
				
				$insert_payment['NET']					= $data->GRANTOTAL;
				
				$insert_payment['NON_CASH']				= $data->NON_CASH;
				$insert_payment['ROUNDING']				= $data->PEMBULATAN;
				
				$insert_payment['REFUND_VCH']			= 0;
				$insert_payment['ITEM_DISCOUNT']		= round($item_discount);
				$insert_payment['SALES_DISCOUNT']		= $data->SALES_DISCOUNT;
				$insert_payment['PROMO_DISCOUNT']		= $item_discount;
				
				
				if($data->dp_trx_id){
					$insert_payment['ID_DP']				= $data->dp_trx_id;
					$insert_payment['DP']					= ceil($data->dp_amount_val);
					$insert_payment['NET_DP']				= ceil($data->dp_amount_val * 1.1);
				}
				
				if($data->EDC_VDR == 0){
					$insert_payment['CASH']					= $data->TUNAI;	
					$insert_payment['REFUND']				= $data->KEMBALI;
				}else if($data->EDC_VDR == 1){
					$insert_payment['CASH']					= $data->TUNAI;	
					$insert_payment['FBCA']					= $data->TUNAI;
					$insert_payment['CARD_AMT']				= $data->TUNAI;
				}else{
					$insert_payment['CASH']					= $data->TUNAI;	
					$insert_payment['CARD_AMT']				= $data->TUNAI; 
				}
				
				
				$insert_payment['CARD_NO']				= $data->REFERENSI_ID_PAY;
				$insert_payment['EDC_VDR']				= $data->EDC_VDR;				
				$insert_payment['NOTE']					= $data->NIK_KAR.'#';
				if($data->PROMOID && (empty($insert_dtl['PROMOID']))){
							
					$qryName	= $this->db->query("select * from tbl_promoh where PromoId = '".$data->PROMOID."'")->row();
					if($qryName){
						$insert_payment['PROMOID']				= $data->PROMOID;
						$insert_payment['PROMODESC']			= $qryName->PromoName;						
					}else{
						$insert_payment['PROMOID']				= $data->PROMOID;
						$insert_payment['PROMODESC']			= $data->PROMOVAL;
					}
				}
				
				$insert_payment['DISCREASON']			= '';
				$insert_payment['DISCTYPE']				= $data->DISCTYPE;
				$insert_payment['DISCVALUE']			= str_replace(",","",$data->DISCVALUE);
				$insert_payment['ITEM_TYPE']			= '';
				$insert_payment['DISCMAX']				= '';
				$insert_payment['GROSS_SALES_DISC']		= '';
				$insert_payment['TRANSRC']				= '';
				$insert_payment['SVCTYPE']				= $data->SVCTYPE;
				$insert_payment['SVCVALUE']				= $data->SVCVALUE;
				$insert_payment['TACHARGE']				= str_replace(",","",$data->TA_CHARGE);
				
				
				
				
				try {
					if(!$this->staging_db->insert('sales_payment',$insert_payment)){
						//echo "<hr />";
						echo $this->staging_db->last_query();
						$data_error			 = "error";	
						$data_error['loc']	 = "Payment Transaction";
						$msg["responsecode"] = 35;
						$msg["message"]		 = $data_error;
						return $this->response(json_encode($msg),401);
						exit;
					}else{
						
						//update trx_ctl
						if($data->RELEASE_HOLD == 0)
						{
							$dataUpd['COUNTER_NO']		=	$newCounter; 
							$whereUpd['TRX_DATE']		=	$data->TRX_DATE; 
							$whereUpd['POS_ID']			=	$data->POS_ID;
							//$whereUpd['OPENING_USER']	=	$data->ENT_USER;
							$this->staging_db->where($whereUpd);
							$this->staging_db->update('trx_ctl',$dataUpd);
						}
						
						
						//inserDp
						if(($data->dp_amount_val > 0) && $data->dp_trx_id == ''){
							$insert_dp['ID'] 		= $data->ID;
							$insert_dp['PRODCODE'] 	= $data->dp_prodcode_val;
							$insert_dp['AMOUNT'] 	= $data->dp_amount_val;
							$insert_dp['NET'] 		= ($data->dp_amount_val * 0.1) + $data->dp_amount_val;
							$insert_dp['CUST_ULT'] 	= $data->dp_name_val;
							$insert_dp['STATUS'] 	= 0;
							$insert_dp['DATE_ULT'] 	= $data->dp_date_val;
							$insert_dp['AMT_DP_AWAL'] 	= $data->dp_amount_val;
							$insert_dp['NET_DP_AWAL'] 	= ($data->dp_amount_val * 0.1) + $data->dp_amount_val;
							
							try {
								if(!$this->staging_db->insert('sales_dp',$insert_dp)){
									//echo "<hr />";
									//echo $this->staging_db->last_query();
									$data_error			 = sqlsrv_errors();	
									$data_error['loc']	 = "DP Transaction";
									$msg["responsecode"] = 35;
									$msg["message"]		 = $data_error;
									return $this->response(json_encode($msg),401);
									exit;
								}else{
									//echo "<hr />";
									//echo $this->staging_db->last_query();
								}
							}catch (Exception $e) {
								$msg["responsecode"] = 400;
								$e->loc				 = "Detail Transaction";
								$msg["message"]		 = $e->getMessage();
								return $this->response(json_encode($msg),401);
								exit;
							
							}
						}else{	
							$dataUpdDp['STATUS']		=	1;
							$dataUpdDp['DIPAKAI']		=	1;
							$dataUpdDp['TGLPAKAI']	=	date("Y-m-d h:i:s");
							$dataUpdDp['USR_PAKAI']	=	$data->ID;
							$dataUpdDp['TRX_PAKAI']	=	$data->TRX_DATE;
							
							$whereUpdDp['ID']				=	$data->dp_trx_id; 
							//$whereUpd['OPENING_USER']	=	$data->ENT_USER;
							$this->staging_db->where($whereUpdDp);
							$this->staging_db->update('sales_dp',$dataUpdDp);
						}
						
						
						
						$msg["responsecode"] = 20;
						$msg["message"]		 = 'Transaction Success';
						return $this->response($msg,200);
						//echo $this->staging_db->last_query();
						//exit;
						//echo "<hr />";
						//echo $this->staging_db->last_query();
					}
				}catch (Exception $e) {
					$msg["responsecode"] = 400;
					$e->loc				 = "Payment Transaction";
					$msg["message"]		 = $e->getMessage();
					return $this->response(json_encode($msg),401);
					exit;
				
				}
			}else{
				
				$data_error			 = sqlsrv_errors();	
				$msg["responsecode"] = 35;
				$data_error['loc']	 = "Table Header Transaction";	
				$msg["message"]		 = $data_error;
				//$savelog	= $this->save_log_transaction($this->data_post,'staging',date("Y-m-d H:i:s"),json_encode($msg),'401',$this->user_id);
				return $this->response(json_encode($msg),401);
				exit;
			}
		}catch (Exception $e) {
			$msg["responsecode"] = 400;
			$e->loc				 = "Table Header Transaction";
			$msg["message"]		 = $e->getMessage();
			//$savelog	= $this->save_log_transaction($this->data_post,'staging',date("Y-m-d H:i:s"),json_encode($msg),'401',$this->user_id);
			return $this->response(json_encode($msg),401);
			exit;		
		}	
	}
	
}