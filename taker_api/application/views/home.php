<?php $this->load->view('header') ?>
<section class="wrapper">
	<div class="row">
		<div class="col-lg-12">
			<div class="row mt">
			<!-- SERVER STATUS PANELS -->
				<?php 
					$i = 1;
					foreach ($recentactivity as $apilist){
						if( $apilist['name'] == 'Setting'){
				?>
							<div class="col-lg-12 col-md-12 col-sm-12 mb">
								<div class="green-panel">
									<div class="green-header profile-01 centered">
										<p><a style="color:#fff;" href="<?php echo site_url()?>/<?php echo $apilist['slug_url']?>/documentation">Click Here To Access Setting Page</a></p>
									</div>
								</div>
							</div>
					
						<?php
						}else{
						?>
							
							
							<div class="col-lg-4 col-md-4 col-sm-4 mb">
						<!-- WHITE PANEL - TOP USER -->
						
						<div class="message-p pn">
						  <div class="message-header">
							<h5><?php echo $apilist['name']?></h5>
							<?php echo $apilist['description']?>
						  </div>
						  <div class="row">
							<div class="col-md-12">
								<?php
									$rowres		= array();
									$rowcount	= array();
									$data_log	= $this->db->query("select response,count(response) as res_count from vw_log_transaction where response REGEXP '^[0-9]+$' and api_group = '".$apilist['api_group']."' group by response order by response")->result_array();
									foreach($data_log as $data_lst){
										$rowres[]	= $data_lst['response'];
										$rowcount[]	= $data_lst['res_count'];
									}
									
								?>	
								<canvas id="myChart<?php echo $i ?>" width="400" height="300"></canvas>								
								<script>
									var i			= "<?php echo $i ?>";
									var ctx 		= document.getElementById('myChart'+i);
									var datalabel	= <?php echo json_encode($rowres) ?>;
									var dataval 	= <?php echo json_encode($rowcount) ?>;

									var myChart = new Chart(ctx, {
										type: 'bar',
										data: {
											//labels: ['Red', 'Blue', 'Yellow', 'Green', 'Purple', 'Orange'],
											labels: datalabel,
											datasets: [{
												data: dataval,
												backgroundColor: [
													'rgba(255, 99, 132, 0.2)',
													'rgba(54, 162, 235, 0.2)',
													'rgba(255, 206, 86, 0.2)',
													'rgba(75, 192, 192, 0.2)',
													'rgba(153, 102, 255, 0.2)',
													'rgba(255, 159, 64, 0.2)'
												],
												borderColor: [
													'rgba(255, 99, 132, 1)',
													'rgba(54, 162, 235, 1)',
													'rgba(255, 206, 86, 1)',
													'rgba(75, 192, 192, 1)',
													'rgba(153, 102, 255, 1)',
													'rgba(255, 159, 64, 1)'
												],
												borderWidth: 1
											}]
										},
										options: {
											scales: {
												yAxes: [{
													ticks: {
														beginAtZero: true
													}
												}]
											},
											legend: {
												display: false
											},
											tooltips: {
												callbacks: {
												   label: function(tooltipItem) {
														  return "total hits : " + tooltipItem.yLabel;
												   }
												}
											}
										}
									});
								</script>
								
							</div>
						  </div>
						</div>
						
						
						<div class="green-panel">
						    <div class="green-header profile-01 centered">
								<p><a style="color:#fff;" href="<?php echo site_url()?>/<?php echo $apilist['slug_url']?>/documentation">Click Here To Show API Detail</a></p>
							</div>
						</div>
					</div>
							
						
						<?php
						}
						?>
				<?php
						$i++;
					}
				?>
			</div>
		</div>
	</div>
</section>
<?php $this->load->view('footer') ?>