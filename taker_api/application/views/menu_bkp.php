 <aside>
      <div id="sidebar" class="nav-collapse ">
        <!-- sidebar menu start-->
        <ul class="sidebar-menu" id="nav-accordion">
			<li class="mt">
				<a href="<?php echo site_url()?>/ovo" <?php echo $dashboard ?>>
					<i class="fa fa-dashboard"></i>
					<span>Dashboard</span>
				</a>
			</li>
			<li class="sub-menu">
				<a href="javascript:;" <?php echo $ovo ?>>
					<i class="fa fa-file"></i>
					<span>OVO API Management</span>
				</a>
				<ul class="sub">
					<li <?php echo $ovo_doc ?> ><a href="<?php echo site_url()?>/ovo/documentation">API Documentation</a></li>
					<li <?php echo $ovo_testing ?>> <a href="<?php echo site_url()?>/ovo/testing">Test transaction</a></li>
					<li <?php echo $ovo_logtransaction ?> ><a  href="<?php echo site_url()?>/ovo/logTransaction">Log Transaction</a></li>
				</ul>
			</li>
			<li class="sub-menu">
				<a href="javascript:;" >
					<i class="fa fa-file"></i>
					<span>DOKU API Management</span>
				</a>
				<ul class="sub">
					<li><a href="#">API Documentation</a></li>
					<li><a href="#">Test transaction</a></li>
					<li  ><a  href="<?php echo site_url()?>/ovo/logTransaction">Log Transaction</a></li>
				</ul>
			</li>
			<li class="sub-menu">
				<a href="javascript:;" >
					<i class="fa fa-file"></i>
					<span>Midstrans API Management</span>
				</a>
				<ul class="sub">
					<li><a href="#">API Documentation</a></li>
					<li><a href="#">Test transaction</a></li>
					<li ><a  href="<?php echo site_url()?>/ovo/logTransaction">Log Transaction</a></li>
				</ul>
			</li>
			<li class="sub-menu">
				<a href="javascript:;" >
					<i class="fa fa-file"></i>
					<span>Virtual Account API </span>
				</a>
				<ul class="sub">
					<li><a href="#">API Documentation</a></li>
					<li><a href="#">Test transaction</a></li>
					<li><a  href="<?php echo site_url()?>/ovo/logTransaction">Log Transaction</a></li>
				</ul>
			</li>
			<li class="sub-menu">
				<a href="javascript:;" >
					<i class="fa fa-file"></i>
					<span>Host 2 Host BCA</span>
				</a>
				<ul class="sub">
					<li><a href="#">API Documentation</a></li>
					<li><a href="#">Test transaction</a></li>
					<li ><a  href="<?php echo site_url()?>/ovo/logTransaction">Log Transaction</a></li>
				</ul>
			</li>
			
		</ul>
        <!-- sidebar menu end-->
      </div>
    </aside>
    <!--sidebar end-->