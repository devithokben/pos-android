<?php $this->load->view('header') ?>
<?php $this->load->view('menu_setting') ?>
    <section id="main-content">
      <section class="wrapper site-min-height">
        <div class="row mt">
          <div class="col-lg-12">
            <div class="row content-panel">
			  <div class="panel-heading">
				<ul class="nav nav-tabs nav-justified">
				  <li class="active">
					<a data-toggle="tab" href="#pushtransaction">Change Log API</a>
				  </li>
				  <li>
					<a data-toggle="tab" href="#responsecode">How To Access</a>
				  </li>
				</ul>
			  </div>
			  <!-- /panel-heading -->
			  <div class="panel-body">
				<div class="tab-content">
					<div id="pushtransaction" class="tab-pane active">
						<div class="row mt mb">								  
							<div class="col-md-12">
								<section class="task-panel tasks-widget">
								  <div class="panel-heading">
									<div class="pull-left">
									  <h5><i class="fa fa-tasks"></i>  Date :: 10-06-2020 <span class="badge bg-warning">Update By Rudy</span></h5> 
									</div>
									<br>
								  </div>
								  <div class="panel-body">
									<div class="task-content">
										<ul id="sortable" class="task-list ui-sortable">
											<li class="list-primary ">
											  <i class=" fa fa-ellipsis-v"></i>
											  <div class="task-checkbox">
												<input type="checkbox" class="list-child" value="" checked readonly />
											  </div>
											  <div class="task-title">
												<span class="task-title-sp">1. Create button start of day on setting</span>
											  </div>
											</li>									
																				
										</ul>
									</div>
								  </div>
								</section>
									<section class="task-panel tasks-widget">
								  <div class="panel-heading">
									<div class="pull-left">
									  <h5><i class="fa fa-tasks"></i>  Date :: 22 - 05-2020 <span class="badge bg-warning">Update By Rudy</span></h5> 
									</div>
									<br>
								  </div>
								  <div class="panel-body">
									<div class="task-content">
										<ul id="sortable" class="task-list ui-sortable">
											<li class="list-primary ">
											  <i class=" fa fa-ellipsis-v"></i>
											  <div class="task-checkbox">
												<input type="checkbox" class="list-child" value="" checked readonly />
											  </div>
											  <div class="task-title">
												<span class="task-title-sp">1. Update Home Page For Setting</span>
											  </div>
											</li>										
											<li class="list-primary ">
											  <i class=" fa fa-ellipsis-v"></i>
											  <div class="task-checkbox">
												<input type="checkbox" class="list-child" value="" checked readonly />
											  </div>
											  <div class="task-title">
												<span class="task-title-sp">2. Add Fild table user ( ent_date,companyname,companydetail,active/notactive user,type) 
													<span class="badge bg-primary">Active : 0 =  Active :: 1 = Not Active</span>
													<span class="badge bg-success"> Type : 0 =  staging :: 1 = live</span>
												</span>
											  </div>
											</li>	
											<li class="list-primary ">
											  <i class=" fa fa-ellipsis-v"></i>
											  <div class="task-checkbox">
												<input type="checkbox" class="list-child" value="" checked readonly />
											  </div>
											  <div class="task-title">
												<span class="task-title-sp">3. Create Form registration for a new partner</span>
											  </div>
											</li>										
											<li class="list-primary ">
											  <i class=" fa fa-ellipsis-v"></i>
											  <div class="task-checkbox">
												<input type="checkbox" class="list-child" value="" checked readonly />
											  </div>
											  <div class="task-title">
												<span class="task-title-sp">4. Create View Data user</span>
											  </div>
											</li>
											<li class="list-primary ">
											  <i class=" fa fa-ellipsis-v"></i>
											  <div class="task-checkbox">
												<input type="checkbox" class="list-child" value="" checked readonly />
											  </div>
											  <div class="task-title">
												<span class="task-title-sp">5. Add Field table has_category (controller_staging,controller_live,urutan) </span>
											  </div>
											</li>												
										</ul>
									</div>
								  </div>
								</section>
									<section class="task-panel tasks-widget">
								  <div class="panel-heading">
									<div class="pull-left">
									  <h5><i class="fa fa-tasks"></i>  Date :: 21 - 05-2020 <span class="badge bg-warning">Update By Rudy</span></h5> 
									</div>
									<br>
								  </div>
								  <div class="panel-body">
									<div class="task-content">
										<ul id="sortable" class="task-list ui-sortable">
											<li class="list-primary ">
											  <i class=" fa fa-ellipsis-v"></i>
											  <div class="task-checkbox">
												<input type="checkbox" class="list-child" value="" checked readonly />
											  </div>
											  <div class="task-title">
												<span class="task-title-sp">1. Update Home Page For Setting</span>
											  </div>
											</li>										
											<li class="list-primary ">
											  <i class=" fa fa-ellipsis-v"></i>
											  <div class="task-checkbox">
												<input type="checkbox" class="list-child" value="" checked readonly />
											  </div>
											  <div class="task-title">
												<span class="task-title-sp">2. Create Controller Configuration</span>
											  </div>
											</li>	
											<li class="list-primary ">
											  <i class=" fa fa-ellipsis-v"></i>
											  <div class="task-checkbox">
												<input type="checkbox" class="list-child" value="" checked readonly />
											  </div>
											  <div class="task-title">
												<span class="task-title-sp">3. Create View configuration_doc</span>
											  </div>
											</li>										
											<li class="list-primary ">
											  <i class=" fa fa-ellipsis-v"></i>
											  <div class="task-checkbox">
												<input type="checkbox" class="list-child" value="" checked readonly />
											  </div>
											  <div class="task-title">
												<span class="task-title-sp">4. Create View Menu menu_setting</span>
											  </div>
											</li>										
										</ul>
									</div>
								  </div>
								</section>
									<section class="task-panel tasks-widget">
								  <div class="panel-heading">
									<div class="pull-left">
									  <h5><i class="fa fa-tasks"></i>  Date :: 20-05-2020 <span class="badge bg-warning">Update By Rudy</span></h5>
									</div>
									<br>
								  </div>
								  <div class="panel-body">
									<div class="task-content">
										<ul id="sortable" class="task-list ui-sortable">
											<li class="list-primary ">
											  <i class=" fa fa-ellipsis-v"></i>
											  <div class="task-checkbox">
												<input type="checkbox" class="list-child" value="" checked readonly />
											  </div>
											  <div class="task-title">
												<span class="task-title-sp">1. Update API : srv_charge_rate</span>
											  </div>
											</li>										
											<li class="list-primary ">
											  <i class=" fa fa-ellipsis-v"></i>
											  <div class="task-checkbox">
												<input type="checkbox" class="list-child" value="" checked readonly />
											  </div>
											  <div class="task-title">
												<span class="task-title-sp">2. Update API : srv_charge_amount</span>
											  </div>
											</li>										
										</ul>
									</div>
								  </div>
								</section>
									<section class="task-panel tasks-widget">
								  <div class="panel-heading">
									<div class="pull-left">
									  <h5><i class="fa fa-tasks"></i>  Date :: 12-05-2020 <span class="badge bg-warning">Update By Rudy</span></h5>
									</div>
									<br>
								  </div>
								  <div class="panel-body">
									<div class="task-content">
										<ul id="sortable" class="task-list ui-sortable">
											<li class="list-primary ">
											  <i class=" fa fa-ellipsis-v"></i>
											  <div class="task-checkbox">
												<input type="checkbox" class="list-child" value="" checked readonly />
											  </div>
											  <div class="task-title">
												<span class="task-title-sp">1. Setting Response Code For Transaction (400,25,30,35,20)</span>
											  </div>
											</li>												
										</ul>
									</div>
								  </div>
								</section>
									<section class="task-panel tasks-widget">
								  <div class="panel-heading">
									<div class="pull-left">
									  <h5><i class="fa fa-tasks"></i>  Date :: 11-05-2020 <span class="badge bg-warning">Update By Rudy</span></h5>
									</div>
									<br>
								  </div>
								  <div class="panel-body">
									<div class="task-content">
										<ul id="sortable" class="task-list ui-sortable">
											<li class="list-primary ">
											  <i class=" fa fa-ellipsis-v"></i>
											  <div class="task-checkbox">
												<input type="checkbox" class="list-child" value="" checked readonly />
											  </div>
											  <div class="task-title">
												<span class="task-title-sp">1. Update type payment (PG OVO, PG WA, SHOPEE, TOPED, BLI BLI)</span>
											  </div>
											</li>										
											<li class="list-primary ">
											  <i class=" fa fa-ellipsis-v"></i>
											  <div class="task-checkbox">
												<input type="checkbox" class="list-child" value="" checked readonly />
											  </div>
											  <div class="task-title">
												<span class="task-title-sp">2. Update Order Status ( WA, O-SHOP )</span>
											  </div>
											</li>
											<li class="list-primary ">
											  <i class=" fa fa-ellipsis-v"></i>
											  <div class="task-checkbox">
												<input type="checkbox" class="list-child" value="" checked readonly />
											  </div>
											  <div class="task-title">
												<span class="task-title-sp">3. Change link home to documentation Page</span>
											  </div>
											</li>
											<li class="list-primary ">
											  <i class=" fa fa-ellipsis-v"></i>
											  <div class="task-checkbox">
												<input type="checkbox" class="list-child" value="" checked readonly />
											  </div>
											  <div class="task-title">
												<span class="task-title-sp">4. Add Button Home</span>
											  </div>
											</li>											
										</ul>
									</div>
								  </div>
								</section>
							
								<section class="task-panel tasks-widget">
								  <div class="panel-heading">
									<div class="pull-left">
									  <h5><i class="fa fa-tasks"></i>  Date :: 8-05-2020 <span class="badge bg-warning">Update By Rudy</span></h5>
									</div>
									<br>
								  </div>
								  <div class="panel-body">
									<div class="task-content">
										<ul id="sortable" class="task-list ui-sortable">
											<li class="list-primary ">
											  <i class=" fa fa-ellipsis-v"></i>
											  <div class="task-checkbox">
												<input type="checkbox" class="list-child" value="" checked readonly />
											  </div>
											  <div class="task-title">
												<span class="task-title-sp">1. Create View Log ALL API Transaction</span>
											  </div>
											</li>										
											<li class="list-primary ">
											  <i class=" fa fa-ellipsis-v"></i>
											  <div class="task-checkbox">
												<input type="checkbox" class="list-child" value="" checked readonly />
											  </div>
											  <div class="task-title">
												<span class="task-title-sp">2.  Update chart detail hits halaman home</span>
											  </div>
											</li>
											<li class="list-primary ">
											  <i class=" fa fa-ellipsis-v"></i>
											  <div class="task-checkbox">
												<input type="checkbox" class="list-child" value="" checked readonly />
											  </div>
											  <div class="task-title">
												<span class="task-title-sp">3. Change link home to documentation Page</span>
											  </div>
											</li>
											<li class="list-primary ">
											  <i class=" fa fa-ellipsis-v"></i>
											  <div class="task-checkbox">
												<input type="checkbox" class="list-child" value="" checked readonly />
											  </div>
											  <div class="task-title">
												<span class="task-title-sp">4. Add Button Home</span>
											  </div>
											</li>											
										</ul>
									</div>
								  </div>
								</section>
								
							</div>
						</div>
						<!-- /OVERVIEW -->
					</div>	
					<div id="responsecode" class="tab-pane ">
						<div class="row">								  
							<div class="col-md-12">
							</div>
						</div>
					</div>
				  <!-- /tab-pane -->
				</div>
				<!-- /tab-content -->
			  </div>
			  <!-- /panel-body -->
			</div>
			
        </div>
        <!-- /container -->
      </section>
      <!-- /wrapper -->
    </section>
  <?php $this->load->view('footer') ?>