<?php $this->load->view('header') ?>
<?php $this->load->view('menu_setting') ?>
    <section id="main-content">
      <section class="wrapper site-min-height">
        <div class="row mt">
			<div class="col-lg-12 col-md-12 col-sm-12 mb">
				<div class="green-panel">
					<div class="green-header profile-01 centered">
						<p><a style="color:#fff;" href="<?php echo site_url()?>/<?php echo $apilist['slug_url']?>/configuration/proc_startofday">Click Here To Start Off Day Date Now is : <?php echo $dateNow?></a></p>
					</div>
				</div>
			</div>
	
          <div class="col-lg-12">
            <div class="row content-panel">
			  <div class="panel-heading">
				<ul class="nav nav-tabs nav-justified">
				  <li class="active">
					<a data-toggle="tab" href="#pushtransaction">Create User</a>
				  </li>
				  <li>
					<a data-toggle="tab" href="#responsecode">All Data User</a>
				  </li>
				</ul>
			  </div>
			  <!-- /panel-heading -->
			  <div class="panel-body">
				<div class="tab-content">
					<div id="pushtransaction" class="tab-pane active">
						<div class="row mt mb">								  
							<div class="col-md-12">
								<form class="contact-form php-mail-form" role="form" action="<?php echo site_url()?>/configuration/proc_data" method="POST">
									<div class="col-md-6">
										<div class="form-group">
											<input type="name" name="name" class="form-control" placeholder="Username"  >
										</div>
										<div class="form-group">
											<input type="name" name="password" class="form-control" placeholder="Password"  >
										</div>
										<div class="form-group">
											<input type="name" name="Companyname" class="form-control" placeholder="Company Name"  >
										</div>
										<div class="form-group">
											<textarea class="form-control" name="companydetail" id="contact-message" placeholder="Company Detail" rows="5"></textarea>
											<div class="validate"></div>
										</div>			

									</div>
									<div class="col-md-6">
										<div class="form-group">
											<label class="col-sm-12 col-lg-12 row" for="inputWarning">Choose API Access</label>
											<select multiple class="form-control" name="apiaccess[]">
												<?php foreach($det_list_category as $lst_cat){ ?>
													<option value="<?php echo $lst_cat['id']?>"><?php echo $lst_cat['name']?></option>
												<?php } ?>
											</select>
										</div>
										<div class="form-group">
											<label class="col-sm-12 col-lg-12 row" for="inputWarning">Choose API Option</label>
											<div class="radio">
												<label>
												  <input type="radio" name="apioption" id="optionsRadios1" value="0" checked>
												  Active
												</label>
											</div>
											<div class="radio">
												<label>
												  <input type="radio" name="apioption" id="optionsRadios2" value="1">
												  Not Active
												</label>
											</div>
										</div>
										<div class="form-group">
											<label class="col-sm-12 col-lg-12 row" for="inputWarning">Choose User type</label>
											<div class="radio">
												<label>
												  <input type="radio" name="type" id="optionsRadios1" value="0" checked>
												  Staging
												</label>
											</div>
											<div class="radio">
												<label>
												  <input type="radio" name="type" id="optionsRadios2" value="1">
												  Live
												</label>
											</div>
										</div>
									</div>
									<div class="col-md-12">
										<div class="form-send">
											<button type="submit" class="btn btn-large btn-primary">Create User</button>
										</div>
									</div>
								</form>
							  
							</div>
						</div>
						<!-- /OVERVIEW -->
					</div>	
					<div id="responsecode" class="tab-pane ">
						<div class="row">								  
							<div class="col-md-12">
								<section id="unseen">
									<table class="table table-bordered table-striped table-condensed">
										<thead>
											<tr>
											  <th>Username</th>
											  <th>Password</th>
											  <th>Real Password</th>
											  <th>Kode Hashing</th>
											  <th>Company</th>
											  <th>Company Detail</th>
											  <th>Type</th>
											</tr>
										</thead>
										<tbody>
											<?php foreach ($lst_user as $allusr){ ?>
												<tr>
												  <td><?php echo $allusr['user_id']?></td>
												  <td><?php echo $allusr['password']?></td>
												  <td><?php echo $allusr['realpass']?></td>
												  <td><?php echo $allusr['hash_key']?></td>
												  <td><?php echo $allusr['companyname']?></td>
												  <td><?php echo $allusr['companydetail']?></td>
												  <td><?php echo $allusr['type_name']?></td>
												</tr>
											<?php } ?>
										</tbody>
									</table>
								</section>
							</div>
						</div>
					</div>
				  <!-- /tab-pane -->
				</div>
				<!-- /tab-content -->
			  </div>
			  <!-- /panel-body -->
			</div>
			
        </div>
        <!-- /container -->
      </section>
      <!-- /wrapper -->
    </section>
  <?php $this->load->view('footer') ?>