<?php $this->load->view('header_login') ?>
    <div id="login-page">
		<div class="container">
			<form class="form-login" action="<?php echo site_url()?>/welcome/login" method="POST">
				<h2 class="form-login-heading">Log in now</h2>
				<div class="login-wrap">
				  <input type="text" name="userid" class="form-control" placeholder="User ID" autofocus>
				  <br>
				  <input type="password" name="password" class="form-control" placeholder="Password"><br />
				  <?php echo $this->session->flashdata('item'); ?>
				  <hr />
				  <button class="btn btn-theme btn-block" href="index.html" type="submit"><i class="fa fa-lock"></i> SIGN IN</button>
				 
				</div>
			</form>
			
		</div>
	</div>
	<br />
	<br />
	
  <?php $this->load->view('footer') ?>