<!--footer start-->
<footer class="site-footer">
  <div class="text-center">
	<p>
	  &copy; Copyrights <strong>HokBen</strong>. All Rights Reserved
	</p>
	<a href="index.html#" class="go-top">
	  <i class="fa fa-angle-up"></i>
	  </a>
  </div>
</footer>


<!--footer end-->
	
  
  <script src="<?php echo  base_url() ?>lib/jquery/jquery.min.js"></script>
  <script src="<?php echo  base_url() ?>lib/bootstrap/js/bootstrap.min.js"></script>
  <script class="include" type="text/javascript" src="<?php echo  base_url() ?>lib/jquery.dcjqaccordion.2.7.js"></script>
  <script src="<?php echo  base_url() ?>lib/jquery.scrollTo.min.js"></script>
  <script src="<?php echo  base_url() ?>lib/jquery.nicescroll.js" type="text/javascript"></script>
  <!--common script for all pages-->
  <script>
	var site_url	= '<?php echo site_url() ?>';
	
	
  </script>
  <script src="<?php echo  base_url() ?>lib/common-scripts.js"></script>
</body>

</html>