<?php $this->load->view('header') ?>
<?php $this->load->view('menu') ?>
<section id="main-content">
	<section class="wrapper">
		<div class="row mb">
		<!-- page start-->
			<div class="content-panel">
				<div class="adv-table">
					<table cellpadding="0" cellspacing="0" border="0" class="display table table-bordered" id="hidden-table-info">
						<thead>
							<tr>
								<th>Transdate</th>
								<th>Device</th>
								<th>Json response</th>
								<th style="display:none;">Sender</th>
								<th style="display:none;">Response Code</th>
							</tr>
						</thead>
					<tbody>
						<?php foreach($listdata as $data) { ?>
							<tr class="gradeX">
								<td><?php echo $data['entry_date']?></td>
								<td><?php echo $data['device']?></td>
								<td><?php echo $data['response']?></td>
								<td style="display:none;"><?php echo htmlentities($data['json_post'])?></td>
								<td style="display:none;"><?php echo $data['response_code']?></td>
							</tr>
						<?php } ?>
					</tbody>
					</table>
				</div>
			</div>
		<!-- page end-->
		</div>
	<!-- /row -->
	</section>
</section>
<script src="<?php echo  base_url() ?>lib/jquery/jquery.min.js"></script>
  <script type="text/javascript" language="javascript" src="<?php echo  base_url() ?>lib/advanced-datatable/js/jquery.js"></script>
  <script src="<?php echo  base_url() ?>lib/bootstrap/js/bootstrap.min.js"></script>
  <script class="include" type="text/javascript" src="<?php echo  base_url() ?>lib/jquery.dcjqaccordion.2.7.js"></script>
  <script src="<?php echo  base_url() ?>lib/jquery.scrollTo.min.js"></script>
  <script src="<?php echo  base_url() ?>lib/jquery.nicescroll.js" type="text/javascript"></script>
  <script type="text/javascript" language="javascript" src="<?php echo  base_url() ?>lib/advanced-datatable/js/jquery.dataTables.js"></script>
  <script type="text/javascript" src="<?php echo  base_url() ?>lib/advanced-datatable/js/DT_bootstrap.js"></script>
  <!--common script for all pages-->
  <script src="<?php echo  base_url() ?>lib/common-scripts.js"></script>
<script type="text/javascript">
    /* Formating function for row details */
    function fnFormatDetails(oTable, nTr) {
      var aData = oTable.fnGetData(nTr);
      var sOut = '<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;" width="100%">';
      sOut += '<tr><td>Response Code:</td><td>' + aData[5] + '</td></tr>';
      sOut += '<tr><td>response:</td><td>'+ aData[4] + '</td></tr>';
      sOut += '</table>';

      return sOut;
    }

    $(document).ready(function() {
      /*
       * Insert a 'details' column to the table
       */
	  var base_url	= "<?php echo  base_url() ?>";
      var nCloneTh = document.createElement('th');
      var nCloneTd = document.createElement('td');
      nCloneTd.innerHTML = '<img src="'+base_url+'lib/advanced-datatable/images/details_open.png">';
      nCloneTd.className = "center";

      $('#hidden-table-info thead tr').each(function() {
        this.insertBefore(nCloneTh, this.childNodes[0]);
      });

      $('#hidden-table-info tbody tr').each(function() {
        this.insertBefore(nCloneTd.cloneNode(true), this.childNodes[0]);
      });

      /*
       * Initialse DataTables, with no sorting on the 'details' column
       */
      var oTable = $('#hidden-table-info').dataTable({
		  
        "aoColumnDefs": [{
          "bSortable": false,
          "aTargets": [0]
        }],
        "aaSorting": [
          [1, 'asc']
        ]
      });

      /* Add event listener for opening and closing details
       * Note that the indicator for showing which row is open is not controlled by DataTables,
       * rather it is done here
       */
      $('#hidden-table-info tbody td img').live('click', function() {
        var nTr = $(this).parents('tr')[0];
        if (oTable.fnIsOpen(nTr)) {
          /* This row is already open - close it */
          this.src = base_url+"lib/advanced-datatable/images/details_open.png";
          oTable.fnClose(nTr);
        } else {
          /* Open this row */
          this.src = base_url+"lib/advanced-datatable/images/details_close.png";
          oTable.fnOpen(nTr, fnFormatDetails(oTable, nTr), 'details');
        }
      });
    });
  </script>
