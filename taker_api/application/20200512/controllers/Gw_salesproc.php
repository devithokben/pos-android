<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

date_default_timezone_set("Asia/Bangkok");
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . 'libraries/REST_Controller.php';
class gw_salesproc extends REST_Controller {	
	
	public function __construct(){
		parent::__construct();		
		$this->ctype		= 'application/json';
		$this->data_post	= file_get_contents('php://input');
		$this->client_id	= '1709f6da3aec791dc76e299fed32ce1539da8e5a';
		//$this->url			= 'http://103.94.170.70:6969/trial_order_dev.php';
		$this->url			= 'http://172.18.100.54:6969/trial_order_dev.php';
	}
	public function pushTransaction_post(){
		//getUserId
		$usr_id = $this->db->query("select user_id from `keys` where `key` = '".$this->client_id."'")->row();
		
		$pushdata	= $this->pushTransaction_proccess();
		$response 	= json_decode($pushdata, TRUE);			
		$res_server	= json_decode($response['message'], TRUE);	
		if($res_server['statusCode']){
			$status		= $res_server['statusCode'];
			$message	= $res_server['message'];
		}else{
			$status 	= $response['status'];
			$message	= $response['message'];
		}
				
		$savelog	= $this->save_log_transaction($this->data_post,'staging',date("Y-m-d H:i:s"),$message,$status,$usr_id->user_id);		
		if($savelog == 0){
			$server['status_server']	= $status;
			$server['msg_server']		= $message;
			$server['save_log']			= "Sukses";
		}else{
			$server['status_server']	= $status;
			$server['msg_server']		= $message;
			$server['save_log']			=  "gagal";
		}
		/*
		$pushptp	= 	array(
								'status' => $response->getStatus(),
								'message' => $response->getBody()
							);										
			$this->set_response($pushptp, REST_Controller::HTTP_OK);*/
		echo json_encode($server, TRUE);
		exit;
	}
	
	private function pushTransaction_proccess(){
		require_once APPPATH.'../HTTP_Request/HTTP/Request2.php';
		$request = new HTTP_Request2();
		$request->setUrl($this->url);
		$request->setMethod(HTTP_Request2::METHOD_POST);
		$request->setConfig(array(
		  'follow_redirects' => TRUE
		));
		$request->setHeader(array(
		  'Content-Type' => $this->ctype,
		  'api_auth_key' => $this->client_id
		));				
		$request->setBody($this->data_post);
		
		
		try {
			$response = $request->send();
			//print_r($response->getStatus());exit;
			if (($response->getStatus() == 200)) {
			
			$pushptp	= 	array(
								'status' => $response->getStatus(),
								'message' => $response->getBody()
							);										
			return json_encode($pushptp, TRUE);
			
		  }
		  else {
			//print_r($response);exit;
			$pushptp	= array(
							'status' => $response->getStatus(),
							'message' => $response->getBody());
				
			return json_encode($pushptp, TRUE);
			exit;
			
		  }
		}
		catch(HTTP_Request2_Exception $e) {
			
			$pushptp	= array(
							'status' => $e,
							'message' => "gagal");
			
			return json_encode($pushptp, TRUE);
			exit;
			
		}	
	}
	private function save_log_transaction($json_post,$device,$entry_date,$response,$response_code,$usr_id){		
		$sql	= "Insert into log_sales_osds (json_post,device,entry_date,response,response_code,usr_id)
					values ( '".$json_post."','".$device."','".$entry_date."','".$response."','".$response_code."','".$usr_id."')";
		//echo $sql;exit;
		if($this->db->query($sql)){
			return  0;
		}else{
			return 1;
		}
	}
}
