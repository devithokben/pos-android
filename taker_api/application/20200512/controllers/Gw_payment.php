<?php

date_default_timezone_set("Asia/Bangkok");
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . 'libraries/REST_Controller.php';
class gw_payment extends CI_Controller {	
	
	public function __construct(){
		parent::__construct();
		
	}
	
	
    public function index()
    {
		$qry_gethistory				=	"select * from log_ovo order by transdate desc limit 3";
		$data['recentactivity']		= 	$this->db->query($qry_gethistory)->result_array();
		$data['PUSHP2P']			= 	$this->getRekapLog('PUSH P2P');
		$data['VoidPayment']		= 	$this->getRekapLog('Void Payment');
		$data['CheckPayment']		= 	$this->getRekapLog('Check Payment');
		$data['dashboard']			=   'class="active"';
		$this->load->view('ovo_dashboard',$data);
    }
	public function category($id)
    {
		$qry_gethistory				=	"select * from has_list_api where type = 1 order by name asc";
		$data['list_category']		= 	$this->db->query($qry_gethistory)->result_array();
		$this->load->view('gwsales_dashboard',$data);
    }
	public function getRekapLog($transaction){
		$qry_gethistory		=	"select count(*) as total from log_ovo where transaction = '".$transaction."'";
		$total				= 	$this->db->query($qry_gethistory)->row();
		return $total->total;
	}
	public function documentation(){
		$data['ovo']					=   'class="active"';
		$data['ovo_doc']				=   'class="active"';
		$this->load->view('ovo_doc',$data);
	}
	public function testing(){
		$data['ovo']					=   'class="active"';
		$data['ovo_testing']			=   'class="active"';
		$this->load->view('ovo_testing',$data);
	}
	public function logtransaction(){
		$qry_gethistory			=	"select * from log_ovo order by transdate";
		$data['listdata']		= 	$this->db->query($qry_gethistory)->result_array();
		$data['ovo']					=   'class="active"';
		$data['ovo_logtransaction']		=   'class="active"';
		$this->load->view('ovo_log',$data);
	}

}
