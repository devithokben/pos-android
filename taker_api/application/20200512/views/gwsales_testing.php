<?php $this->load->view('header') ?>
<?php $this->load->view('menu') ?>
    <section id="main-content">
      <section class="wrapper site-min-height">
        <div class="row mt">
			<div class="col-lg-12">
            <div class="row content-panel">
				<div class="col-md-6 profile-text mt mb">
					<div class="right-divider hidden-sm hidden-xs">
						<h5>{</h5>
							<h5>&nbsp;&nbsp;"no_voucher":"",</h5>
							<h5>&nbsp;&nbsp;"orderstatus":"02",</h5>
							<h5>&nbsp;&nbsp;"order_notes":"",</h5>
							<h5>&nbsp;&nbsp;"city":"JKT  ",</h5>
							<h5>&nbsp;&nbsp;"storeid":"535",</h5>
							<h5>&nbsp;&nbsp;"order_detail":</h5>
								<h5>&nbsp;&nbsp;&nbsp;{</h5>
									<h5>&nbsp;&nbsp;&nbsp;&nbsp;"000377":"1|11000.0|COCA COLA  EVENT",</h5>
									<h5>&nbsp;&nbsp;&nbsp;&nbsp;"801007":"1|30000.0|SHRIMP ROLL RB",</h5>
									<h5>&nbsp;&nbsp;&nbsp;&nbsp;"803011":"1|15000.0|SHUMAY STEAM 3 PCS (TB)"</h5>
								<h5>&nbsp;&nbsp;&nbsp;},</h5>
							<h5>&nbsp;&nbsp;"order_name":"Rizka Amanda",</h5>
							<h5>&nbsp;&nbsp;"order_phone":"082114678984",</h5>
							<h5>&nbsp;&nbsp;"provinci":"DKI Jakarta",</h5>
							<h5>&nbsp;&nbsp;"nominal_disc":"0.0",</h5>
							<h5>&nbsp;&nbsp;"contact_type":"1",</h5>
							<h5>&nbsp;&nbsp;"order_address":"9, Jl. D No.91, RT.9\/RW.2, Gandaria Utara, Kec. Kby. Baru, Kota Jakarta Selatan, Daerah Khusus Ibukota Jakarta 12140, Indonesia\n[Address Notes: no.54 depan rumah biru pager coklat tingkat dua]",</h5>
							<h5>&nbsp;&nbsp;"cust_name":"Rizka Amanda",</h5>
							<h5>&nbsp;&nbsp;"order_total_price":"56000.0",</h5>
							<h5>&nbsp;&nbsp;"cust_phone":"6282114678984",</h5>
							<h5>&nbsp;&nbsp;"type_payment":"2",</h5>
							<h5>&nbsp;&nbsp;"sex":"",</h5>
							<h5>&nbsp;&nbsp;"long_address":"106.789000700000003",</h5>
							<h5>&nbsp;&nbsp;"order_apps_id":"200420\/535\/00120",</h5>
							<h5>&nbsp;&nbsp;"lat_address":"-6.25368370000000073",</h5>
							<h5>&nbsp;&nbsp;"order_date":"2020-04-20 19:22:18",</h5>
							<h5>&nbsp;&nbsp;"delivery_charge":"16000.0",</h5>
							<h5>&nbsp;&nbsp;"type_service":"DL",</h5>
							<h5>&nbsp;&nbsp;"device":"web",</h5>
							<h5>&nbsp;&nbsp;"cust_id":"125315",</h5>
							<h5>&nbsp;&nbsp;"ship_date":"2020-04-20 19:25:00"</h5>
						<h5>}</h5>
					</div>
				</div>
				<div class="col-md-6 mb">
					<h1>Please, Try it yourself !!</h1>
					<div class="col-md-12">
						<div class="message-p pn">
						  <div class="message-header">
							<h5>Header Security & URL</h5>
						  </div>
						  <div class="row">
							<div class="col-md-9">
							  <p>
								<name>Header Key</name>
								api_auth_key 
							  </p>
							  <p>
								<name>Header Value</name>
								<?php echo $valueheader ?>
							  </p>
							  <p>
								<name>URL</name>
								<?php echo site_url()?>/gw_salesproc/pushTransaction
							  </p>
							</div>
						  </div>
						</div>
					</div>
					
					<div class="col-lg-12 mt">
						<div class="row content-panel">
						  <div class="panel-heading">
							<ul class="nav nav-tabs nav-justified">
							  <li class="active">
								<a data-toggle="tab" href="#pushtransaction">PUSH TRANSACTION TO STAGING</a>
							  </li>
							</ul>
						  </div>
						  <!-- /panel-heading -->
						  <div class="panel-body">
							<div class="tab-content">
							  <div id="pushtransaction" class="tab-pane active">
								<div class="row">								  
								  <div class="col-md-12">
									<input type="hidden" id="usr_id" value="<?php echo  $this->session->userdata("id") ?>" />
									<input type="hidden" id="key" value="<?php echo  $valueheader?>" />
									<textarea rows="3" class="form-control" id="json_request" placeholder="Masukan Json Sesuai Format Diatas"></textarea>
									<div class="grey-style">
									  <div class="pull-right">
										<button class="btn btn-sm btn-theme03" id="btn_post">POST</button>
									  </div>
									</div>
								  </div>
								</div>
								<!-- /OVERVIEW -->
							  </div>
							  <!-- /tab-pane -->
							</div>
							<!-- /tab-content -->
						  </div>
						  <!-- /panel-body -->
						</div>
						<!-- /col-lg-12 -->
					</div>
					<div class="col-lg-12 mt">
						<div class="task-content">
							<ul id="sortable" class="task-list ui-sortable">
								<li class="list-info" id="list-info">
								  <i class=" fa fa-ellipsis-v"></i>
								  <div class="task-title">
										<h3>::Response::</h3>
										<h5><span class="task-title-sp" id="task-title-sp"></span></h5>
								  </div>
								</li>
								<li class="list-danger">
								  <i class=" fa fa-ellipsis-v"></i>
								  <div class="task-title">
									<span class="task-title-sp">All Process Sent to Development Enviroment.. If you want publish to production contact HokBen IT division</span>
								  </div>
								</li>

							</ul>
						</div>
					</div>
				</div>
            </div>
            <!-- /row -->
          </div>
		
		
          <!-- /col-lg-12 -->
          
          <!-- /row -->
        </div>
        <!-- /container -->
      </section>
      <!-- /wrapper -->
    </section>
  <?php $this->load->view('footer') ?>