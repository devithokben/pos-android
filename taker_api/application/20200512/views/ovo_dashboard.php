<?php $this->load->view('header') ?>
<?php $this->load->view('menu') ?>
<section id="main-content">
	<section class="wrapper">
		<div class="row">
			<div class="col-lg-9">
				<div class="row mt">
				<!-- SERVER STATUS PANELS -->
					<div class="col-md-4 col-sm-4 mb">
						<div class="grey-panel pn donut-chart">
							<div class="grey-header">
								<h5>OVO API HIT TRANSACTION</h5>
							</div>                  
							<div class="row">
								<div class="col-sm-6 col-xs-6 goleft">
									<p>PTP<br/>Push Payment:</p>
								</div>
								<div class="col-sm-6 col-xs-6">
									<h2><?php echo $PUSHP2P ?></h2>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-6 col-xs-6 goleft">
									<p>Check<br/> Payment:</p>
								</div>
								<div class="col-sm-6 col-xs-6">
									<h2><?php echo $CheckPayment ?></h2>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-6 col-xs-6 goleft">
									<p>PTP<br/>Void Payment:</p>
								</div>
								<div class="col-sm-6 col-xs-6">
									<h2><?php echo $VoidPayment ?></h2>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-3 ds">
				<h4 class="centered mt">RECENT ACTIVITY</h4>
				<?php foreach ($recentactivity as $activity){ ?>
					<div class="desc">
						<div class="thumb">
							<span class="badge bg-theme"><i class="fa fa-clock-o"></i></span>
						</div>
						<div class="details">
							<p>
							<muted><?php echo $activity['transdate']?></muted>
							<br/>
							<a href="#"><?php echo $activity['response']?></a> <?php echo $activity['transaction']?>.
							<p><?php echo $activity['ovo_response_code']?></p>
							<br/>
							</p>
						</div>
					</div>
				<?php } ?>
			</div>
		</div>
	</section>
</section>
<?php $this->load->view('footer') ?>