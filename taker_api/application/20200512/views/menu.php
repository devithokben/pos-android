 <aside>
      <div id="sidebar" class="nav-collapse ">
        <!-- sidebar menu start-->
        <ul class="sidebar-menu" id="nav-accordion">
			<li class="sub-menu">
				<a href="<?php echo  site_url()?>/home">
					<i class="fa fa-home"></i>
					<span>Back To Home</span>
				</a>
			</li>
			<?php foreach ($list_category as $lst){ ?>
				<li class="sub-menu">
					<a href="javascript:;" <?php echo $ovo ?>>
						<i class="fa fa-file"></i>
						<span><?php echo $lst['name']?></span>
					</a>
					<ul class="sub">
						<li <?php echo $ovo_doc ?> ><a href="<?php echo site_url()?>/<?php echo $lst['slug_url']?>/documentation">API Documentation</a></li>
						<li <?php echo $ovo_testing ?>> <a href="<?php echo site_url()?>/<?php echo $lst['slug_url']?>/testing">Test transaction</a></li>
						<li <?php echo $ovo_logtransaction ?> ><a  href="<?php echo site_url()?>/<?php echo $lst['slug_url']?>/logTransaction/<?php echo $this->session->userdata('id')?>">Log Transaction</a></li>
					</ul>
				</li>
			<?php } ?>
		</ul>
        <!-- sidebar menu end-->
      </div>
    </aside>
    <!--sidebar end-->