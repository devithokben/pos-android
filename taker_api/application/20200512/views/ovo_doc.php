<?php $this->load->view('header') ?>
<?php $this->load->view('menu') ?>
    <section id="main-content">
      <section class="wrapper site-min-height">
        <div class="row mt">
          <div class="col-lg-12">
            <div class="row content-panel">
              <div class="col-md-12 profile-text">
                <h3>Documentation OVO</h3>
                <h6>OVO Technical Documentation Version 1.6</h6>
                <p>This guide is intended to assist software developer who want to develop applications integrated with OVO Payment systems.</p>
                <br>
                <p><a href="<?php echo base_url() ?>files/ovo16.pdf" class="btn btn-theme" download><i class="fa fa-download"></i> Download PDF</a></p>
              </div>
            </div>
            <!-- /row -->
          </div>
        </div>
        <!-- /container -->
      </section>
      <!-- /wrapper -->
    </section>
  <?php $this->load->view('footer') ?>