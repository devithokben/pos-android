<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="Dashboard">
  <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
  <title>API MANAGEMENT SYSTEM</title>

  <!-- Favicons -->
  <link href="<?php echo base_url()?>img/favicon.png" rel="icon">
  <link href="<?php echo base_url()?>img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Bootstrap core CSS -->
  <link href="<?php echo base_url()?>lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!--external css-->
  <link href="<?php echo base_url()?>/lib/font-awesome/css/font-awesome.css" rel="stylesheet" />
  <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>css/zabuto_calendar.css">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>lib/gritter/css/jquery.gritter.css" />
  <!-- Custom styles for this template -->
  <link href="<?php echo base_url()?>css/style.css" rel="stylesheet">
  <link href="<?php echo base_url()?>css/style-responsive.css" rel="stylesheet">
   <link href="<?php echo base_url()?>lib/advanced-datatable/css/demo_table.css" rel="stylesheet" />
  <link rel="stylesheet" href="<?php echo base_url()?>lib/advanced-datatable/css/DT_bootstrap.css" />
  <script src="<?php echo base_url()?>lib/chart-master/Chart.js"></script>

</head>

<body>
  <section id="container">
    <!-- **********************************************************************************************************************************************************
        TOP BAR CONTENT & NOTIFICATIONS
        *********************************************************************************************************************************************************** -->
    <!--header start-->
    <header class="header black-bg">
      <!--logo start-->
      <a href="#" class="logo"><b>HokBen Api Management System</b></a>
    </header>