<?php $this->load->view('header') ?>
<?php $this->load->view('menu') ?>
    <section id="main-content">
      <section class="wrapper site-min-height">
        <div class="row mt">
          <div class="col-lg-12">
            <div class="row content-panel">
              <div class="col-md-12 profile-text">
                <h3>Documentation Integration To OSDS System</h3>
                <div class="col-lg-12">
						<h5><i class="fa fa-angle-right"></i>Transaction Push Data</h5>
						<section id="unseen">
							<table class="table table-bordered table-striped table-condensed">
							  <thead>
								<tr>
								  <th>Parameter</th>
								  <th>Availability</th>
								  <th>Description</th>
								</tr>
							  </thead>
							  <tbody>
								<tr>
								  <td>no_voucher</td>
								  <td>Not Mandatory</td>
								  <td>Type Code of Voucher if Exist</td>
								</tr>
								<tr>
								  <td>orderstatus</td>
								  <td>Mandatory</td>
								  <td><p>02 : Web</p><p>03 : Apps</p><p>04 : WA</p></td>
								</tr>
								<tr>
								  <td>order_notes</td>
								  <td>Not Mandatory</td>
								  <td>Type Note of Transaction</td>
								</tr>
								<tr>
								  <td>city</td>
								  <td>Mandatory</td>
								  <td> 
										<h5>Location Customer Hokben</h5>
										<p>BDG : BANDUNG</p>
										<p>BLI : BALI</p>
										<p>BTM : BATAM</p>
										<p>CRB : CIREBON</p>
										<p>JKT : JABODETABEK</p>
										<p>KRW : KARAWANG</p>
										<p>LPG : LAMPUNG</p>
										<p>MLG : MALANG</p>
										<p>PLB : PALEMBANG</p>
										<p>SBY : SURABAYA</p>
										<p>SLO : SOLO</p>
										<p>SMG : SEMARANG</p>
										<p>TSM : TASIKMALAYA</p>
										<p>YGY : YOGYAKART</p>
									</td>
								</tr>
								<tr>
								  <td>storeid</td>
								  <td>Mandatory</td>
								  <td>Type Store ID Hokben. Provide by Hokben</td>
								</tr>
								<tr>
								  <td>order_detail</td>
								  <td>Mandatory</td>
								  <td><h5>Type Of Detail Order Customer</h5>
								  <p>Format : "{PRODUCT_CODE}" : "{QTY}|{PRICE}|{PRODUCT_NAME}"</p>
								  <p>Product Code Provide by HokBen</p>
								  </td>
								</tr>
								<tr>
								  <td>order_name</td>
								  <td>Mandatory</td>
								  <td>Type Name Of Customer who create transaction
								  </td>
								</tr>
								<tr>
								  <td>order_phone</td>
								  <td>Mandatory</td>
								  <td>Type Phone Number Of Customer who create transaction
								  </td>
								</tr>
								<tr>
								  <td>provinci</td>
								  <td>Mandatory</td>
								  <td>Type Location Of Customer who create transaction
								  </td>
								</tr>
								<tr>
								  <td>nominal_disc</td>
								  <td>Not Mandatory</td>
								  <td>Type nominal discount if exist
								  </td>
								</tr>
								<tr>
								  <td>contact_type</td>
								  <td>Mandatory</td>
								  <td>
										<h5>List Contact Type</h5>
										<p>1 : PERORANGAN</p>
										<p>2 : BADAN USAHA</p>
										<p>A : PEMERINTAH</p>
										<p>B : SWASTA</p>
										<p>C : PENDIDIKAN</p>
										<p>D : EO</p>
										<p>E : PARTAI</p>
										<p>F : YAYASAN+GEREJA</p>
										<p>G : PERUMAHAN</p>
										<p>H : RUMAH SAKIT</p>
										<p>I : HOTEL&GEDUNG</p>
								  </td>
								</tr>
								<tr>
								  <td>order_address</td>
								  <td>Mandatory</td>
								  <td>Type the destination address</td>
								</tr>								 
								<tr>
								  <td>cust_name</td>
								  <td>Mandatory</td>
								  <td>Type customer name</td>
								</tr>
								<tr>
								  <td>cust_name</td>
								  <td>Mandatory</td>
								  <td>Type the name of the destination</td>
								</tr>
								<tr>
								  <td>order_total_price</td>
								  <td>Mandatory</td>
								  <td>Type total price</td>
								</tr>
								<tr>
								  <td>cust_phone</td>
								  <td>Mandatory</td>
								  <td>Type the Phone Number of the destination</td>
								</tr>
								<tr>
								  <td>type_payment</td>
								  <td>Mandatory</td>
								  <td>
									<h5>List Payment type</h5>
									<p>1 : COD</p>
									<p>10 : PG WA</p>
									<p>2 : PG MANDIRI</p>
									<p>3 : TRANSFER BANK</p>
									<p>4 : DP</p>
									<p>6 : VA SINGLE TC</p>
									<p>7 : VA MERGE TC</p>
									<p>8 : PG CIMB</p>
									<p>9 : OVO</p>
								  </td>
								</tr>
								<tr>
								  <td>sex</td>
								  <td>Mandatory</td>
								  <td><p>F : Female</p><p>M : Male</p>
								  </td>
								</tr>
								<tr>
								  <td>long_address</td>
								  <td>Mandatory</td>
								  <td>longitude customer
								  </td>
								</tr>
								<tr>
								  <td>order_apps_id</td>
								  <td>Mandatory</td>
								  <td>Invoice ID Payment Transaction
								  </td>
								</tr>
								<tr>
								  <td>lat_address</td>
								  <td>Mandatory</td>
								  <td>Latitude Customer
								  </td>
								</tr>
								<tr>
								  <td>order_date</td>
								  <td>Mandatory</td>
								  <td>Type Order Date Transaction
								  </td>
								</tr>
								<tr>
								  <td>delivery_charge</td>
								  <td>Mandatory</td>
								  <td>Type Delivery Charge Transaction. Provide By Hokben
								  </td>
								</tr>
								<tr>
								  <td>type_service</td>
								  <td>Mandatory</td>
								  <td><h5>List Service </h5>
										<p>DL : Delivery</p>
										<p>DI : Dine In</p>
										<p>TA : Take Away</p>
										<p>DT : Drive Thru</p>
								  </td>
								</tr>
								<tr>
								  <td>device</td>
								  <td>Mandatory</td>
								  <td><h5>List device </h5>
										<p>APPS</p>
										<p>WEB</p>
										<p>WA</p>
										<p>DIGIRESTO</p>
								  </td>
								</tr>
								<tr>
								  <td>cust_id</td>
								  <td>Mandatory</td>
								  <td>ID Customer
								  </td>
								</tr>
								<tr>
								  <td>ship_date</td>
								  <td>Mandatory</td>
								  <td>Shipping Date
								  </td>
								</tr>
							  </tbody>
							</table>
						</section>
					</div>
              </div>
            </div>
            <!-- /row -->
          </div>
        </div>
        <!-- /container -->
      </section>
      <!-- /wrapper -->
    </section>
  <?php $this->load->view('footer') ?>