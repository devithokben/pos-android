<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="Dashboard">
  <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
  <title>API MANAGEMENT SYSTEM</title>

  <!-- Favicons -->
  <link href="<?php echo base_url()?>img/favicon.png" rel="icon">
  <link href="<?php echo base_url()?>img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Bootstrap core CSS -->
  <link href="<?php echo base_url()?>lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!--external css-->
  <link href="<?php echo base_url()?>/lib/font-awesome/css/font-awesome.css" rel="stylesheet" />
  <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>css/zabuto_calendar.css">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>lib/gritter/css/jquery.gritter.css" />
  <!-- Custom styles for this template -->
  <link href="<?php echo base_url()?>css/style.css" rel="stylesheet">
  <link href="<?php echo base_url()?>css/style-responsive.css" rel="stylesheet">
   <link href="<?php echo base_url()?>lib/advanced-datatable/css/demo_table.css" rel="stylesheet" />
  <link rel="stylesheet" href="<?php echo base_url()?>lib/advanced-datatable/css/DT_bootstrap.css" />
  <script src="<?php echo base_url()?>lib/chart-master/node_modules/chart.js/dist/Chart.js"></script>
  <link href="<?php echo base_url()?>lib/chart-master/node_modules/chart.js/dist/Chart.css" />

</head>

<body>
  <section id="container">
    <!-- **********************************************************************************************************************************************************
        TOP BAR CONTENT & NOTIFICATIONS
        *********************************************************************************************************************************************************** -->
    <!--header start-->
    <header class="header black-bg">
      <div class="sidebar-toggle-box">
        <div class="fa fa-bars tooltips" data-placement="right" data-original-title="Toggle Navigation"></div>
      </div>
      <!--logo start-->
      <a href="<?php echo site_url()?>/home" class="logo"><b>HokBen API</b></a>
      <div class="top-menu">
        <ul class="nav pull-right top-menu">
          <li><a class="logout" href="<?php echo site_url()?>\welcome\logout">Logout</a></li>
        </ul>
      </div>
    </header>