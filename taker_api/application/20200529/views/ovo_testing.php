<?php $this->load->view('header') ?>
<?php $this->load->view('menu') ?>
    <section id="main-content">
      <section class="wrapper site-min-height">
        <div class="row mt">
			<div class="col-lg-12">
            <div class="row content-panel">
				<div class="col-md-4 profile-text mt mb">
					<div class="right-divider hidden-sm hidden-xs">
						<h4>{</h4>
						<h4>&nbsp;&nbsp;"amount":"10000",</h4>
						<h4>&nbsp;&nbsp;   "date":"2020-04-07 10:18:07.281",</h4>
						<h4>&nbsp;&nbsp;   "referenceNumber":"8889",</h4>
						<h4>&nbsp;&nbsp;   "transactionRequestData":{</h4>
						<h4>&nbsp;&nbsp;&nbsp;&nbsp;	  "batchNo":"030420",</h4>
						<h4>&nbsp;&nbsp;&nbsp;&nbsp;	  "phone":"085694522356",</h4>
						<h4>&nbsp;&nbsp;&nbsp;&nbsp;	  "merchantInvoice":"1010101010"</h4>
						<h4>&nbsp;&nbsp;	}</h4>
						<h4>}</h4>
					</div>
				</div>
				<div class="col-md-8 mb">
					<br />
					<div class="message-p pn">
					  <div class="message-header">
						<h5>Description</h5>
					  </div>
					  <div class="row">
						<div class="col-md-11">
							<p>
								<name>amount</name>
								sent you a message.
							</p>
							<p>
								<name>date</name>
								sent you a message.
							</p>
							<p>
								<name>referenceNumber</name>
								sent you a message.
							</p>
							<p>
								<name>transactionRequestData</name>
								sent you a message.
							</p>
							<p>
								<name>batchNo</name>
								sent you a message.
							</p>
							<p>
								<name>phone</name>
								sent you a message.
							</p>
							<p>
								<name>merchantInvoice</name>
								sent you a message.
							</p>
						</div>
					  </div>
					</div>
                <!-- /Message Panel-->
              </div>
            </div>
            <!-- /row -->
          </div>
		
		
          <!-- /col-lg-12 -->
          <div class="col-lg-12 mt">
            <div class="row content-panel">
              <div class="panel-heading">
                <ul class="nav nav-tabs nav-justified">
                  <li class="active">
                    <a data-toggle="tab" href="#overview">PUSH PTP</a>
                  </li>
                  <li>
                    <a data-toggle="tab" href="#contact" class="contact-map">CHECK PAYMENT</a>
                  </li>
                  <li>
                    <a data-toggle="tab" href="#edit">VOID PAYMENT</a>
                  </li>
                </ul>
              </div>
              <!-- /panel-heading -->
              <div class="panel-body">
                <div class="tab-content">
                  <div id="overview" class="tab-pane active">
                    <div class="row">
                      <div class="col-md-12">
                        <textarea rows="3" class="form-control" placeholder="Masukan Json Sesuai Format Diatas"></textarea>
                        <div class="grey-style">
                          <div class="pull-left">
                            <i class="fa fa-link btn btn-sm btn-theme"></i> http://online.hokben.net
                          </div>
                          <div class="pull-right">
                            <button class="btn btn-sm btn-theme03">POST</button>
                          </div>
                        </div>
                      </div>
                    </div>
                    <!-- /OVERVIEW -->
                  </div>
                  <!-- /tab-pane -->
                  <div id="contact" class="tab-pane">
                    <div class="row">
                      <div class="col-md-12">
                        <textarea rows="3" class="form-control" placeholder="Masukan Json Sesuai Format Diatas"></textarea>
                        <div class="grey-style">
                          <div class="pull-left">
                            <i class="fa fa-link btn btn-sm btn-theme"></i> http://online.hokben.net
                          </div>
                          <div class="pull-right">
                            <button class="btn btn-sm btn-theme03">POST</button>
                          </div>
                        </div>
                      </div>
                    </div>
                    <!-- /row -->
                  </div>
                  <!-- /tab-pane -->
                  <div id="edit" class="tab-pane">
                    <div class="row">
                      <div class="col-md-12">
                        <textarea rows="3" class="form-control" placeholder="Masukan Json Sesuai Format Diatas"></textarea>
                        <div class="grey-style">
                          <div class="pull-left">
                            <i class="fa fa-link btn btn-sm btn-theme"></i> http://online.hokben.net
                          </div>
                          <div class="pull-right">
                            <button class="btn btn-sm btn-theme03">POST</button>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <!-- /tab-pane -->
                </div>
                <!-- /tab-content -->
              </div>
              <!-- /panel-body -->
            </div>
            <!-- /col-lg-12 -->
          </div>
          <!-- /row -->
        </div>
        <!-- /container -->
      </section>
      <!-- /wrapper -->
    </section>
  <?php $this->load->view('footer') ?>