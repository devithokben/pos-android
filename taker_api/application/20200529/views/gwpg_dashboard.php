<?php $this->load->view('header') ?>
<?php $this->load->view('menu') ?>
<section id="main-content">
	<section class="wrapper">
		<div class="row">
			<div class="col-lg-12">
				<div class="row mt">
				<!-- SERVER STATUS PANELS -->
					<?php foreach ($list_category as $lst){ ?>
						<div class="col-md-4 col-sm-4 mb">
							<div class="grey-panel pn donut-chart">
								<div class="grey-header">
									<h5><?php echo strtoupper($lst['name'])?> TRANSACTION</h5>
								</div>                  
								<div class="row">
									<div class="col-sm-6 col-xs-6 goleft">
										<p>PTP<br/>Push Payment:</p>
									</div>
									<div class="col-sm-6 col-xs-6">
										<h2><?php echo $PUSHP2P ?></h2>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-6 col-xs-6 goleft">
										<p>Check<br/> Payment:</p>
									</div>
									<div class="col-sm-6 col-xs-6">
										<h2><?php echo $CheckPayment ?></h2>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-6 col-xs-6 goleft">
										<p>PTP<br/>Void Payment:</p>
									</div>
									<div class="col-sm-6 col-xs-6">
										<h2><?php echo $VoidPayment ?></h2>
									</div>
								</div>
							</div>
						</div>
					<?php } ?>
				</div>
			</div>
		</div>
	</section>
</section>
<?php $this->load->view('footer') ?>