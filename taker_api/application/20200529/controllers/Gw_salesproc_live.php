<?php
date_default_timezone_set("Asia/Bangkok");
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . 'libraries/REST_Controller.php';
class gw_salesproc_live extends REST_Controller {	
	
	public function __construct(){
		parent::__construct();		
		$this->ctype		= 'application/json';
		$this->data_post	= file_get_contents('php://input');
		$this->client_id	= '1709f6da3aec791dc76e299fed32ce1539da8e5a';
		//$this->url		= 'http://103.94.170.70:6969/trial_order_dev.php';
		$this->url			= 'http://172.18.100.54:6969/trial_order_dev.php';
		$this->staging_db	= $this->load->database('live_db',TRUE);
		if(!$this->staging_db){
			$msg["responsecode"] = 401;
			$msg["message"]		 = "Cannot Connect DB";
			return $this->response(json_encode($msg),401);
			exit;
		}
		$this->gross		= 0;
		
		$usr_id = $this->db->query("select user_id from `keys` where `key` = '".$this->client_id."'")->row();
		$this->user_id	= $usr_id->user_id;
	}
	public function pushTransaction_post(){
		//getUserId
		$usr_id = $this->db->query("select user_id from `keys` where `key` = '".$this->client_id."'")->row();
		//$pushdata	= $this->pushTransaction_proccess();
		$pushdata	= $this->validateCustomer($usr_id);
		$response 	= json_decode($pushdata, TRUE);			
		$res_server	= json_decode($response['message'], TRUE);	
		if($res_server['statusCode']){
			$status		= $res_server['statusCode'];
			$message	= $res_server['message'];
		}else{
			$status 	= $response['status'];
			$message	= $response['message'];
		}
				
		//$savelog	= $this->save_log_transaction($this->data_post,'staging',date("Y-m-d H:i:s"),$message,$status,$usr_id->user_id);		
		if($savelog == 0){
			$server['status_server']	= $status;
			$server['msg_server']		= $message;
			$server['save_log']			= "Sukses";
		}else{
			$server['status_server']	= $status;
			$server['msg_server']		= $message;
			$server['save_log']			=  "gagal";
		}
		/*
		$pushptp	= 	array(
								'status' => $response->getStatus(),
								'message' => $response->getBody()
							);										
			$this->set_response($pushptp, REST_Controller::HTTP_OK);*/
		echo json_encode($server, TRUE);
		exit;
	}
	private function validateCustomer($usr_id){
		$data			= json_decode(str_replace("\u0027"," ",$this->data_post));
		
		//print_r($data);exit;
		//$this->savelog(json_encode($data));
		$phone_no		= 0;
		if($contact_id 	= $this->memberExist($data->cust_name,$data->cust_phone,$data->city,$data->provinci))
		{
			$contact_id;
			
			//getMasterPhone
			$slc_phone		= "select PHONE from CONTACT where id='".$contact_id."'";
			
			$result_phone 	= $this->staging_db->query($slc_phone)->row();
			$phone_no		= $result_phone->PHONE;
		}
		else
		{
			/*Update gender 20200416 $data->sex to M*/
			$query	= "INSERT INTO CONTACT
						(
							NAME,CONTACT_TYPE,SEX,PHONE,B_ADDR1,B_ADDR2,STATUS,STORE_ID,ENT_DATE,ENT_USER,B_CITY,B_AREA
						)
						values
						(	'".$data->cust_name."','".$data->contact_type."','M','".$data->cust_phone."',
							'".str_replace("'","-",$data->order_address)."','','1','".$data->storeid."','".date("Y-m-d H:i:s")."','system','".$data->city."','".$data->provinci."')";
			
		
			if($result = $this->staging_db->query($query))
			{
						
				$contact_id	= $this->memberExist($data->cust_name,$data->cust_phone,$data->city,$data->provinci);	
				//echo $contact_id;exit;
				$slc_phone		= "select PHONE from CONTACT where id='".$contact_id."'";
				$result_phone 	= $this->staging_db->query($slc_phone)->row();
				$phone_no		= $result_phone->PHONE;
			}
			else
			{
				echo $query;
				exit;
			}
			
		}
		
		$order			= array(
									'member_id' => $data->cust_id,
									'contact_id'=> $contact_id,
									'phone_no'	=> $phone_no
								);
		return $this->pustTransaction_new($data,$order,$usr_id);
	}
	private function pustTransaction_new($data,$order,$usr_id){
		$sql_trx	=	"select * from trx_ctl Where trx_date = '".date("Y-m-d",strtotime($data->order_date))."' and TRX_STATUS = '0' ";
		try {	
			$row_trx   	= 	$this->staging_db->query($sql_trx)->row();
			if ($this->staging_db->query($sql_trx)->num_rows() == 0){		
				//echo date("Y-m-d",strtotime($data->order_date)) ."<". date("Y-m-d");
				if(date("Y-m-d",strtotime($data->order_date)) < date("Y-m-d")){
					$msg["responsecode"] = 25;
					$msg["message"]		 = "Transaction date is less than the current date";
				}else{	
					$msg["responsecode"] = 30;
					$msg["message"]		 = "Error Start of Day";
				}
				$savelog	= $this->save_log_transaction($this->data_post,'staging',date("Y-m-d H:i:s"),json_encode($msg),'400',$usr_id->user_id);
				return $this->response(json_encode($msg),400);
				exit;
				
			}else{
				$row_trx   	= 	$this->staging_db->query($sql_trx)->row();
				if($row_trx->TRX_ID == 0)
				{
					return ' {"statusCode":400, "message":"error start of day"}';
					exit;
				}
			}		
			
			$this->save_database($data,$row_trx,$order);
			
		}
		catch (Exception $e) {
			$msg["responsecode"] = 400;
			$msg["message"]		 = $e->getMessage();
			return $this->response(json_encode($msg),401);
			exit;
		
		}	
	}
	
	private function save_database($data,$row_trx,$order){
		
		//getcounterno
		$sql_count	=	"select * from TBL_ORDER_NO where trx_id = '".$row_trx->TRX_ID."' and store_id = '".$data->storeid."'";
		$row_count 	= 	$this->staging_db->query($sql_count)->row();
		$count		= 	$row_count->LAST_NO + 1;
		
		//getstatus
		$sql_status	= "select * from order_hdr where PHONE = '".$data->order_phone."'";
		$qry_status = $this->staging_db->query($sql_status);
		if($qry_status->num_rows() == 0)
		{
			$status_cust = 1;
		}
		else
		{
			$status_cust = 0;
		}
		foreach ($data->order_detail as $key=>$val)
		{
			$value				= explode("|",$val);
			$this->gross		= $this->gross + ($value[0] * $value[1]);
			
			
		}
		
		$tax_amount		= (ceil($this->gross / 1.1) + (ceil($data->delivery_charge / 1.1))) * 0.1;
		$net			= ($tax_amount + ceil($this->gross / 1.1) + (ceil($data->delivery_charge / 1.1))) - $data->nominal_disc ; 
		
		// insert header
		$idorder	= date("ymd",strtotime($data->order_date)).$data->storeid.".".str_pad($count,5,'0',STR_PAD_LEFT);
		
		
		$insert_hdr['ID']					= $idorder;
		$insert_hdr['TRX_ID']				= $row_trx->TRX_ID;
		$insert_hdr['CONTACT_ID']			= $order['contact_id'];
		$insert_hdr['NAME']					= $data->cust_name;
		$insert_hdr['STORE_ID']				= $data->storeid;
		$insert_hdr['SHIPPED_PHONE']		= $data->order_phone;
		$insert_hdr['SHIPPED_CONTACT']		= $data->order_name;
		$insert_hdr['SHIPPED_CONTACT_TYPE']	= $data->contact_type;
		$insert_hdr['SHIPPED_ADDR1']		= str_replace("'","-",$data->order_address);
		$insert_hdr['SHIPPED_CITY']			= $data->city;
		$insert_hdr['SHIPPED_AREA']			= $data->provinci;
		$insert_hdr['SHIPPED_DATE']			= $data->ship_date;
		$insert_hdr['BILL_STORE_ID']		= $data->storeid;
		$insert_hdr['BILL_CONTACT']			= $data->order_name;
		$insert_hdr['BILL_ADDR1']			= str_replace("'","-",$data->order_address);
		$insert_hdr['PHONE']				= $order['phone_no'];
		$insert_hdr['ENT_DATE']				= $data->order_date;
		$insert_hdr['ENT_USER']				= 'system';
		$insert_hdr['TAX_RATE']				= 10;
		$insert_hdr['TAX_AMOUNT']			= $tax_amount;
		$insert_hdr['GROSS']				= ceil($this->gross / 1.1);
		$insert_hdr['DELIVERY_COST']		= ceil($data->delivery_charge / 1.1);
		$insert_hdr['NET']					= ceil($net);
		$insert_hdr['CASH']					= ceil($net);
		$insert_hdr['ORDER_APPS_ID']		= $data->order_apps_id;
		$insert_hdr['TYPE_ORDER']			= $data->orderstatus;
		$insert_hdr['TYPE_SERVICE']			= $data->type_service;
		$insert_hdr['TYPE_PAYMENT']			= $data->type_payment;
		$insert_hdr['STATUS_PAYMENT']		= '01';
		$insert_hdr['STATUS']				= 1;
		$insert_hdr['SHIPPED_ADDR_TYPE']	= 'HOME';
		$insert_hdr['MSBS']					= 0;
		$insert_hdr['REC_DATE']				= $data->order_date;
		$insert_hdr['REV_NO']				= 0;
		$insert_hdr['NOTES']				= $data->order_notes."#".$data->no_voucher;
		$insert_hdr['status_cust']			= $status_cust;
		$insert_hdr['BILL_CITY']			= $data->city;
		$insert_hdr['BILL_AREA']			= $data->provinci;
		$insert_hdr['SALES_DISCOUNT']		= $data->nominal_disc;
		$insert_hdr['srv_charge_rate']		= $data->srv_charge_rate;
		$insert_hdr['srv_charge_amount']	= $data->srv_charge_amount;
		;
		
		try {	
			if($this->staging_db->insert('order_hdr',$insert_hdr)){
				//echo $this->staging_db->last_query();
				foreach ($data->order_detail as $key=>$val)
				{
					$value				= explode("|",$val);
					$price_before_tax	= ceil($value[1] / 1.1);
					
					$insert_dtl['ID']			= $idorder;
					$insert_dtl['PRODCODE']		= $key;
					$insert_dtl['QTY']			= $value[0];
					$insert_dtl['PRICE']		= $price_before_tax;
					$insert_dtl['AMOUNT']		= ($value[0] * $price_before_tax);
					$insert_dtl['ENT_USER']		= 'system';
					$insert_dtl['ENT_DATE']		= $data->order_date;
					$insert_dtl['STATUS']		= 1;
					$insert_dtl['DISC_ITEM']	= 0;
					try {
						if(!$this->staging_db->insert('ORDER_DTL',$insert_dtl)){
							//echo "<hr />";
							//echo $this->staging_db->last_query();
							$data_error			 = sqlsrv_errors();	
							$data_error['loc']	 = "Detail Transaction";
							$msg["responsecode"] = 35;
							$msg["message"]		 = $data_error;
							return $this->response(json_encode($msg),401);
							exit;
						}else{
							//echo "<hr />";
							//echo $this->staging_db->last_query();
						}
					}catch (Exception $e) {
						$msg["responsecode"] = 400;
						$e->loc				 = "Detail Transaction";
						$msg["message"]		 = $e->getMessage();
						return $this->response(json_encode($msg),401);
						exit;
					
					}
				} 
				
				$qry_update 	= "update TBL_ORDER_NO set LAST_NO = '".$count."' where trx_id = '".$row_trx->TRX_ID."' and store_id = '".$data->storeid."'";
				//echo $qry_update;
				try {
					
					if(!$this->staging_db->query($qry_update)){
						//echo "<hr />";
						//echo $this->staging_db->last_query();
						$data_error			 = sqlsrv_errors();	
						$msg["responsecode"] = 35;
						$data_error['loc']	 = "Table Order No Transaction";	
						$msg["message"]		 = $data_error;
						$savelog	= $this->save_log_transaction($this->data_post,'staging',date("Y-m-d H:i:s"),json_encode($msg),'401',$this->user_id);
						return $this->response(json_encode($msg),401);
						exit;
					}else{
						$msg["responsecode"] = 20;
						$msg["message"]		 = $idorder;
						$savelog	= $this->save_log_transaction($this->data_post,'staging',date("Y-m-d H:i:s"),json_encode($msg),'200',$this->user_id);
						return $this->response(json_encode($msg),200);
					}
				}catch (Exception $e) {
					$msg["responsecode"] = 400;
					$msg["message"]		 = $e->getMessage();
					$e->loc				 = "Table Order No Transaction";
					$savelog	= $this->save_log_transaction($this->data_post,'staging',date("Y-m-d H:i:s"),json_encode($msg),'401',$this->user_id);
					return $this->response(json_encode($msg),401);
					exit;
				}
			}else{
				
				$data_error			 = sqlsrv_errors();	
				$msg["responsecode"] = 35;
				$data_error['loc']	 = "Table Header Transaction";	
				$msg["message"]		 = $data_error;
				$savelog	= $this->save_log_transaction($this->data_post,'staging',date("Y-m-d H:i:s"),json_encode($msg),'401',$this->user_id);
				return $this->response(json_encode($msg),401);
				exit;
			}
		}catch (Exception $e) {
			$msg["responsecode"] = 400;
			$e->loc				 = "Table Header Transaction";
			$msg["message"]		 = $e->getMessage();
			$savelog	= $this->save_log_transaction($this->data_post,'staging',date("Y-m-d H:i:s"),json_encode($msg),'401',$this->user_id);
			return $this->response(json_encode($msg),401);
			exit;		
		}	
	}
	
	private function memberExist($name, $cellphone,$city,$provinci) {	
		$query = "SELECT * FROM CONTACT  WHERE name = '".$name."' AND phone = '".$cellphone."'";
		$row   = $this->staging_db->query($query)->row();
		if($row->B_CITY)
		{
			$id    = isset($row->ID) ? $row->ID : false;
		}
		else
		{
			$id    = isset($row->ID) ? $row->ID : false;
			$this->staging_db->query("update CONTACT set B_CITY = '".$city."', B_AREA = '".$provinci."' where id = '".$id."'",$this->mssql_handle);
		}
		
		return $id;
	}
	
	private function pushTransaction_proccess(){
		require_once APPPATH.'../HTTP_Request/HTTP/Request2.php';
		$request = new HTTP_Request2();
		$request->setUrl($this->url);
		$request->setMethod(HTTP_Request2::METHOD_POST);
		$request->setConfig(array(
		  'follow_redirects' => TRUE
		));
		$request->setHeader(array(
		  'Content-Type' => $this->ctype,
		  'api_auth_key' => $this->client_id
		));				
		$request->setBody($this->data_post);
		
		
		try {
			$response = $request->send();
			//print_r($response->getStatus());exit;
			if (($response->getStatus() == 200)) {
			
			$pushptp	= 	array(
								'status' => $response->getStatus(),
								'message' => $response->getBody()
							);										
			return json_encode($pushptp, TRUE);
			
		  }
		  else {
			//print_r($response);exit;
			$pushptp	= array(
							'status' => $response->getStatus(),
							'message' => $response->getBody());
				
			return json_encode($pushptp, TRUE);
			exit;
			
		  }
		}
		catch(HTTP_Request2_Exception $e) {
			
			$pushptp	= array(
							'status' => $e,
							'message' => "gagal");
			
			return json_encode($pushptp, TRUE);
			exit;
			
		}	
	}
	private function save_log_transaction($json_post,$device,$entry_date,$response,$response_code,$usr_id){		
		$sql	= "Insert into log_sales_osds (json_post,device,entry_date,response,response_code,usr_id)
					values ( '".$json_post."','".$device."','".$entry_date."','".$response."','".$response_code."','".$usr_id."')";
		//echo $sql;exit;
		if($this->db->query($sql)){
			return  0;
		}else{
			return 1;
		}
	}
}
