<?php

date_default_timezone_set("Asia/Bangkok");
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . 'libraries/REST_Controller.php';
class gw_sales extends CI_Controller {	
	
	public function __construct(){
		parent::__construct();
		if(!$this->session->userdata('id')){
			redirect('welcome');
		}
	}
	
	
    public function index()
    {
		$qry_gethistory				=	"select * from log_ovo order by transdate desc limit 3";
		$data['recentactivity']		= 	$this->db->query($qry_gethistory)->result_array();
		$data['PUSHP2P']			= 	$this->getRekapLog('PUSH P2P');
		$data['VoidPayment']		= 	$this->getRekapLog('Void Payment');
		$data['CheckPayment']		= 	$this->getRekapLog('Check Payment');
		$data['dashboard']			=   'class="active"';
		$this->load->view('ovo_dashboard',$data);
    }
	public function category($id)
    {
		$qry_gethistory				=	"select * from has_list_api where type = 2 order by name asc";
		$data['PUSHP2P']			= 	$this->getRekapLog('PUSH P2P');
		$data['VoidPayment']		= 	$this->getRekapLog('Void Payment');
		$data['CheckPayment']		= 	$this->getRekapLog('Check Payment');
		$data['list_category']		= 	$this->db->query($qry_gethistory)->result_array();
		$this->load->view('gwpg_dashboard',$data);
    }
	public function getRekapLog($transaction){
		$qry_gethistory		=	"select count(*) as total from log_ovo where transaction = '".$transaction."'";
		$total				= 	$this->db->query($qry_gethistory)->row();
		return $total->total;
	}
	public function documentation(){
		$qry_gethistory				=	"select * from has_list_api where type = 2 order by name asc";
		$data['list_category']		= 	$this->db->query($qry_gethistory)->result_array();
		$data['ovo']					=   'class="active"';
		$data['ovo_doc']				=   'class="active"';		
		$this->load->view('gwsales_doc',$data);
	}
	public function testing(){
		$qry_getkey					=	$this->db->query("select `key` as keyval from `keys` where user_id = '".$this->session->userdata('id')."'")->row();
		$qry_gethistory				=	"select * from has_list_api where type = 2 order by name asc";
		$data['list_category']		= 	$this->db->query($qry_gethistory)->result_array();
		$data['ovo']					=   'class="active"';
		$data['ovo_testing']			=   'class="active"';
		$data['valueheader']			=   $qry_getkey->keyval;
		$this->load->view('gwsales_testing',$data);
	}
	public function testing_proc(){
		$datajson	= $this->input->post('json_req');
		$key		= $this->input->post('key');
		//print_r(site_url()."/gw_salesproc/pushTransaction".$datajson."==".$key); exit;
		$pushdata 	= $this->send_http_request("https://103.94.170.71/index.php/gw_salesproc/pushTransaction",$datajson,$key);
		echo $pushdata;
		exit;
	}
	public function testing_proc_notdl(){
		$datajson	= $this->input->post('json_req');
		$key		= $this->input->post('key');
		//print_r(site_url()."/gw_salesproc/pushTransaction".$datajson."==".$key); exit;
		$pushdata 	= $this->send_http_request("https://103.94.170.71/index.php/gw_salesproc/pushTransactionnotdl",$datajson,$key);
		echo $pushdata;
		exit;
	}
	public function send_http_request($url,$data,$key){
		$ch = curl_init(); 
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch,CURLOPT_USERAGENT,'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);     
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		$headers = [
			'api_auth_key: '.$key
		];

		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		$return_out = curl_exec($ch); 
		curl_close($ch); 
		
		print_r($return_out);exit;
		if($return_out === false)
		{
			echo 'Curl error: ' . curl_error($ch);
		}
		else
		{
			$output = $return_out; 
		}
		
		
		
		return $output;
	}
	public function logtransaction($id){
		$qry_getmenu			=	"select * from has_list_api where type = 2 order by name asc";
		$data['list_category']	= 	$this->db->query($qry_getmenu)->result_array();
		$qry_gethistory			=	"select * from log_sales_osds where usr_id = '".$id."' order by entry_date";
		$data['listdata']		= 	$this->db->query($qry_gethistory)->result_array();
		$data['ovo']					=   'class="active"';
		$data['ovo_logtransaction']		=   'class="active"';
		$this->load->view('osds_log',$data);
	}
	public function save_log_transaction($json_post,$device,$entry_date,$response,$response_code,$usr_id){		
		$sql	= "Insert into log_sales_osds (json_post,device,entry_date,response,response_code,usr_id)
					values ( '".$json_post."','".$device."','".$entry_date."','".$response."','".$response_code."','".$usr_id."')";
		//echo $sql;exit;
		if($this->db->query($sql)){
			return  0;
		}else{
			return 1;
		}
	}
}
