<?php
	date_default_timezone_set("Asia/Jakarta");
	error_reporting(0);
	ini_set('display_errors', 0);

	require 'dbconnect.php';
	mysqli_query($conpos, "TRUNCATE TABLE tb_dualscreen");
	
	//getuser
	$getuser	= mysqli_query($conpos,"select * from tbluser where USER_1STLOGIN = 1" );
	$datauser	= mysqli_fetch_object($getuser);
	
	//getsession
	$getTrans	= mysqli_query($conpos,"select * from pos_init where CLOSING_STATUS = 'N' and USERID = '".$datauser->USER_ID."' " );
	$dataTrans	= mysqli_fetch_object($getTrans);
	
	$data['ID']					= $_POST['strukid'];	
	$data['RELEASE_HOLD']		= $_POST['release_hold_val'];	
	$data['RESET_NO']			= 0;
	
	$split_struk				= explode(".",$_POST['strukid']);
	$data['COUNTER_NO']			= $split_struk[2];
	$data['TRX_ID']				= $split_struk[1];
	
	$data['TRX_DATE']			= $_POST['trx_date'];	
	$data['BRANCH_ID']			= $_POST['branch_id'];			
	$data['POS_ID']				= $split_struk[0];	
	$data['ENT_DATE']			= date("Y-m-d H:i:s");	
	$data['ENT_USER']			= $datauser->USER_ID;	
	$data['SESSION_ID']			= $dataTrans->SESSION_ID;	
	$data['tmpvalue']			= $_POST['tmpvalue'];	
	//$data['TAX_RATE']			= $_POST['pajakresto'];	
	$data['TAX_RATE']			= 10;	
	$data['TAX']				= $_GET['tax'];	
	$data['HREF']				= $_GET['ref_void']; //1 : transaction success, H : Transaction Hold	
	if($_GET['ref_void'] != ''){
		$getTrans	= mysqli_query($conpos,"update sales_hdr SET HREF = '".$data['ID']."' where ID = '".$_GET['ref_void']."' " );
	}
	$data['STATUS']				= $_GET['status']; //1 : transaction success, H : Transaction Hold	
	$data['VALID_STATUS']		= $_GET['valid_status']; //1 : Transaction Success, x : Transaction VOid, 0 : Transaction Hold
	$data['VALID_DATE']			= date("Y-m-d H:i:s");
	
	$data['PRN_REV_NO']			= 1;
	
	$data['TRX_STATUS']			= $_GET['trx_status']; //CLOSE : Transaction Success , HOLD : Transaction HOLD
	$data['KD_PEMAKAI']			= $kd_pemakai;
	$data['PEMBULATAN']			= $_GET['pembulatan'];
	$data['TUNAI']				= $_GET['tunai'];
	$data['KEMBALI']			= $_GET['kembali'];
	$data['GRANTOTAL']			= $_GET['grandtotal'];
	$data['PROMOID']			= $_GET['promo_id'];
	$data['PROMOVAL']			= $_GET['promo_val'];
	$data['PAYMENT_TYPE']		= $_GET['promo_val'];
	$data['STATUS_TRANS']		= $_GET['status'];
	$data['VALID_STATUS']		= $_GET['valid_status'];
	$data['DELIVERY_NO']		= $_POST['layanan_pref'].$_POST['idrider'];
	$data['DELIVERY_ID']		= $_POST['delivery_id'];
	$data['DELIVERY_ID_MASK']	= $_POST['mask_delivery_id'];
	$data['EDC_VDR']			= $_GET['edc_vdr'];
	$data['NON_CASH']			= $_GET['non_tunai'];
	$data['TA_CHARGE']			= str_replace(",","",$_GET['ta_charge']);
	$data['REFERENSI_ID_PAY']	= $_GET['referensi_pay'];
	$data['SALES_DISCOUNT']		= $_GET['sales_discs'];
	$data['SUB_TOTAL']			= $_GET['subtotal'];
	$data['NIK_KAR']			= $_POST['nik_kar'];
	$data['DISCTYPE']			= $_POST['val_disc_type'];
	$data['GUEST']				= $_POST['guest_id'];
	$data['DISCVALUE']			= $_POST['val_disc_val'];
	
	$data['dp_amount_val']			= $_POST['dp_amount_val'];
	$data['dp_name_val']			= $_POST['dp_name_val'];
	$data['dp_date_val']			= $_POST['dp_date_val'];
	$data['dp_prodcode_val']		= $_POST['dp_prodcode_val'];
	$data['dp_trx_id']				= $_POST['dp_trx_id'];
	
	$data['CLIENT_ID']				= $_GET['CLIENT_ID'];
	
	$data['GROSS']				= $_GET['gross'];
	if($_POST['svc_type'] == ''){
		$data['SVCTYPE']			= 1;
	}
	else if($_POST['svc_type'] == 'persen'){
		$data['SVCTYPE']			= 1;
	}else if($_POST['svc_type'] == 'amount'){
		$data['SVCTYPE']			= 2;
	}else{
		$data['SVCTYPE']			= 1;
	}
	$data['TOTAL_COST']			= str_replace(",","",$_POST['service_charge']);
	$data['SVCVALUE']			= str_replace(",","",$_POST['svc_val']);
	
	$total_gross = 0;

	for($i = 0 ; $i <= count($_POST['PRICE']) - 1; $i++){
		//Format : [<prodcode>] = <id>|<prodname>|<qty>|<seq_no>|<price>|<type>|<disc>|<promoid>|<promodesc>|<disctype>|<discvalue>
	$data['detail'][$i] = $_POST['strukid']."|".$_POST['PRODNAME'][$i]."|".$_POST['QTY'][$i]."|".$_POST['SEQ_NO'][$i]."|".$_POST['PRICE'][$i]."|".$_POST['type'][$i]."|".$_POST['disc'][$i]."|".$_POST['PROMOID'][$i]."|".$_POST['PROMODESC'][$i]."|".$_POST['DISCTYPE'][$i]."|".$_POST['DISCVALUE'][$i]."|".$_POST['PRODCODE'][$i];
		
		$total_gross  = $total_gross  + ($_POST['QTY'][$i] * $_POST['PRICE'][$i]);
	}
	
	if($data['STATUS_TRANS'] != 'H'){
	
		$total_net	= $data['GROSS'] - $data['SALES_DISCOUNT'] + $data['TOTAL_COST'] + $data['TAX'] - $data['PEMBULATAN'] + $data['TA_CHARGE'] - $_GET['item_disc'];
		
	
		
		//echo $total_net ."!=". $data['GRANTOTAL'];exit;
															
		if($total_net != $data['GRANTOTAL']){
			$msg['responsecode']	= '10';
			//$msg['msg']	= 'Selisih Payment' .$total_net ."!=". $data['GRANTOTAL']."=====>".$data['GROSS'] ."-". $data['SALES_DISCOUNT'] ."+". $data['TOTAL_COST'] ."+". $data['TAX'] ."-". $data['PEMBULATAN'] ."+". $data['TA_CHARGE'];
			$msg['msg']	= json_encode($data);
			echo json_encode($msg);
			exit;
		}else if($data['GROSS'] != $total_gross){
			$msg['responsecode']	= '10';
			$msg['msg']	= 'Gross Dan Detail Tidak Sama'.$data['GROSS']  ."!=". $total_gross;
			echo json_encode($msg);
			exit;
		}else if($data['GRANTOTAL'] ==0){
			$msg['responsecode']	= '10';
			$msg['msg']	= 'Grand Total Tidak Boleh Kosong';
			echo json_encode($msg);
		}
	}
	//exit;
	//echo "aduh";
	$myfile = fopen("./datafiles/".str_replace(".","_",$_POST['strukid']).".txt", "w") or die("Unable to open file!");
	fwrite($myfile, json_encode($data));
	fclose($myfile);

	/*
	echo $url_api;
	echo "<br />";
	echo json_encode($data);
	exit;*/
	$url = $url_api;
	$ch = curl_init(); 
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch,CURLOPT_USERAGENT,'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);     
	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
	curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
	$return_out = curl_exec($ch); 
	curl_close($ch); 
	if($return_out === false)
	{
		$output	= 'Curl error: ' . curl_error($ch).$url;
	}
	else
	{
		$output = $return_out; 
	}
	
	function availableUrl($host, $timeout=1) { 
		$fp = fSockOpen($host, 80, $errno, $errstr, $timeout); 
		return $fp!=false;
	}
	
	if($data_kp->VALUE != ''){
		//echo "DISII".availableUrl($data_kp->VALUE);
		//exit;
		if(availableUrl($data_kp->VALUE) == 1){
			if(!$conskp = mysqli_connect($data_kp->VALUE, 'root', 'tahu1', 'storedb')){
				$errordb = 1;
			}else{
				$errordb = 0;
			}
			
				
			//mysqli_query("DELETE FROM prn_dtl where ID = '".$_POST['strukid']."'", $dataheader);
			$get_data = 0;
			for($i = 0 ; $i <= count($_POST['PRICE']) - 1; $i++){
			
				if($data_ta	= mysqli_query($conpos, "SELECT * FROM products_dtl WHERE PRODCODE = '".$_POST['PRODCODE'][$i]."' and TRX_CODE = '".$_POST['type'][$i]."' and KITCHEN_NOTIFY =  1")){
					$rowcount=mysqli_num_rows($data_ta);
					if($rowcount == 1){
						$data = "INSERT INTO
								prn_dtl
								(ID,PRODCODE,PRODNAME,QTY)
							values
								(	
									'".$_POST['strukid']."',
									'".$_POST['PRODCODE'][$i]."',
									'".$_POST['type'][$i]."-".$_POST['PRODNAME'][$i]."',
									'".$_POST['QTY'][$i]."'
								)";
						$get_data	= $get_data + 1;
						mysqli_query($conskp, $data);
					}
				}
			//echo  $data; exit;
				
			
			}
			if($get_data ==0){
				mysqli_query($conskp, "DELETE FROM prn_hdr WHERE ID = '".$_POST['strukid']."'");
			}else{
				//echo $errordb;exit;
			//mysqli_query("DELETE FROM prn_hdr where ID = '".$_POST['strukid']."'", $dataheader);
				$dataheader = "INSERT INTO
							prn_hdr
							(ID,POS_ID,VALID_DATE,PRN_STATUS)
						values
							(	
								'".$_POST['strukid']."',
								'".$_POST['pos_id']."',
								'".date("Y-m-d H:i:s")."',
								'0'
							)";
				//echo  $dataheader; exit;
					mysqli_query($conskp, $dataheader);
			}
		}
		else{	
			if($data['STATUS_TRANS'] != 'H'){
				$msg['responsecode']	= '12';
				$msg['msg']	= 'Koneksi Ke Kitchen Printer Gagal';
				$msg['msg_out']	= $output;
				echo json_encode($msg);
				exit;
			}
		}
		
	}
	//redeem point
	
	include("cust_point.php");
	//exit;
	echo  $output;
	

?>