<?php 
	ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

	session_start();
	date_default_timezone_set("Asia/Bangkok");
	include("dbconnect.php");
	include("template/header_h2h.php");

	//CEkStart Of day
	$get_sod 	= mysqli_query($conpos,"SELECT MAX(TRX_DATE) AS TRX_DATE FROM trx_ctl WHERE TRX_STATUS=0");
	if($data_sod	= mysqli_fetch_object($get_sod))
	{
		//echo "SELECT COUNTER_NO FROM trx_ctl WHERE TRX_DATE='".$data_sod->TRX_DATE."'";
		$sql_cekcounter = mysqli_query($conpos,"SELECT COUNTER_NO FROM trx_ctl WHERE TRX_DATE='".$data_sod->TRX_DATE."'");
		if($data_cekcounter	= mysqli_fetch_object($sql_cekcounter)){
			
			if($data_cekcounter->COUNTER_NO > 0){	
				echo "<script>
					Swal.fire({
					  title: 'ERROR!',
					  text: 'POS sudah ada transaksi',
					  icon: 'error',
					  confirmButtonText: 'CLOSE'
					}).then((result) => {
					  if (result.isConfirmed) {
						location.href='master_product.php?pr=product&active=master_product'
					  }
					});
				</script>";
			}else{
				
				//delete temporary
                $sql_deltemp = mysqli_query($conpos,"DELETE FROM ztemp_products");
				
				//get service
				$ch = curl_init(); 
				curl_setopt($ch, CURLOPT_URL, $url_websvc.'update_harga.php?storeid='.$kd_pemakai.'&tanggal='.date("Ymd",strtotime($data_sod->TRX_DATE)));
				curl_setopt($ch,CURLOPT_USERAGENT,'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
				curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);     
				curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
				curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
				$return_out = curl_exec($ch); 
				curl_close($ch); 
				if($return_out === false)
				{
					$output	= 'Curl error: ' . curl_error($ch);
				}
				else
				{
					$output = $return_out; 
				}
				$result[0]	= json_decode($output);
				//echo "<pre>";
				//print_r($result[0]);
				//echo "</pre>";
				
				foreach ($result[0] as $val) {
					foreach ($val as $key2) {
						$insert_temp = "INSERT INTO ztemp_products 
						(PRODCODE,PRODNAME,STATUS,PROMO_ID,PROMO_COUNT,PROD_GROUP_ID,TRX_CODE,UNIT_PRICE,LAST_UPD,LAST_UPD_USER,PRICE_AT,KET,AUDDATE,AUDUSER)
						VALUES 
						(
						'".$key2->NOPLU."',
						'".$key2->NMPLU."',
						'".$key2->STATUS."',
						'".$key2->PROMO_ID."',
						'".$key2->PROMO_COUNT."',
						'".$key2->PROD_GROUP_ID."',
						'".$key2->PLU_TYPE."',
						'".$key2->HARGA."',
						'".date("Y-m-d",strtotime($key2->LAST_DATE))."',
						'".$key2->LAST_USER."',
						'".$key2->HARGA."',
						'".$key2->KDHARGA."',
						'".date("Y-m-d")."',
						'POS WEB UPDATE'
						)";
						//echo $insert_temp;
						mysqli_query($conpos,$insert_temp);
						//exit;
					
						//exit;
					}
					
				}
				
				mysqli_query($conpos,"update products_dtl a inner join ztemp_products b
										on a.prodcode=b.prodcode and a.trx_code=b.trx_code 
											set a.unit_price=b.unit_price,
											a.last_upd=now(),
											a.last_upd_user='POS WEB UPDATE'
											where a.unit_price<>b.unit_price");
				
				echo "<script>
					Swal.fire({
					  title: 'SUCCESS!',
					  text: 'UPDATE HARGA BERHASIL',
					  icon: 'success',
					  confirmButtonText: 'CLOSE'
					}).then((result) => {
					  if (result.isConfirmed) {
						location.href='master_product.php?pr=product';
					  }
					});
				</script>";
			}
		}
	}
	else
	{
		echo "masuk sini";
		echo "<script>
		alert('masuk');
			Swal.fire({
			  title: 'ERROR!',
			  text: 'POS BELUM START OF DAY',
			  icon: 'error',
			  confirmButtonText: 'CLOSE'
			}).then((result) => {
			  if (result.isConfirmed) {
				location.href='master_product.php?pr=product';
			  }
			});
		</script>";
	}
	
	
	include("template/footer_h2h.php");
?>
