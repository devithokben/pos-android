<?php 
	session_start();
	date_default_timezone_set("Asia/Bangkok");
	include("dbconnect.php");
	include("template/header_h2h.php");
?>

	<link href="css/jquery.dataTables.min.css" rel="stylesheet">
	<div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="page-breadcrumb">
                <div class="row align-items-center">
					
					<div class="col-xs-12 col-md-12 col-sm-12" style="font-size:16px;">
						<hr />
						<form method="POST" action="<?php echo $_SERVER['PHP_SELF'] ?>">
						
						<h4>Silahkan Tekan Tombol Dibawah Untuk Mengupdate Discount</h4>
						<hr />
						<input type="submit" value="UPDATE NOW" name="submit" class="btn btn-warning" />
						</form>
						<hr />
					</div>
					<div class="col-xs-12 col-md-12 col-sm-12" id="information" >
						
					</div>
					
					<div class="col-xs-12 col-md-12 col-sm-12" >
						<table id="table_id" class="table table-striped table-bordered" style="width:100%">
							<thead>
								<tr>
									<th>ID</th>
									<th>Name</th>
									<th>ACTION</th>
								</tr>
							</thead>
							<tbody>
						<?php
							$get_prod 	= mysqli_query($conpos,"select * from tbl_lst_disc order by discid ASC");
							while($dataProd	= mysqli_fetch_object($get_prod)){
						?>
								<tr>
									<td><?php echo $dataProd->discid; ?></td>
									<td><?php echo $dataProd->discname; ?></td>
									<td><a class="btn btn-success" href="update_discount_detail.php?id=<?php echo $dataProd->discid; ?>&pr=discount">Detail</a></td>
								</tr>
						<?php
							}
						?>
							</tbody>
						</table>
					 </div>
				</div>
			</div>
		</div>
<?php include("template/footer_h2h.php");?>

<?php
	if($_POST){
		
        if(mysqli_query($conpos,"CALL sp_cleardiscount")){
			$ch = curl_init(); 
			curl_setopt($ch, CURLOPT_URL, $url_websvc.'discount.php?storeid='.$kd_pemakai);
			curl_setopt($ch,CURLOPT_USERAGENT,'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);     
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
			curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
			$return_out = curl_exec($ch); 
			curl_close($ch); 
			if($return_out === false)
			{
				$output	= 'Curl error: ' . curl_error($ch);
			}
			else
			{
				$output = $return_out; 
			}
			$result[0]	= json_decode($output);
			echo "<pre>";
			
		
			foreach ($result[0] as $key=>$val) {
					if($temtable != $key){
						$temtable = $key;
						//echo  "<span class='alert alert-info'>".$key." :: ";
						
						
						
						foreach ($result[0]->$key as $val2) {
							
							$newArray	= (array)$val2;	
							
							$field	= array_keys($newArray);
							$value	= array();
						
							//print_r($field);
							//print_r(array_values($newArray));
						
							for($loop = 0; $loop <= count($field) - 1;$loop++){
								if(is_object(array_values($newArray)[$loop])){
									//echo "masul";
									$value[$loop] = '';
								}else{
									if(
										(array_keys($newArray)[$loop] == 'startdate') ||
										(array_keys($newArray)[$loop] == 'enddate') 
									){
										//echo "masukdate";
										$value[$loop] = date("Y-m-d H:i:s",strtotime(array_values($newArray)[$loop]));
									}else{
										$value[$loop] = array_values($newArray)[$loop];
									}
								}
								
								//print_r($value);
							}
							
							
							
						
							$sql = "insert into ".$temtable." (".implode(",",array_keys($newArray)).") values ('".implode("','",$value)."')";
							//echo strtolower($sql);
							if(!mysqli_query($conpos,strtolower($sql))){
								echo "gagal".strtolower($sql);
							}								
						}
						//print_r($result[0]->$key);
						//echo  "Sukses Update :: </span>";
					}
                }
					echo "</pre>";
			echo "<script>					
					Swal.fire({
					  title: 'SUCCESS!',
					  text: 'Proses Update Discount Berhasil',
					  icon: 'success',
					  confirmButtonText: 'CLOSE'
					}).then((result) => {
					  if (result.isConfirmed) {
						$(document).ready(function() {
							location.href='update_discount.php?pr=discount';
						} );
					  }
					});
					//alert('Start Off Day Berhasil');
				</script>";
		}else{
			echo "<script>					
					Swal.fire({
					  title: 'GAGAL!',
					  text: 'Proses Update Promo GAGAL',
					  icon: 'error',
					  confirmButtonText: 'CLOSE'
					}).then((result) => {
					  if (result.isConfirmed) {
					//	location.href='dashboard.php';
					  }
					});
					//alert('Start Off Day Berhasil');
				</script>";
		}
	}
	
	
?>


<script src="<?php echo  $base_url ?>js/jquery.dataTables.min.js" type="text/javascript"></script>
<script>
	$.extend( true, $.fn.dataTable.defaults, {
		"searching": false
	} );
	$(document).ready(function() {
		$('#table_id').DataTable();
	} );
</script>