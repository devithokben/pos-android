 </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <script src="<?php echo  $base_url ?>assets/plugins/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
   
    <script src="<?php echo  $base_url ?>assets/plugins/bootstrap/dist/js/bootstrap.min.js"></script>
 
    <!--Menu sidebar -->
    <script src="<?php echo  $base_url ?>js/sidebarmenu.js"></script>
    <!-- ============================================================== -->
    <!-- This page plugins -->
    <!-- ============================================================== -->
   <!--c3 JavaScript -->
  
    <!--Custom JavaScript -->
    <script src="<?php echo  $base_url ?>js/custom.js"></script>
	
	<script src="<?php echo  $base_url ?>assets_front/vendor/jquery/jquery.min.js"></script>
	<script src="<?php echo  $base_url ?>assets_front/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
	<script src="<?php echo  $base_url ?>js/jquery-ui.js"></script>
  
  
	<script>
		  $( function() {
			$( "#datepicker" ).datepicker({
				 dateFormat:"yy-mm-dd",
			  });
		  } );
		
		$("#menu-toggle").click(function(e) {
		  e.preventDefault();
		  $("#wrapper").toggleClass("toggled");
		});
	</script>
	
</body>

</html>