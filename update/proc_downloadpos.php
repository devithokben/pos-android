<?php
include("dbconnect.php");
function array_to_xml($array, &$xml_user_info) {
    foreach($array as $key => $value) {
        if(is_array($value)) {
            if(!is_numeric($key)){
                $subnode = $xml_user_info->addChild("$key");
                array_to_xml($value, $subnode);
            }else{
                $subnode = $xml_user_info->addChild("item$key");
                array_to_xml($value, $subnode);
            }
        }else {
            $xml_user_info->addChild("$key",htmlspecialchars("$value"));
        }
    }
}

//creating object of SimpleXMLElement
$xml_user_info = new SimpleXMLElement("<?xml version=\"1.0\"?><SALES></SALES>");
//trx_ctl
$sql_trxctl_m	= " select * from trx_ctl where TRX_DATE = '".$_POST['tgl_trans']."' and TRX_STATUS != 0";
$get_trxctl_m 	= mysqli_query($conpos,$sql_trxctl_m);

if (mysqli_num_rows($get_trxctl_m) == 0){
	echo "<script>location.href='".$base_url."downloadpos.php?err=1'</script>";
	exit;
}
while ($row_trxctl = mysqli_fetch_assoc($get_trxctl_m)) {
 // $data['SALES_HDR'] = $row;
  $trxctl	= $xml_user_info->addChild("TRX_CTL");
  $trxctl->addAttribute('ID', $row_trxctl['TRX_ID']);
  $row_trxctl["TRX_DATE"]= date('m/d/Y h:i:s A', strtotime($row_trxctl["TRX_DATE"]));
  $row_trxctl["PRIOR_TRX_DATE"]= date('m/d/Y h:i:s A', strtotime($row_trxctl["PRIOR_TRX_DATE"]));
  $row_trxctl["OPENING_DATE"]= date('m/d/Y h:i:s A', strtotime($row_trxctl["OPENING_DATE"]));
  $row_trxctl["CLOSING_DATE"]	= 	date('m/d/Y h:i:s A', strtotime($row_trxctl["CLOSING_DATE"]));
  array_to_xml($row_trxctl,$trxctl);
}

$sql_hdr_m	= " select ID from sales_hdr where TRX_DATE = '".$_POST['tgl_trans']."' order by ENT_DATE ASC";
//echo $sql_hdr_m;
$get_hdr_m 	= mysqli_query($conpos,$sql_hdr_m);
$data = array();
while ($row = mysqli_fetch_assoc($get_hdr_m)) {
 // $data['SALES_HDR'] = $row;
  $struk	= $xml_user_info->addChild("STRUK");
  $struk->addAttribute('ID', $row['ID']);
  
  
	//sales_hdr
	$sql_hdr_d	= " select * from sales_hdr where ID = '".$row['ID']."' order by ENT_DATE ASC";
	//echo $sql_hdr_m;
	$get_hdr_d 	= mysqli_query($conpos,$sql_hdr_d);
	$data 		= array();
	$idheader	= 1;
	while ($row_hdr = mysqli_fetch_assoc($get_hdr_d)) {
		$sales_hdr	= $struk->addChild("SALES_HDR");
		$row_header	= $sales_hdr->addChild("ROW");
		$row_header->addAttribute('ID', $idheader);
		$row_hdr["TRX_DATE"]	= 	date('m/d/Y h:i:s A', strtotime($row_hdr["TRX_DATE"]));
		$row_hdr["ENT_DATE"]	= 	date('m/d/Y h:i:s A', strtotime($row_hdr["ENT_DATE"]));
		$row_hdr["VALID_DATE"]	= 	date('m/d/Y h:i:s A', strtotime($row_hdr["VALID_DATE"]));
		array_to_xml($row_hdr,$row_header);
		$idheader++;
	}
  
  
	//sales dtl
	$sql_dtl_d	= " select * from sales_dtl where ID = '".$row['ID']."'";
	//echo $sql_dtl_d;
	$get_dtl_d 	= mysqli_query($conpos,$sql_dtl_d);
	$data 		= array();
	$iddetail	= 1;
	while ($row_dtl = mysqli_fetch_assoc($get_dtl_d)) {
		$sales_dtl	= $struk->addChild("SALES_DTL");
		$row_detail	= $sales_dtl->addChild("ROW");		
		$row_detail->addAttribute('ID', $iddetail);
		array_to_xml($row_dtl,$row_detail);
		//echo $iddetail;
		$iddetail++;
	}
	
  
    /*sales payment*/
	$sql_pay_d	= " select * from sales_payment where ID = '".$row['ID']."'";
	//echo $sql_dtl_d;
	$get_pay_d 	= mysqli_query($conpos,$sql_pay_d);
	$data 		= array();
	$idpay		= 1;
	while ($row_pay = mysqli_fetch_assoc($get_pay_d)) {
		$sales_payment	= $struk->addChild("SALES_PAYMENT");
		$row_payment	= $sales_payment->addChild("ROW");		
		$row_payment->addAttribute('ID', $idpay);
		array_to_xml($row_pay,$row_payment);
		//echo $iddetail;
		$idpay++;
	}
	
	/*sales cancel*/
	$sql_cancel_d	= " select * from sales_cancel where ID = '".$row['ID']."'";
	//echo $sql_dtl_d;
	$get_cancel_d 	= mysqli_query($conpos,$sql_cancel_d);
	$data 		= array();
	$idcan		= 1;
	while ($row_can = mysqli_fetch_assoc($get_cancel_d)) {
		$sql_cancel	= $struk->addChild("SALES_CANCEL");
		$row_cancel	= $sql_cancel->addChild("ROW");		
		$row_cancel->addAttribute('ID', $idcan);
		$row_can["AUD_DATE"]	= 	date('m/d/Y h:i:s A', strtotime($row_can["AUD_DATE"]));
		array_to_xml($row_can,$row_cancel);
		//echo $iddetail;
		$idcan++;
	}
	
	/*sales print*/
	$sql_print_d	= " select * from sales_print where ID = '".$row['ID']."'";
	//echo $sql_print_d;
	$get_print_d 	= mysqli_query($conpos,$sql_print_d);
	$data 			= array();
	$idprint		= 1;
	while ($row_prn = mysqli_fetch_assoc($get_print_d)) {
		$sales_print	= $struk->addChild("SALES_PRINT");
		$row_print		= $sales_print->addChild("ROW");		
		$row_print->addAttribute('ID', $idprint);
		$row_prn["PRN_DATE"]	= 	date('m/d/Y h:i:s A', strtotime($row_prn["PRN_DATE"]));
		array_to_xml($row_prn,$row_print);
		//echo $iddetail;
		$idprint++;
	}
	
	/*sales vch*/
	$sql_vch_d		= " select * from sales_vch where ID = '".$row['ID']."'";
	//echo $sql_print_d;
	$get_vch_d 		= mysqli_query($conpos,$sql_vch_d);
	$data 			= array();
	$idvch		= 1;
	while ($row_vch = mysqli_fetch_assoc($get_vch_d)) {
		$sales_vch		= $struk->addChild("SALES_VCH");
		$row_voucher		= $sales_vch->addChild("ROW");		
		$row_voucher->addAttribute('ID', $idvch);
		$row_vch["PRN_DATE"]	= 	date('m/d/Y h:i:s A', strtotime($row_vch["PRN_DATE"]));
		array_to_xml($row_vch,$row_voucher);
		//echo $iddetail;
		$idvch++;
	}
 // array_to_xml($row,$sales_hdr);
}




$xml_file = $xml_user_info->asXML('data_xml/'.$pos_id."_".str_replace("-","_",$_POST['tgl_trans']).'.xml');

//success and error message based on xml creation
if($xml_file){
    echo 'XML file have been generated successfully.';
}else{
    echo 'XML file generation error.';
}
echo '<a href="dashboard.php">Back To Dashboard</a>';
?>