<?php 
	ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

	session_start();
	date_default_timezone_set("Asia/Bangkok");
	include "dbconnect.php";
	include("template/header_h2h.php");
	$get_prod 	= mysqli_query($conpos,"select * from tbl_promoh where PromoId = '".$_GET['id']."'");
	$dataProd	= mysqli_fetch_object($get_prod);
	//echo "select * from tbl_promoh where PromoId = '".$_GET['id']."'";
	//print_r($dataProd);
?>

	<link href="css/jquery.dataTables.min.css" rel="stylesheet">
	<div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="page-breadcrumb">
                <div class="row align-items-center">
					
					 <div class="card col-xs-12 col-md-12 col-sm-12" >
						<!-- Nav tabs -->
						<ul class="nav nav-tabs profile-tab" role="tablist">
							<li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#detail"
									role="tab">Detail Promo</a>
							</li>
							<li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#plugroup"
									role="tab">PLU Group</a> </li>
							<li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#location"
									role="tab">Location</a>
							</li>
							<li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#delivery"
									role="tab">Delivery</a>
							</li>
						</ul>
						<div class="tab-content">
							<div class="tab-pane active" id="detail" role="tabpanel">
								<div class="card-body">
									<div class="col-xs-12 col-md-12 col-sm-12" id="information" >
										<table width="100%" class="table table-striped table-bordered">
											<tr>
												<th>
													Promo Id
												</th>
												<th>
													Promo Name
												</th>
												<th>
													Description
												</th>
											</tr>
											<tr>
												<td>
													<?php echo $dataProd->PromoId ?>
												</td>
												<td>
													 <?php echo $dataProd->PromoName ?>
												</td>
												<td>
													<?php echo $dataProd->PromoDesc ?>
												</td>
											</tr>
										</table>
										<table width="100%" class="table table-striped table-bordered">
											<tr>
												<th colspan="4">
													Periode Date and Time
												</th>
											</tr>
											<tr>
												<td>
													Start Date
												</td>
												<td>
													End Date
												</td>
												<td>
													Start Time
												</td>
												<td>
													Start Time
												</td>
											</tr>
											<tr>
												<td>
													<?php echo date("d/m/Y",strtotime($dataProd->StartDate)) ?>
												</td>
												<td>
													 <?php echo date("d/m/Y",strtotime($dataProd->EndDate)) ?>
												</td>
												<td>
													 <?php echo date("H:i",strtotime($dataProd->StartTime)) ?>
												</td>
												<td>
													<?php echo date("H:i",strtotime($dataProd->EndTime)) ?>
												</td>
											</tr>
										</table>
										<table width="100%" class="table table-striped table-bordered">
											<tr>
												<th >
													Periode Day
												</th>
												<th >
													Term
												</th>
											</tr>
											<tr>
												<td>
													<?php 
														if(substr($dataProd->FreqDay,1,1) == 1){
															echo "<input type='checkbox' Checked disabled readonly> SENIN ";
														}else{
															echo "<input type='checkbox' disabled readonly> SENIN ";
														}
														if(substr($dataProd->FreqDay,2,1) == 1){
															echo "<input type='checkbox' Checked disabled readonly> SELASA ";
														}else{
															echo "<input type='checkbox' disabled readonly> SELASA ";
														}
														if(substr($dataProd->FreqDay,3,1) == 1){
															echo "<input type='checkbox' Checked disabled readonly> RABU ";
														}else{
						
						echo "<input type='checkbox' disabled readonly> RABU ";
														}
														if(substr($dataProd->FreqDay,4,1) == 1){
															echo "<input type='checkbox' Checked disabled readonly> KAMIS ";
														}else{
															echo "<input type='checkbox' disabled readonly> KAMIS ";
														}
														if(substr($dataProd->FreqDay,5,1) == 1){
															echo "<input type='checkbox' Checked disabled readonly> JUM'AT ";
														}else{
															echo "<input type='checkbox' disabled readonly> JUM'AT ";
														}
														if(substr($dataProd->FreqDay,6,1) == 1){
															echo "<input type='checkbox' Checked disabled readonly> SABTU ";
														}else{
															echo "<input type='checkbox' disabled readonly> SABTU ";
														}
														if(substr($dataProd->FreqDay,0,1) == 1){
															echo "<input type='checkbox' Checked disabled readonly> MINGGU ";
														}else{
															echo "<input type='checkbox' disabled readonly> MINGGU ";
														}
													?>
												</td>
												<td>
													Service Type : 
													<?php
														if(substr($dataProd->ServiceTypeVal,0,1) == 1){
															echo "<input type='checkbox' Checked disabled readonly> BP ";
														}else{
															echo "<input type='checkbox' disabled readonly> BP ";
														}
														if(substr($dataProd->ServiceTypeVal,1,1) == 1){
															echo "<input type='checkbox' Checked disabled readonly> DI ";
														}else{
															echo "<input type='checkbox' disabled readonly> DI ";
														}
														if(substr($dataProd->ServiceTypeVal,2,1) == 1){
															echo "<input type='checkbox' Checked disabled readonly> DL ";
														}else{
															echo "<input type='checkbox' disabled readonly> DL ";
														}
														if(substr($dataProd->ServiceTypeVal,3,1) == 1){
															echo "<input type='checkbox' Checked disabled readonly> DT ";
														}else{
															echo "<input type='checkbox' disabled readonly> DT ";
														}
														if(substr($dataProd->ServiceTypeVal,4,1) == 1){
															echo "<input type='checkbox' Checked disabled readonly> EX ";
														}else{
															echo "<input type='checkbox' disabled readonly> EX ";
														}
														if(substr($dataProd->ServiceTypeVal,5,1) == 1){
															echo "<input type='checkbox' Checked disabled readonly> TA ";
														}else{
															echo "<input type='checkbox' disabled readonly> TA ";
														}
														if(substr($dataProd->ServiceTypeVal,6,1) == 1){
															echo "<input type='checkbox' Checked disabled readonly> GF ";
														}else{
															echo "<input type='checkbox' disabled readonly> GF ";
														}
														if(substr($dataProd->ServiceTypeVal,7,1) == 1){
															echo "<input type='checkbox' Checked disabled readonly> GR ";
														}else{
															echo "<input type='checkbox' disabled readonly> GR ";
														}
														
													?>
													<br />
													Multiple / Kelipatan : 
													<?php
														if($dataProd->IsMultiply == 1){
															echo "<input type='checkbox' Checked disabled readonly> ";
														}else{
															echo "<input type='checkbox' disabled readonly> ";
														}
													?>
												</td>
											</tr>
										</table>
										<table width="100%" class="table table-striped table-bordered">
											<tr>
												<th width="50%">
													Payment 
												</th>
												<th>
													Delivery 
												</th>
											</tr>
											<tr>
												<td>
													<?php
														if($dataProd->PayTypeVal){
															$sqlPayment = mysqli_query($conpos,"select * from tbl_lst_bayard where PARAM IN ('".str_replace(",","','",$dataProd->PayTypeVal)."')");
															while($dataProdPay	= mysqli_fetch_object($sqlPayment)){
																echo "<span class='alert alert-success'>".$dataProdPay->DESCRIPTION."</span> &nbsp;";
															}	
														}else{
															echo "<span class='alert alert-success'>ALL PAYMENT CHANEL</span> &nbsp;";
														}
														
													?>
												</td>
												<td>
													<?php
														$sqlPayment = mysqli_query($conpos,"select * from tbl_lst_delivery where deliveryid IN ('".str_replace(",","','",$dataProd->DeliveryVal)."')");
														//echo "select * from tbl_lst_delivery where deliveryid IN ('".str_replace(",","','",$dataProd->DeliveryVal)."')";
														while($dataProdDel	= mysqli_fetch_object($sqlPayment)){
															echo "<span class='alert alert-danger'>".$dataProdDel->deliveryname."</span> &nbsp;";
														}
													?>
												</td>
											</tr>
										</table>
										<table width="100%" class="table table-striped table-bordered">
											<tr>
												<th colspan="4">
													Location
												</th>
											</tr>
											<tr>
												<td>
													<?php
														$loc = explode(",",$dataProd->LocationVal);
														for($i = 0; $i < count($loc); $i++ ){
															echo "<span class='alert alert-warning'>".$loc[$i]."</span> &nbsp;";
														}
													?>
												</td>
											</tr>
										</table>
									</div>
									<div class="col-xs-12 col-md-12 col-sm-12" >
										<table width="100%" class="table table-striped table-bordered">
											<tr>
												<th colspan="14">
													Promo Line
												</th>
											</tr>
											<tr>
												<td>
													Type
												</td>
												<td>
													Item Relation
												</td>
												<td>
													Sales Amount
												</td>
												<td>
													Vat
												</td>
												<td>
													QTY
												</td>
												<td>
													Varian
												</td>
												<td>
													Discount Type
												</td>
												<td>
													Discount Value
												</td>
												<td>
													Max Value
												</td>
												<td>
													Triger Row
												</td>
												<td>
													POP UP
												</td>
												<td>
													Match Qty
												</td>
												<td>
													Voucher Mask
												</td>
											</tr>
										<?php
											$get_prod_line 	= mysqli_query($conpos,"select * from tbl_promoline where PromoId = '".$_GET['id']."'");
											while($dataProd_line	= mysqli_fetch_object($get_prod_line)){
										?>
											<tr>
												<td>
													<?php echo $dataProd_line->LineType ?>
												</td>
												<td>
													<?php echo $dataProd_line->ItemRelation ?>
												</td>
												<td>
													<?php echo  number_format($dataProd_line->Amount) ?>
												</td>
												<td>
													<?php
													if(substr($dataProd_line->VAT,0,1) == 1){
														echo "<input type='checkbox' Checked disabled readonly> ";
													}else{
														echo "<input type='checkbox' disabled readonly> ";
													}
													?>
												</td>
												<td>
													<?php echo  number_format($dataProd_line->ItemQty) ?>
												</td>
												<td>
													<?php echo $dataProd_line->ItemVariant ?>
												</td>
												<td>
													<?php echo $dataProd_line->DiscType ?>
												</td>
												<td>
													<?php echo number_format($dataProd_line->DiscValue) ?>
												</td>
												<td>
													<?php echo  number_format($dataProd_line->DiscAmountMax) ?>
												</td>
												<td>
													ALL
												</td>
												<td>
													<?php echo $dataProd_line->IsPopup ?>
												</td>
												<td>
													<?php echo $dataProd_line->IsExactly ?>
												</td>
												<td>
													<?php echo substr($dataProd_line->EditMask,0,3) ?>
												</td>
											</tr>
										<?php	
											}
										?>
										</table>
									 </div>
								</div>
							</div>
							<div class="tab-pane" id="plugroup" role="tabpanel" >
								<div class="card-body">
									<div class="col-xs-12 col-md-12 col-sm-12" >
										<div class="row">
											<div class="col-xs-8 col-md-8 col-sm-8">
												<table width="100%" class="table table-striped table-bordered">
													<tr>
														<th>
															GROUP
														</th>
														<th>
															NAME
														</th>
														<th>
															ACTION
														</th>
													</tr>
												<?php
													$get_prod 	= mysqli_query($conpos,"select * from tbl_promo_grouph");
													while($dataProd	= mysqli_fetch_object($get_prod)){
												?>
													<tr>
														<td>
															<?php echo $dataProd->groupid ?>
														</td>
														<td>
															 <?php echo $dataProd->groupname ?>
														</td>
														<td>
															<a href="#" onclick="show_detail_group('<?php echo $dataProd->groupid ?>')">Show Detail</a>
														</td>
													</tr>
												<?php
													}
												?>
												</table>
											</div>
											
											<div class="col-xs-4 col-md-4 col-sm-4">
												<table  class="table table-striped table-bordered" style="width:100%">
													<thead>
														<tr>
															<th>PLU</th>
															<th>NAMA</th>
															<th>QTY</th>
															<th>HARGA</th>
														</tr>
													</thead>
													<tbody id="detail_groupd"></tbody>
												</table>
											</div>
										</div>
									</div>
									<script>
										function show_detail_group(groupid){
											$(document).ready(function(){
												$.ajax({
													type : 'GET',
													url : 'get_list_promogroup.php?groupid='+groupid+'&act=group',
													success : function(data){
														$("#detail_groupd").html(data);
													}
												})
											});
										}
										</script>
								</div>
							</div>
							<div class="tab-pane" id="location" role="tabpanel">
								<div class="card-body">
									<div class="col-xs-12 col-md-12 col-sm-12 row" >
										<div class="col-xs-8 col-md-8 col-sm-8">
											<table width="100%" class="table table-striped table-bordered" id="table_locd" >
												<thead>
													<tr>
														<th>
															ID
														</th>
														<th>
															DESCRIPTION
														</th>
														<th>
															ACTION
														</th>
													</tr>
												</thead>
												<tbody>
											<?php
												$get_prod 	= mysqli_query($conpos,"select * from tbl_promo_loch");
												while($dataProd	= mysqli_fetch_object($get_prod)){
											?>
												<tr>
													<td>
														<?php echo $dataProd->Id ?>
													</td>
													<td>
														 <?php echo $dataProd->Description ?>
													</td>
													<td>
														<a href="#" onclick="show_detail_location('<?php echo $dataProd->Id ?>')">Show Detail</a>
													</td>
												</tr>
											<?php
												}
											?>
												</tbody>
											</table>
										</div>
										<script>
										function show_detail_location(locid){
											$(document).ready(function(){
												$.ajax({
													type : 'GET',
													url : 'get_list_promogroup.php?locid='+locid+'&act=location',
													success : function(data){
														$("#detail_locd").html(data);
													}
												})
											});
										}
										</script>
										<div class="col-xs-4 col-md-4 col-sm-4">
											<table  class="table table-striped table-bordered" style="width:100%" id="table_locd_2">
												<thead>
													<tr>
														<th>STORE ID</th>
													</tr>
												</thead>
												<tbody id="detail_locd"></tbody>
											</table>
										</div>
									</div>
								</div>
							</div>
							<div class="tab-pane" id="delivery" role="tabpanel">
								<div class="card-body">
									<div class="col-xs-12 col-md-12 col-sm-12" >
										<table width="100%" class="table table-striped table-bordered" id="table_locd" >
											<thead>
												<tr>
													<th>
														
													</th>
													<th>
														
													</th>
													<th>
														
													</th>
													<th colspan="2" style="text-align:center">
														Service
													</th>
													<th colspan="4" style="text-align:center">
														Delivery
													</th>
												</tr>
												<tr>
													<th>
														Active
													</th>
													<th>
														ID
													</th>
													<th>
														Name
													</th>
													<th>
														Prefix
													</th>
													<th>
														Type
													</th>
													<th>
														Value
													</th>
													<th>
														Ask
													</th>
													<th>
														PLU
													</th>
													<th>
														Sign
													</th>
													<th>
														Minimal
													</th>
												</tr>
											</thead>
											<tbody>
										<?php
											$get_prod 	= mysqli_query($conpos,"select * from tbl_lst_delivery");
											while($dataProd	= mysqli_fetch_object($get_prod)){
										?>
											<tr>
													<td>
														<?php
															if($dataProd->enabled == 1){
																echo "<input type='checkbox' Checked disabled readonly> ";
															}else{
																echo "<input type='checkbox' disabled readonly> ";
															}
														?>
													</td>
													<td>														
														<?php echo $dataProd->deliveryid ?>
													</td>
													<td>
														<?php echo $dataProd->deliveryname ?>
													</td>
													<td>
														<?php echo $dataProd->prefix ?>
													</td>
													<td>
														<?php echo $dataProd->schargetype ?>
													</td>
													<td>
														<?php echo $dataProd->scharge ?>
													</td>
													<td>
														<?php
															if($dataProd->askdlcharge == 1){
																echo "<input type='checkbox' Checked disabled readonly> ";
															}else{
																echo "<input type='checkbox' disabled readonly> ";
															}
														?>
													</td>
													<td>
														<?php echo $dataProd->dlcharge ?>
													</td>
													<td>
														<?php
															if($dataProd->dlminchk == 1){
																echo "<input type='checkbox' Checked disabled readonly> ";
															}else{
																echo "<input type='checkbox' disabled readonly> ";
															}
														?>
													</td>
													<td>
														<?php echo $dataProd->dlminvalue ?>
													</td>
												</tr>
										<?php
											}
										?>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
<?php include("template/footer_h2h.php");?>



<script src="<?php echo  $base_url ?>js/jquery.dataTables.min.js" type="text/javascript"></script>
<script>
	$(document).ready(function() {
		$('#table_locd').DataTable();
	} );
</script>