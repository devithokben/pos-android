<style>
	.num-keypad{
		font-size : 20px;
		line-height : 20px;
	}
</style>
<div class="col-xs-12 col-md-12 col-sm-12"> 
	<div id="keypad" class="row">
		<button class="num-keypad num col-xs-2 col-md-2 col-sm-2" value="1">
			1
		</button>
		<button class="num-keypad num col-xs-2 col-md-2 col-sm-2" value="2">
			2
		</button>
		<button class="num-keypad num col-xs-2 col-md-2 col-sm-2" value="3">
			3
		</button>
		<button class="num-keypad num col-xs-2 col-md-2 col-sm-2" value="4">
			4
		</button>
		<button class="num-keypad num col-xs-2 col-md-2 col-sm-2" value="5">
			5
		</button>
		<button class="num-keypad num col-xs-2 col-md-2 col-sm-2" value="6">
			6
		</button>
		<button class="num-keypad num col-xs-2 col-md-2 col-sm-2" value="7">
			7
		</button>
		<button class="num-keypad num col-xs-2 col-md-2 col-sm-2" value="8">
			8
		</button>
		<button class="num-keypad num col-xs-2 col-md-2 col-sm-2" value="9">
			9
		</button>
		<button class="num-keypad num col-xs-2 col-md-2 col-sm-2" value="0">
			0
		</button>
		<button class="num-keypad col-xs-4 col-md-8 col-sm-4" onclick="cleartxt()" style="margin:0px none;padding:0px none;">
			CLR
		</button>	
	</div>
</div>