<?php 
date_default_timezone_set("Asia/Bangkok");
ini_set('display_errors', '0');
ini_set('display_startup_errors', '0');
error_reporting(0);
	
	require '../dbconnect.php';
	include("../template/header_h2h.php");
	
	
	
?>
		<div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="page-breadcrumb">
                <div class="row align-items-center">
					<div class="col-xs-12 col-md-12 col-sm-12">
		<a href="#" onclick="BtPrint(document.getElementById('pre_print').innerText)" style="font-size:16px;">Print Report</a>
		<form method="POST" >	
			<table style="font-size:14px;">
				<tr>
					<td>
						Kode Transaksi : 
					</td>
					<td>
						<?php
							$getlistdetail 		= mysqli_query($conpos,"select * from trx_ctl order by trx_date DESC");
							//;
							echo "<select name='trx_id'>";
							while($data	= mysqli_fetch_object($getlistdetail)){
								$trx_date = $data->TRX_DATE;
								print_r($data);
								echo "<option value='".$data->TRX_DATE."' style='font-size:16px;'>".date("d-m-Y",strtotime($data->TRX_DATE))." (".$data->TRX_ID.")</option>";
							}
							echo "</select>";
						?>
					</td>
					<td>
						<input type="submit" value="find data" />
					</td>
				</tr>
			</table>
		</form>
		<pre id="pre_print" class="col-xs-5 col-md-5 col-sm-5" style="font-size:14px;"> 
<?php
if($_POST){	
echo"
".str_pad("DISCOUNT LIST",0," ",STR_PAD_LEFT)."\n".str_pad($branchname,15," ",STR_PAD_LEFT)."\n".str_pad("POS ID ".$pos_id,0," ",STR_PAD_LEFT)."\n".str_pad("AS OF DATE ".date("d-m-Y",strtotime($trx_date)),8," ",STR_PAD_LEFT)."\n".str_pad("PRINT DATE ".date("d-m-Y H:i"),10," ",STR_PAD_LEFT);
$totalqty = 0;
$totalamt = 0;
echo "
--------------------------------\n";
$grand_total		= 0;
$getid_group 		= mysqli_query($conpos,"select * from (
select 'nominal_discount' AS `disc_type`,`posdb`.`sales_hdr`.`POS_ID` AS `pos_id`,case `posdb`.`sales_hdr`.`SDISC_RATE` when 5 then cast(`posdb`.`sales_hdr`.`SDISC_RATE` as char charset utf8mb4) when 50 then cast(`posdb`.`sales_hdr`.`SDISC_RATE` as char charset utf8mb4) when 10 then cast(`posdb`.`sales_hdr`.`SDISC_RATE` as char charset utf8mb4) else 'lain-lain' end AS `disc_rate`,count(0) AS `trx_count`,sum(`posdb`.`sales_payment`.`SALES_DISCOUNT`) AS `amount`,`posdb`.`trx_ctl`.`TRX_DATE` AS `trx_date`,`posdb`.`tbl_lst_branch`.`BRANCH_NAME` AS `branch_name`,`posdb`.`trx_ctl`.`TRX_STATUS` AS `closing_status`,`posdb`.`trx_ctl`.`CLOSING_DATE` AS `closing_date` from (((`posdb`.`sales_hdr` join `posdb`.`sales_payment` on(`posdb`.`sales_hdr`.`ID` = `posdb`.`sales_payment`.`ID`)) join `posdb`.`trx_ctl` on(`posdb`.`trx_ctl`.`TRX_ID` = `posdb`.`sales_hdr`.`TRX_ID`)) join `posdb`.`tbl_lst_branch` on(`posdb`.`sales_hdr`.`BRANCH_ID` = `posdb`.`tbl_lst_branch`.`BRANCH_ID`)) where `posdb`.`sales_hdr`.`TRX_STATUS` = 'close' group by `posdb`.`sales_hdr`.`POS_ID`,case `posdb`.`sales_hdr`.`SDISC_RATE` when 5 then cast(`posdb`.`sales_hdr`.`SDISC_RATE` as char charset utf8mb4) when 50 then cast(`posdb`.`sales_hdr`.`SDISC_RATE` as char charset utf8mb4) when 10 then cast(`posdb`.`sales_hdr`.`SDISC_RATE` as char charset utf8mb4) else 'lain-lain' end,`posdb`.`trx_ctl`.`TRX_DATE`,`posdb`.`tbl_lst_branch`.`BRANCH_NAME`,`posdb`.`trx_ctl`.`TRX_STATUS`,`posdb`.`trx_ctl`.`CLOSING_DATE` union select 'sales_discount' AS `disc_type`,`posdb`.`sales_hdr`.`POS_ID` AS `pos_id`,case `posdb`.`sales_hdr`.`SDISC_RATE` when 5 then cast(`posdb`.`sales_hdr`.`SDISC_RATE` as char charset utf8mb4) when 50 then cast(`posdb`.`sales_hdr`.`SDISC_RATE` as char charset utf8mb4) when 10 then cast(`posdb`.`sales_hdr`.`SDISC_RATE` as char charset utf8mb4) else 'lain-lain' end AS `disc_rate`,count(0) AS `trx_count`,sum(`posdb`.`sales_payment`.`SALES_DISCOUNT`) AS `amount`,`posdb`.`trx_ctl`.`TRX_DATE` AS `trx_date`,`posdb`.`tbl_lst_branch`.`BRANCH_NAME` AS `branch_name`,`posdb`.`trx_ctl`.`TRX_STATUS` AS `closing_status`,`posdb`.`trx_ctl`.`CLOSING_DATE` AS `closing_date` from (((`posdb`.`sales_hdr` join `posdb`.`sales_payment` on(`posdb`.`sales_hdr`.`ID` = `posdb`.`sales_payment`.`ID`)) join `posdb`.`trx_ctl` on(`posdb`.`trx_ctl`.`TRX_ID` = `posdb`.`sales_hdr`.`TRX_ID`)) join `posdb`.`tbl_lst_branch` on(`posdb`.`sales_hdr`.`BRANCH_ID` = `posdb`.`tbl_lst_branch`.`BRANCH_ID`)) where `posdb`.`sales_hdr`.`TRX_STATUS` = 'close' and `posdb`.`sales_hdr`.`SDISC_RATE` > 0 group by `posdb`.`sales_hdr`.`POS_ID`,case `posdb`.`sales_hdr`.`SDISC_RATE` when 5 then cast(`posdb`.`sales_hdr`.`SDISC_RATE` as char charset utf8mb4) when 50 then cast(`posdb`.`sales_hdr`.`SDISC_RATE` as char charset utf8mb4) when 10 then cast(`posdb`.`sales_hdr`.`SDISC_RATE` as char charset utf8mb4) else 'lain-lain' end,`posdb`.`trx_ctl`.`TRX_DATE`,`posdb`.`tbl_lst_branch`.`BRANCH_NAME`,`posdb`.`trx_ctl`.`TRX_STATUS`,`posdb`.`trx_ctl`.`CLOSING_DATE` union select `dsc`.`disc_type` AS `disc_type`,`dsc`.`pos_id` AS `pos_id`,`dsc`.`disc_rate` AS `disc_rate`,`dsc`.`trx_count` AS `trx_count`,sum(`dsc`.`amount`) AS `amount`,`dsc`.`trx_date` AS `trx_date`,`dsc`.`branch_name` AS `branch_name`,`dsc`.`closing_status` AS `closing_status`,`dsc`.`closing_date` AS `closing_date` from (select `posdb`.`sales_hdr`.`ID` AS `id`,'item discount' AS `disc_type`,`posdb`.`sales_hdr`.`POS_ID` AS `pos_id`,case `posdb`.`sales_dtl`.`DISC_ITEM` when 5 then cast(`posdb`.`sales_dtl`.`DISC_ITEM` as char charset utf8mb4) when 50 then cast(`posdb`.`sales_dtl`.`DISC_ITEM` as char charset utf8mb4) when 10 then cast(`posdb`.`sales_dtl`.`DISC_ITEM` as char charset utf8mb4) else 'lain-lain' end AS `disc_rate`,count(0) AS `trx_count`,`posdb`.`sales_payment`.`ITEM_DISCOUNT` AS `amount`,`posdb`.`trx_ctl`.`TRX_DATE` AS `trx_date`,`posdb`.`tbl_lst_branch`.`BRANCH_NAME` AS `branch_name`,`posdb`.`trx_ctl`.`TRX_STATUS` AS `closing_status`,`posdb`.`trx_ctl`.`CLOSING_DATE` AS `closing_date` from ((((`posdb`.`sales_dtl` join `posdb`.`sales_hdr` on(`posdb`.`sales_hdr`.`ID` = `posdb`.`sales_dtl`.`ID`)) join `posdb`.`sales_payment` on(`posdb`.`sales_hdr`.`ID` = `posdb`.`sales_payment`.`ID`)) join `posdb`.`trx_ctl` on(`posdb`.`trx_ctl`.`TRX_ID` = `posdb`.`sales_hdr`.`TRX_ID`)) join `posdb`.`tbl_lst_branch` on(`posdb`.`sales_hdr`.`BRANCH_ID` = `posdb`.`tbl_lst_branch`.`BRANCH_ID`)) where `posdb`.`sales_hdr`.`TRX_STATUS` = 'close' and `posdb`.`sales_dtl`.`DISC_ITEM` > 0 group by `posdb`.`sales_hdr`.`ID`,`posdb`.`sales_payment`.`ITEM_DISCOUNT`,`posdb`.`sales_hdr`.`POS_ID`,case `posdb`.`sales_dtl`.`DISC_ITEM` when 5 then cast(`posdb`.`sales_dtl`.`DISC_ITEM` as char charset utf8mb4) when 50 then cast(`posdb`.`sales_dtl`.`DISC_ITEM` as char charset utf8mb4) when 10 then cast(`posdb`.`sales_dtl`.`DISC_ITEM` as char charset utf8mb4) else 'lain-lain' end,`posdb`.`trx_ctl`.`TRX_DATE`,`posdb`.`tbl_lst_branch`.`BRANCH_NAME`,`posdb`.`trx_ctl`.`TRX_STATUS`,`posdb`.`trx_ctl`.`CLOSING_DATE`) `dsc` group by `dsc`.`disc_type`,`dsc`.`pos_id`,`dsc`.`disc_rate`,`dsc`.`trx_count`,`dsc`.`trx_date`,`dsc`.`branch_name`,`dsc`.`closing_status`,`dsc`.`closing_date` 

) as xxx WHERE trx_date = '".$_POST['trx_id']."' group by disc_type");
//echo "select * from vw_discount WHERE TRX_ID = '".$_POST['trx_id']."' group by disc_type";
while($datatrx_group	= mysqli_fetch_object($getid_group)){ 
echo"".str_pad("<strong>".$datatrx_group->disc_type."</strong>",32," ",STR_PAD_RIGHT)."\n";
$subtotal			= 0;
$getid_detail 		= mysqli_query($conpos,"select * from vw_discount WHERE trx_date = '".$_POST['trx_id']."' and disc_type = '".$datatrx_group->disc_type."'");
while($datatrx_detail	= mysqli_fetch_object($getid_detail)){ 
if (is_numeric($datatrx_detail->disc_rate)) {
echo "".str_pad(number_format($datatrx_detail->disc_rate)."%",15," ",STR_PAD_RIGHT)."".str_pad(number_format($datatrx_detail->trx_count),10," ",STR_PAD_RIGHT)."".str_pad(number_format($datatrx_detail->amount),15," ",STR_PAD_LEFT)."\n";
}else{
echo "".str_pad($datatrx_detail->disc_rate,15," ",STR_PAD_RIGHT)."".str_pad(number_format($datatrx_detail->trx_count),10," ",STR_PAD_RIGHT)."".str_pad(number_format($datatrx_detail->amount),15," ",STR_PAD_LEFT)."\n";
}
$subtotal	= $subtotal + $datatrx_detail->amount;
}
echo "--------------------------------\n".str_pad("SUB TOTAL",5," ",STR_PAD_LEFT)."".str_pad(number_format($subtotal),20," ",STR_PAD_LEFT)."\n--------------------------------\n";
	$grand_total	= $grand_total + $subtotal;
}

echo "--------------------------------\n".str_pad("GRAND TOTAL",5," ",STR_PAD_LEFT)."".str_pad(number_format($grand_total),20," ",STR_PAD_LEFT)."\n--------------------------------\n";
}
echo "
".chr(29)."V".chr(66).chr(3);
?>

		</pre>

	</div>
	</div>
	</div>
	</div>
<?php include("../template/footer_h2h.php");?>
<script>

window.onload = function() {
//alert("masuk");

		//location.href = "system_pos.php";
		//window.close();
   //BtPrint(document.getElementById('pre_print').innerText);
}
function BtPrint(prn){
		var textEncoded = encodeURI(prn);
		Android.showToast(textEncoded);
		/*
        var S = "#Intent;scheme=rawbt;";
        var P =  "package=ru.a402d.rawbtprinter;end;";
        var textEncoded = encodeURI(prn);
        window.location.href="intent:"+textEncoded+S+P;*/
}
    
</script>