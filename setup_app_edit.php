<?php 
	session_start();
	date_default_timezone_set("Asia/Bangkok");
	include("dbconnect.php");
	include("template/header_h2h.php");
	//print_r($_SESSION);
	if ($_GET['id']){
		$sql = "SELECT * FROM ap_setup where ID = '".$_GET['id']."'";
		//echo $sql;
		if($qry = mysqli_query($conpos,$sql)){	
			$row_data = mysqli_fetch_object($qry);
		}else{
			echo "<script>					
						Swal.fire({
						  title: 'GAGAL!',
						  text: 'Koneksi Terputus',
						  icon: 'error',
						  confirmButtonText: 'CLOSE'
						}).then((result) => {
						  if (result.isConfirmed) {
							$(document).ready(function() {
								location.href='setup_app.php';
							} );
						  }
						});
						//alert('Start Off Day Berhasil');
					</script>";
		}
	}else if ($_GET['act']){
		$sql = "UPDATE ap_setup SET
					DESCRIPTION = '".$_POST['description']."',
					VALUE = '".trim($_POST['value'])."' where ID = '".$_POST['id']."'";
		//echo $sql;exit;
		if(mysqli_query($conpos,$sql)){	
			echo "<script>					
						Swal.fire({
						  title: 'SUKSES!',
						  text: 'UPDATE BERHASIL',
						  icon: 'success',
						  confirmButtonText: 'CLOSE'
						}).then((result) => {
						  if (result.isConfirmed) {
							$(document).ready(function() {
								location.href='setup_app.php';
							} );
						  }
						});
						//alert('Start Off Day Berhasil');
					</script>";
		}else{
			echo "<script>					
						Swal.fire({
						  title: 'GAGAL!',
						  text: 'UPDATE GAGAL',
						  icon: 'error',
						  confirmButtonText: 'CLOSE'
						}).then((result) => {
						  if (result.isConfirmed) {
							$(document).ready(function() {
								location.href='setup_app.php';
							} );
						  }
						});
						//alert('Start Off Day Berhasil');
					</script>";
		}
	}	
?>
	<div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="page-breadcrumb">
                <div class="row align-items-center">
					
					<div class="col-xs-12 col-md-12 col-sm-12">
						<div class="row">
							<div class="col-lg-12 col-md-12">
							
								<div class="card">
									<div class="card-body">
										<h3 class="card-title">Detail Configuration </h3>
									</div>
									<div>
										<hr class="m-t-0 m-b-0">
									</div>
									<div class="card-body row">
										<form class="form-horizontal form-material" method="POST" action="setup_app_edit.php?act=update">
											<div class="row">
												<div class="form-group col-md-12">
													<label class="col-md-12">DESCRIPTION</label>
													<div class="col-md-12">
														<input type="hidden" name="id" value="<?php echo $row_data->ID ?>">
														<input type="text" name="description" value="<?php echo $row_data->DESCRIPTION ?> "
															class="form-control character form-control-line pl-0">
													</div>
												</div>
											
												<div class="form-group col-md-12">
													<label class="col-md-12">VALUE</label>
													<div class="col-md-12">
														<?php
															if($_GET['id'] == 'U_NC'){
														?>
															<input type="text" name="value" value="<?php echo trim($row_data->VALUE)?>"
															class="form-control character form-control-line pl-0">
															ex: jika tidak ada koneksi ke new counter ketikan <b>localhost</b>
														<?php
															}else if($_GET['id'] == 'U_KP'){
														?>
															<input type="text" name="value" value="<?php echo trim($row_data->VALUE)?>"
															class="form-control character form-control-line pl-0">
															ex: Jika tidak di gunakan maka <b>kosongkan dan jangan diisi apapun</b>
														<?php
															}else{
														?>
															<input type="text" name="value" value="<?php echo trim($row_data->VALUE)?>"
															class="form-control character form-control-line pl-0">
														<?php
															}
														?>
													</div>
												</div>
												<div class="form-group col-md-12">
													<div class="col-md-12">
														<input type="submit" value="Update" class="btn btn-warning" />
													</div>
												</div>													
												
											</div>		
                                        </form>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
<?php include("template/footer_h2h.php");?>
<script src="Keyboard/js/jquery.keyboard.js"></script>
<!-- keyboard extensions (optional) -->
<script src="Keyboard/js/jquery.mousewheel.js"></script>
<script>
	$(function(){
		$('.numeric').keyboard({layout : 'num' });
		$('.character').keyboard({layout: 'qwerty'});
	});
</script>