<?php 
require 'dbconnect.php';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
<link href="css/style-pos.css" rel="stylesheet" type="text/css">
<link href="css/bootstrap.css" rel="stylesheet" type="text/css">
<script src="js/jquery.js" type="text/javascript"></script>
<script src="js/bootstrap.js" type="text/javascript"></script>
<style>
*:fullscreen
	*:-ms-fullscreen,
	*:-webkit-full-screen,
	*:-moz-full-screen {
	   overflow: auto !important;
	   background:#fff;
	}
	body {
	  background: url("acak.jpg");
	  color: #000; }

	html:-webkit-full-screen-ancestor {
	  background-color: inherit; }

	html:-moz-full-screen-ancestor {
	  background-color: inherit; }
</style>
<script>
window.onload = function() {
	
	var screen,
		output,
		limit,
    
    screen = document.getElementById("result");
	var elem = document.querySelectorAll(".num");
    var len = elem.length;
    
    for(var i = 0; i < len; i++ ) {
        elem[i].addEventListener("click",function() {
			 num = this.value;
			 output = screen.innerHTML += num;
			 limit = output.length;
			 if(limit > 16 ) {
				alert("Sorry no more input is allowed");
			 }
			 else if(limit == 1 && num == 0) {
				screen.innerHTML = '';
			 }
     	},false);
    }
	
	$('#totalpayval').val(0); 
}

function cleartxt(){
	$(result).html('');
}

function menuitem(i){
    divObj = document.getElementById("result");
	
    var nm = $('#name'+i).val();
	var pr = $('#price'+i).val();
	
	
	if ( divObj.textContent ){ // FF
    	var div = parseInt(divObj.textContent);
    }else{  // IE           
    	var div = parseInt(divObj.innerText);
    }
	
	if(div > 0){
		qty = div;
	}
	else{
		qty = 1;
	}
	
	total = qty * pr;
	unique = Date.now();
	console.log(qty);
	$('#list').append('<tr tabindex="'+i+'" id="un'+unique+'" class="finishclear" onclick=tmpvalue("'+unique+'")><td width="9%" align="left">'+i+'</td><td width="30%" align="left">'+nm+'</td><td width="15%" align="right">'+qty+'</td><td width="15%" align="right">'+numberWithCommas(pr)+'</td><td width="15%" align="right" class="totalprice">'+numberWithCommas(total)+'</td></tr>');
	
	totalpay(total, 0)
	
	cleartxt();
}

function totalpay(ttl, tmin){
	TotalValue = 0;
	
	lasttotal = $('#totalpayval').val();
	
	if(tmin == 0){
		TotalValue = parseFloat(lasttotal) + parseFloat(ttl);
	}
	else if(tmin == 1){
		TotalValue = parseFloat(lasttotal) - parseFloat(ttl);
	}
	
	if(TotalValue !== 0){
    	$('#totalpay').html('SUB TOTAL = '+numberWithCommas(TotalValue));
	}
	else{
		$('#totalpay').html('');
	}
	$('#totalpayval').val(TotalValue);
}

function tmpvalue(v){
	$('#tmpvalue').val(v);
	//$('#un'+v).hide();
}

function itemvoid(){
	valselect = $('#tmpvalue').val();
	if(parseInt(valselect) !== 0){
		mintotal = parseFloat($('#un'+valselect+' .totalprice').text());
		totalpay(mintotal, 1);
		$('#un'+valselect).html('');
		$('#tmpvalue').val(0);
	}
	else{
		alert('No item select');
	}
}
$.ajax({
	url: 'product.php?id=01',				
	dataType: 'html',
	success: function (data){
		$('#rtvmenu').html(data);
	}
});	
function catproduct(id){
    $.ajax({
		url: 'product.php?id='+id,				
		dataType: 'html',
		success: function (data){
			$('#rtvmenu').html(data);
		}
	});	
}

function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}
</script>


</head>
	
<body>
	<div style="background-color:#fff;">
		<div class="row">
			<div class="col-lg-6 col-xlg-6 col-md-6">
				<button class="requestfullscreen btn btn-primary" id="chk_endingbalance" onclick="toggleFullScreen(document.body)" type="button">
					<i class="fa fa-expand fa-2x"></i>
				</button> 
				<div class="">
					<div class="col-lg-12 col-xlg-12 col-md-12" style="height:500px">
						<div id="">
							<div id="header-left-info">
								<div class="digit totalpay">
									<span id="totalpay"></span>
								</div> 
							</div>
							<div id="order-list">
								<table width="100%" cellpadding="0" cellspacing="0" id="info-header">
									<tr>
										<td>Date : <?=date('d F Y')?></td>
										<td>POS Name : Drive Thru</td>
										<td>Drive Thru 24 Jam</td>
									</tr>
								</table>
								<table width="100%" cellpadding="0" cellspacing="0" class="table-list">
									<tr>
										<th width="9%">PLU</th>
										<th width="30%">Item Name</th>
										<th width="15%" align="right">Qty</th>
										<th width="15%" align="right">Price</th>
										<th width="15%" align="right">Total</th>
										<th width="1%">&nbsp;</th>
									</tr>
								</table>
								<div id="table-list-order">
									<table width="100%" cellpadding="0" cellspacing="0" class="table-list" id="table-list">
										<tbody id="list">
										</tbody>
									</table>
									<input type="hidden" id="tmpvalue" value="0"/>
									<input type="hidden" id="totalpayval" value="0"/>
								</div>
							</div>
						</div>
					</div>
					<div class="col-lg-12 col-xlg-12 col-md-12"  style="height:50px">
						<div id="header-left-info2">
							<div class="digit">
								<div id="result"></div>
							</div> 
						</div>
					</div>
					<div class="col-lg-12 col-xlg-12 col-md-12"  style="height:150px">
						<div class="row">
							<div class="col-lg-10 col-xlg-10 col-md-10"  style="height:150px">
								<div id="keypad">
									<button class="num-keypad num col-lg-4 col-xlg-4 col-md-4" value="1">
										1
									</button>
									<button class="num-keypad num col-lg-4 col-xlg-4 col-md-4" value="2">
										2
									</button>
									<button class="num-keypad num col-lg-4 col-xlg-4 col-md-4" value="3">
										3
									</button>
									<button class="num-keypad num col-lg-4 col-xlg-4 col-md-4" value="4">
										4
									</button>
									<button class="num-keypad num col-lg-4 col-xlg-4 col-md-4" value="5">
										5
									</button>
									<button class="num-keypad num col-lg-4 col-xlg-4 col-md-4" value="6">
										6
									</button>
									<button class="num-keypad num col-lg-4 col-xlg-4 col-md-4" value="7">
										7
									</button>
									<button class="num-keypad num col-lg-4 col-xlg-4 col-md-4" value="8">
										8
									</button>
									<button class="num-keypad num col-lg-4 col-xlg-4 col-md-4" value="9">
										9
									</button>
									<button class="num-keypad num col-lg-4 col-xlg-4 col-md-4" value="0">
										0
									</button>
									<button class="num-keypad col-lg-4 col-xlg-4 col-md-4" onclick="cleartxt()">
										CLEAR
									</button>	
									<div class="nav-pos-bot col-lg-4 col-xlg-4 col-md-4">
										<div class="row">
											<a href="javascript:void(0)" class="menu-logout col-lg-12 col-xlg-12 col-md-12" style="background:#C3030B;line-height: 31px" onclick="itemvoid()">
												Item Delete
											</a>
										</div>
									</div>									
								</div>
							</div>
							<div class="col-lg-2 col-xlg-2 col-md-2"  style="height:150px">								
								<button class="num-keypad enter" id="eqn-bg" value="=" onclick="mypayBtn()">
									Enter
								</button>
							</div>
						</div>
					</div>	
				</div>
			</div>
			<div class="col-lg-6 col-xlg-6 col-md-6">
				<div class="col-lg-12 col-xlg-12 col-md-12" style="height:500px">
					<div id="menu-content">
						<span id="rtvmenu"></span>
					</div>
				</div>
				<div class="col-lg-12 col-xlg-12 col-md-12" style="height:200px">
					<div id="user-content">
						<?php 
						$cat = mysqli_query($conpos, "SELECT * FROM tblgmnu");
						while($ct = mysqli_fetch_object($cat)){
						?>
						<a class="menu-void" onclick="catproduct('<?=$ct->ID?>')" style="background:#333; font-size:13px; font-family:Tahoma, Geneva, sans-serif;font-weight:bold;">
							<?=$ct->DESCRIPTION?>
						</a>
						<?php }?>
						
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="mypayModal" class="pay-modal">

	  <!-- Modal content -->
	  <div class="pay-modal-content">
		<span class="pay-close" onclick="spanpay()">&times;</span>
		<p>
			<h3 style="font-weight:bold;">TOTAL</h3>
			<div align="center" style="font-size:16px; margin-bottom:5px;">
			<table width="100%">
				<tr>
					<td>SUB TOTAL </td><td align="right" style="font-weight:bold">Rp <span id="ttlpay"></span></td>
				</tr>
				<tr>
					<td>PAJAK RESTO 10%</td><td align="right" style="font-weight:bold">Rp <span id="pjkpay"></span></td>
				</tr>
				<tr>
					<td>TOTAL </td><td align="right" style="font-weight:bold">Rp <span id="grtotal"></span></td>
				</tr>
			</table>
			</div>
			<div>
				<button style="background:#0099FF; width:100%; font-size:14px; color:#FFF; padding:10px; font-weight:bold;" onclick="processpay()">SUBMIT</button>
			</div>
		</p>
	  </div>

	</div>
	<script>
		
		// When the user clicks the button, open the modal 
		function mypayBtn() {
			total = $('#totalpayval').val();
			pjk = Math.round((10 * parseFloat(total))/100);
			grtotal = parseFloat(total) + pjk;
			if(parseInt(total) !== 0){
				$('#ttlpay').html(numberWithCommas(parseFloat(total)));
				$('#pjkpay').html(numberWithCommas(pjk));
				$('#grtotal').html(numberWithCommas(parseFloat(grtotal)));
				var modalpay = document.getElementById('mypayModal');
				modalpay.style.display = "block";
			}
			else{
				return false;
			}
		}
		
		function processpay(){
			ttl = $('#totalpayval').val();
			$.ajax({
				type : 'post',
				dataType: "json",
				url : 'processpay.php',
				data :  {"ttl": ttl},
				success : function(data){
					$('.finishclear').html('');
					$("#totalpay").html('SUCCESS :: THANK YOU');
					$('#totalpayval').val(0);
					$('#tmpvalue').val(0);
					spanpay();
				}
			});
		}
		
		// When the user clicks on <span> (x), close the modal
		function spanpay() {
			var modalpay = document.getElementById('mypayModal');
			modalpay.style.display = "none";
		}
		
		// When the user clicks anywhere outside of the modal, close it
		window.onclick = function(event) {
			var modalpay = document.getElementById('mypayModal');
			if (event.target == modalpay) {
				modalpay.style.display = "none";
			}
		}
	</script>
	<script>
function toggleFullScreen(elem) {
	  if ((document.fullScreenElement !== undefined && document.fullScreenElement === null) || (document.msFullscreenElement !== undefined && document.msFullscreenElement === null) || (document.mozFullScreen !== undefined && !document.mozFullScreen) || (document.webkitIsFullScreen !== undefined && !document.webkitIsFullScreen)) {
		if (elem.requestFullScreen) {
		  elem.requestFullScreen();
		} else if (elem.mozRequestFullScreen) {
		  elem.mozRequestFullScreen();
		} else if (elem.webkitRequestFullScreen) {
		  elem.webkitRequestFullScreen(Element.ALLOW_KEYBOARD_INPUT);
		} else if (elem.msRequestFullscreen) {
		  elem.msRequestFullscreen();
		}
	  } else {
		if (document.cancelFullScreen) {
		  document.cancelFullScreen();
		} else if (document.mozCancelFullScreen) {
		  document.mozCancelFullScreen();
		} else if (document.webkitCancelFullScreen) {
		  document.webkitCancelFullScreen();
		} else if (document.msExitFullscreen) {
		  document.msExitFullscreen();
		}
	  }
	}
</script>
</body>
</html>