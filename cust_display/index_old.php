<?php
	
	require '../dbconnect.php';
	date_default_timezone_set("Asia/Bangkok");
	error_reporting(0);
	ini_set('display_errors', 1);
	$product 	= mysqli_query($conpos, "SELECT * FROM tb_dualscreen ORDER BY seq_no DESC LIMIT 1");
	$last_item	= mysqli_fetch_object($product);
	//include 'bar128.php';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>Untitled Document</title>
		<link href="../css/bootstrap.css" rel="stylesheet" type="text/css">
		<style>
			body{
				font-family: 'Roboto Condensed', sans-serif;
			}
			.hedding{
				font-size: 20px;
				color:#ab8181`;
			}
			.main-section{
				position: absolute;
				left:50%;
				right:50%;
				transform: translate(-50%,5%);
			}
			.left-side-product-box img{
				width: 100%;
			}
			.left-side-product-box .sub-img img{
				margin-top:5px;
				width:83px;
				height:100px;
			}
			.right-side-pro-detail span{
				font-size:15px;
			}
			.right-side-pro-detail p{
				font-size:25px;
				color:#a1a1a1;
			}
			.right-side-pro-detail .price-pro{
				font-size:30px;
				color:#E45641;
				font-weight:bold;
			}
			.right-side-pro-detail .tag-section{
				font-size:18px;
				color:#5D4C46;
			}
			.pro-box-section .pro-box img{
				width: 100%;
				height:200px;
			}
			@media (min-width:360px) and (max-width:640px) {
				.pro-box-section .pro-box img{
					height:auto;
				}
			}
			</style>
		<script src="../js/jquery.js" type="text/javascript"></script>
		<script src="../js/bootstrap.js" type="text/javascript"></script>
		<script>
		$(document).ready(function(){
			setTimeout(function(){ 
				location.reload();
			},3000);
		})
		</script>
	</head>
	<body>
	
	<?php 


	$getId 		= mysqli_query($conpos,"select * from trx_ctl where trx_status = 0");
	$dataTrx	= mysqli_fetch_object($getId);

	
	
?>
	<body>
		<?php if(mysqli_num_rows($product) == 0){?>
		<div class="row">
			<div class="col-lg-12">
				<table width="100%" border="0">
					<tr>
						<td>
							<img src="product/mainpromo.jpg" width="100%" height="550px" >
						</td>
					</tr>
				</table>
			</div>
		</div>
		<?php }else{ ?>
		<div class="row">
			<div class="col-lg-12">
				<table width="100%" border="0" height= '500px' cellspacing=0 cellpadding=0 border="1">
					
					<tr>
						<td width="30%" align="center">
							<br />
							<br />
							<?php 
								if(file_exists("product/".$last_item->prodcode.".jpg")){
							?>
								<img src="product/<?php echo $last_item->prodcode?>.jpg" width="80%"  />
							<?php
								}else{
							?>
								<img src="product/no_image.jpg" width="80%" />
							<?php
								}
							?>
							<p class="m-0 p-0" style="font-size:30px;"><?php echo $last_item->prodname?></p>
							<div class="right-side-pro-detail border p-3 m-0">
								<div class="row">
									<div class="col-lg-12">
										<p class="m-0 p-0 price-pro"  style="font-size:50px;">Rp. <?php echo number_format($last_item->price)?></p>
									</div>
									
								</div>
							</div>
						</td>
						<td  valign="top" >
							<table width="70%" cellpadding="0" cellspacing="0" class="table table-striped" border="0" style="font-size:14px;">
								<thead>
								<tr>
									<th >Name</th>
									<th align="left">QTY</th>
									<th align="left">HARGA</th>
									<th align="left">JUMLAH</th>
								</tr>
								</thead>
								<?php
									$product_all 	= mysqli_query($conpos, "SELECT * FROM tb_dualscreen ORDER BY seq_no ASC");
									while($all_last_item	= mysqli_fetch_object($product_all)){
								?>
									<tr>
										<td ><?php echo $all_last_item->prodname?> <b>(<?php echo $all_last_item->type?>)</b></td>
										<td align="left"><?php echo $all_last_item->qty?></td>
										<td align="left"><?php echo number_format($all_last_item->price)?></td>
										<td align="left"><?php echo number_format($all_last_item->qty * $all_last_item->price)?></td>
										
									</tr>
								<?php
										$total = $total + ($all_last_item->qty * $all_last_item->price);
									}
								?>
								<tr>
									<th colspan="4" align="right" style="font-weight:bold;">
										<?php
											$getCharge 	= mysqli_query($conpos, "SELECT * FROM tb_dualscreen LIMIT 1");
											$getAllCharge	= mysqli_fetch_object($getCharge);
										?>
										<table width="50%" align="left" style="font-size:24px;">
											<tr>
												<td>
													<h2 style="font-weight:bold;">Total Rp. <?php echo number_format($getAllCharge->service_charge + $getAllCharge->ta_charge + $getAllCharge->pajak + $total  ) ?></h2>
												</td>
											</tr>
										</table>
										<table width="50%" align="right" style="font-size:18px;">
											<tr>
												<th>
													Sub Total 
												</th>
												<th align="right" style="text-align:right" >
													Rp. <?php echo number_format($total) ?>
												</th>
											</tr>
											<tr>
												<th>
													Service Charge 
												</th>
												<th align="right" style="text-align:right" >
													Rp. <?php echo number_format($getAllCharge->service_charge) ?>
												</th>
											</tr>
											<tr>
												<th>
													TA Charge 
												</th>
												<th align="right" style="text-align:right" >
													Rp. <?php echo number_format($getAllCharge->ta_charge) ?>
												</th>
											</tr>
											<tr>
												<th>
													PAJAK 
												</th>
												<th align="right" style="text-align:right" >
													Rp. <?php echo number_format($getAllCharge->pajak) ?>
												</th>
											</tr>
										</table>
										
									</th>
								</tr>
								<tr>
									<td colspan="4">
										<ul class="list-inline text-center text-lg-right mb-0" style="line-height: 30px">
											<!--
											<li class="list-inline-item">
												<a href="https://www.youtube.com/hokbenable" target="_blank"><img src="https://www.hokben.co.id/assets/img/icon/Youtube_48px.svg" width="35px" height="35px"></a>
											</li>
											<li class="list-inline-item">
												<a href="https://twitter.com/HokBen" target="_blank"><img src="https://www.hokben.co.id/assets/img/icon/Twitter_48px.svg" width="35px" height="35px"></a>
											</li>
											<li class="list-inline-item">
												<a href="https://www.facebook.com/pages/Hoka-Hoka-Bento/22801653893" target="_blank"><img src="https://www.hokben.co.id/assets/img/icon/Facebook_48px.svg" width="35px" height="35px"></a>
											</li>
											<li class="list-inline-item">
												<a href="https://www.instagram.com/hokben_id/" target="_blank"><img src="https://www.hokben.co.id/assets/img/icon/instagram.svg" width="27px" height="27px"></a>
											</li>
											-->
											<li class="list-inline-item">
												Email : <b>info@hokben.co.id</b>
											</li>
										</ul>
									</td>
								</tr>
							</table>
							
						</td>
					</tr>
					
				</table>
				
			</div>
		</div>
		<?php } ?>
	</div>	
	</body>
</html>
