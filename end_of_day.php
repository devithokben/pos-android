<?php 
	session_start();

	include("dbconnect.php");
	include("template/header_h2h.php");
	error_reporting(1);
	
	$get_eod 	= mysqli_query($conpos,"select * from trx_ctl where TRX_STATUS = '0'");
	$dataeod	= mysqli_fetch_object($get_eod);
	if(!isset($dataeod)){
		echo "<script>
			Swal.fire({
			  title: 'ERROR!',
			  text: 'Start Of Day Belum Dilakukan',
			  icon: 'error',
			  confirmButtonText: 'CLOSE'
			}).then((result) => {
			  if (result.isConfirmed) {
				location.href='master_pos.php';
			  }
			});
		</script>";
	}
?>
<script>
	//const Swal = require('sweetalert2');
			
	function eod(act){	
		if(act == 1){
			$.ajax({
			type : 'GET',
				url : 'proc_endofday.php',
				success : function(data){
					// alert(data);
					if(data == 0){
						$.ajax({
							type : 'GET',
								url : 'proc_endofday.php?act=proc_eod',
								success : function(data){
									if(data == 0){
										Swal.fire({
										  title: 'SUCCESS!',
										  text: 'END OFF DAY berhasil dilakukan',
										  icon: 'success',
										  confirmButtonText: 'CLOSE'
										}).then((result) => {
										  if (result.isConfirmed) {
											location.href="master_pos.php";
										  }
										});
									}else if(data == 3){
										Swal.fire({
										  title: 'Error!',
										  text: 'EOD Tidak dapat dilakukan karena tidak ada transaksi',
										  icon: 'error',
										  confirmButtonText: 'CLOSE'
										});
										//alert("");
									}
								}
							})
						
					}else if(data == 2){
						Swal.fire({
						  title: 'Error!',
						  text: 'Selesaikan Hold Transaksi Terlebih Dahulu',
						  icon: 'error',
						  confirmButtonText: 'CLOSE'
						});
						//alert("");
					}else{
						Swal.fire({
						  title: 'Error!',
						  text: 'Lakukan Akhir Session Untuk Setiap Kasir',
						  icon: 'error',
						  confirmButtonText: 'CLOSE'
						});
						//alert("");
					}
				}
			})
		}
		else if(act == 2){
			$.ajax({
							type : 'GET',
								url : 'proc_endofday.php?act=proc_reset',
								success : function(data){
									Swal.fire({
										  title: 'SUCCESS!',
										  text: 'Reset berhasil dilakukan',
										  icon: 'success',
										  confirmButtonText: 'CLOSE'
										}).then((result) => {
										  if (result.isConfirmed) {
											location.href="master_pos.php";
										  }
										});
								}
							});
		}
		else{
			location.href="master_pos.php";
		}
		
		//;
	}
</script>
	<div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="page-breadcrumb">
                <div class="row align-items-center">
					<div class="col-xs-12 col-md-12 col-sm-12">
						<h3>Login Kasir</h3>
						<table class="table table-strip">
							<thead>
								<tr>
									<th>TRX ID</th>
									<th>TRX DATE</th>
									<th>POS ID</th>
									<th>USER ID</th>
									<th>USER NAME</th>
									<th>SESSION ID</th>
								</tr>
							</thead>
							<tbody>
								<?php
										$getId 		= mysqli_query($conpos,"select * from pos_init where CLOSING_STATUS = 'N'");
										while($dataTrx	= mysqli_fetch_object($getId)){ //print_r($dataTrx);
								?>
									<tr>
										<td><?php echo $dataTrx->TRX_ID?></td>
										<td><?php echo $dataTrx->ENT_DATE?></td>
										<td><?php echo $dataTrx->POS_ID?></td>
										<td><?php echo $dataTrx->USERID?></td>
										<td><?php echo $dataTrx->USERID?></td>
										<td><?php echo $dataTrx->SESSION_ID?></td>
									</tr>
								<?php } ?>
							</tbody>
						</table>

					</div>
					
					<div class="col-xs-12 col-md-12 col-sm-12">
					<hr />
						<h3>Hold Transaksi</h3>
						<table class="table table-strip" width="100%">
							<thead>
								<tr>
									<th colspan="6">Struk ID</th>
								</tr>
							</thead>
							<tbody>
								<?php
										$get_eod 	= mysqli_query($conpos,"select * from trx_ctl where TRX_STATUS = '0'");
										$dataeod	= mysqli_fetch_object($get_eod);
									
										$get_ht 	= mysqli_query($conpos,"select * from sales_hdr  where STATUS = 'H' and TRX_DATE = '".$dataeod->TRX_DATE."'");
										while($dataTrx_h	= mysqli_fetch_object($get_ht)){ //print_r($dataTrx);
								?>
									<tr>
										<td colspan="6"><?php echo $dataTrx_h->ID?></td>
									</tr>
								<?php } ?>
								<tr>
									<td colspan="3" align="left">
										<input type="button" class="btn btn-warning" onclick="eod(1)" value="Proses End Of Day Now"/>
										<?php if($dataeod->COUNTER_NO == 0){?>
										<input type="button" class="btn btn-success" onclick="eod(2)" value="Reset SOD"/>
										<?php }?>
									</td>
									<td colspan="3" align="left"><input type="button" class="btn btn-danger" onclick="eod(0)" value="CANCEL"/></td>
								</tr>
							</tbody>
						</table>

					</div>
				</div>
			</div>
		</div>
<?php include("template/footer_h2h.php");?>