<?php 
error_reporting(0);
function availableUrl($host, $timeout=5) { 
	$fp = fSockOpen($host, 6969, $errno, $errstr, $timeout); 
	return $fp!=false;
}
require 'dbconnect.php';


?>


<!DOCTYPE html>
<html dir="ltr" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
   
    <title>Halaman Login POS</title>
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="assets/images/favicon.png">
    <!-- chartist CSS -->
    <link href="assets/plugins/chartist-js/dist/chartist.min.css" rel="stylesheet">
    <link href="assets/plugins/chartist-js/dist/chartist-init.css" rel="stylesheet">
    <link href="assets/plugins/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.css" rel="stylesheet">
    <!--This page css - Morris CSS -->
    <link href="assets/plugins/c3-master/c3.min.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="css/style.min.css" rel="stylesheet">
	<link href="<?php echo  $base_url ?>sweetalert2/dist/sweetalert2.min.css" rel="stylesheet">
	 
	<!-- keyboard widget css & script (required) -->
	<link href="Keyboard/css/keyboard.css" rel="stylesheet">
	<style>
		.ui-keyboard{
			background : #eee;
		}
		.ui-keyboard-button{
				height: 4em;
				min-width: 1em;
				margin: .1em;
				cursor: pointer;
				overflow: hidden;
				line-height: 1em;
				-moz-user-focus: ignore;
				
			}
		.ui-keyboard-text{
			font-size:16px;
		}

		@media only screen and (min-width: 600px) {

			
			.ui-keyboard-button{
				height: 4em;
				min-width: 4em;
				margin: .2em;
				cursor: pointer;
				overflow: hidden;
				line-height: 4em;
				-moz-user-focus: ignore;
				
			}
			.ui-keyboard-text{
				font-size:20px;
			}
		}
		
	</style>
</head>

<body>
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <div class="lds-ripple">
            <div class="lds-pos"></div>
            <div class="lds-pos"></div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper" data-layout="vertical" data-navbarbg="skin6" data-sidebartype="full"
        data-sidebar-position="absolute" data-header-position="absolute" data-boxed-layout="full">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <header class="topbar" data-navbarbg="skin6">
            <nav class="navbar top-navbar navbar-expand-md navbar-dark">
                <div class="navbar-header" data-logobg="skin6">
                    <!-- ============================================================== -->
                    <!-- Logo -->
                    <!-- ============================================================== -->
                    <a class="navbar-brand ml-4" href="index.html">
                       
                        <!--End Logo icon -->
                        <!-- Logo text -->
                        <span class="logo-text" style="color:#fff">
							<?php
								$getIdStore 	= mysqli_query($conpos,"select * from cprofile");
								$dataStore		= mysqli_fetch_object($getIdStore);
							?>
                           <h2>Login POS  <?php echo $dataStore->BRANCH_ID?> :: <?php echo $dataStore->BRANCH_NAME?> </h2>

                        </span>
                    </a>
                    <!-- ============================================================== -->
                    <!-- End Logo -->
                    <!-- ============================================================== -->
                    <!-- ============================================================== -->
                    <!-- toggle and nav items -->
                    <!-- ============================================================== -->
                    <a class="nav-toggler waves-effect waves-light text-white d-block d-md-none"
                        href="javascript:void(0)"><i class="ti-menu ti-close"></i></a>
                </div>
                <!-- ============================================================== -->
                <!-- End Logo -->
              
            </nav>
        </header>

	<div class="">

	<!-- ============================================================== -->
	<!-- End Bread crumb and right sidebar toggle -->
	<!-- ============================================================== -->
	<!-- ============================================================== -->
	<!-- Container fluid  -->
	<!-- ============================================================== -->
	<div class="container-fluid">
		<!-- ============================================================== -->
		<!-- Start Page Content -->
		<!-- ============================================================== -->
		<!-- Row -->
		<div class="row">
			<?php
				$error_svc = 0;
				if($data_idpos->VALUE != 'localhost'){
					if(availableUrl($ip_url_websvc) == 1){
						$ch = curl_init(); 
						curl_setopt($ch, CURLOPT_URL, $url_websvc.'check_update.php');
						curl_setopt($ch,CURLOPT_USERAGENT,'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');
						curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
						curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
						curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);     
						curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
						curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
						$return_out = curl_exec($ch); 
						curl_close($ch); 
						if($return_out === false)
						{
							$output	= 'Curl error: ' . curl_error($ch);
						}
						else
						{
							$output = json_decode($return_out); 
						}
						
						//get local version
						$qry_detail_table 	= mysqli_query($conpos,"SELECT * from ap_setup where ID = 'VER'");
						$data_detail_table	= mysqli_fetch_object($qry_detail_table);
						if($output->version != $data_detail_table->VALUE){
							echo "<script> location.href='update_pos.php?ver=".$output->version."&qry=".urlencode(json_encode($output->query))."';</script>";
						}
						//print_r($output->app_version); 
						
						if($output->app_version == 2){
							//echo "masuk";
							$download =  "<div class='form-group'>
											<div class='alert alert-success'><h3><a href='http://online.hokben.net/update/apk/pos.apk' download>Klik Untuk Downlod APK Terbaru</a></h3></div>
										</div>";
						}
						
						//getLocalUpdateDate
						$qry_detail_update	= mysqli_query($conpos,"SELECT * from ap_setup where ID = 'UPD_'");
						$data_detail_update	= mysqli_fetch_object($qry_detail_update);
						if(date('Y-m-d') != date('Y-m-d',strtotime($data_detail_update->VALUE))){
							echo "<script> location.href='update_promo_index.php?ver=".$output->version."&qry=".urlencode(json_encode($output->query))."';</script>";
						}
						
						
						
						//exit;
					}else{
						$error_svc = 1;
					}
				}
			?>
			
			<?php 
				if($errordb == 1){
			?>
				<div class="col-lg-12 col-xlg-12 col-md-12" style="text-align:center;">
					<br/><br/><br/><br/>
					<img src="images/lost_server.png" />
				</div>
				<div class="col-lg-12 col-xlg-12 col-md-12" style="text-align:center;">
					<br  />
					<br  />
					<a href="#" onclick="BtPrint()" class="btn btn-danger mx-auto mx-md-0 text-white" style="font-size:30px;">Close</a>
				</div>
			<?php
				}else{
			?>
				<!-- Column -->
				<div class="col-lg-4 col-xlg-3 col-md-5">
					
				</div>
				<!-- Column -->
				<!-- Column -->
				<div class="col-lg-4 col-xlg-9 col-md-7">
					<div class="card" style="margin-top:40px;">
						<div class="card-body">
						   
							<form class="form-horizontal form-material" action="<?php $_SERVER['PHP_SELF'] ?>?act=login" method="post">
								<div class="form-group">
									<div class="alert alert-danger">Pastikan Kode dan Nama Store Di Atas Sudah Sesuai, Jika Tidak Hubungi DIV IT </div>
								</div>								
								<div class="form-group">
									<label class="col-md-12 mb-0">Username</label>
									<div class="col-md-12">
										<input type="text" placeholder="Masukan Nama Anda" name="user" id="user"
											class="form-control pl-0 form-control-line">
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-12 mb-0">Password</label>
									<div class="col-md-12">
										<input type="password" name="password" placeholder="Masukan Password Anda" id="password"
											class="form-control pl-0 form-control-line">
									</div>
								</div>
								<div class="form-group">
									<div class="col-sm-12 d-flex">
										<div class="col-sm-9 d-flex">
											<input type="submit" class="btn btn-success mx-auto mx-md-0 text-white" value="LOGIN"/>
										</div>
										<div class="col-sm-2 d-flex">
											<a href="#" onclick="BtPrint()" class="btn btn-danger mx-auto mx-md-0 text-white" style="float:right">Close</a>
										</div>
									</div>
								</div>
								<?php echo $download ?>
								
								<?php
									if($error_svc == 1){
								?>
									<div class="form-group">
										<div class="alert alert-warning">Info : Server <?php echo $data_urlsrv->VALUE?> Versioning Down </div>
									</div>
								<?php	
									}
								?>
							</form>
						</div>
					</div>
				</div>
				<!-- Column -->
			<?php
				}
			?>
		</div>
		<!-- Row -->
		<!-- ============================================================== -->
		<!-- End PAge Content -->
		<!-- ============================================================== -->
		<!-- ============================================================== -->
		<!-- Right sidebar -->
		<!-- ============================================================== -->
		<!-- .right-sidebar -->
		<!-- ============================================================== -->
		<!-- End Right sidebar -->
		<!-- ============================================================== -->
	</div>
</div>


 </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <script src="assets/plugins/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="assets/plugins/popper.js/dist/umd/popper.min.js"></script>
    <script src="assets/plugins/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="js/app-style-switcher.js"></script>
    <!--Wave Effects -->
    <script src="js/waves.js"></script>
    <!--Menu sidebar -->
    <script src="js/sidebarmenu.js"></script>
    <!-- ============================================================== -->
    <!-- This page plugins -->
    <!-- ============================================================== -->
    <!-- chartist chart -->
    <script src="assets/plugins/chartist-js/dist/chartist.min.js"></script>
    <script src="assets/plugins/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.min.js"></script>
    <!--c3 JavaScript -->
    <script src="assets/plugins/d3/d3.min.js"></script>
    <script src="assets/plugins/c3-master/c3.min.js"></script>
    <!--Custom JavaScript -->
    <script src="js/pages/dashboards/dashboard1.js"></script>
    <script src="js/custom.js"></script>
	
	<script src="<?php echo  $base_url ?>assets_front/vendor/jquery/jquery.min.js"></script>
  <script src="<?php echo  $base_url ?>assets_front/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="<?php echo  $base_url ?>js/jquery-ui.js"></script>
  
  <script src="<?php echo  $base_url ?>sweetalert2/dist/sweetalert2.all.min.js" type="text/javascript"></script>
 
	<script src="Keyboard/js/jquery.keyboard.js"></script>

	<!-- keyboard extensions (optional) -->
	<script src="Keyboard/js/jquery.mousewheel.js"></script>
	
  
	<script>
		  $( function() {
			$( "#datepicker" ).datepicker({
				 dateFormat:"yy-mm-dd",
			  });
		  } );
		
		$("#menu-toggle").click(function(e) {
		  e.preventDefault();
		  $("#wrapper").toggleClass("toggled");
		});
		function BtPrint(){
			Android.closeWin();
	}
	</script>
	<script>
		$(function(){
			$('#user').keyboard({layout: 'qwerty'});
			$('#password').keyboard({layout: 'qwerty'});
		});
	</script>
	<?php
		if($errordb == 0){
			if($data_m_branch->BRANCH_ID == ''){
				echo "<SCRIPT>
				Swal.fire({
				  title: 'Error!',
				  text: 'BRANCH_ID Harus Diisi, Silahkan cek table branch',
				  icon: 'error',
				  confirmButtonText: 'CLOSE'
				}).then((result) => {
					if (result.isConfirmed) {
						location.href='index.php';
					}
				});
				</SCRIPT>";
			}
		}
		if($_GET['act'] == 'login'){
			$getId 		= mysqli_query($conpos,"select * from tbluser where USER_ID = '".$_POST['user']."' and USER_PASS = '".$_POST['password']."'");
			//echo "select * from tbuser where USER_ID = '".$_POST['user']."' and USER_PASS = '".$_POST['password']."'";
			//exit;
			$datauser	= mysqli_fetch_object($getId);
			//print_r($datauser);
			if(!isset($datauser)){
				$err	= "User Dan Password Salah";
			}else{
				
				
				mysqli_query($conpos,"UPDATE tbluser SET USER_1STLOGIN = 1 where USER_ID = '".$_POST['user']."' and USER_PASS = '".$_POST['password']."'");
				;
				if($datauser->ROLE_ID == '01'){
					echo "<script>
						Swal.fire({
						  title: 'INFORMASI PENTING!',
						  text: 'Kode POS ini adalah ".$data_m_branch->BRANCH_ID.$data_idpos->VALUE." Jika Kode POS salah silahkan hubungi IT',
						  icon: 'info',
						  confirmButtonText: 'CLOSE'
						}).then((result) => {
						  if (result.isConfirmed) {
							location.href='system_pos.php';
						  }
						});
					</script>";
				}else{
					echo "<script>
						Swal.fire({
						  title: 'INFORMASI!',
						  text: 'Kode POS ini adalah ".$data_m_branch->BRANCH_ID.$data_idpos->VALUE." Jika Kode POS salah silahkan hubungi IT',
						  icon: 'info',
						  confirmButtonText: 'CLOSE'
						}).then((result) => {
						  if (result.isConfirmed) {
							location.href='dashboard.php';
						  }
						});
					</script>";
				}
				
			}
				
		}

	?>
</body>

</html>