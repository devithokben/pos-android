<?php
	use Mike42\Escpos\PrintConnectors\NetworkPrintConnector;
	use Mike42\Escpos\Printer;
	$connector = new FilePrintConnector("/dev/ttyS3");
	$printer = new Printer($connector);
	try {
		$printer -> text("Hello World!\n");
		$printer -> cut();
		$printer -> close();
	} finally {
		$printer -> close();
	}
?>
