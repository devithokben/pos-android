<?php
require 'dbconnect.php';
$getPromoProc 		= mysqli_query($conpos,"select * from tbl_promoline where PromoId = '".$_GET['promoid']."' and SeqNo = 2");
$dataPromoProc		= mysqli_fetch_object($getPromoProc);


//print_r($dataPromoProc);
/*Keterangan
Promo type untuk item discount => $result['promo_type'] = item/sales/tupperware/itemdiscount ;
line_type : 
			30. Promo Free Item + Discount
*/


if ($dataPromoProc->LineType == 3){ //itemgroup
	$getItemGroup 		= mysqli_query($conpos,"select * from tbl_promo_groupd left join products on tbl_promo_groupd.ItemRelation = products.PRODCODE where groupid = '".$dataPromoProc->ItemRelation."' ");
	
	if($dataPromoProc->DiscType == 2){ //
		$message	=  '<hr />';
		while($dataItemGroup		= mysqli_fetch_object($getItemGroup)){	
			$message	.=  '	<label class="col-sm-12 btn btn-danger" style="margin:10px;" >'.$dataItemGroup->PRODNAME.'
					  <input  type="radio" name="radio_promo" value="'.$dataItemGroup->ItemRelation.'|'.$dataPromoProc->ItemQty.'|'.$dataPromoProc->DiscValue.'" onchange="changeRadioPromo(this)">
					</label>';
		}
		$message	.=  "<hr />";
		$result['message']		= $message;
		$result['linetype']		= $dataPromoProc->LineType;
		$result['promo_id']		= $_GET['promoid'];
		$result['promo_val']	= '';
		$result['promo_type']	= '';
		
		echo json_encode($result);
	}else{
		$message	=  '<hr />';
		while($dataItemGroup		= mysqli_fetch_object($getItemGroup)){	
			$message	.=  '	<label class="col-sm-12 btn btn-danger" style="margin:10px;" >'.$dataItemGroup->PRODNAME.'
					  <input  type="radio" name="radio_promo" value="'.$dataItemGroup->ItemRelation.'|'.$dataPromoProc->ItemQty.'" onchange="changeRadioPromo(this)">
					</label>';
		}
		$message	.=  "<hr />";
		$result['message']		= $message;
		$result['linetype']		= $dataPromoProc->LineType;
		$result['promo_id']		= $_GET['promoid'];
		$result['promo_val']	= '';
		$result['promo_type']	= '';
		
		echo json_encode($result);
	}
}
else if ($dataPromoProc->LineType == 10){ //discount value
	if($dataPromoProc->DiscType == 1){ //discount %
		$nominal_disc	= $dataPromoProc->DiscValue / 100;
		$total_disc		= $_GET['amount'] * $nominal_disc;
		if($total_disc > $dataPromoProc->DiscAmountMax && (int)$dataPromoProc->DiscAmountMax != 0){
			$result['message']	=  "Total Potongan : Rp.".number_format($dataPromoProc->DiscAmountMax);
			$result['promo_val']= number_format($dataPromoProc->DiscAmountMax);
		}else{
			$result['message']	=  "<h3>Total Potongan : Rp.".number_format($total_disc)."</h3>";
			$result['promo_val']= number_format($total_disc);
		}
		$result['promo_id']		= $_GET['promoid'];	
		$result['promo_type']	= "sales";	
		$result['linetype']	= $dataPromoProc->LineType;		
		echo json_encode($result);
	}else if($dataPromoProc->DiscType == 2){ //discount nominal
		$total_disc		= $_GET['amount'] - $dataPromoProc->DiscValue;
		$result['promo_val']= number_format($dataPromoProc->DiscValue);
		$result['promo_type']	= "sales";	
		if($dataPromoProc->DiscAmountMax > 0){
			$result['message']	= "Sisa Pembayaran Setelah Promo : Rp.".number_format($total_disc);			
		}else{
			$result['message']	= "Sisa Pembayaran Setelah Promo : Rp.".number_format($total_disc);
		}
		$result['promo_id']	= $_GET['promoid'];	
		$result['linetype']	= $dataPromoProc->LineType;		
		echo json_encode($result);
	}
}
else if(($dataPromoProc->LineType == 40) ){
	$result['promo_val']= 1;
	$result['message']	= "Promo Pengurang Sub Total";
	$result['promo_id']	= $_GET['promoid'];	
	$result['linetype']	= $dataPromoProc->LineType;	
	$result['promo_type']	= "sales";		
	echo json_encode($result);
}
else if(($dataPromoProc->LineType == 50) ){
	
	//get price
	$promo_val = 0;	
	foreach($_POST['PRODCODE'] as $key => $val){
		$getPromoProd 		= mysqli_query($conpos,"select * from tbl_promo_groupd where groupid = '".$dataPromoProc->ItemRelation."' and ItemRelation = '".$val."'");
		if($dataPromoProd		= mysqli_fetch_object($getPromoProd)){
			//normal price
			$normal_price	= $_POST['QTY'][$key] * $_POST['PRICE'][$key];
			
			//Discount
			$discount_price	= $_POST['QTY'][$key] * $dataPromoProd->Price;
			
			$promo_val		=  $promo_val + ($normal_price - $discount_price );
		
		}else{
			//echo $promo_val;
		}
	}
	//exit;
	//print_r($dataPromoProc->itemre);
	$result['promo_val']= number_format($promo_val);
	$result['message']	= "Promo Pengurang Sub Total";
	$result['promo_id']	= $_GET['promoid'];	
	$result['linetype']	= $dataPromoProc->LineType;	
	$result['promo_type']	= "sales";		
	echo json_encode($result);
}
else if($dataPromoProc->LineType == 2){
	
	if($dataPromoProc->DiscType == 1){ //discount %
		$nominal_disc	= $dataPromoProc->DiscValue / 100;
		$total_disc		= $_GET['amount'] * $nominal_disc;
		if($total_disc > $dataPromoProc->DiscAmountMax){
			$result['message']	=  "Total Potongan : Rp.".number_format($dataPromoProc->DiscAmountMax);
			$result['promo_val']= number_format($dataPromoProc->DiscAmountMax);
		}else{
			$result['message']	=  "<h3>Total Potongan : Rp.".number_format($total_disc)."</h3>";
			$result['promo_val']= number_format($total_disc);
		}
		$result['promo_id']		= $_GET['promoid'];	
		$result['promo_type']	= "sales";		
		$result['linetype']	= $dataPromoProc->LineType;		
		$result['qty']= $dataPromoProc->ItemQty;
		$result['item']	= $dataPromoProc->ItemRelation;	
		echo json_encode($result);
	}
	else if($dataPromoProc->DiscType == 2){ //discount nominal
		$total_disc				= $_GET['amount'] - $dataPromoProc->DiscValue;
		$result['promo_val']	= number_format($dataPromoProc->DiscValue);
		$result['promo_type']	= "itemdiscount";		
		if($dataPromoProc->DiscAmountMax > 0){
			$result['message']	= "Total Potongan : Rp.".number_format($dataPromoProc->DiscValue);			
		}else{
			$result['message']	= "Total Potongan : Rp.".number_format($dataPromoProc->DiscValue);
		}
		$result['promo_id']		= $_GET['promoid'];	
		$result['qty']			= $dataPromoProc->ItemQty;
		$result['linetype']		= 30;	
		$result['item']			= $dataPromoProc->ItemRelation;	
		echo json_encode($result);
	}
	else{
		
		$getPromoProd 		= mysqli_query($conpos,"select * from products where PRODCODE = '".$dataPromoProc->ItemRelation."'");
		$dataPromoProd		= mysqli_fetch_object($getPromoProd);
		
		$result['promo_val']= 1;
		$result['qty']= $dataPromoProc->ItemQty;
		$result['message']	= "Promo Item : ".$dataPromoProd->PRODNAME;
		$result['promo_id']	= $_GET['promoid'];	
		$result['linetype']	= $dataPromoProc->LineType;	
		$result['promo_type']	= $dataPromoProc->ItemRelation;		
		echo json_encode($result);
	}
}else if ($dataPromoProc->LineType == 70){
	$result['promo_val']= 1;
	$result['message']	= "Promo Tupper Ware";
	$result['promo_id']	= $_GET['promoid'];	
	$result['linetype']	= $dataPromoProc->LineType;	
	$result['promo_type']	= "tupperware";		
	echo json_encode($result);
}


?>